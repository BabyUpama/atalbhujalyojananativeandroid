package in.co.gcrs.ataljal.adapters;


import androidx.fragment.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.fragment.GeoTaggingArtificialRecharge;
import in.co.gcrs.ataljal.fragment.WellInventory;
import in.co.gcrs.ataljal.model.GeotaggingModel;
import in.co.gcrs.ataljal.model.WellInventoryModel;

public class WellInventoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private ArrayList<WellInventoryModel> arrayList;
    private Context context;
    FragmentManager fragmentManager;
    Fragment fragment;
    private ProgressBar progressBar;
    private RequestQueue requestQueue;
    Date c ;
    String formattedDate="",logDate="";
    SimpleDateFormat df,df1;

    public WellInventoryAdapter(Context context, ArrayList<WellInventoryModel> arrayList){

        this.context = context;
        this.arrayList  = arrayList;
        c= Calendar.getInstance().getTime();

        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        logDate = df1.format(c).toUpperCase();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView grampanchayatName,villageName,date,sequence;
        ImageView edit,delete;
        ProgressBar progressBar;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            this.sequence=itemView.findViewById(R.id.sequence);
            this.grampanchayatName=itemView.findViewById(R.id.grampanchayatName);
            this.villageName=itemView.findViewById(R.id.villageName);
            this.date=itemView.findViewById(R.id.tvDate);
            this.edit = itemView.findViewById(R.id.edit);
            this.delete=itemView.findViewById(R.id.delete);
            this.progressBar=itemView.findViewById(R.id.progressbar);

        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view= layoutInflater.inflate(R.layout.well_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        getData((MyViewHolder) holder,position);
    }

    private void getData(MyViewHolder holder, int position)
    {
        final WellInventoryModel wellInventoryModel = arrayList.get(position);

        if(wellInventoryModel!=null) {

            Log.d("kcr",""+wellInventoryModel.getDatetime());
            holder.grampanchayatName.setText(wellInventoryModel.getGramPanchayat());
            holder.villageName.setText(wellInventoryModel.getVillage());
            holder.date.setText(wellInventoryModel.getDatetime());
            holder.sequence.setText(String.valueOf(holder.getAdapterPosition()+1));


            holder.edit.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder dialog=new AlertDialog.Builder(context);

                    dialog .setTitle("Edit Entry");
                    dialog .setMessage("Are you sure you want to edit this entry?");

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            // Continue with edit operation

                            Bundle bundle = new Bundle();

                            bundle.putString("id",wellInventoryModel.getWellInId());
                            bundle.putString("date",wellInventoryModel.getDate());

                            bundle.putString("wellno",wellInventoryModel.getWellNo());
                            bundle.putString("state",wellInventoryModel.getState());
                            bundle.putString("district",wellInventoryModel.getDistrict());
                            bundle.putString("block",wellInventoryModel.getBlock());
                            bundle.putString("village",wellInventoryModel.getVillage());
                            bundle.putString("locationdetails",wellInventoryModel.getLocationDetails());
                            bundle.putString("sitename",wellInventoryModel.getSiteName());
                            bundle.putString("latitude",wellInventoryModel.getLatitude());
                            bundle.putString("longitude",wellInventoryModel.getLongitude());
                            bundle.putString("welltype",wellInventoryModel.getWellType());
                            bundle.putString("ownername",wellInventoryModel.getOwnerName());

                            bundle.putString("diameter",wellInventoryModel.getWellDiameter());
                            bundle.putString("welldepth",wellInventoryModel.getWellDepth());
                            bundle.putString("measuringpoint",wellInventoryModel.getMeasuringPoint());
                            bundle.putString("wellwaterpotable",wellInventoryModel.getWellWaterPotable());
                            bundle.putString("waterlevel",wellInventoryModel.getWaterLevelDepth());
                            bundle.putString("use",wellInventoryModel.getUse());
                            bundle.putString("pumpinstalled",wellInventoryModel.getPumpInstalled());
                            bundle.putString("electricmeterselecteditem",wellInventoryModel.getElectricMeterSelecteditem());
                            bundle.putString("image",wellInventoryModel.getImage());



                            fragment = new WellInventory();
                            fragment.setArguments(bundle);
                            fragmentManager.beginTransaction()
                                    .replace(R.id.main_fragment, fragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .addToBackStack("Home")
                                    .commit();

                        }
                    });

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });
                    dialog.setIcon(android.R.drawable.ic_dialog_alert);
                    dialog.show();

                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder dialog=new AlertDialog.Builder(context);

                    dialog .setTitle("Delete Entry");
                    dialog .setMessage("Are you sure you want to delete this entry?");

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            // Continue with delete operation
                            if (!holder.progressBar.isShown())
                            {
                                holder.progressBar.setVisibility(View.VISIBLE);
                            }
                            deleteWellInventory(holder.getAdapterPosition(),wellInventoryModel,holder);
                        }
                    });

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });
                    dialog.setIcon(android.R.drawable.ic_dialog_alert);
                    dialog.show();
                }
            });
        }
    }

    public void deleteWellInventory(final int pos,WellInventoryModel wellInventoryModel,MyViewHolder holder)
    {
        requestQueue= Volley.newRequestQueue(context);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"deletewellinventory.jsp", new Response.Listener<String>()
        {

            @Override
            public void onResponse(String response)
            {
                Log.d("kcrwellinven",response);
                try {

                    JSONObject heroObject = new JSONObject(response);

                    if(heroObject.getString("status").equals("success"))
                    {
                        //notifyDataSetChanged();
                        arrayList.remove(pos);
                        notifyItemRemoved(pos);
                        notifyItemRangeChanged(pos,arrayList.size());
                        Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();


                        insertUserLog(wellInventoryModel.getEmail(),getIpAddress(),logDate,"well inventory record delete ", "record  deleted successfully ",holder);

                    }
                    else if (heroObject.getString("status").equals("fail"))
                    {
                        Toast.makeText(context, "Failed to remove Data", Toast.LENGTH_SHORT).show();
                        insertUserLog(wellInventoryModel.getEmail(),getIpAddress(),logDate,"well inventory record delete", "record  deletion failed",holder);
                    }


                            /*if (progressBar.isShown())
                            {
                                progressBar.setVisibility(View.GONE);
                            }*/




                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();

                params.put("id",wellInventoryModel.getWellInId());
                Log.e("params delete",params.toString());
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status,MyViewHolder holder) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));
                                if (!holder.progressBar.isShown())
                                {
                                    holder.progressBar.setVisibility(View.VISIBLE);
                                }
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);




                return params;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }
    @Override
    public int getItemViewType(int position)
    {
        return position ;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}

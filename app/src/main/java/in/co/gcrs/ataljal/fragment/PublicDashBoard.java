package in.co.gcrs.ataljal.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.app.PublicPrefsManager;

public class PublicDashBoard extends Fragment {

    private CardView groundWaterMonitoring;
    private TextView newWellCount,existingCount,wellInventoryCount,geotaggedCount;

    FragmentManager fragmentManager;
    Fragment fragment;
    PublicPrefsManager publicPrefsManager;
    private RequestQueue requestQueue;


    public PublicDashBoard() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_public_dash_board, container, false);

        fragmentManager = getActivity().getSupportFragmentManager();
        publicPrefsManager = new PublicPrefsManager(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        newWellCount = view.findViewById(R.id.newWellCount);
        existingCount = view.findViewById(R.id.existingCount);
        wellInventoryCount = view.findViewById(R.id.wellInventoryCount);
        geotaggedCount = view.findViewById(R.id.geotaggedCount);

        groundWaterMonitoring = view.findViewById(R.id.groundWaterMonitoring);

        groundWaterMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!publicPrefsManager.getPublicstate().equals("")&&!publicPrefsManager.getPublicdistrict().equals("")&&
                        !publicPrefsManager.getPublicblock().equals("")&&!publicPrefsManager.getPublicgrampanchayat().equals("")) {

                    fragment = new PublicViewMap();
                    fragmentManager.beginTransaction()
                            .replace(R.id.public_fragment, fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .addToBackStack(null)
                            .commit();
                }else {
                    Toast.makeText(getContext(), "Select State/ District/ Block/ Gram Panchayat", Toast.LENGTH_SHORT).show();
                }
            }
        });


        if (isNetworkAvailable()) {

            if (!publicPrefsManager.getPublicstate().equals("")&&!publicPrefsManager.getPublicdistrict().equals("")&&
            !publicPrefsManager.getPublicblock().equals("")&&!publicPrefsManager.getPublicgrampanchayat().equals("")) {

                getgeoTagCount();
                getWellInventory();
                getnewwell();
                getExistingwell();

                Toast.makeText(getContext(), "Click on Ground Water monitoring Image for Ground Water Monitoring stations", Toast.LENGTH_LONG).show();

            }else {
                Toast.makeText(getContext(), "Select State/ District/ Block/ Gram Panchayat", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(getContext(), "Internet not available!", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    protected void getgeoTagCount() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getgeotagcount.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
//getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);


                            if (obj.getString("status").equals("success")) {

                                geotaggedCount.setText(obj.getString("count"));


                            } else {
                                geotaggedCount.setText(obj.getString("0"));
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();


                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("state",publicPrefsManager.getPublicstate());
                params.put("district",publicPrefsManager.getPublicdistrict());
                params.put("block",publicPrefsManager.getPublicblock());
                params.put("gp",publicPrefsManager.getPublicgrampanchayat());


                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    protected void getWellInventory() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getwellinventorycount.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
//getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);


                            if (obj.getString("status").equals("success")) {

                                wellInventoryCount.setText(obj.getString("count"));


                            } else {
                                wellInventoryCount.setText(obj.getString("0"));
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();


                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("state",publicPrefsManager.getPublicstate());
                params.put("district",publicPrefsManager.getPublicdistrict());
                params.put("block",publicPrefsManager.getPublicblock());
                params.put("gp",publicPrefsManager.getPublicgrampanchayat());


                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    protected void getnewwell() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                //ConstantValues.URL1+"web/users.jsp",
                BuildConfig.SERVER_URL+"getnewwellcount.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
//getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);


                            if (obj.getString("status").equals("success")) {

                                newWellCount.setText(obj.getString("count"));


                            } else {
                                newWellCount.setText(obj.getString("0"));
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();


                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("state",publicPrefsManager.getPublicstate());
                params.put("district",publicPrefsManager.getPublicdistrict());
                params.put("block",publicPrefsManager.getPublicblock());
                params.put("gp",publicPrefsManager.getPublicgrampanchayat());


                return params;
            }


        };


        requestQueue.add(stringRequest);
    }
    protected void getExistingwell() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                //ConstantValues.URL1+"web/users.jsp",
                BuildConfig.SERVER_URL+"getexistingwellcount.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
//getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);


                            if (obj.getString("status").equals("success")) {

                                existingCount.setText(obj.getString("count"));


                            } else {
                                existingCount.setText(obj.getString("0"));
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();


                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("state",publicPrefsManager.getPublicstate());
                params.put("district",publicPrefsManager.getPublicdistrict());
                params.put("block",publicPrefsManager.getPublicblock());
                //params.put("gp",myAppPrefsManager.getGrampanchayat());


                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
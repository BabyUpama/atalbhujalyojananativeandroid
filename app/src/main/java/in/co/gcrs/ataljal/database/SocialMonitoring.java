package in.co.gcrs.ataljal.database;

public class SocialMonitoring {

    public static final String smId="id";
    public static final String state="state";
    public static final String district="district";
    public static final String block="block";
    public static final String grampanchayat="grampanchayat";
    public static final String latitude="latitude";
    public static final String longitude="longitude";
    public static final String date="date";
    public static final String location="location";
    public static final String noOfAttendees="noofattendees";
    public static final String maleParticipants="maleparticipants";
    public static final String femaleParticipants="femaleparticipants";
    public static final String otherParticipants="otherParticipants";
    public static final String event="event";
    public static final String imagePath="imagepath";
    public static final String mom="mom";
    public static final String smemail="email";
    public static final String momFileName="momfilename";
    public static final String imageName="imagename";
    public static final String datetime="datetime";
    public static final String otherevent="otherevent";
}

package in.co.gcrs.ataljal.database;

public class Rainfall {
    public static final String rId="id";
    public static final String state="state";
    public static final String district="district";
    public static final String block="block";
    public static final String gramPanchayat="grampanchayat";
    public static final String sitename="sitename";
    public static final String rainfall="rainfall";
    public static final String latitude="latitude";
    public static final String longitude="longitude";
    public static final String raingauge="raingauge";
    public static final String date="date";
    public static final String image1="image1";
    public static final String image2="image2";
    public static final String remail="email";
    public static final String rImageName1="imagename1";
    public static final String rImageName2="imagename2";


}

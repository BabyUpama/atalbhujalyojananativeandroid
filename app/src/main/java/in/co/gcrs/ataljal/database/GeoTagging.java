package in.co.gcrs.ataljal.database;

public class GeoTagging {

    public static final String gId="id";
    public static final String state="state";
    public static final String district="district";
    public static final String block="block";
    public static final String gramPanchayat="grampanchayat";
    public static final String village="village";
    public static final String sitename="sitename";
    public static final String locationDetails1 = "locationdetails1";
    public static final String latitude="latitude";
    public static final String longitude="longitude";
    public static final String calenderdate = "date";
    public static final String typeofStructure="typeofstructure";
    public static final String storageCapacity="storagecapacity";
    public static final String noOfFillings="nooffillings";
    public static final String image1="image1";
    public static final String image2="image2";
    //public static final String deviceid="deviceid";
    public static final String others="others";
    public static final String gtdate="datetime";
    public static final String gtemail="email";
    public static final String gtImageName1="gtimagename1";
    public static final String gtimagename2="gtimagename2";
}

package in.co.gcrs.ataljal.interfaces;

public interface IOnBackPress {

    boolean onBackPressed();
}

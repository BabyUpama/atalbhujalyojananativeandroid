package in.co.gcrs.ataljal.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import in.co.gcrs.ataljal.R;


public class WellInventoryDasboard extends Fragment
{
    private CardView cardViewCollect,cardViewSentEdit;

    Fragment fragment;
    FragmentManager fragmentManager;


    public WellInventoryDasboard()
    {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_well_inventory_dasboard, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionWellInventory));

        fragmentManager=getActivity().getSupportFragmentManager();
        cardViewCollect=view.findViewById(R.id.cardViewCollect);
        cardViewSentEdit=view.findViewById(R.id.cardViewSentEdit);

        cardViewCollect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                fragment=new WellInventory();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment,fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("Home")
                        .commit();
            }
        });

        cardViewSentEdit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                fragment=new WellList();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment,fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("Home")
                        .commit();
            }
        });

        return view;
    }
}
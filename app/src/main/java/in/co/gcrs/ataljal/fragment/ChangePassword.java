package in.co.gcrs.ataljal.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.activities.Login;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;


public class ChangePassword extends Fragment {

    private EditText user_oldpassword,user_password,user_cnfpassword,editCaptcha;
    private TextView user_email;
    Button saveBtn;
    private ProgressDialog progressDialog;
    private RequestQueue requestQueue;
    private String password="",cnfPassword="";
    private ImageView show_pass_btn_password,show_pass_btn_confirmpassword,show_pass_old_btn_password,captcha,refresh;
    private MyAppPrefsManager myAppPrefsManager;
    private String logDate="",captcha_id="";
    StringBuffer passText,passNum,passSpecial;
    StringBuffer emailText,emailNum;

    public ChangePassword() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_change_password, container, false);
        myAppPrefsManager = new MyAppPrefsManager(getContext());
        requestQueue = Volley.newRequestQueue(getContext());
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        logDate = mdformat.format(calendar.getTime()).toUpperCase();

        user_email = view.findViewById(R.id.user_email);
        user_oldpassword = view.findViewById(R.id.user_oldpassword);
        user_password = view.findViewById(R.id.user_password);
        user_cnfpassword = view.findViewById(R.id.user_cnfpassword);
        show_pass_old_btn_password=view.findViewById(R.id.show_pass_old_btn_password);
        show_pass_btn_password=view.findViewById(R.id.show_pass_btn_password);
        show_pass_btn_confirmpassword=view.findViewById(R.id.show_pass_btn_confirmpassword);
        saveBtn = view.findViewById(R.id.saveBtn);
        captcha=view.findViewById(R.id.captcha);
        refresh=view.findViewById(R.id.refresh);
        editCaptcha = view.findViewById(R.id.editCaptcha);

        if (myAppPrefsManager.isUserLoggedIn()){
            user_email.setText(myAppPrefsManager.getUserEmail());
        }

        if (isNetworkAvailable()){

            if (progressDialog!=null&&!progressDialog.isShowing()){
                progressDialog.show();
            }
            getCaptcha();
        }

        user_oldpassword.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        user_oldpassword.setLongClickable(false);
        user_oldpassword.setTextIsSelectable(false);

        user_password.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        user_password.setLongClickable(false);
        user_password.setTextIsSelectable(false);

        user_cnfpassword.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        user_cnfpassword.setLongClickable(false);
        user_cnfpassword.setTextIsSelectable(false);

        show_pass_old_btn_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId()==R.id.show_pass_old_btn_password){

                    if(user_oldpassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance()))
                    {
                        ((ImageView)(view)).setImageResource(R.drawable.ic_hide_password);

                        //Show Password
                        user_oldpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        user_oldpassword.setSelection(user_oldpassword.length());
                    }
                    else{
                        ((ImageView)(view)).setImageResource(R.drawable.ic_password_eye);

                        //Hide Password
                        user_oldpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        user_oldpassword.setSelection(user_oldpassword.length());

                    }
                }
            }
        });

        show_pass_btn_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId()==R.id.show_pass_btn_password){

                    if(user_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance()))
                    {
                        ((ImageView)(view)).setImageResource(R.drawable.ic_hide_password);

                        //Show Password
                        user_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        user_password.setSelection(user_password.length());
                    }
                    else{
                        ((ImageView)(view)).setImageResource(R.drawable.ic_password_eye);

                        //Hide Password
                        user_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        user_password.setSelection(user_password.length());

                    }
                }
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()){

                    if (progressDialog!=null&&!progressDialog.isShowing()){
                        progressDialog.show();
                    }
                    getCaptcha();
                }else {
                    Toast.makeText(getActivity(), "Internet connection not available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        show_pass_btn_confirmpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId()==R.id.show_pass_btn_confirmpassword){

                    if(user_cnfpassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance()))
                    {
                        ((ImageView)(view)).setImageResource(R.drawable.ic_hide_password);

                        //Show Password
                        user_cnfpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        user_cnfpassword.setSelection(user_cnfpassword.length());
                    }
                    else{
                        ((ImageView)(view)).setImageResource(R.drawable.ic_password_eye);

                        //Hide Password
                        user_cnfpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                    }
                }
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()){

                    String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+[a-z]";
                    String emailpattern1 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+[a-z]+\\.+[a-z]+[a-z]";
                    if (!user_email.getText().toString().equals("")) {
                        if (user_email.getText().toString().trim().matches(emailpattern)||user_email.getText().toString().trim().matches(emailpattern1)) {
                            String email = user_email.getText().toString().trim();
                            String[] email1 = email.split("@");
                            String userId1 = email1[0];
                            String userId2 = email1[1];
                            if (!user_oldpassword.getText().toString().trim().equals("")) {
                                if (user_oldpassword.getText().toString().trim().length() >= 8) {
                                    if (!user_password.getText().toString().trim().equals("")) {
                                        if (user_password.getText().toString().trim().length() >= 8) {
                                            password = user_password.getText().toString().trim();
                                            if (isValidPassword(password)) {
                                                if (!user_cnfpassword.getText().toString().trim().equals("")) {
                                                    if (user_cnfpassword.getText().toString().trim().length() >= 8) {
                                                        cnfPassword = user_cnfpassword.getText().toString().trim();
                                                        if (isValidPassword(cnfPassword)) {
                                                            if (password.equals(cnfPassword)) {
                                                                splitPassword(password);
                                                                splitEmail(userId1);
                                                                if (!emailText.toString().toLowerCase().contains(passText.toString().toLowerCase())) {
                                                                    if(!user_oldpassword.getText().toString().equals(password)){
                                                                    if (!editCaptcha.getText().toString().trim().equals("")) {
                                                                        if (editCaptcha.getText().toString().trim().length() == 6) {

                                                                            if (progressDialog != null && !progressDialog.isShowing()) {
                                                                                progressDialog.show();
                                                                            }


                                                                            String hash = get_SHA_256_SecurePassword(user_password.getText().toString().trim());
                                                                            String oldhash = get_SHA_256_SecurePassword(user_oldpassword.getText().toString().trim());

                                                                            changePassword(user_email.getText().toString().trim().toLowerCase(), oldhash,
                                                                                    user_password.getText().toString().trim(), hash, captcha_id, editCaptcha.getText().toString().trim());
                                                                        } else {
                                                                            Toast.makeText(getContext(), "Enter valid captcha", Toast.LENGTH_SHORT).show();
                                                                        }

                                                                        } else {
                                                                            Toast.makeText(getContext(), "Enter Captcha code", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }else{
                                                                        Toast.makeText(getContext(), "new password and old password cannot be same", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                } else {
                                                                    Toast.makeText(getContext(), "password should not be same as email address", Toast.LENGTH_SHORT).show();
                                                                }
                                                            } else {
                                                                Toast.makeText(getContext(), "Password should match with Confirm Password", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {
                                                            Toast.makeText(getContext(), "Confirm Password should Contain Capital Letter, Numbers and Special Characters", Toast.LENGTH_LONG).show();
                                                        }
                                                    } else {
                                                        user_cnfpassword.setError("Please enter minimum 8 characters");
                                                        user_cnfpassword.requestFocus();
                                                    }
                                                } else {
                                                    user_cnfpassword.setError("Please enter Valid  Confirm Password");
                                                    user_cnfpassword.requestFocus();
                                                }
                                            } else {
                                                Toast.makeText(getContext(), "Password should Contain Capital Letter, Numbers and Special Characters", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            user_password.setError("Please enter minimum 8 characters");
                                            user_password.requestFocus();
                                        }
                                    } else {
                                        user_password.setError("Please enter Valid Password");
                                        user_password.requestFocus();
                                    }
                                }else{
                                    user_oldpassword.setError("Please enter minimum 8 characters");
                                    user_oldpassword.requestFocus();
                                }
                            }else {
                                user_oldpassword.setError("Please enter Valid Password");
                                user_oldpassword.requestFocus();
                            }
                        }else {
                            Toast.makeText(getContext(), "Enter Valid Email address", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getContext(), "Enter Email Address", Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Toast.makeText(getContext(), "Internet connection not available!", Toast.LENGTH_SHORT).show();
                }
            }
        });



        return view;
    }

    protected void changePassword(final String email, final String oldpassword,final String password,final String hashed,final String capid,
                                  final String captcha) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"changepassword_encrypt.jsp",
                //BuildConfig.SERVER_URL+"changepassword_encrypt_test.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", ""+response);


                        try {

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }


                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Change Password","Password Changed Successfully");
                                insertPasswordLog(user_email.getText().toString().trim().toLowerCase(),hashed,logDate);

                                myAppPrefsManager.setUserLoggedIn(false);
                                myAppPrefsManager.setUserImage("");
                                myAppPrefsManager.clearSession();

                                Intent intent = new Intent(getContext(), Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                getActivity().finish();

                            }else if (obj.getString("status").equals("Invalid captcha")){

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Change Password","Invalid Captcha");

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }
                                Toast.makeText(getContext(), ""+obj.getString("status"), Toast.LENGTH_LONG).show();



                            } else  {
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Change Password","Password Change Failed");

                                Toast.makeText(getContext(), ""+obj.getString("status"), Toast.LENGTH_LONG).show();

                                if (progressDialog!=null&&!progressDialog.isShowing()){
                                    progressDialog.show();
                                }
                                getCaptcha();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            if (isNetworkAvailable()) {
                                if (progressDialog != null && !progressDialog.isShowing()) {
                                    progressDialog.show();
                                }
                                getCaptcha();
                            }


                            Log.d("kcr test", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        if (isNetworkAvailable()) {
                            if (progressDialog != null && !progressDialog.isShowing()) {
                                progressDialog.show();
                            }
                            getCaptcha();
                        }

                        Log.d("kcr", "volleyerror "+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("oldhashpassword", oldpassword);
                params.put("password", password);
                params.put("hashpassword",hashed);
                params.put("capid",capid);
                params.put("captcha",captcha);
                Log.e("params",params.toString());
                return params;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);
                Log.e("params",params.toString());
                return params;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    protected void getCaptcha() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                BuildConfig.SERVER_URL+"captcha.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        //getting the whole json object from the response
                        try {
                            JSONObject obj = new JSONObject(response);

                            captcha_id = obj.getString("captcha_id");
                            String byteCapctha = obj.getString("captcha");
                            byte[] decodedString = Base64.decode(byteCapctha, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            captcha.setImageBitmap(decodedByte);

                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Log.d("kcr",e.getLocalizedMessage());

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    protected void insertPasswordLog(final String email,final String password, final String datetime) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"passwordlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcrpassword", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            } else {

                                Log.d("kcr",""+obj.getString("status"));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("password", password);
                params.put("datetime", datetime);
                Log.e("paramspaaswordlog",params.toString());
                return params;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private static String get_SHA_256_SecurePassword(String passwordToHash)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            //md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    private void splitPassword(String str)
    {
        passText = new StringBuffer();
        passNum = new StringBuffer();
        passSpecial = new StringBuffer();

        for (int i=0; i<str.length(); i++)
        {
            if (Character.isDigit(str.charAt(i)))
                passNum.append(str.charAt(i));
            else if(Character.isAlphabetic(str.charAt(i)))
                passText.append(str.charAt(i));
            else
                passSpecial.append(str.charAt(i));
        }

    }

    private void splitEmail(String str)
    {
        emailText = new StringBuffer();
        emailNum = new StringBuffer();

        for (int i=0; i<str.length(); i++)
        {
            if (Character.isDigit(str.charAt(i)))
                emailNum.append(str.charAt(i));
            else if(Character.isAlphabetic(str.charAt(i)))
                emailText.append(str.charAt(i));

        }

    }
}
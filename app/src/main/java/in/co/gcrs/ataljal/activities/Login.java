package in.co.gcrs.ataljal.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.customs.RootUtil;


public class Login extends AppCompatActivity {


    View parentView;
    Toolbar toolbar;
    ActionBar actionBar;

    EditText user_email, user_password,editCaptcha;
    TextView forgotPassword, signupText;
    Button loginBtn;


    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;

    private MyAppPrefsManager myAppPrefsManager;

    private static final int REQUEST_CHECK_LOCATION_SETTINGS = 1001;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 100;
    private static final int PERMISSION_REQUEST_CODE = 1;


    private double latitude, longitude;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    RequestQueue requestQueue;
    private ProgressDialog progressDialog;
    String strDate,logDate="",email="",emailData="",captcha_id="",salt="",mixed="";
    private ImageView show_pass_btn_password,captcha,refresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        myAppPrefsManager = new MyAppPrefsManager(getApplicationContext());



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        }


        else{
            displayLocationSettings();
        }


        if(myAppPrefsManager.isUserLoggedIn()){

            Intent intent = new Intent(Login.this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            finish();

        }


        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(Login.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH);
        SimpleDateFormat mdformat1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        strDate = mdformat.format(calendar.getTime()).toUpperCase();
        logDate = mdformat1.format(calendar.getTime()).toUpperCase();


        // setting Toolbar
        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.app_name));
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);


        // Binding Layout Views
        user_email = (EditText) findViewById(R.id.user_email);
        user_password = (EditText) findViewById(R.id.user_password);
        editCaptcha = (EditText) findViewById(R.id.editCaptcha);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        signupText = (TextView) findViewById(R.id.login_signupText);
        forgotPassword = (TextView) findViewById(R.id.forgotPassword);
        show_pass_btn_password=findViewById(R.id.show_pass_btn_password);
        captcha=findViewById(R.id.captcha);
        refresh=findViewById(R.id.refresh);

        parentView = signupText;

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()){

                    if (progressDialog!=null&&!progressDialog.isShowing()){
                        progressDialog.show();
                    }
                    getCaptcha();
                    secureRandomGenerator();
                }else {
                    Toast.makeText(Login.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                }
            }
        });


        if (isNetworkAvailable()){

            if (progressDialog!=null&&!progressDialog.isShowing()){
                progressDialog.show();
            }
            getCaptcha();
            secureRandomGenerator();
        }

        user_email.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        user_email.setLongClickable(false);
        user_email.setTextIsSelectable(false);

        user_password.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        user_password.setLongClickable(false);
        user_password.setTextIsSelectable(false);

        editCaptcha.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        editCaptcha.setLongClickable(false);
        editCaptcha.setTextIsSelectable(false);

        show_pass_btn_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId()==R.id.show_pass_btn_password){

                    if(user_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance()))
                    {
                        ((ImageView)(view)).setImageResource(R.drawable.ic_hide_password);

                        //Show Password
                        user_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        user_password.setSelection(user_password.length());
                    }
                    else{
                        ((ImageView)(view)).setImageResource(R.drawable.ic_password_eye);

                        //Hide Password
                        user_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        user_password.setSelection(user_password.length());

                    }
                }
            }
        });

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("kcr", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        myAppPrefsManager.setKeyFcmUserKey(token);
                        // Log and toast
                        String msg = "fcm key: "+token;
                        Log.d("kcr", msg);
                    }
                });



        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);


        user_email.setText(sharedPreferences.getString("userEmail", null));


        //forgot password

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Intent intent = new Intent(Login.this,ForgotPassword.class);
                startActivity(intent);*/
                String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+[a-z]";
                if (user_email.getText().toString().trim().matches(emailpattern)) {
                    if (!editCaptcha.getText().toString().trim().equals("")) {
                        if (editCaptcha.getText().toString().trim().length()==6) {

                            captchaValidation(captcha_id,editCaptcha.getText().toString().trim());

                        }else {
                            Toast.makeText(Login.this, "Enter valid captcha", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(Login.this, "Please Enter Captcha", Toast.LENGTH_SHORT).show();
                    }

                }else {

                    user_email.setError("Please enter Valid Email");
                    user_email.requestFocus();

                }

            }
        });

        signupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigate to SignUp Activity
                startActivity(new Intent(Login.this, Signup.class));
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
            }
        });

        editCaptcha.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //do what you want on the press of 'done'
                    submit();
                }
                return false;
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (isNetworkAvailable()) {

                    submit();

                }else {
                    Toast.makeText(Login.this, "Internet not available!", Toast.LENGTH_SHORT).show();
                }





            }
        });



    }

    private void submit(){
        String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+[a-z]";
        String emailpattern1 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+[a-z]+\\.+[a-z]+[a-z]";
        if (!user_email.getText().toString().equals("") && !user_password.getText().toString().equals("")) {
            if (user_email.getText().toString().trim().matches(emailpattern)||user_email.getText().toString().trim().matches(emailpattern1)) {
                if (!editCaptcha.getText().toString().trim().equals("")) {
                    if (editCaptcha.getText().toString().trim().length()==6) {

                        if (progressDialog != null && !progressDialog.isShowing()) {
                            progressDialog.show();
                        }


                        String singleHash = get_SHA_256_SecurePassword(user_password.getText().toString().trim());
                        String saltedHash = singleHash+salt;
                        String hash = get_SHA_256_SecurePassword(saltedHash);

                        setLogin(user_email.getText().toString().trim().toLowerCase(), /*user_password.getText().toString(),*/
                                hash,mixed,captcha_id,editCaptcha.getText().toString().trim());

                        Log.d("kcr",""+hash);

                    }else{

                        Toast.makeText(Login.this, "Enter valid captcha", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(Login.this, "Enter Captcha code", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(Login.this, "Enter Valid Email address", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(Login.this, "Enter Email Address And Password", Toast.LENGTH_SHORT).show();
        }
    }

    //*********** Receives the result from a previous call of startActivityForResult(Intent, int) ********//

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {

            case REQUEST_CHECK_LOCATION_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation();


                        break;

                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Please Enable Location", Toast.LENGTH_SHORT).show();
                        displayLocationSettings();

                }
                break;

        }

    }




    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        }


        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(Login.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {


                    latitude = location.getLatitude();
                    longitude = location.getLongitude();



                    myAppPrefsManager.setLatitude(String.valueOf(latitude));
                    myAppPrefsManager.setLongitude(String.valueOf(longitude));


                }

            }
        });
    }




    @Override
    protected void onStart() {
        super.onStart();

    }


    private void displayLocationSettings() {



        final LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (ContextCompat.checkSelfPermission(Login.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                for (Location location : locationResult.getLocations()) {
                                    //Do what you want with location
                                    //like update camera


                                    myAppPrefsManager.setLatitude(String.valueOf(location.getLatitude()));
                                    myAppPrefsManager.setLongitude(String.valueOf(location.getLongitude()));

                                }

                            }
                        },null);

                    }
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(Login.this, REQUEST_CHECK_LOCATION_SETTINGS);
                                break;
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.

                            displayLocationSettings();
                            break;
                    }
                }}
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    displayLocationSettings();
                }
                else{
                    androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
                    alertDialogBuilder.setTitle("Change Permissions in Settings");
                    alertDialogBuilder
                            .setMessage("" +
                                    "\nClick SETTINGS to Manually Set\n" + "Permissions to Access Location")
                            .setCancelable(false)
                            .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, 1000);
                                }
                            });

                    androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }

            }
            break;


            default:
                // Toast.makeText(this, "Please Allow", Toast.LENGTH_SHORT).show();
                // getDeviceLocation();
                break;


        }

    }

    protected void setLogin(final String email, /*final String password,*/final String hashed,final String mixed,
                            final String capid,final String captcha) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"signin_encrypt.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){


                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }


                                obj.getString("status");

                                /*myAppPrefsManager.setUserName(obj.getString("UserName"));
                                myAppPrefsManager.setUserEmail(obj.getString("Email"));
                                myAppPrefsManager.setState(obj.getString("StateName"));
                                myAppPrefsManager.setDistrict(obj.getString("DistrictName"));
                                myAppPrefsManager.setBlock(obj.getString("BlockName"));
                                myAppPrefsManager.setGrampanchayat(obj.getString("GramPanchayatName"));
                                myAppPrefsManager.setUserId(obj.getString("UserId"));*/

                                myAppPrefsManager.setUserName(obj.getString("username"));
                                myAppPrefsManager.setUserEmail(obj.getString("email"));
                                myAppPrefsManager.setState(obj.getString("statename"));
                                myAppPrefsManager.setDistrict(obj.getString("districtname"));
                                myAppPrefsManager.setBlock(obj.getString("blockname"));
                                myAppPrefsManager.setGrampanchayat(obj.getString("gramPanchayatname"));
                                myAppPrefsManager.setCategory(obj.getString("nodalofficer"));
                                myAppPrefsManager.setUserId(obj.getString("id"));

                                myAppPrefsManager.setLastLogin("Last Login:"+strDate);
                                myAppPrefsManager.setUserLoggedIn(true);

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Valid Login","Login Success");

                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                                Intent intent = new Intent(Login.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();



                            } else if (obj.getString("status").equals("Invalid captcha")){

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Login","Invalid Captcha");

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }
                                Toast.makeText(Login.this, ""+obj.getString("status"), Toast.LENGTH_LONG).show();
                            }
                            else {
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Invalid Login","Login Failed");
                                //Toast.makeText(Login.this, "Wrong User email and password", Toast.LENGTH_SHORT).show();
                                Toast.makeText(Login.this, ""+obj.getString("status"), Toast.LENGTH_SHORT).show();

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }
                            }





                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            if (isNetworkAvailable()){

                                if (progressDialog!=null&&!progressDialog.isShowing()){
                                    progressDialog.show();
                                }
                                getCaptcha();
                            }


                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        Toast.makeText(Login.this, "Please try again later", Toast.LENGTH_SHORT).show();

                        Log.d("kcr", "volleyerror "+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Email", email);
                //params.put("Password", password);
                params.put("hashpassword", hashed);
                params.put("mixed", mixed);
                params.put("capid",capid);
                params.put("captcha",captcha);
                Log.e("params",params.toString());
                return params;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(RootUtil.isDeviceRooted()){
            showAlertDialogAndExitApp("This device is rooted. You can't use this app.");
        }else if (RootUtil.isEmulator(getApplicationContext())){
            showAlertDialogAndExitApp("This is Emulator. You can't use this app.");
        }

        checkLocationPermission();


    }

    public void showAlertDialogAndExitApp(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }

    private void checkLocationPermission(){



        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    showAppSettings();
                }

            }
        } else {
            displayLocationSettings();

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(Login.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    private void showAppSettings(){

        Toast.makeText(Login.this, "Please Allow the permission for Location in settings", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Login.this);
        alertDialogBuilder.setTitle("Change Permissions in Settings");
        alertDialogBuilder
                .setMessage("" +
                        "\nClick SETTINGS to Manually Set\n" + "Permissions to Access Location")
                .setCancelable(false)
                .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1000);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void sendEmail(final String data){
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL3,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {

                            if (response.equals("Please Check Your Mail")) {

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Forgot Password","Email Sent");

                                Toast.makeText(Login.this, "Reset link has been sent to your registered email address", Toast.LENGTH_LONG).show();

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }

                            }else if(response.equals("You Have Not Registered")) {

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                Toast.makeText(Login.this, ""+response, Toast.LENGTH_LONG).show();

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }

                            }else if (response.equals("You had Registered but, you have to Activate your Account, Please check your Email")){

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Forgot Password",response);

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                Toast.makeText(Login.this, ""+response, Toast.LENGTH_LONG).show();

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }
                                //Toast.makeText(Login.this, "Please try after some time", Toast.LENGTH_LONG).show();
                            }else if (response.equals("State Officer not Approved Your Credentials")){

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Forgot Password",response);

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                Toast.makeText(Login.this, ""+response, Toast.LENGTH_LONG).show();

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }
                                //Toast.makeText(Login.this, "Please try after some time", Toast.LENGTH_LONG).show();
                            }else{

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Forgot Password",response);

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                Toast.makeText(Login.this, ""+response, Toast.LENGTH_LONG).show();

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }
                            }

                        }catch (Exception e) {
                            // JSON error
                            e.printStackTrace();
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            Log.d("kcr catch",""+e.getLocalizedMessage());

                            if (isNetworkAvailable()){

                                if (progressDialog!=null&&!progressDialog.isShowing()){
                                    progressDialog.show();
                                }
                                getCaptcha();
                            }

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("kcr error",""+error.getLocalizedMessage());
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        if (error.getLocalizedMessage()==null){

                            Toast.makeText(Login.this, "Please try after some time", Toast.LENGTH_LONG).show();
                        }

                        if (isNetworkAvailable()){

                            if (progressDialog!=null&&!progressDialog.isShowing()){
                                progressDialog.show();
                            }
                            getCaptcha();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();

                params.put("Data",data);

                Log.e("params",params.toString());
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);

                Log.e("params",params.toString());
                return params;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    protected void getCaptcha() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                BuildConfig.SERVER_URL+"captcha.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        //getting the whole json object from the response
                        try {
                            JSONObject obj = new JSONObject(response);

                            captcha_id = obj.getString("captcha_id");
                            String byteCapctha = obj.getString("captcha");
                            byte[] decodedString = Base64.decode(byteCapctha, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            captcha.setImageBitmap(decodedByte);
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Log.d("kcr",e.getLocalizedMessage());

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    protected void captchaValidation(final String capid,final String captcha) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"validate_captcha.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        //getting the whole json object from the response
                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                if (progressDialog != null) {
                                    progressDialog.show();
                                }

                                email = user_email.getText().toString().trim().toLowerCase();
                                emailData = "1";
                                emailData = emailData + "," + email;
                                emailData = emailData + "," + "1";

                                sendEmail(emailData);

                            }else if (obj.getString("status").equals("Invalid captcha")){

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Login","Invalid Captcha");

                                if (isNetworkAvailable()){

                                    if (progressDialog!=null&&!progressDialog.isShowing()){
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }
                                Toast.makeText(Login.this, ""+obj.getString("status"), Toast.LENGTH_LONG).show();
                            }else {

                                progressDialog.dismiss();

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Login",obj.getString("status"));
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Log.d("kcr",e.getLocalizedMessage());

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("capid",capid);
                params.put("captcha",captcha);
                Log.e("paramscaptcha",params.toString());
                return params;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void secureRandomGenerator(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                BuildConfig.SERVER_URL+"secure_random_generator.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {


                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            salt = obj.getString("random");
                            String s1 = salt.substring(0,3);
                            String s2 = salt.substring(3,6);
                            mixed = s2+s1;



                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Log.d("kcr delete vollyerror",""+error.getLocalizedMessage());
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    private static String get_SHA_256_SecurePassword(String passwordToHash)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            //md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

}


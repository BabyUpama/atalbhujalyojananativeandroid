package in.co.gcrs.ataljal.model;

public class GWMStations
{
    private String id,sitename,state,district,block,depth,gp,welltype,wellId,latitude,longitude,source,pre2019,post2019,uid,
            jan2010,feb2010,mar2010,apr2010,may2010,june2010,july2010,aug2010,sept2010,oct2010,nov2010,dec2010,
            jan2011,feb2011,mar2011,apr2011,may2011,june2011,july2011,aug2011,sept2011,oct2011,nov2011,dec2011,
            jan2012,feb2012,mar2012,apr2012,may2012,june2012,july2012,aug2012,sept2012,oct2012,nov2012,dec2012,
            jan2013,feb2013,mar2013,apr2013,may2013,june2013,july2013,aug2013,sept2013,oct2013,nov2013,dec2013,
            jan2014,feb2014,mar2014,apr2014,may2014,june2014,july2014,aug2014,sept2014,oct2014,nov2014,dec2014,
            jan2015,feb2015,mar2015,apr2015,may2015,june2015,july2015,aug2015,sept2015,oct2015,nov2015,dec2015,
            jan2016,feb2016,mar2016,apr2016,may2016,june2016,july2016,aug2016,sept2016,oct2016,nov2016,dec2016,
            jan2017,feb2017,mar2017,apr2017,may2017,june2017,july2017,aug2017,sept2017,oct2017,nov2017,dec2017,
            jan2018,feb2018,mar2018,apr2018,may2018,june2018,july2018,aug2018,sept2018,oct2018,nov2018,dec2018,
            jan2019,feb2019,mar2019,apr2019,may2019,june2019,july2019,aug2019,sept2019,oct2019,nov2019,dec2019,
            jan2020,image,well;

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getJan2010() {
        return jan2010;
    }

    public void setJan2010(String jan2010) {
        this.jan2010 = jan2010;
    }

    public String getFeb2010() {
        return feb2010;
    }

    public void setFeb2010(String feb2010) {
        this.feb2010 = feb2010;
    }

    public String getMar2010() {
        return mar2010;
    }

    public void setMar2010(String mar2010) {
        this.mar2010 = mar2010;
    }

    public String getApr2010() {
        return apr2010;
    }

    public void setApr2010(String apr2010) {
        this.apr2010 = apr2010;
    }

    public String getMay2010() {
        return may2010;
    }

    public void setMay2010(String may2010) {
        this.may2010 = may2010;
    }

    public String getJune2010() {
        return june2010;
    }

    public void setJune2010(String june2010) {
        this.june2010 = june2010;
    }

    public String getJuly2010() {
        return july2010;
    }

    public void setJuly2010(String july2010) {
        this.july2010 = july2010;
    }

    public String getAug2010() {
        return aug2010;
    }

    public void setAug2010(String aug2010) {
        this.aug2010 = aug2010;
    }

    public String getSept2010() {
        return sept2010;
    }

    public void setSept2010(String sept2010) {
        this.sept2010 = sept2010;
    }

    public String getOct2010() {
        return oct2010;
    }

    public void setOct2010(String oct2010) {
        this.oct2010 = oct2010;
    }

    public String getNov2010() {
        return nov2010;
    }

    public void setNov2010(String nov2010) {
        this.nov2010 = nov2010;
    }

    public String getDec2010() {
        return dec2010;
    }

    public void setDec2010(String dec2010) {
        this.dec2010 = dec2010;
    }

    public String getJan2011() {
        return jan2011;
    }

    public void setJan2011(String jan2011) {
        this.jan2011 = jan2011;
    }

    public String getFeb2011() {
        return feb2011;
    }

    public void setFeb2011(String feb2011) {
        this.feb2011 = feb2011;
    }

    public String getMar2011() {
        return mar2011;
    }

    public void setMar2011(String mar2011) {
        this.mar2011 = mar2011;
    }

    public String getApr2011() {
        return apr2011;
    }

    public void setApr2011(String apr2011) {
        this.apr2011 = apr2011;
    }

    public String getMay2011() {
        return may2011;
    }

    public void setMay2011(String may2011) {
        this.may2011 = may2011;
    }

    public String getJune2011() {
        return june2011;
    }

    public void setJune2011(String june2011) {
        this.june2011 = june2011;
    }

    public String getJuly2011() {
        return july2011;
    }

    public void setJuly2011(String july2011) {
        this.july2011 = july2011;
    }

    public String getAug2011() {
        return aug2011;
    }

    public void setAug2011(String aug2011) {
        this.aug2011 = aug2011;
    }

    public String getSept2011() {
        return sept2011;
    }

    public void setSept2011(String sept2011) {
        this.sept2011 = sept2011;
    }

    public String getOct2011() {
        return oct2011;
    }

    public void setOct2011(String oct2011) {
        this.oct2011 = oct2011;
    }

    public String getNov2011() {
        return nov2011;
    }

    public void setNov2011(String nov2011) {
        this.nov2011 = nov2011;
    }

    public String getDec2011() {
        return dec2011;
    }

    public void setDec2011(String dec2011) {
        this.dec2011 = dec2011;
    }

    public String getJan2012() {
        return jan2012;
    }

    public void setJan2012(String jan2012) {
        this.jan2012 = jan2012;
    }

    public String getFeb2012() {
        return feb2012;
    }

    public void setFeb2012(String feb2012) {
        this.feb2012 = feb2012;
    }

    public String getMar2012() {
        return mar2012;
    }

    public void setMar2012(String mar2012) {
        this.mar2012 = mar2012;
    }

    public String getApr2012() {
        return apr2012;
    }

    public void setApr2012(String apr2012) {
        this.apr2012 = apr2012;
    }

    public String getMay2012() {
        return may2012;
    }

    public void setMay2012(String may2012) {
        this.may2012 = may2012;
    }

    public String getJune2012() {
        return june2012;
    }

    public void setJune2012(String june2012) {
        this.june2012 = june2012;
    }

    public String getJuly2012() {
        return july2012;
    }

    public void setJuly2012(String july2012) {
        this.july2012 = july2012;
    }

    public String getAug2012() {
        return aug2012;
    }

    public void setAug2012(String aug2012) {
        this.aug2012 = aug2012;
    }

    public String getSept2012() {
        return sept2012;
    }

    public void setSept2012(String sept2012) {
        this.sept2012 = sept2012;
    }

    public String getOct2012() {
        return oct2012;
    }

    public void setOct2012(String oct2012) {
        this.oct2012 = oct2012;
    }

    public String getNov2012() {
        return nov2012;
    }

    public void setNov2012(String nov2012) {
        this.nov2012 = nov2012;
    }

    public String getDec2012() {
        return dec2012;
    }

    public void setDec2012(String dec2012) {
        this.dec2012 = dec2012;
    }

    public String getJan2013() {
        return jan2013;
    }

    public void setJan2013(String jan2013) {
        this.jan2013 = jan2013;
    }

    public String getFeb2013() {
        return feb2013;
    }

    public void setFeb2013(String feb2013) {
        this.feb2013 = feb2013;
    }

    public String getMar2013() {
        return mar2013;
    }

    public void setMar2013(String mar2013) {
        this.mar2013 = mar2013;
    }

    public String getApr2013() {
        return apr2013;
    }

    public void setApr2013(String apr2013) {
        this.apr2013 = apr2013;
    }

    public String getMay2013() {
        return may2013;
    }

    public void setMay2013(String may2013) {
        this.may2013 = may2013;
    }

    public String getJune2013() {
        return june2013;
    }

    public void setJune2013(String june2013) {
        this.june2013 = june2013;
    }

    public String getJuly2013() {
        return july2013;
    }

    public void setJuly2013(String july2013) {
        this.july2013 = july2013;
    }

    public String getAug2013() {
        return aug2013;
    }

    public void setAug2013(String aug2013) {
        this.aug2013 = aug2013;
    }

    public String getSept2013() {
        return sept2013;
    }

    public void setSept2013(String sept2013) {
        this.sept2013 = sept2013;
    }

    public String getOct2013() {
        return oct2013;
    }

    public void setOct2013(String oct2013) {
        this.oct2013 = oct2013;
    }

    public String getNov2013() {
        return nov2013;
    }

    public void setNov2013(String nov2013) {
        this.nov2013 = nov2013;
    }

    public String getDec2013() {
        return dec2013;
    }

    public void setDec2013(String dec2013) {
        this.dec2013 = dec2013;
    }

    public String getJan2014() {
        return jan2014;
    }

    public void setJan2014(String jan2014) {
        this.jan2014 = jan2014;
    }

    public String getFeb2014() {
        return feb2014;
    }

    public void setFeb2014(String feb2014) {
        this.feb2014 = feb2014;
    }

    public String getMar2014() {
        return mar2014;
    }

    public void setMar2014(String mar2014) {
        this.mar2014 = mar2014;
    }

    public String getApr2014() {
        return apr2014;
    }

    public void setApr2014(String apr2014) {
        this.apr2014 = apr2014;
    }

    public String getMay2014() {
        return may2014;
    }

    public void setMay2014(String may2014) {
        this.may2014 = may2014;
    }

    public String getJune2014() {
        return june2014;
    }

    public void setJune2014(String june2014) {
        this.june2014 = june2014;
    }

    public String getJuly2014() {
        return july2014;
    }

    public void setJuly2014(String july2014) {
        this.july2014 = july2014;
    }

    public String getAug2014() {
        return aug2014;
    }

    public void setAug2014(String aug2014) {
        this.aug2014 = aug2014;
    }

    public String getSept2014() {
        return sept2014;
    }

    public void setSept2014(String sept2014) {
        this.sept2014 = sept2014;
    }

    public String getOct2014() {
        return oct2014;
    }

    public void setOct2014(String oct2014) {
        this.oct2014 = oct2014;
    }

    public String getNov2014() {
        return nov2014;
    }

    public void setNov2014(String nov2014) {
        this.nov2014 = nov2014;
    }

    public String getDec2014() {
        return dec2014;
    }

    public void setDec2014(String dec2014) {
        this.dec2014 = dec2014;
    }

    public String getJan2015() {
        return jan2015;
    }

    public void setJan2015(String jan2015) {
        this.jan2015 = jan2015;
    }

    public String getFeb2015() {
        return feb2015;
    }

    public void setFeb2015(String feb2015) {
        this.feb2015 = feb2015;
    }

    public String getMar2015() {
        return mar2015;
    }

    public void setMar2015(String mar2015) {
        this.mar2015 = mar2015;
    }

    public String getApr2015() {
        return apr2015;
    }

    public void setApr2015(String apr2015) {
        this.apr2015 = apr2015;
    }

    public String getMay2015() {
        return may2015;
    }

    public void setMay2015(String may2015) {
        this.may2015 = may2015;
    }

    public String getJune2015() {
        return june2015;
    }

    public void setJune2015(String june2015) {
        this.june2015 = june2015;
    }

    public String getJuly2015() {
        return july2015;
    }

    public void setJuly2015(String july2015) {
        this.july2015 = july2015;
    }

    public String getAug2015() {
        return aug2015;
    }

    public void setAug2015(String aug2015) {
        this.aug2015 = aug2015;
    }

    public String getSept2015() {
        return sept2015;
    }

    public void setSept2015(String sept2015) {
        this.sept2015 = sept2015;
    }

    public String getOct2015() {
        return oct2015;
    }

    public void setOct2015(String oct2015) {
        this.oct2015 = oct2015;
    }

    public String getNov2015() {
        return nov2015;
    }

    public void setNov2015(String nov2015) {
        this.nov2015 = nov2015;
    }

    public String getDec2015() {
        return dec2015;
    }

    public void setDec2015(String dec2015) {
        this.dec2015 = dec2015;
    }

    public String getJan2016() {
        return jan2016;
    }

    public void setJan2016(String jan2016) {
        this.jan2016 = jan2016;
    }

    public String getFeb2016() {
        return feb2016;
    }

    public void setFeb2016(String feb2016) {
        this.feb2016 = feb2016;
    }

    public String getMar2016() {
        return mar2016;
    }

    public void setMar2016(String mar2016) {
        this.mar2016 = mar2016;
    }

    public String getApr2016() {
        return apr2016;
    }

    public void setApr2016(String apr2016) {
        this.apr2016 = apr2016;
    }

    public String getMay2016() {
        return may2016;
    }

    public void setMay2016(String may2016) {
        this.may2016 = may2016;
    }

    public String getJune2016() {
        return june2016;
    }

    public void setJune2016(String june2016) {
        this.june2016 = june2016;
    }

    public String getJuly2016() {
        return july2016;
    }

    public void setJuly2016(String july2016) {
        this.july2016 = july2016;
    }

    public String getAug2016() {
        return aug2016;
    }

    public void setAug2016(String aug2016) {
        this.aug2016 = aug2016;
    }

    public String getSept2016() {
        return sept2016;
    }

    public void setSept2016(String sept2016) {
        this.sept2016 = sept2016;
    }

    public String getOct2016() {
        return oct2016;
    }

    public void setOct2016(String oct2016) {
        this.oct2016 = oct2016;
    }

    public String getNov2016() {
        return nov2016;
    }

    public void setNov2016(String nov2016) {
        this.nov2016 = nov2016;
    }

    public String getDec2016() {
        return dec2016;
    }

    public void setDec2016(String dec2016) {
        this.dec2016 = dec2016;
    }

    public String getJan2017() {
        return jan2017;
    }

    public void setJan2017(String jan2017) {
        this.jan2017 = jan2017;
    }

    public String getFeb2017() {
        return feb2017;
    }

    public void setFeb2017(String feb2017) {
        this.feb2017 = feb2017;
    }

    public String getMar2017() {
        return mar2017;
    }

    public void setMar2017(String mar2017) {
        this.mar2017 = mar2017;
    }

    public String getApr2017() {
        return apr2017;
    }

    public void setApr2017(String apr2017) {
        this.apr2017 = apr2017;
    }

    public String getMay2017() {
        return may2017;
    }

    public void setMay2017(String may2017) {
        this.may2017 = may2017;
    }

    public String getJune2017() {
        return june2017;
    }

    public void setJune2017(String june2017) {
        this.june2017 = june2017;
    }

    public String getJuly2017() {
        return july2017;
    }

    public void setJuly2017(String july2017) {
        this.july2017 = july2017;
    }

    public String getAug2017() {
        return aug2017;
    }

    public void setAug2017(String aug2017) {
        this.aug2017 = aug2017;
    }

    public String getSept2017() {
        return sept2017;
    }

    public void setSept2017(String sept2017) {
        this.sept2017 = sept2017;
    }

    public String getOct2017() {
        return oct2017;
    }

    public void setOct2017(String oct2017) {
        this.oct2017 = oct2017;
    }

    public String getNov2017() {
        return nov2017;
    }

    public void setNov2017(String nov2017) {
        this.nov2017 = nov2017;
    }

    public String getDec2017() {
        return dec2017;
    }

    public void setDec2017(String dec2017) {
        this.dec2017 = dec2017;
    }

    public String getJan2018() {
        return jan2018;
    }

    public void setJan2018(String jan2018) {
        this.jan2018 = jan2018;
    }

    public String getFeb2018() {
        return feb2018;
    }

    public void setFeb2018(String feb2018) {
        this.feb2018 = feb2018;
    }

    public String getMar2018() {
        return mar2018;
    }

    public void setMar2018(String mar2018) {
        this.mar2018 = mar2018;
    }

    public String getApr2018() {
        return apr2018;
    }

    public void setApr2018(String apr2018) {
        this.apr2018 = apr2018;
    }

    public String getMay2018() {
        return may2018;
    }

    public void setMay2018(String may2018) {
        this.may2018 = may2018;
    }

    public String getJune2018() {
        return june2018;
    }

    public void setJune2018(String june2018) {
        this.june2018 = june2018;
    }

    public String getJuly2018() {
        return july2018;
    }

    public void setJuly2018(String july2018) {
        this.july2018 = july2018;
    }

    public String getAug2018() {
        return aug2018;
    }

    public void setAug2018(String aug2018) {
        this.aug2018 = aug2018;
    }

    public String getSept2018() {
        return sept2018;
    }

    public void setSept2018(String sept2018) {
        this.sept2018 = sept2018;
    }

    public String getOct2018() {
        return oct2018;
    }

    public void setOct2018(String oct2018) {
        this.oct2018 = oct2018;
    }

    public String getNov2018() {
        return nov2018;
    }

    public void setNov2018(String nov2018) {
        this.nov2018 = nov2018;
    }

    public String getDec2018() {
        return dec2018;
    }

    public void setDec2018(String dec2018) {
        this.dec2018 = dec2018;
    }

    public String getJan2019() {
        return jan2019;
    }

    public void setJan2019(String jan2019) {
        this.jan2019 = jan2019;
    }

    public String getFeb2019() {
        return feb2019;
    }

    public void setFeb2019(String feb2019) {
        this.feb2019 = feb2019;
    }

    public String getMar2019() {
        return mar2019;
    }

    public void setMar2019(String mar2019) {
        this.mar2019 = mar2019;
    }

    public String getApr2019() {
        return apr2019;
    }

    public void setApr2019(String apr2019) {
        this.apr2019 = apr2019;
    }

    public String getMay2019() {
        return may2019;
    }

    public void setMay2019(String may2019) {
        this.may2019 = may2019;
    }

    public String getJune2019() {
        return june2019;
    }

    public void setJune2019(String june2019) {
        this.june2019 = june2019;
    }

    public String getJuly2019() {
        return july2019;
    }

    public void setJuly2019(String july2019) {
        this.july2019 = july2019;
    }

    public String getAug2019() {
        return aug2019;
    }

    public void setAug2019(String aug2019) {
        this.aug2019 = aug2019;
    }

    public String getSept2019() {
        return sept2019;
    }

    public void setSept2019(String sept2019) {
        this.sept2019 = sept2019;
    }

    public String getOct2019() {
        return oct2019;
    }

    public void setOct2019(String oct2019) {
        this.oct2019 = oct2019;
    }

    public String getNov2019() {
        return nov2019;
    }

    public void setNov2019(String nov2019) {
        this.nov2019 = nov2019;
    }

    public String getDec2019() {
        return dec2019;
    }

    public void setDec2019(String dec2019) {
        this.dec2019 = dec2019;
    }

    public String getJan2020() {
        return jan2020;
    }

    public void setJan2020(String jan2020) {
        this.jan2020 = jan2020;
    }

    public String getGp() {
        return gp;
    }

    public void setGp(String gp) {
        this.gp = gp;
    }

    public String getWelltype() {
        return welltype;
    }

    public void setWelltype(String welltype) {
        this.welltype = welltype;
    }

    public String getWellId() {
        return wellId;
    }

    public void setWellId(String wellId) {
        this.wellId = wellId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getPre2019() {
        return pre2019;
    }

    public void setPre2019(String pre2019) {
        this.pre2019 = pre2019;
    }

    public String getPost2019() {
        return post2019;
    }

    public void setPost2019(String post2019) {
        this.post2019 = post2019;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWell() {
        return well;
    }

    public void setWell(String well) {
        this.well = well;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

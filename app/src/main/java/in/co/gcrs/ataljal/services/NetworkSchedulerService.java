package in.co.gcrs.ataljal.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Geocoder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.database.DbHelper;
import in.co.gcrs.ataljal.model.SocialMonitoringModel;
import in.co.gcrs.ataljal.model.GeotaggingModel;
import in.co.gcrs.ataljal.model.GpProfileModel;
import in.co.gcrs.ataljal.model.NewWellModel;

import in.co.gcrs.ataljal.model.RainfallModel;
import in.co.gcrs.ataljal.model.SarpanchDetailsModel;
import in.co.gcrs.ataljal.model.WellInventoryModel;
import in.co.gcrs.ataljal.recievers.ConnectivityReceiver;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;

public class NetworkSchedulerService extends JobService implements
        ConnectivityReceiver.ConnectivityReceiverListener {

    private static final String TAG = "kcr";
    private ConnectivityReceiver mConnectivityReceiver;
    private DbHelper dbHelper;
    RequestQueue requestQueue ;
    private MyAppPrefsManager myAppPrefsManager;
    private Geocoder geocoder;
    private String wellinventoryServerPath="",newWellServerPath="",serverImagePath="",serverFilePath1="";
    private String serverPath1="",serverPath2="";
    private String rainfallServerPath1="",rainfallServerPath2="",logDate="";
    Date c ;
    SimpleDateFormat df;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service created");
        mConnectivityReceiver = new ConnectivityReceiver(this);

        dbHelper = new DbHelper(getApplicationContext());
        myAppPrefsManager = new MyAppPrefsManager(getApplicationContext());

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        geocoder = new Geocoder(getApplicationContext(), Locale.ENGLISH);

        c= Calendar.getInstance().getTime();
        df= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        logDate = df.format(c).toUpperCase();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        //registerReceiver(mConnectivityReceiver, new IntentFilter(CONNECTIVITY_ACTION));
        return START_NOT_STICKY;
    }


    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Log.i(TAG, "onStartJob" + mConnectivityReceiver);
        registerReceiver(mConnectivityReceiver, new IntentFilter(CONNECTIVITY_ACTION));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.i(TAG, "onStopJob");
        unregisterReceiver(mConnectivityReceiver);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //unregisterReceiver(mConnectivityReceiver);
    }



    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        Log.i(TAG, String.valueOf(mConnectivityReceiver.isConnected(getApplicationContext())));

        if(mConnectivityReceiver.isConnected(getApplicationContext())){

            if (dbHelper!=null){

                // Well Inventiry

                if (dbHelper.getWellInventoryList().size()>0){

                    ArrayList<WellInventoryModel> wellInventoryArrayList = dbHelper.getWellInventoryList();

                    for (int i=0;i<wellInventoryArrayList.size();i++){

                        if (!wellInventoryArrayList.get(i).getImage().equals("")){
                            insertWellinventoryImage(wellInventoryArrayList.get(i).getImage(),wellInventoryArrayList.get(i).getEmail(),wellInventoryArrayList.get(i).getImageName());
                        }



                    }
                }

                // Geo Tagging

                if (dbHelper.getGeoTaggingList().size()>0){

                    ArrayList<GeotaggingModel> geoTaggingArrayList = dbHelper.getGeoTaggingList();

                    for (int i=0;i<geoTaggingArrayList.size();i++){
                        if (!geoTaggingArrayList.get(i).getImage1().equals("")) {

                            insertImage1(geoTaggingArrayList.get(i).getImage1(),geoTaggingArrayList.get(i).getEmail(),
                                    geoTaggingArrayList.get(i).getImageName1(),geoTaggingArrayList.get(i).getImageName2());

                        }else if (!geoTaggingArrayList.get(i).getImage2().equals("")){

                            insertImage2(geoTaggingArrayList.get(i).getImage2(),geoTaggingArrayList.get(i).getEmail(),
                                    geoTaggingArrayList.get(i).getImageName2());
                        }

                    }
                }
                // Sarpanch Details

                // Rainfall

                if (dbHelper.getRainfallList().size()>0){

                    ArrayList<RainfallModel> rainfallArrayList = dbHelper.getRainfallList();

                    for (int i=0;i<rainfallArrayList.size();i++){

                        /*insertRainfall(rainfallArrayList.get(i).getState(),rainfallArrayList.get(i).getDistrict(),rainfallArrayList.get(i).getBlock(),
                                rainfallArrayList.get(i).getGramPanchayat(),rainfallArrayList.get(i).getSitename(),rainfallArrayList.get(i).getYear(),
                                rainfallArrayList.get(i).getMonth(),rainfallArrayList.get(i).getRainfall(),rainfallArrayList.get(i).getLatitude(),
                                rainfallArrayList.get(i).getLongitude(),rainfallArrayList.get(i).getrId());*/
                        insertRainfallImage1(rainfallArrayList.get(i).getImage1(),rainfallArrayList.get(i).getImage2(),rainfallArrayList.get(i).getEmail(),
                                rainfallArrayList.get(i).getImagename1(),rainfallArrayList.get(i).getImagename2());

                    }
                }

                // Social Monititoring
                if (dbHelper.getSocialMonitoringList().size()>0){

                    ArrayList<SocialMonitoringModel> socialArrayList = dbHelper.getSocialMonitoringList();

                    for (int i=0;i<socialArrayList.size();i++){

                        insertImage(socialArrayList.get(i).getImagePath(),socialArrayList.get(i).getMom(),socialArrayList.get(i).getEmail(),
                                socialArrayList.get(i).getImageFilename(),socialArrayList.get(i).getMomFilename());

                    }
                }

                // New Observation Well
                if (dbHelper.getNewWellList().size()>0){

                    ArrayList<NewWellModel> newWellList = dbHelper.getNewWellList();

                    for (int i=0;i<newWellList.size();i++){

                        if (newWellList.get(i).getImage().equals("")){
                            insertNewWellImage(newWellList.get(i).getImage(),newWellList.get(i).getEmail(),newWellList.get(i).getImageName());
                        }
                    }
                }




            }
        }

    }

    public void insertWellInventory(final String wellno,final String date,final String state,final String district,
                             final String block,final String gp,final String village,
                             final String sitename,final String locationdetails,final String ownername,
                             final String welldiameter,
                             final String welldepth,final String measuringpoint,final String spinnerWellTypeSelectedItem,
                             final String wellWaterPotableSelectedItem,final String waterleveldepth,final String spinnerUseSelectedItem,final String pumpInstalledSelectedItem,
                             final String electricMeterSelectedItem,final String datetime,final String email,final String image,final String id) {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"insertwellinventory.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);


                    if (obj.getString("status").equals("success")) {

                        insertUserLog(email,getIpAddress(),logDate,"Well Inventory","Data Insertion Successful");


                        if (dbHelper.deleteWellInventory(id)){
                            Log.d("kcr","delete");

                        }else {

                            Log.d("kcr","not deleted");
                        }

                    }else {

                        insertUserLog(email,getIpAddress(),logDate,"Well Inventory","Data Insertion Unsuccessful");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();


                    Log.d("kcr", ""+e.getLocalizedMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();
                params.put("wellno",wellno);
                params.put("date",date);
                params.put("state",state);
                params.put("district",district);
                params.put("block",block);
                params.put("gp",gp);
                params.put("village",village);
                params.put("sitename",sitename);
                params.put("locationdetails",locationdetails);
                params.put("ownername",ownername);
                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("welldiameter",welldiameter);
                params.put("welldepth",welldepth);
                params.put("measuringpoint",measuringpoint);
                params.put("welltype",spinnerWellTypeSelectedItem);
                params.put("wellwaterpotable",wellWaterPotableSelectedItem);
                params.put("waterleveldepth",waterleveldepth);
                params.put("use",spinnerUseSelectedItem);
                params.put("pumpinstalled",pumpInstalledSelectedItem);
                params.put("electricmeterselecteditem",electricMeterSelectedItem);
                params.put("datetime",datetime);
                params.put("email",email);
                params.put("image",image);
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }


    protected void insertGeoTagging(final String state,final String district,final String block,final String grampanchayat,final String village,final String sitename,
                                    final String latitude,final String longitude,final String structure,final String storageCapacity,
                                    final String noofFillings,final String id,final String image1,final String image2,
                                    final String others,final String formattedDate,final String email) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                //ConstantValues.URL1+"web/users.jsp",
                BuildConfig.SERVER_URL+"insertgeotagging.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                insertUserLog(email,getIpAddress(),logDate,"Geo Tagging Artificial Recharge Structure","Data Insertion Successful");

                                //Toast.makeText(getApplicationContext(), "Successfully added", Toast.LENGTH_SHORT).show();

                                if (dbHelper.deleteGeoTagging(id)){
                                    Log.d("kcr","delete");

                                }else {

                                    Log.d("kcr","not deleted");
                                }

                            }else {

                                insertUserLog(email,getIpAddress(),logDate,"Geo Tagging Artificial Recharge Structure","Data Insertion Unsuccessful");

                                Log.d("kcr","not inserted");
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();


                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("statename", state);
                params.put("districtname", district);
                params.put("blockname", block);
                params.put("gpname", grampanchayat);
                params.put("village", village);
                params.put("sitename", sitename);
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("typeofstructure", structure);
                params.put("storagecapacity", storageCapacity);
                params.put("nooffillings", noofFillings);
                params.put("image1", image1);
                params.put("image2", image2);
                //params.put("deviceid", deviceid);
                params.put("others", others);
                params.put("date",formattedDate);
                params.put("email",email);





                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    protected void insertRainfall(final String state,final String district,final String block,final String gramPanchayat,
            final String siteName,final String rainfall,final String latitude,
                                  final String longitude,final String id,final String raingauge,final String date,final String image1,
                                  final String image2,final String email) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                //ConstantValues.URL1+"web/users.jsp",
                BuildConfig.SERVER_URL+"insertrainfall.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                        //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);



                            if (obj.getString("status").equals("success")){

                                insertUserLog(email,getIpAddress(),logDate,"Rainfall","Data Insertion Successful");
                                //Toast.makeText(getApplicationContext(), "Successfully added", Toast.LENGTH_SHORT).show();

                                if (dbHelper.deleteRainfallList(id)){
                                    Log.d("kcr","delete");

                                }else {

                                    Log.d("kcr","not deleted");
                                }


                            }else {
                                insertUserLog(email,getIpAddress(),logDate,"Rainfall","Data Insertion Unsuccessful");
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();


                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("statename",state);
                params.put("districtname", district);
                params.put("blockname", block);
                params.put("gpname",gramPanchayat);
                params.put("sitename", siteName);
                params.put("rainfall", rainfall);
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("raingauge", raingauge);
                params.put("date", date);
                params.put("image1", image1);
                params.put("image2", image2);
                params.put("email", email);




                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    public void insertCommunityParticipation(final String state,final String district,final String block,final String grampanchayat,final String latitude,final String longitude,
                                             final String location,final String date,final String male,final String female,final String others,
                                             final String mom,final String attendees,final String event,final String imagePath,final String id,
                                             final String email,final String datetime,final String otherEvent) {

        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"insertsocialmonitoring.jsp",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("kcr",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("status").equals("success"))
                            {

                                insertUserLog(email,getIpAddress(),logDate,"Social Monitoring","Data Insertion Successful");

                                if (dbHelper.deleteSocialMonitoringList(id)){
                                    Log.d("kcr","delete");

                                }else {

                                    Log.d("kcr","not deleted");
                                }

                            }else{

                                insertUserLog(email,getIpAddress(),logDate,"Social Monitoring","Data Insertion Unsuccessful");
                            }
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();

                params.put("state",state);
                params.put("district",district);
                params.put("block",block);
                params.put("grampanchayat",grampanchayat);
                params.put("location",location);
                params.put("date",date);
                params.put("numberofattendees",attendees);
                params.put("male",male);
                params.put("female",female);
                params.put("others",others);
                params.put("event",event);
                params.put("minutesofmeeting",mom);
                params.put("image",imagePath);
                params.put("latitude",latitude);
                params.put("longitude",longitude);
                params.put("email",email);
                params.put("datetime",datetime);
                params.put("otherevent",otherEvent);

                return params;
            }

        };
        requestQueue.add(stringRequest);
    }



    private void  insertWellinventoryImage(final String image,final String email,final String imagename){
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
                .setTimeout(60 * 60 * 1000)
                .setMultipartFile("img", "multipart/form-data", new File(image))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                                if (jsonObject.getString("status").equals("success"))
                                {

                                    wellinventoryServerPath = jsonObject.getString("path");

                                    insertUserLog(email,getIpAddress(),logDate,"Well Inventory","Image Upload "+imagename+" Successful");

                                        if (dbHelper!=null){
                                            if (dbHelper.getWellInventoryList().size()>0){
                                                ArrayList<WellInventoryModel> wellInventoryArrayList = dbHelper.getWellInventoryList();

                                                for (int i=0;i<wellInventoryArrayList.size();i++){

                                                    insertWellInventory(wellInventoryArrayList.get(i).getWellNo(),wellInventoryArrayList.get(i).getDate(),wellInventoryArrayList.get(i).getState(),
                                                            wellInventoryArrayList.get(i).getDistrict(),wellInventoryArrayList.get(i).getBlock(),wellInventoryArrayList.get(i).getGramPanchayat(),
                                                            wellInventoryArrayList.get(i).getVillage(),wellInventoryArrayList.get(i).getSiteName(),wellInventoryArrayList.get(i).getLocationDetails(),
                                                            wellInventoryArrayList.get(i).getOwnerName(),wellInventoryArrayList.get(i).getWellDiameter(),wellInventoryArrayList.get(i).getWellDepth(),
                                                            wellInventoryArrayList.get(i).getMeasuringPoint(),wellInventoryArrayList.get(i).getWellType(),wellInventoryArrayList.get(i).getWellWaterPotable(),
                                                            wellInventoryArrayList.get(i).getWaterLevelDepth(),wellInventoryArrayList.get(i).getUse(),wellInventoryArrayList.get(i).getPumpInstalled(),
                                                            wellInventoryArrayList.get(i).getElectricMeterSelecteditem(),wellInventoryArrayList.get(i).getDatetime(),wellInventoryArrayList.get(i).getEmail(),
                                                           wellinventoryServerPath,wellInventoryArrayList.get(i).getWellInId());

                                                }
                                            }

                                        }

                                } else {

                                    insertUserLog(email,getIpAddress(),logDate,"Well Inventory","Image Upload "+imagename+" Unsuccessful");

                                    Toast.makeText(getApplicationContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();


                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());

                            }





                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelse", e.toString());
                            }else {
                                Log.d("kcrelse", "failed");
                            }


                        }


                    }
                });
    }

    private void  insertImage(final String image,final String mom,final String email,final String imageName,final String momFileName){
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
                .setTimeout(60 * 60 * 1000)
                .setMultipartFile("img", "multipart/form-data", new File(image))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                                if (jsonObject.getString("status").equals("success"))
                                {

                                    serverImagePath = jsonObject.getString("path");


                                    insertUserLog(email,getIpAddress(),logDate,"Social Monitoring","Image Upload "+imageName+" Successful");

                                    if (!mom.equals("")){
                                        if (mom.contains(".pdf"))
                                        {
                                            insertPdfFileMOM(mom,momFileName,email);

                                        }else if (mom.contains(".docx"))
                                        {
                                            insertWordFileMOM(mom,momFileName,email);
                                        }

                                    }else {
                                        if (dbHelper!=null){
                                            if (dbHelper.getSocialMonitoringList().size()>0){
                                                ArrayList<SocialMonitoringModel> socialArrayList = dbHelper.getSocialMonitoringList();

                                                for (int i=0;i<socialArrayList.size();i++){

                                                    insertCommunityParticipation(socialArrayList.get(i).getState(),socialArrayList.get(i).getDistrict(),socialArrayList.get(i).getBlock(),socialArrayList.get(i).getGrampanchayat(),
                                                            socialArrayList.get(i).getLatitude(),socialArrayList.get(i).getLongitude(),socialArrayList.get(i).getLocation(),socialArrayList.get(i).getDate(),
                                                            socialArrayList.get(i).getMaleParticipants(),socialArrayList.get(i).getFemaleParticipants(),socialArrayList.get(i).getOtherParticipants(),serverFilePath1,
                                                            socialArrayList.get(i).getNoOfAttendees(),socialArrayList.get(i).getEvent(),serverImagePath,socialArrayList.get(i).getId(),socialArrayList.get(i).getEmail(),
                                                            socialArrayList.get(i).getDatetime(),socialArrayList.get(i).getOtherEvent());

                                                }
                                            }

                                        }

                                    }





                                } else {

                                    insertUserLog(email,getIpAddress(),logDate,"Social Monitoring","Image Upload "+imageName+" Unsuccessful");

                                    Toast.makeText(getApplicationContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();


                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());

                            }





                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelse", e.toString());
                            }else {
                                Log.d("kcrelse", "failed");
                            }


                        }


                    }
                });
    }

    private void insertPdfFileMOM(final String filepathMOM,final String momFileName,final String email)
    {
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"pdfupload.jsp")
                .setTimeout(60 * 60 * 1000)
                .setMultipartFile("application/pdf", "multipart/form-data", new File(filepathMOM))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>()
                {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                          if (jsonObject.getString("status").equals("success"))
                                {

                                    serverFilePath1 = jsonObject.getString("path");

                                    insertUserLog(email,getIpAddress(),logDate,"Social Monitoring","Pdf File Upload "+momFileName+" Successful");
                                    //Log.d("kcr",serverFilePathMom);

                                    if (dbHelper!=null){
                                        if (dbHelper.getSocialMonitoringList().size()>0){
                                            ArrayList<SocialMonitoringModel> socialArrayList = dbHelper.getSocialMonitoringList();

                                            for (int i=0;i<socialArrayList.size();i++){

                                                insertCommunityParticipation(socialArrayList.get(i).getState(),socialArrayList.get(i).getDistrict(),socialArrayList.get(i).getBlock(),socialArrayList.get(i).getGrampanchayat(),
                                                        socialArrayList.get(i).getLatitude(),socialArrayList.get(i).getLongitude(),socialArrayList.get(i).getLocation(),socialArrayList.get(i).getDate(),
                                                        socialArrayList.get(i).getMaleParticipants(),socialArrayList.get(i).getFemaleParticipants(),socialArrayList.get(i).getOtherParticipants(), serverFilePath1,
                                                        socialArrayList.get(i).getNoOfAttendees(),socialArrayList.get(i).getEvent(),serverImagePath,socialArrayList.get(i).getId(),socialArrayList.get(i).getEmail(),
                                                        socialArrayList.get(i).getDatetime(),socialArrayList.get(i).getOtherEvent());

                                            }
                                        }

                                    }


                                } else {

                                insertUserLog(email,getIpAddress(),logDate,"Social Monitoring","Pdf File Upload "+momFileName+" Unsuccessful");
                                    //Toast.makeText(getApplicationContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();


                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());

                            }





                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelsepdf", e.toString());
                            }else {
                                Log.d("kcrelsepdf", "failed");
                            }

                        }


                    }
                });
    }
    private void  insertWordFileMOM(final String filepath,final String momFileName,final String email){
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"wordupload.jsp")
                .setTimeout(60 * 60 * 1000)
                .setMultipartFile("application/pdf", "multipart/form-data", new File(filepath))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                                if (jsonObject.getString("status").equals("success")) {

                                    serverFilePath1 = jsonObject.getString("path");

                                    insertUserLog(email,getIpAddress(),logDate,"Social Monitoring","Word File Upload "+momFileName+" Successful");
                                    Log.d("kcr",serverFilePath1);

                                    if (dbHelper!=null){
                                        if (dbHelper.getSocialMonitoringList().size()>0){
                                            ArrayList<SocialMonitoringModel> socialArrayList = dbHelper.getSocialMonitoringList();

                                            for (int i=0;i<socialArrayList.size();i++){

                                                insertCommunityParticipation(socialArrayList.get(i).getState(),socialArrayList.get(i).getDistrict(),socialArrayList.get(i).getBlock(),socialArrayList.get(i).getGrampanchayat(),
                                                        socialArrayList.get(i).getLatitude(),socialArrayList.get(i).getLongitude(),socialArrayList.get(i).getLocation(),socialArrayList.get(i).getDate(),
                                                        socialArrayList.get(i).getMaleParticipants(),socialArrayList.get(i).getFemaleParticipants(),socialArrayList.get(i).getOtherParticipants(), serverFilePath1,
                                                        socialArrayList.get(i).getNoOfAttendees(),socialArrayList.get(i).getEvent(),serverImagePath,socialArrayList.get(i).getId(),socialArrayList.get(i).getEmail(),
                                                        socialArrayList.get(i).getDatetime(),socialArrayList.get(i).getOtherEvent());

                                            }
                                        }

                                    }

                                } else {

                                    insertUserLog(email,getIpAddress(),logDate,"Social Monitoring","Word File Upload "+momFileName+" Unsuccessful");
                                    //Toast.makeText(getApplicationContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();



                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());


                            }





                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelse", e.toString());
                            }else {
                                Log.d("kcrelse", "failed");
                            }
                        }


                    }
                });
    }

    public void insertNewWell(final String year, final String month,final String wellno,final String latitude,final String longitude,
                          final String state,final String district,final String block,final String grampanchayat,
                          final String sitename,final String  welltype,final String source,final String waterlevel,final String date,final String email,
                              final String image,final String id)
    {

        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"insertgroundwaterlevels.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("kcr",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("status").equals("success")) {

                                insertUserLog(email,getIpAddress(),logDate,"GWM Stations","Data Insertion Successful");

                                if (dbHelper.deleteNewWellList(id)){
                                    Log.d("kcr","delete");

                                }else {

                                    Log.d("kcr","not deleted");
                                }

                            }else {

                                insertUserLog(email,getIpAddress(),logDate,"GWM Stations","Data Insertion Unsuccessful");

                                Log.d("kcr","failed");
                            }
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.d("kcr",""+e.getLocalizedMessage());
                        }


                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d("kcr",""+error.getLocalizedMessage());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("year",year);
                params.put("month",month);
                params.put("wellno",wellno);
                params.put("latitude",latitude);
                params.put("longitude",longitude);
                params.put("state",state);
                params.put("district",district);
                params.put("block",block);
                params.put("grampanchayat",grampanchayat);
                params.put("sitename",sitename);
                params.put("typeofwell",welltype);
                params.put("source",source);
                params.put("groundwaterlevel",waterlevel);
                params.put("datetime",date);
                params.put("email",email);
                params.put("image",image);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void  insertImage1(final String imagePath1,final String email,final String imagename1,final String imagename2){
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
                .setTimeout(60 * 60 * 1000)

                .setMultipartFile("img", "multipart/form-data", new File(imagePath1))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                                if (jsonObject.getString("status").equals("success")) {

                                    serverPath1 = jsonObject.getString("path");

                                    insertUserLog(email,getIpAddress(),logDate,"Geo Tagging Artificial Recharge Structure","Image Upload "+imagename1+" Successful");

                                    Log.d("kcr",serverPath1);
                                    if (dbHelper.getGeoTaggingList().size()>0) {

                                        ArrayList<GeotaggingModel> geoTaggingArrayList = dbHelper.getGeoTaggingList();

                                        for (int i = 0; i < geoTaggingArrayList.size(); i++) {
                                            if (!geoTaggingArrayList.get(i).getImage2().equals("")) {

                                                insertImage2(geoTaggingArrayList.get(i).getImage2(),geoTaggingArrayList.get(i).getEmail(),geoTaggingArrayList.get(i).getImageName2());

                                            }else {
                                                insertGeoTagging(geoTaggingArrayList.get(i).getState(), geoTaggingArrayList.get(i).getDistrict(), geoTaggingArrayList.get(i).getBlock(),
                                                        geoTaggingArrayList.get(i).getGramPanchayat(),geoTaggingArrayList.get(i).getVillagename(),geoTaggingArrayList.get(i).getSitename(),
                                                        geoTaggingArrayList.get(i).getLatitude(), geoTaggingArrayList.get(i).getLongitude(),
                                                        geoTaggingArrayList.get(i).getTypeofStructure(), geoTaggingArrayList.get(i).getStorageCapacity(), geoTaggingArrayList.get(i).getNoOfFillings(),
                                                        geoTaggingArrayList.get(i).getgId(), serverPath1, serverPath2,geoTaggingArrayList.get(i).getOthers(),
                                                        geoTaggingArrayList.get(i).getDate(),geoTaggingArrayList.get(i).getEmail());
                                            }
                                        }
                                    }


                                } else {

                                    insertUserLog(email,getIpAddress(),logDate,"Geo Tagging Artificial Recharge Structure","Image Upload "+imagename1+" Unsuccessful");

                                    Toast.makeText(getApplicationContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();


                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());

                            }


                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelse", e.toString());
                            }else {
                                Log.d("kcrelse", "failed");
                            }
                        }


                    }
                });
    }

    private void  insertImage2(final String imagePath2,final String email,final String imagename2){
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
                .setTimeout(60 * 60 * 1000)

                .setMultipartFile("img", "multipart/form-data", new File(imagePath2))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                                if (jsonObject.getString("status").equals("success")) {

                                    serverPath2 = jsonObject.getString("path");

                                    insertUserLog(email,getIpAddress(),logDate,"Geo Tagging Artificial Recharge Structure","Image Upload "+imagename2+" Successful");

                                    Log.d("kcr",serverPath2);
                                    if (dbHelper!=null){
                                        if (dbHelper.getGeoTaggingList().size()>0) {

                                            ArrayList<GeotaggingModel> geoTaggingArrayList = dbHelper.getGeoTaggingList();

                                            for (int i = 0; i < geoTaggingArrayList.size(); i++) {

                                                insertGeoTagging(geoTaggingArrayList.get(i).getState(), geoTaggingArrayList.get(i).getDistrict(), geoTaggingArrayList.get(i).getBlock(),
                                                        geoTaggingArrayList.get(i).getGramPanchayat(),geoTaggingArrayList.get(i).getVillagename(),geoTaggingArrayList.get(i).getSitename(),
                                                        geoTaggingArrayList.get(i).getLatitude(), geoTaggingArrayList.get(i).getLongitude(),
                                                        geoTaggingArrayList.get(i).getTypeofStructure(), geoTaggingArrayList.get(i).getStorageCapacity(), geoTaggingArrayList.get(i).getNoOfFillings(),
                                                        geoTaggingArrayList.get(i).getgId(), serverPath1, serverPath2,geoTaggingArrayList.get(i).getOthers(),
                                                        geoTaggingArrayList.get(i).getDate(),geoTaggingArrayList.get(i).getEmail());

                                            }
                                        }
                                    }

                                } else {

                                    insertUserLog(email,getIpAddress(),logDate,"Geo Tagging Artificial Recharge Structure","Image Upload "+imagename2+" Unsuccessful");
                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());
                            }


                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelse", e.toString());
                            }else {
                                Log.d("kcrelse", "failed");
                            }
                        }


                    }
                });
    }

    private void  insertRainfallImage1(final String imagePath1,final String imagePath2,final String email,final String imageName1,final String imageName2){
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
                .setTimeout(60 * 60 * 1000)

                .setMultipartFile("img", "multipart/form-data", new File(imagePath1))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                                if (jsonObject.getString("status").equals("success")) {

                                    rainfallServerPath1 = jsonObject.getString("path");

                                    insertUserLog(email,getIpAddress(),logDate,"Rainfall","Image Upload "+imageName1+" Successful");

                                    Log.d("kcr",rainfallServerPath1);
                                    insertRainfallImage2(imagePath2,email,imageName2);

                                } else {
                                    Log.d("kcr",jsonObject.getString("status"));

                                    insertUserLog(email,getIpAddress(),logDate,"Rainfall","Image Upload "+imageName1+" Unsuccessful");

                                    //Toast.makeText(getApplicationContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();


                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());

                            }


                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelse", e.toString());
                            }else {
                                Log.d("kcrelse", "failed");
                            }
                        }


                    }
                });
    }

    private void  insertRainfallImage2(final String imagePath2,final String email,final String imageName2){
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
                .setTimeout(60 * 60 * 1000)

                .setMultipartFile("img", "multipart/form-data", new File(imagePath2))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                                if (jsonObject.getString("status").equals("success")) {

                                    rainfallServerPath2 = jsonObject.getString("path");

                                    insertUserLog(email,getIpAddress(),logDate,"Rainfall","Image Upload "+imageName2+" Successful");

                                    Log.d("kcr",rainfallServerPath2);
                                    if (dbHelper!=null){

                                        if (dbHelper.getRainfallList().size()>0) {

                                            ArrayList<RainfallModel> rainfallArrayList = dbHelper.getRainfallList();

                                            for (int i = 0; i < rainfallArrayList.size(); i++) {

                                                insertRainfall(rainfallArrayList.get(i).getState(),rainfallArrayList.get(i).getDistrict(),rainfallArrayList.get(i).getBlock(),
                                                        rainfallArrayList.get(i).getGramPanchayat(),rainfallArrayList.get(i).getSitename(),rainfallArrayList.get(i).getRainfall(),
                                                        rainfallArrayList.get(i).getLatitude(),rainfallArrayList.get(i).getLongitude(),rainfallArrayList.get(i).getrId(),
                                                        rainfallArrayList.get(i).getRainGauge(),rainfallArrayList.get(i).getDate(),rainfallServerPath1,rainfallServerPath2,
                                                        rainfallArrayList.get(i).getEmail());
                                            }
                                        }

                                    }

                                } else {
                                    insertUserLog(email,getIpAddress(),logDate,"Rainfall","Image Upload "+imageName2+" Unsuccessful");
                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());
                            }


                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelse", e.toString());
                            }else {
                                Log.d("kcrelse", "failed");
                            }
                        }


                    }
                });
    }

    private void  insertNewWellImage(final String image,final String email,final String imagename){
        Ion.with(getApplicationContext())
                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
                .setTimeout(60 * 60 * 1000)
                .setMultipartFile("img", "multipart/form-data", new File(image))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {

                        if(result!=null) {

                            Log.d("kcr1234", result.getResult());


                            try {

                                JSONObject jsonObject = new JSONObject(result.getResult());

                                if (jsonObject.getString("status").equals("success"))
                                {

                                    newWellServerPath = jsonObject.getString("path");

                                    insertUserLog(email,getIpAddress(),logDate,"GWM Stations","Image Upload "+imagename+" Successful");

                                    if (dbHelper.getNewWellList().size()>0){

                                        ArrayList<NewWellModel> newWellList = dbHelper.getNewWellList();

                                        for (int i=0;i<newWellList.size();i++){

                                            if (newWellList.get(i).getImage().equals("")) {
                                                insertNewWell(newWellList.get(i).getYear(), newWellList.get(i).getMonth(), newWellList.get(i).getWellNo(),
                                                        newWellList.get(i).getLatitude(), newWellList.get(i).getLongitude(), newWellList.get(i).getState(),
                                                        newWellList.get(i).getDistrict(), newWellList.get(i).getBlock(), newWellList.get(i).getGramPanchayat(),
                                                        newWellList.get(i).getVillageName(), newWellList.get(i).getWellType(), newWellList.get(i).getSource(),
                                                        newWellList.get(i).getWaterLevel(), newWellList.get(i).getDatetime(), newWellList.get(i).getEmail(),
                                                        newWellServerPath,newWellList.get(i).getWellId());
                                            }

                                        }
                                    }

                                } else {

                                    insertUserLog(email,getIpAddress(),logDate,"GWM Stations","Image Upload "+imagename+" Unsuccessful");

                                    Toast.makeText(getApplicationContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();


                                }

                            } catch (JSONException ex) {
                                Log.d("kcr", "error" + ex.toString());

                            }





                        }
                        else {

                            if(e!=null) {
                                Log.d("kcrelse", e.toString());
                            }else {
                                Log.d("kcrelse", "failed");
                            }


                        }


                    }
                });
    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);




                return params;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }
}

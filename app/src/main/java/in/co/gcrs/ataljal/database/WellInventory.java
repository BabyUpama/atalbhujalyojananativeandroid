package in.co.gcrs.ataljal.database;

public class WellInventory {

    public static final String wellInId = "id";
    public static final String wellNo = "wellno";
    public static final String date = "date";
    public static final String state = "state";
    public static final String district = "district";
    public static final String block = "block";
    public static final String gramPanchayat = "grampanchayat";
    public static final String village = "village";
    public static final String siteName = "sitename";
    public static final String locationDetails = "locationdetails";
    public static final String ownerName = "ownername";
    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String wellDiameter = "welldiameter";
    public static final String wellDepth = "welldepth";
    public static final String measuringPoint = "measuringpoint";
    public static final String wellType = "welltype";
    public static final String wellWaterPotable = "wellwaterpotable";
    public static final String waterLevelDepth = "waterleveldepth";
    public static final String use = "use";
    public static final String pumpInstalled = "pumpinstalled";
    public static final String electricMeterSelecteditem = "electricmeterselecteditem";
    public static final String widate = "datetime";
    public static final String wiemail = "email";
    public static final String wiimage = "image";
    public static final String wiimagename = "imagename";

}

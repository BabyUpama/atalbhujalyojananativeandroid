package in.co.gcrs.ataljal.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.AuthFailureError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.adapters.WellInventoryAdapter;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.model.WellInventoryModel;


public class WellList extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    RequestQueue requestQueue;
    private MyAppPrefsManager myAppPrefsManager;
    private ArrayList<WellInventoryModel> arrayList = new ArrayList<>();
    private WellInventoryModel wellInventoryModel;
    private WellInventoryAdapter wellInventoryAdapter;
    private TextView text;
    private double currentLatitude,currentLongitude,latitude1,longitude1;

    public WellList() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_well_list, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionWellInventory));

        myAppPrefsManager = new MyAppPrefsManager(getContext());
        requestQueue = Volley.newRequestQueue(getContext());
        progressBar =view.findViewById(R.id.progressbar);
        text = view.findViewById(R.id.text);

        recyclerView = view.findViewById(R.id.recyclerViewWell);
        wellInventoryAdapter = new WellInventoryAdapter(getContext(),arrayList);


        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (isNetworkAvailable()){
            if (!progressBar.isShown()) {
                progressBar.setVisibility(View.VISIBLE);
            }
            getData();
        }else {
            Toast.makeText(getContext(), "Internet not available..!", Toast.LENGTH_SHORT).show();
            if (progressBar.isShown()) {
                progressBar.setVisibility(View.GONE);
            }

            text.setVisibility(View.VISIBLE);
        }

        return view;
    }
    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    protected void  getData() {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getwellinventory.jsp", new Response.Listener<String>() {

            @Override
            public void onResponse(String response)
            {
                Log.d("kcrwellinven",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    arrayList.clear();
                    wellInventoryAdapter.notifyDataSetChanged();
                    if (jsonArray.length()>0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);

                                wellInventoryModel = new WellInventoryModel();
                                wellInventoryModel.setWellInId(heroObject.getString("id"));
                                wellInventoryModel.setDate(heroObject.getString("date"));
                                wellInventoryModel.setWellNo(heroObject.getString("wellno"));
                                wellInventoryModel.setOwnerName(heroObject.getString("ownername"));
                                wellInventoryModel.setUse(heroObject.getString("use"));
                                wellInventoryModel.setGramPanchayat(heroObject.getString("gp"));
                                wellInventoryModel.setLocationDetails(heroObject.getString("locationdetails"));
                                wellInventoryModel.setPumpInstalled(heroObject.getString("pumpinstalled"));
                                wellInventoryModel.setElectricMeterSelecteditem(heroObject.getString("electricmeterselecteditem"));
                                wellInventoryModel.setMeasuringPoint(heroObject.getString("measuringpoint"));
                                wellInventoryModel.setWellWaterPotable(heroObject.getString("wellwaterpotable"));
                                wellInventoryModel.setWellDepth(heroObject.getString("welldepth"));
                                wellInventoryModel.setDistrict(heroObject.getString("district"));
                                wellInventoryModel.setSiteName(heroObject.getString("sitename"));
                                wellInventoryModel.setWellDiameter(heroObject.getString("welldiameter"));
                                wellInventoryModel.setBlock(heroObject.getString("block"));
                                try {
                                    wellInventoryModel.setWaterLevelDepth(heroObject.getString("waterleveldepth"));
                                }catch(Exception e){

                                }
                                wellInventoryModel.setState(heroObject.getString("state"));
                                wellInventoryModel.setVillage(heroObject.getString("village"));
                                wellInventoryModel.setWellType(heroObject.getString("welltype"));
                                wellInventoryModel.setLatitude(heroObject.getString("latitude"));
                                wellInventoryModel.setLongitude(heroObject.getString("longitude"));
                                wellInventoryModel.setDatetime(heroObject.getString("datetime"));
                                wellInventoryModel.setImage(heroObject.getString("image"));

                            arrayList.add(wellInventoryModel);

                            if (progressBar.isShown()) {
                                progressBar.setVisibility(View.GONE);
                            }

                        }

                        wellInventoryAdapter = new WellInventoryAdapter(getContext(),arrayList);
                        recyclerView.setAdapter(wellInventoryAdapter);

                    }else {
                        Toast.makeText(getContext(), "The data not available for Well Inventory", Toast.LENGTH_SHORT).show();
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                        text.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();

                params.put("state",myAppPrefsManager.getState());
                params.put("district",myAppPrefsManager.getDistrict());
                params.put("block",myAppPrefsManager.getBlock());
                params.put("gp",myAppPrefsManager.getGrampanchayat());
                Log.e("params list",params.toString());
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}
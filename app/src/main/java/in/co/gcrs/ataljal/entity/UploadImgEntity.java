package in.co.gcrs.ataljal.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UploadImgEntity implements Serializable {

    @SerializedName("status")
    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package in.co.gcrs.ataljal.myviewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import in.co.gcrs.ataljal.api.Repository;
import in.co.gcrs.ataljal.api.Resource;
import in.co.gcrs.ataljal.entity.UploadImgEntity;
import okhttp3.MultipartBody;

public class UploadImgViewModel extends ViewModel {
    private LiveData<Resource<UploadImgEntity>> saveDefectsWithFileResponse;
    private LiveData<Resource<UploadImgEntity>> saveDefectsPdfWithFileResponse;
    private LiveData<Resource<UploadImgEntity>> saveDefectsWordWithFileResponse;
    public LiveData<Resource<UploadImgEntity>> saveImageFileApi(MultipartBody.Part imagePart) {
        saveDefectsWithFileResponse = Repository.saveImageFileApi(imagePart);
        return saveDefectsWithFileResponse;
    }

    public LiveData<Resource<UploadImgEntity>> savePdfFileApi(MultipartBody.Part imagePart) {
        saveDefectsPdfWithFileResponse = Repository.savePdfFileApi(imagePart);
        return saveDefectsPdfWithFileResponse;
    }

    public LiveData<Resource<UploadImgEntity>> saveWordFileApi(MultipartBody.Part imagePart) {
        saveDefectsWordWithFileResponse = Repository.saveWordFileApi(imagePart);
        return saveDefectsWordWithFileResponse;
    }
}

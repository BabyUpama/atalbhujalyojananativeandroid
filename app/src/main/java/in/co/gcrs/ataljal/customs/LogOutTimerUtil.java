package in.co.gcrs.ataljal.customs;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class LogOutTimerUtil
{
    public interface LogOutListener {
        void doLogout();
    }

    static Timer longTimer;
    //static final int LOGOUT_TIME = 1800000; // 30 min
    static final int LOGOUT_TIME = 60000; // 1 min= 60 *1000 ms

    private static int mNumOfActivitiesInOnStarttoOnStopLifeCycle=0;

    public static synchronized void startLogoutTimer(final Context context, final LogOutListener logOutListener) {
        if (longTimer != null) {
            longTimer.cancel();
            longTimer = null;
        }
        if (longTimer == null) {

            longTimer = new Timer();

            longTimer.schedule(new TimerTask() {

                public void run() {

                    cancel();

                    longTimer = null;

                    try {
                        boolean foreGround = new ForegroundCheckTask().execute(context).get();

                        boolean background=new BackgroundCheckTask().execute(context).get();

                        if (foreGround)
                        {
                            Log.d("kcr","foreground");
                            logOutListener.doLogout();
                        }
                        else if(background)
                        {
                            Log.d("kcr","background");
                            logOutListener.doLogout();
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                }
            }, LOGOUT_TIME);
        }
    }

    public static synchronized void stopLogoutTimer(final Context context, final LogOutListener logOutListener) {
        if (longTimer != null) {
            longTimer.cancel();
            longTimer = null;
            startLogoutTimer(context,logOutListener);
        }
    }

    static class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(Context... params)
        {
            final Context context = params[0].getApplicationContext();
            return isAppOnForeground(context);
        }

        private boolean isAppOnForeground(Context context)
        {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null)
            {
                return false;
            }
            final String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName))
                {
                    Log.d("kcr","OnForeground");
                    return true;
                }
                else if(appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND && appProcess.processName.equals(packageName))
                {
                    Log.d("kcr","Background");
                }
            }
            return false;
        }
    }

    static class BackgroundCheckTask extends AsyncTask<Context, Void, Boolean>
    {

        @Override
        protected Boolean doInBackground(Context... params)
        {
            final Context context = params[0].getApplicationContext();
            return isAppOnBackground();
        }
        public static boolean isAppOnBackground()
        {
            //Log.d("kcr","num->"+mNumOfActivitiesInOnStarttoOnStopLifeCycle+"\tisOnBackground->"+(mNumOfActivitiesInOnStarttoOnStopLifeCycle==0));
            return mNumOfActivitiesInOnStarttoOnStopLifeCycle==0;
        }

        /*private boolean isAppOnBackground(Context context)
        {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null)
            {
                return false;
            }
            final String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName))
                {
                    Log.d("kcr","Foreground");

                }
                else if(appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND && appProcess.processName.equals(packageName))
                {
                    Log.d("kcr","OnBackground");
                    return true;
                }

                Log.d("kcr",""+appProcess.importance);
            }

            return false;
        }*/
    }
}
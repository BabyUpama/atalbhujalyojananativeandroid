package in.co.gcrs.ataljal.database;

public class GpProfile {

    public static final String gpId="id";
    public static final String state="state";
    public static final String district="district";
    public static final String block="block";
    public static final String gramPanchayat="grampanchayat";
    public static final String latitude="latitude";
    public static final String longitude="longitude";

    public static final String totalPopulation="totalpopulation";
    public static final String malePopulation="malepopulation";
    public static final String femalePopulation="femalepopulation";
    public static final String childPopulation="childpopulation";

    /*Other Categories*/

    public static final String apl="apl";
    public static final String bpl="bpl";
    public static final String sc="sc";
    public static final String st="st";
    public static final String obc="obc";

    /*HouseHolds*/

    public static final String households="households";

    /*Live Stock Population*/

    public static final String livePopulation="livepopulation";
    public static final String cows="cows";
    public static final String bullocks="bullocks";
    public static final String buffalos="buffalos";
    public static final String sheepsGoats="sheepsGoats";
    public static final String liveStockOthers="liveStockOthers";

    /*Gp Area*/
    public static final String gpArea="gparea";
    public static final String forestArea="forestarea";
    public static final String fallowLand="fallowland";
    public static final String cultivableLand="cultivableland";
    public static final String netSownArea="netsownarea";

    /*area under cultivation*/
    public static final String irrigatedAreaKharif="irrigatedareakharif";
    public static final String irrigatedAreaRabi="irrigatedarearabi";
    public static final String irrigatedAreaSummer="irrigatedareasummer";
    public static final String nonIrrigatedArea="nonirrigatedarea";

    /*Irrigation Type*/
    public static final String canal="canal";
    public static final String tanks="tanks";
    public static final String dugwells="dugwells";
    public static final String dugwellsBorewells="dugwellsborewells";
    public static final String borewellTubewell="borewelltubewell";

    /*Existing Well*/

    public static final String dugwell="dugwell";
    public static final String borewell="borewell";
    public static final String tubewell="tubewell";
    public static final String dugcumBorewell="dugcumborewell";
    public static final String existingWellOthers="existingwellothers";

    /*water lifting sources*/

    public static final String nonEnergised="nonenergised";
    public static final String dieselPump="dieselpump";
    public static final String electricPump="electricpump";
    public static final String centrifugalPump="centrifugalpump";
    public static final String submersiblePump="submersiblepump";

    /*water saving devices*/

    public static final String sprinklerIrrigation="sprinklerirrigation";
    public static final String dripIrrigation="dripirrigation";
    public static final String waterSavingDevicesOthers="watersavingdevicesothers";

    public static final String wellsWithElectricMeter="wellswithelectricmeter";
    public static final String wellWithWaterMeter="wellswithwatermeter";
}

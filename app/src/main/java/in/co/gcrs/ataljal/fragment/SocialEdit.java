package in.co.gcrs.ataljal.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.adapters.SocialMonitoringAdapter;
import in.co.gcrs.ataljal.adapters.WellInventoryAdapter;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.model.SocialMonitoringModel;
import in.co.gcrs.ataljal.model.WellInventoryModel;

public class SocialEdit extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    RequestQueue requestQueue;
    private MyAppPrefsManager myAppPrefsManager;
    private ArrayList<SocialMonitoringModel> arrayList;
    private SocialMonitoringModel socialMonitoringModel;
    private SocialMonitoringAdapter socialMonitoringAdapter;
    private TextView text;

    public SocialEdit() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_social_edit, container, false);

        myAppPrefsManager = new MyAppPrefsManager(getContext());
        requestQueue = Volley.newRequestQueue(getContext());
        progressBar =view.findViewById(R.id.progressbar);
        text = view.findViewById(R.id.text);
        arrayList = new ArrayList<>();

        recyclerView = view.findViewById(R.id.recyclerViewEditSocialMonitoring);
        socialMonitoringAdapter = new SocialMonitoringAdapter(getContext(),arrayList);
        recyclerView.setHasFixedSize(true);

        /*LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter( socialMonitoringAdapter );
        recyclerView.setHasFixedSize(true);*/

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(socialMonitoringAdapter);

        if (isNetworkAvailable()){
            if (!progressBar.isShown()) {
                progressBar.setVisibility(View.VISIBLE);
            }
            getData();
        }else {
            Toast.makeText(getContext(), "Internet not available..!", Toast.LENGTH_SHORT).show();
            if (progressBar.isShown()) {
                progressBar.setVisibility(View.GONE);
            }

            text.setVisibility(View.VISIBLE);
        }

        return view;
    }
    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    protected void  getData() {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getsocialmonitoring.jsp", new Response.Listener<String>() {

            @Override
            public void onResponse(String response)
            {
                Log.d("kcr",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    arrayList.clear();
                    socialMonitoringAdapter.notifyDataSetChanged();

                    if (jsonArray.length()>0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);

                            socialMonitoringModel = new SocialMonitoringModel();

                            socialMonitoringModel.setDate(heroObject.getString("date"));
                            socialMonitoringModel.setImagePath(heroObject.getString("image"));
                            socialMonitoringModel.setNoOfAttendees(heroObject.getString("numberofattendees"));
                            socialMonitoringModel.setMom(heroObject.getString("minutesofmeeting"));
                            socialMonitoringModel.setGrampanchayat(heroObject.getString("grampanchayat"));
                            socialMonitoringModel.setDistrict(heroObject.getString("district"));
                            socialMonitoringModel.setBlock(heroObject.getString("block"));
                            socialMonitoringModel.setLocation(heroObject.getString("location"));
                            socialMonitoringModel.setState(heroObject.getString("state"));
                            socialMonitoringModel.setId(heroObject.getString("id"));
                            socialMonitoringModel.setFemaleParticipants(heroObject.getString("female"));
                            socialMonitoringModel.setEvent(heroObject.getString("event"));
                            socialMonitoringModel.setMaleParticipants(heroObject.getString("male"));
                            socialMonitoringModel.setOtherParticipants(heroObject.getString("others"));
                            socialMonitoringModel.setEmail(heroObject.getString("email"));
                            socialMonitoringModel.setMomFilename(heroObject.getString("mom_filename"));
                            socialMonitoringModel.setImageFilename(heroObject.getString("photograph_filename"));
                            socialMonitoringModel.setDatetime(heroObject.getString("datetime"));
                            socialMonitoringModel.setOtherEvent(heroObject.getString("otherevent"));

                            arrayList.add(socialMonitoringModel);

                            if (progressBar.isShown()) {
                                progressBar.setVisibility(View.GONE);
                            }

                        }

                        socialMonitoringAdapter = new SocialMonitoringAdapter(getContext(),arrayList);
                        recyclerView.setAdapter(socialMonitoringAdapter);


                    }else {
                        Toast.makeText(getContext(), "The data not available for social monitoring", Toast.LENGTH_SHORT).show();
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                        text.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
                Log.d("kcrerror",""+error.getLocalizedMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();

                params.put("state",myAppPrefsManager.getState());
                params.put("district",myAppPrefsManager.getDistrict());
                params.put("block",myAppPrefsManager.getBlock());
                params.put("grampanchayat",myAppPrefsManager.getGrampanchayat());

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
package in.co.gcrs.ataljal.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;


import in.co.gcrs.ataljal.app.MyAppPrefsManager;


public class DashBoardFragment extends Fragment
{

    private CardView fieldDataCollection,rainFallGaugeStation,socialMonitoring,contactUs;
    String statename,districtname,blockname,panchayatname,villagename;

    public static final int MULTIPLE_PERMISSIONS = 10;

    String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    Fragment fragment;
    FragmentManager fragmentManager;


    MyAppPrefsManager myAppPrefsManager;

    RequestQueue requestQueue;


    private Dialog dialog;


    Spinner spinnerState , spinnerDistrict,spinnerBlock,spinnerGramPanchayat;

    private ArrayList<String> stateArrayList,districtArrayList,blockArrayList,gramPanchayatArrayList;



    String category="";

    LinearLayout stateLinear,districtLinear,blockLinear,grampanchayatLinear;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View v= inflater.inflate(R.layout.fragment_dashboard, container, false);

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.app_name));



        stateArrayList = new ArrayList<>();
        districtArrayList = new ArrayList<>();
        blockArrayList = new ArrayList<>();


        if(checkPermissions()){

        }


        fragmentManager = getActivity().getSupportFragmentManager();


        myAppPrefsManager = new MyAppPrefsManager(getContext());

        requestQueue = Volley.newRequestQueue(getContext());
        fieldDataCollection = v.findViewById(R.id.fieldDataCollection);
        rainFallGaugeStation = v.findViewById(R.id.rainFallGaugeStation);
        socialMonitoring = v.findViewById(R.id.socialMonitoring);
        contactUs = v.findViewById(R.id.contactUs);

        fieldDataCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!myAppPrefsManager.getState().equals("")&&!myAppPrefsManager.getDistrict().equals("")&&
                !myAppPrefsManager.getBlock().equals("")&&!myAppPrefsManager.getGrampanchayat().equals("")){
                    fragment =new FieldDataCollection();
                    fragmentManager.beginTransaction()
                            .replace(R.id.main_fragment,fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .addToBackStack("Home")
                            .commit();
                }else{
                    Toast.makeText(getContext(), "Select State/ District/ Block/ Gram Panchayat", Toast.LENGTH_SHORT).show();
                }

            }
        });

        rainFallGaugeStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!myAppPrefsManager.getState().equals("")&&!myAppPrefsManager.getDistrict().equals("")&&
                        !myAppPrefsManager.getBlock().equals("")&&!myAppPrefsManager.getGrampanchayat().equals("")){
                fragment = new RainFallMain();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("Home")
                        .commit();
                }else{
                    Toast.makeText(getContext(), "Select State/ District/ Block/ Gram Panchayat", Toast.LENGTH_SHORT).show();
                }
            }
        });

        socialMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!myAppPrefsManager.getState().equals("")&&!myAppPrefsManager.getDistrict().equals("")&&
                        !myAppPrefsManager.getBlock().equals("")&&!myAppPrefsManager.getGrampanchayat().equals("")){
                fragment = new SocialMonitoringDashBoard();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("Home")
                        .commit();
            }else{
                Toast.makeText(getContext(), "Select State/ District/ Block/ Gram Panchayat", Toast.LENGTH_SHORT).show();
            }
            }
        });

        contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new ContactUs();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("Home")
                        .commit();
            }
        });

        return v;
    }




    private void getStates(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getstateslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            stateArrayList.clear();
                            stateArrayList.add("Select State");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                stateArrayList.add(jsonObject.getString("state_name"));
                            }


                            ArrayAdapter statesArrayAdapter = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,stateArrayList);
                            statesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerState.setAdapter(statesArrayAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();

                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("encpassword","this is a password");
                Log.e("dashboard1",params.toString());
                return params;
            }

        };

        requestQueue.add(stringRequest);

    }

    private void getDistricts(final  String stateName){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getdistrictslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            districtArrayList.clear();
                            districtArrayList.add("Select District");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);



                                districtArrayList.add(jsonObject.getString("district_name"));


                            }


                            ArrayAdapter disctrictArrayAdapter = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,districtArrayList);
                            disctrictArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerDistrict.setAdapter(disctrictArrayAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();

                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("state_name",stateName);
                Log.e("dashboard1",params.toString());
                return params;
            }

        };

        requestQueue.add(stringRequest);

    }

    private void getBlocks(final String statename,final String districtName){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getblockslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            blockArrayList.clear();
                            blockArrayList.add("Select Block");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);


                                blockArrayList.add(jsonObject.getString("block"));


                            }


                            ArrayAdapter blockArrayAdapter = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,blockArrayList);
                            blockArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerBlock.setAdapter(blockArrayAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();

                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("state_name",statename);
                params.put("district_name",districtName);
                Log.e("dashboard1",params.toString());
                return params;
            }

        };

        requestQueue.add(stringRequest);

    }

    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(getActivity(),p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS:{
                if (grantResults.length > 0) {
                    String permissionsDenied = "";
                    for (String per : permissionsList) {
                        if(grantResults[0] == PackageManager.PERMISSION_DENIED){
                            permissionsDenied += "\n" + per;

                        }

                    }
                    // Show permissionsDenied

                }
                return;
            }
        }
    }




    public  void showUserDetails(){

        dialog = new Dialog(getContext());
        //    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.ground_water_levels_filter);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);


        stateLinear = dialog.findViewById(R.id.stateLinear);
        districtLinear = dialog.findViewById(R.id.districtLinear);
        blockLinear = dialog.findViewById(R.id.blockLinear);
        grampanchayatLinear = dialog.findViewById(R.id.panchayatLinear);



        spinnerState = dialog.findViewById(R.id.spinneerState);
        spinnerDistrict = dialog.findViewById(R.id.spinnerDistrict);
        spinnerBlock = dialog.findViewById(R.id.spinnerBlock);
        spinnerGramPanchayat = dialog.findViewById(R.id.spinnerGrampanchayat);



        getStates();

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position!=0){

                    statename = spinnerState.getSelectedItem().toString();
                    myAppPrefsManager.setState(spinnerState.getSelectedItem().toString());
                    getDistricts(spinnerState.getSelectedItem().toString());



                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position!=0){

                    districtname = spinnerDistrict.getSelectedItem().toString();
                    if(!spinnerDistrict.getSelectedItem().toString().equals("Select District")){
                        districtname = spinnerDistrict.getSelectedItem().toString();
                        myAppPrefsManager.setDistrict(spinnerDistrict.getSelectedItem().toString());
                        getBlocks(spinnerDistrict.getSelectedItem().toString());


                    }



                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerBlock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!=0){
                    blockname = spinnerBlock.getSelectedItem().toString();
                    if (!spinnerBlock.getSelectedItem().toString().equals("Select Block")){
                        blockname = spinnerBlock.getSelectedItem().toString();
                        myAppPrefsManager.setBlock(spinnerBlock.getSelectedItem().toString());
                        getGramPanchayat(blockname);

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerGramPanchayat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!=0){
                    /*grampanchayat = spinnerGramPanchayat.getSelectedItem().toString();
                    myAppPrefsManager.setGrampanchayat(grampanchayat);*/

                    myAppPrefsManager.setGrampanchayat( spinnerGramPanchayat.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        Button submit= dialog.findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(dialog!=null){
                    if(dialog.isShowing()){
                        dialog.dismiss();
                    }
                }


                if (!spinnerState.getSelectedItem().toString().equals("Select State")){

                    category="state";
                    myAppPrefsManager.setCategory(category);


                    if (!spinnerDistrict.getSelectedItem().toString().equals("Select District")){
                        category="district";
                        myAppPrefsManager.setCategory(category);


                        if (!spinnerBlock.getSelectedItem().toString().equals("Select Block")) {

                            category="block";
                            myAppPrefsManager.setCategory(category);


                            if (!spinnerGramPanchayat.getSelectedItem().toString().equals("Select Gram Panchayat")) {

                                category="grampanchayat";
                                myAppPrefsManager.setCategory(category);





                            }else {

                                category="block";
                                myAppPrefsManager.setCategory(category);


                            }
                        }else {
                            category="district";
                            myAppPrefsManager.setCategory(category);


                        }
                    }else {

                        category="state";
                        myAppPrefsManager.setCategory(category);


                    }

                }else {
                    Toast.makeText(getContext(), "Please Select State", Toast.LENGTH_SHORT).show();
                }

            }
        });


        if(dialog!=null){
            dialog.show();

        }
    }


    private void getGramPanchayat(final String block){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getgrampanchayatlist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            gramPanchayatArrayList.clear();
                            gramPanchayatArrayList.add("Select Gram Panchayat");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                gramPanchayatArrayList.add(jsonObject.getString("gname"));

                            }


                            ArrayAdapter gramPanchayatArrayAdapter = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,gramPanchayatArrayList);
                            gramPanchayatArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerGramPanchayat.setAdapter(gramPanchayatArrayAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",statename);
                params.put("district_name",districtname);
                params.put("block",block);
                Log.e("dashboard1",params.toString());
                return params;
            }

        };

        requestQueue.add(stringRequest);

    }


    private void getBlocks(final String district_name){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getblockslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            blockArrayList.clear();

                            blockArrayList.add("Select Block");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);



                                blockArrayList.add(jsonObject.getString("block"));



                            }


                            ArrayAdapter blockArrayAdapter = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,blockArrayList);
                            blockArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerBlock.setAdapter(blockArrayAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",statename);

                params.put("district_name",district_name);
                Log.e("dashboard1",params.toString());
                return params;
            }

        };

        requestQueue.add(stringRequest);

    }

}




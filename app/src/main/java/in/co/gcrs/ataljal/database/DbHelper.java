package in.co.gcrs.ataljal.database;

import android.content.ContentValues;
import android.content.Context;


import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import android.database.Cursor;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.model.CredentialModel;
import in.co.gcrs.ataljal.model.SocialMonitoringModel;
import in.co.gcrs.ataljal.model.GeotaggingModel;
import in.co.gcrs.ataljal.model.GpProfileModel;

import in.co.gcrs.ataljal.model.NewWellModel;

import in.co.gcrs.ataljal.model.RainfallModel;
import in.co.gcrs.ataljal.model.SarpanchDetailsModel;

import in.co.gcrs.ataljal.model.WellInventoryModel;

import static in.co.gcrs.ataljal.database.GeoTagging.calenderdate;
import static in.co.gcrs.ataljal.database.GeoTagging.gId;
import static in.co.gcrs.ataljal.database.GeoTagging.gtImageName1;
import static in.co.gcrs.ataljal.database.GeoTagging.gtdate;
import static in.co.gcrs.ataljal.database.GeoTagging.gtemail;
import static in.co.gcrs.ataljal.database.GeoTagging.gtimagename2;
import static in.co.gcrs.ataljal.database.GeoTagging.image1;
import static in.co.gcrs.ataljal.database.GeoTagging.image2;
import static in.co.gcrs.ataljal.database.GeoTagging.locationDetails1;
import static in.co.gcrs.ataljal.database.GeoTagging.noOfFillings;
import static in.co.gcrs.ataljal.database.GeoTagging.others;
import static in.co.gcrs.ataljal.database.GeoTagging.storageCapacity;
import static in.co.gcrs.ataljal.database.GeoTagging.typeofStructure;
import static in.co.gcrs.ataljal.database.Rainfall.rId;
import static in.co.gcrs.ataljal.database.Rainfall.raingauge;
import static in.co.gcrs.ataljal.database.Rainfall.sitename;
import static in.co.gcrs.ataljal.database.WellInventory.date;
import static in.co.gcrs.ataljal.database.WellInventory.electricMeterSelecteditem;
import static in.co.gcrs.ataljal.database.WellInventory.gramPanchayat;
import static in.co.gcrs.ataljal.database.WellInventory.locationDetails;
import static in.co.gcrs.ataljal.database.WellInventory.measuringPoint;
import static in.co.gcrs.ataljal.database.WellInventory.ownerName;
import static in.co.gcrs.ataljal.database.WellInventory.pumpInstalled;
import static in.co.gcrs.ataljal.database.WellInventory.siteName;
import static in.co.gcrs.ataljal.database.WellInventory.use;
import static in.co.gcrs.ataljal.database.WellInventory.village;
import static in.co.gcrs.ataljal.database.WellInventory.waterLevelDepth;
import static in.co.gcrs.ataljal.database.WellInventory.wellDepth;
import static in.co.gcrs.ataljal.database.WellInventory.wellDiameter;
import static in.co.gcrs.ataljal.database.WellInventory.wellInId;
import static in.co.gcrs.ataljal.database.WellInventory.wellNo;
import static in.co.gcrs.ataljal.database.WellInventory.wellType;
import static in.co.gcrs.ataljal.database.WellInventory.wellWaterPotable;
import static in.co.gcrs.ataljal.database.WellInventory.widate;
import static in.co.gcrs.ataljal.database.WellInventory.wiemail;
import static in.co.gcrs.ataljal.database.WellInventory.wiimage;
import static in.co.gcrs.ataljal.database.WellInventory.wiimagename;


public class DbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "ataljal";
    String DB_Path = "/data/data/in.co.gcrs.ataljal.database/databases/";

    // tasks table name

    public static final String Table_WellInventory = "wellinventory";
    public static final String Table_GeoTagging = "geotagging";
    public static final String Table_Rainfall = "rainfall";
    public static final String Table_SocialMonitoring = "socialmonitoring";
    public static final String Table_NewWell = "newwell";



    public static SQLiteDatabase dbase;
    private static DbHelper instance;
    private static Context context;
    public static boolean enableSQLCypher = true;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
        context = DbHelper.context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        dbase = db;

        db.execSQL(wellInventory());
        db.execSQL(geoTagging());
        db.execSQL(rainfall());
        db.execSQL(socialMonitoring());
        db.execSQL(newWell());




    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        // Drop older table if existed

        db.execSQL("DROP TABLE IF EXISTS " + Table_WellInventory);
        db.execSQL("DROP TABLE IF EXISTS " + Table_GeoTagging);
        db.execSQL("DROP TABLE IF EXISTS " + Table_Rainfall);
        db.execSQL("DROP TABLE IF EXISTS " + Table_SocialMonitoring);
        db.execSQL("DROP TABLE IF EXISTS " + Table_NewWell);

        // Create tables again
        onCreate(db);

    }

    public static synchronized DbHelper getInstance(Context context,String pswd) {
        if (instance == null) {
            instance = new DbHelper(context);
            dbase = instance.getWritableDatabase(pswd);
            //db = instance.getWritableDatabase();


        }
        return instance;
    }

    // Well Inventory

    public static String wellInventory() {
        return "CREATE TABLE " + Table_WellInventory +
                "(" +
                 wellInId + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                 wellNo + " TEXT, " +
                 date + " TEXT, " +
                 WellInventory.state + " TEXT, " +
                 WellInventory.district + " TEXT, " +
                 WellInventory.block + " TEXT, " +
                 WellInventory.gramPanchayat + " TEXT, " +
                 village + " TEXT, " +
                 siteName + " TEXT, " +
                 locationDetails + " TEXT, " +
                 ownerName + " TEXT, " +
                 WellInventory.latitude + " TEXT, " +
                 WellInventory.longitude + " TEXT, " +
                 wellDiameter + " TEXT, " +
                 wellDepth + " TEXT, " +
                 measuringPoint + " TEXT, " +
                 wellType + " TEXT, " +
                 wellWaterPotable + " TEXT, " +
                 waterLevelDepth + " TEXT, " +
                 use + " TEXT, " +
                 pumpInstalled + " TEXT, " +
                 electricMeterSelecteditem + " TEXT, " +
                 widate + " TEXT, " +
                 wiemail + " TEXT, " +
                 wiimage + " TEXT, " +
                 wiimagename + " TEXT " +
                ")";
    }

    public boolean insertWellInventory(WellInventoryModel wellInventoryModel){

        //SQLiteDatabase db = this.getWritableDatabase();

        //dbase = getReadableDatabase("#Ataljal@1121!");
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        ContentValues cv = new ContentValues();

        cv.put(wellInId,wellInventoryModel.getWellInId());
        cv.put(wellNo,wellInventoryModel.getWellNo());
        cv.put(date,wellInventoryModel.getDate());
        cv.put(WellInventory.state,wellInventoryModel.getState());
        cv.put(WellInventory.district,wellInventoryModel.getDistrict());
        cv.put(WellInventory.block,wellInventoryModel.getBlock());
        cv.put(WellInventory.gramPanchayat,wellInventoryModel.getGramPanchayat());
        cv.put(WellInventory.village,wellInventoryModel.getVillage());
        cv.put(WellInventory.siteName,wellInventoryModel.getSiteName());
        cv.put(WellInventory.locationDetails,wellInventoryModel.getLocationDetails());
        cv.put(WellInventory.ownerName,wellInventoryModel.getOwnerName());
        cv.put(WellInventory.latitude,wellInventoryModel.getLatitude());
        cv.put(WellInventory.longitude,wellInventoryModel.getLongitude());
        cv.put(WellInventory.wellDiameter,wellInventoryModel.getWellDiameter());
        cv.put(WellInventory.wellDepth,wellInventoryModel.getWellDepth());
        cv.put(WellInventory.measuringPoint,wellInventoryModel.getMeasuringPoint());
        cv.put(WellInventory.wellType,wellInventoryModel.getWellType());
        cv.put(WellInventory.wellWaterPotable,wellInventoryModel.getWellWaterPotable());
        cv.put(WellInventory.waterLevelDepth,wellInventoryModel.getWaterLevelDepth());
        cv.put(WellInventory.use,wellInventoryModel.getUse());
        cv.put(WellInventory.pumpInstalled,wellInventoryModel.getPumpInstalled());
        cv.put(WellInventory.electricMeterSelecteditem,wellInventoryModel.getElectricMeterSelecteditem());
        cv.put(WellInventory.widate,wellInventoryModel.getDatetime());
        cv.put(WellInventory.wiemail,wellInventoryModel.getEmail());
        cv.put(WellInventory.wiimage,wellInventoryModel.getImage());
        cv.put(WellInventory.wiimagename,wellInventoryModel.getImageName());

        //long rid =   db.insert(Table_WellInventory, null, cv);
        long rid =   dbase.insert(Table_WellInventory, null, cv);

        //sqLiteDatabase.close();
        dbase.close();
        if (rid!=-1){
            return true;
        }else {
            return false;
        }



    }

    public ArrayList<WellInventoryModel> getWellInventoryList() {

        //SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<WellInventoryModel> list = new ArrayList<>();

        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        Cursor cursor = dbase.rawQuery("SELECT * FROM " + Table_WellInventory, null);

        WellInventoryModel wellInventoryModel;

        if (cursor.moveToFirst()) {
            do {
                wellInventoryModel = new WellInventoryModel();

                wellInventoryModel.setWellInId(cursor.getString(cursor.getColumnIndex(wellInId)));
                wellInventoryModel.setWellNo(cursor.getString(cursor.getColumnIndex(wellNo)));
                wellInventoryModel.setDate(cursor.getString(cursor.getColumnIndex(date)));
                wellInventoryModel.setState(cursor.getString(cursor.getColumnIndex(WellInventory.state)));
                wellInventoryModel.setDistrict(cursor.getString(cursor.getColumnIndex(WellInventory.district)));
                wellInventoryModel.setBlock(cursor.getString(cursor.getColumnIndex(WellInventory.block)));
                wellInventoryModel.setGramPanchayat(cursor.getString(cursor.getColumnIndex(WellInventory.gramPanchayat)));
                wellInventoryModel.setVillage(cursor.getString(cursor.getColumnIndex(village)));
                wellInventoryModel.setSiteName(cursor.getString(cursor.getColumnIndex(siteName)));
                wellInventoryModel.setLocationDetails(cursor.getString(cursor.getColumnIndex(locationDetails)));
                wellInventoryModel.setOwnerName(cursor.getString(cursor.getColumnIndex(ownerName)));
                wellInventoryModel.setLatitude(cursor.getString(cursor.getColumnIndex(WellInventory.latitude)));
                wellInventoryModel.setLongitude(cursor.getString(cursor.getColumnIndex(WellInventory.longitude)));
                wellInventoryModel.setWellDiameter(cursor.getString(cursor.getColumnIndex(wellDiameter)));
                wellInventoryModel.setWellDepth(cursor.getString(cursor.getColumnIndex(wellDepth)));
                wellInventoryModel.setMeasuringPoint(cursor.getString(cursor.getColumnIndex(measuringPoint)));
                wellInventoryModel.setWellType(cursor.getString(cursor.getColumnIndex(wellType)));
                wellInventoryModel.setWellWaterPotable(cursor.getString(cursor.getColumnIndex(wellWaterPotable)));
                wellInventoryModel.setWaterLevelDepth(cursor.getString(cursor.getColumnIndex(waterLevelDepth)));
                wellInventoryModel.setUse(cursor.getString(cursor.getColumnIndex(use)));
                wellInventoryModel.setPumpInstalled(cursor.getString(cursor.getColumnIndex(pumpInstalled)));
                wellInventoryModel.setElectricMeterSelecteditem(cursor.getString(cursor.getColumnIndex(electricMeterSelecteditem)));
                wellInventoryModel.setDatetime(cursor.getString(cursor.getColumnIndex(widate)));
                wellInventoryModel.setEmail(cursor.getString(cursor.getColumnIndex(wiemail)));
                wellInventoryModel.setImage(cursor.getString(cursor.getColumnIndex(wiimage)));
                wellInventoryModel.setImageName(cursor.getString(cursor.getColumnIndex(wiimagename)));


                list.add(wellInventoryModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        dbase.close();

        return list;
    }

    public boolean deleteWellInventory(String id){


        //SQLiteDatabase db = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        return  dbase.delete(Table_WellInventory, wellInId+"=?",new String[]{id})>0;
    }

    // GeoTagging Artificial Recharge

    public static String geoTagging() {
        return "CREATE TABLE " + Table_GeoTagging +
                "(" +
                gId + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                GeoTagging.state + " TEXT, " +
                GeoTagging.district + " TEXT, " +
                GeoTagging.block + " TEXT, " +
                GeoTagging.gramPanchayat + " TEXT, " +
                GeoTagging.village + " TEXT, " +
                GeoTagging.sitename + " TEXT, " +
                locationDetails1 + " TEXT, " +
                GeoTagging.latitude + " TEXT, " +
                GeoTagging.longitude + " TEXT, " +
                calenderdate + " TEXT, " +
                GeoTagging.typeofStructure + " TEXT, " +
                GeoTagging.storageCapacity + " TEXT, " +
                GeoTagging.noOfFillings + " TEXT, " +
                GeoTagging.image1 + " TEXT, " +
                GeoTagging.image2 + " TEXT, " +
                GeoTagging.others + " TEXT, " +
                GeoTagging.gtdate + " TEXT, " +
                GeoTagging.gtemail + " TEXT, " +
                GeoTagging.gtImageName1 + " TEXT, " +
                GeoTagging.gtimagename2 + " TEXT " +
                ")";

    }

    public boolean insertGeoTagging(GeotaggingModel geotaggingModel){

        //SQLiteDatabase db = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        ContentValues cv = new ContentValues();

        cv.put(gId,geotaggingModel.getgId());
        cv.put(GeoTagging.state,geotaggingModel.getState());
        cv.put(GeoTagging.district,geotaggingModel.getDistrict());
        cv.put(GeoTagging.block,geotaggingModel.getBlock());
        cv.put(GeoTagging.gramPanchayat,geotaggingModel.getGramPanchayat());
        cv.put(GeoTagging.village,geotaggingModel.getVillagename());
//        cv.put(GeoTagging.sitename,geotaggingModel.getSitename());
        cv.put(locationDetails1,geotaggingModel.getLocationDetails1());
        cv.put(GeoTagging.latitude,geotaggingModel.getLatitude());
        cv.put(GeoTagging.longitude,geotaggingModel.getLongitude());
        cv.put(calenderdate,geotaggingModel.getCalenderdate());
        cv.put(GeoTagging.typeofStructure,geotaggingModel.getTypeofStructure());
        cv.put(GeoTagging.storageCapacity,geotaggingModel.getStorageCapacity());
        cv.put(GeoTagging.noOfFillings,geotaggingModel.getNoOfFillings());
        cv.put(GeoTagging.image1,geotaggingModel.getImage1());
        cv.put(GeoTagging.image2,geotaggingModel.getImage2());
        cv.put(GeoTagging.others,geotaggingModel.getOthers());
        cv.put(GeoTagging.gtdate,geotaggingModel.getDate());
        cv.put(GeoTagging.gtemail,geotaggingModel.getEmail());
        cv.put(GeoTagging.gtImageName1,geotaggingModel.getImageName1());
        cv.put(GeoTagging.gtimagename2,geotaggingModel.getImageName2());

        //long rid =   sqLiteDatabase.insert(Table_GeoTagging, null, cv);
        long rid =   dbase.insert(Table_GeoTagging, null, cv);

        dbase.close();
        if (rid!=-1){
            return true;
        }else {
            return false;
        }



    }

    public ArrayList<GeotaggingModel> getGeoTaggingList() {

        //SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<GeotaggingModel> list = new ArrayList<>();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        Cursor cursor = dbase.rawQuery("SELECT * FROM " + Table_GeoTagging, null);

        GeotaggingModel geotaggingModel;

        if (cursor.moveToFirst()) {
            do {
                geotaggingModel = new GeotaggingModel();

                geotaggingModel.setgId(cursor.getString(cursor.getColumnIndex(gId)));
                geotaggingModel.setState(cursor.getString(cursor.getColumnIndex(GeoTagging.state)));
                geotaggingModel.setDistrict(cursor.getString(cursor.getColumnIndex(GeoTagging.district)));
                geotaggingModel.setBlock(cursor.getString(cursor.getColumnIndex(GeoTagging.block)));
                geotaggingModel.setGramPanchayat(cursor.getString(cursor.getColumnIndex(GeoTagging.gramPanchayat)));
                geotaggingModel.setVillagename(cursor.getString(cursor.getColumnIndex(GeoTagging.village)));
                geotaggingModel.setSitename(cursor.getString(cursor.getColumnIndex(GeoTagging.sitename)));
                geotaggingModel.setLocationDetails1(cursor.getString(cursor.getColumnIndex(locationDetails1)));
                geotaggingModel.setLatitude(cursor.getString(cursor.getColumnIndex(GeoTagging.latitude)));
                geotaggingModel.setLongitude(cursor.getString(cursor.getColumnIndex(GeoTagging.longitude)));
                geotaggingModel.setCalenderdate(cursor.getString(cursor.getColumnIndex(calenderdate)));
                geotaggingModel.setTypeofStructure(cursor.getString(cursor.getColumnIndex(typeofStructure)));
                geotaggingModel.setStorageCapacity(cursor.getString(cursor.getColumnIndex(storageCapacity)));
                geotaggingModel.setNoOfFillings(cursor.getString(cursor.getColumnIndex(noOfFillings)));
                geotaggingModel.setImage1(cursor.getString(cursor.getColumnIndex(image1)));
                geotaggingModel.setImage2(cursor.getString(cursor.getColumnIndex(image2)));
                //geotaggingModel.setDeviceid(cursor.getString(cursor.getColumnIndex(deviceid)));
                geotaggingModel.setOthers(cursor.getString(cursor.getColumnIndex(others)));
                geotaggingModel.setDate(cursor.getString(cursor.getColumnIndex(gtdate)));
                geotaggingModel.setEmail(cursor.getString(cursor.getColumnIndex(gtemail)));
                geotaggingModel.setImageName1(cursor.getString(cursor.getColumnIndex(gtImageName1)));
                geotaggingModel.setImageName2(cursor.getString(cursor.getColumnIndex(gtimagename2)));


                list.add(geotaggingModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        dbase.close();

        return list;
    }

    public boolean deleteGeoTagging(String id){


        //SQLiteDatabase db = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        return  dbase.delete(Table_GeoTagging, gId+"=?",new String[]{id})>0;
    }

    //Rainfall

    public static String rainfall() {
        return "CREATE TABLE " + Table_Rainfall +
                "(" +
                rId + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                Rainfall.state + " TEXT, " +
                Rainfall.district + " TEXT, " +
                Rainfall.block + " TEXT, " +
                Rainfall.gramPanchayat + " TEXT, " +
                Rainfall.sitename + " TEXT, " +
                Rainfall.raingauge + " TEXT, " +
                Rainfall.date + " TEXT, " +
                Rainfall.rainfall + " TEXT, " +
                Rainfall.latitude + " TEXT, " +
                Rainfall.longitude + " TEXT, " +
                Rainfall.image1 + " TEXT, " +
                Rainfall.image2 + " TEXT, " +
                Rainfall.remail + " TEXT, " +
                Rainfall.rImageName1 + " TEXT, " +
                Rainfall.rImageName2 + " TEXT " +
                ")";

    }

    public boolean insertRainfall(RainfallModel rainfallModel){

        //SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        ContentValues cv = new ContentValues();

        cv.put(rId,rainfallModel.getrId());
        cv.put(Rainfall.state,rainfallModel.getState());
        cv.put(Rainfall.district,rainfallModel.getDistrict());
        cv.put(Rainfall.block,rainfallModel.getBlock());
        cv.put(Rainfall.gramPanchayat,rainfallModel.getGramPanchayat());
        cv.put(Rainfall.sitename,rainfallModel.getSitename());
        cv.put(Rainfall.raingauge,rainfallModel.getRainGauge());
        cv.put(Rainfall.date,rainfallModel.getDate());
        cv.put(Rainfall.rainfall,rainfallModel.getRainfall());
        cv.put(Rainfall.latitude,rainfallModel.getLatitude());
        cv.put(Rainfall.longitude,rainfallModel.getLongitude());
        cv.put(Rainfall.image1,rainfallModel.getImage1());
        cv.put(Rainfall.image2,rainfallModel.getImage2());
        cv.put(Rainfall.remail,rainfallModel.getEmail());
        cv.put(Rainfall.rImageName1,rainfallModel.getImagename1());
        cv.put(Rainfall.rImageName2,rainfallModel.getImagename2());

        //long rid =   sqLiteDatabase.insert(Table_Rainfall, null, cv);
        long rid =   dbase.insert(Table_Rainfall, null, cv);

        //sqLiteDatabase.close();
        dbase.close();
        if (rid!=-1){
            return true;
        }else {
            return false;
        }



    }

    public ArrayList<RainfallModel> getRainfallList() {

        //SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<RainfallModel> list = new ArrayList<>();

        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        Cursor cursor = dbase.rawQuery("SELECT * FROM " + Table_Rainfall, null);

        RainfallModel rainfallModel;

        if (cursor.moveToFirst()) {
            do {
                rainfallModel = new RainfallModel();

                rainfallModel.setrId(cursor.getString(cursor.getColumnIndex(rId)));
                rainfallModel.setState(cursor.getString(cursor.getColumnIndex(Rainfall.state)));
                rainfallModel.setDistrict(cursor.getString(cursor.getColumnIndex(Rainfall.district)));
                rainfallModel.setBlock(cursor.getString(cursor.getColumnIndex(Rainfall.block)));
                rainfallModel.setGramPanchayat(cursor.getString(cursor.getColumnIndex(Rainfall.gramPanchayat)));
                rainfallModel.setSitename(cursor.getString(cursor.getColumnIndex(sitename)));
                rainfallModel.setRainGauge(cursor.getString(cursor.getColumnIndex(raingauge)));
                rainfallModel.setDate(cursor.getString(cursor.getColumnIndex(Rainfall.date)));
                rainfallModel.setRainfall(cursor.getString(cursor.getColumnIndex(Rainfall.rainfall)));
                rainfallModel.setLatitude(cursor.getString(cursor.getColumnIndex(Rainfall.latitude)));
                rainfallModel.setLongitude(cursor.getString(cursor.getColumnIndex(Rainfall.longitude)));
                rainfallModel.setImage1(cursor.getString(cursor.getColumnIndex(Rainfall.image1)));
                rainfallModel.setImage2(cursor.getString(cursor.getColumnIndex(Rainfall.image2)));
                rainfallModel.setEmail(cursor.getString(cursor.getColumnIndex(Rainfall.remail)));
                rainfallModel.setImagename1(cursor.getString(cursor.getColumnIndex(Rainfall.rImageName1)));
                rainfallModel.setImagename2(cursor.getString(cursor.getColumnIndex(Rainfall.rImageName2)));


                list.add(rainfallModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        dbase.close();

        return list;
    }

    public boolean deleteRainfallList(String id){


        //SQLiteDatabase db = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        return  dbase.delete(Table_Rainfall, rId+"=?",new String[]{id})>0;
    }

    // Social Monitoring

    public static String socialMonitoring() {
        return "CREATE TABLE " + Table_SocialMonitoring +
                "(" +
                SocialMonitoring.smId + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                SocialMonitoring.state + " TEXT, " +
                SocialMonitoring.district + " TEXT, " +
                SocialMonitoring.block + " TEXT, " +
                SocialMonitoring.grampanchayat + " TEXT, " +
                SocialMonitoring.latitude + " TEXT, " +
                SocialMonitoring.longitude + " TEXT, " +
                SocialMonitoring.date + " TEXT, " +
                SocialMonitoring.location + " TEXT, " +
                SocialMonitoring.noOfAttendees + " TEXT, " +
                SocialMonitoring.maleParticipants + " TEXT, " +
                SocialMonitoring.femaleParticipants + " TEXT, " +
                SocialMonitoring.otherParticipants + " TEXT, " +
                SocialMonitoring.event + " TEXT, " +
                SocialMonitoring.imagePath + " TEXT, " +
                SocialMonitoring.mom + " TEXT, " +
                SocialMonitoring.momFileName + " TEXT, " +
                SocialMonitoring.imageName + " TEXT, " +
                SocialMonitoring.datetime + " TEXT, " +
                SocialMonitoring.otherevent+ " TEXT " +

                ")";

    }

    public boolean insertSocialMonitoring(SocialMonitoringModel socialMonitoringModel){

        //SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        ContentValues cv = new ContentValues();

        cv.put(SocialMonitoring.smId, socialMonitoringModel.getId());
        cv.put(SocialMonitoring.state, socialMonitoringModel.getState());
        cv.put(SocialMonitoring.district, socialMonitoringModel.getDistrict());
        cv.put(SocialMonitoring.block, socialMonitoringModel.getBlock());
        cv.put(SocialMonitoring.grampanchayat, socialMonitoringModel.getGrampanchayat());
        cv.put(SocialMonitoring.latitude, socialMonitoringModel.getLatitude());
        cv.put(SocialMonitoring.longitude, socialMonitoringModel.getLongitude());
        cv.put(SocialMonitoring.date, socialMonitoringModel.getDate());
        cv.put(SocialMonitoring.location, socialMonitoringModel.getLocation());
        cv.put(SocialMonitoring.noOfAttendees, socialMonitoringModel.getNoOfAttendees());
        cv.put(SocialMonitoring.maleParticipants, socialMonitoringModel.getMaleParticipants());
        cv.put(SocialMonitoring.femaleParticipants, socialMonitoringModel.getFemaleParticipants());
        cv.put(SocialMonitoring.otherParticipants, socialMonitoringModel.getOtherParticipants());
        cv.put(SocialMonitoring.event, socialMonitoringModel.getEvent());
        cv.put(SocialMonitoring.imagePath, socialMonitoringModel.getImagePath());
        cv.put(SocialMonitoring.mom, socialMonitoringModel.getMom());
        cv.put(SocialMonitoring.smemail, socialMonitoringModel.getEmail());
        cv.put(SocialMonitoring.momFileName, socialMonitoringModel.getMomFilename());
        cv.put(SocialMonitoring.imageName, socialMonitoringModel.getImageFilename());
        cv.put(SocialMonitoring.datetime, socialMonitoringModel.getDatetime());
        cv.put(SocialMonitoring.otherevent, socialMonitoringModel.getOtherEvent());


        //long rid =   sqLiteDatabase.insert(Table_SocialMonitoring, null, cv);
        long rid =   dbase.insert(Table_SocialMonitoring, null, cv);

        //sqLiteDatabase.close();
        dbase.close();
        if (rid!=-1){
            return true;
        }else {
            return false;
        }



    }

    public ArrayList<SocialMonitoringModel> getSocialMonitoringList() {

        //SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<SocialMonitoringModel> list = new ArrayList<>();

        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        Cursor cursor = dbase.rawQuery("SELECT * FROM " + Table_SocialMonitoring, null);

        SocialMonitoringModel socialMonitoringModel;

        if (cursor.moveToFirst()) {
            do {
                socialMonitoringModel = new SocialMonitoringModel();

                socialMonitoringModel.setId(cursor.getString(cursor.getColumnIndex(SocialMonitoring.smId)));
                socialMonitoringModel.setState(cursor.getString(cursor.getColumnIndex(SocialMonitoring.state)));
                socialMonitoringModel.setDistrict(cursor.getString(cursor.getColumnIndex(SocialMonitoring.district)));
                socialMonitoringModel.setBlock(cursor.getString(cursor.getColumnIndex(SocialMonitoring.block)));
                socialMonitoringModel.setGrampanchayat(cursor.getString(cursor.getColumnIndex(SocialMonitoring.grampanchayat)));
                socialMonitoringModel.setLatitude(cursor.getString(cursor.getColumnIndex(SocialMonitoring.latitude)));
                socialMonitoringModel.setLongitude(cursor.getString(cursor.getColumnIndex(SocialMonitoring.longitude)));
                socialMonitoringModel.setDate(cursor.getString(cursor.getColumnIndex(SocialMonitoring.date)));
                socialMonitoringModel.setLocation(cursor.getString(cursor.getColumnIndex(SocialMonitoring.location)));
                socialMonitoringModel.setNoOfAttendees(cursor.getString(cursor.getColumnIndex(SocialMonitoring.noOfAttendees)));
                socialMonitoringModel.setMaleParticipants(cursor.getString(cursor.getColumnIndex(SocialMonitoring.maleParticipants)));
                socialMonitoringModel.setFemaleParticipants(cursor.getString(cursor.getColumnIndex(SocialMonitoring.femaleParticipants)));
                socialMonitoringModel.setOtherParticipants(cursor.getString(cursor.getColumnIndex(SocialMonitoring.otherParticipants)));
                socialMonitoringModel.setEvent(cursor.getString(cursor.getColumnIndex(SocialMonitoring.event)));
                socialMonitoringModel.setImagePath(cursor.getString(cursor.getColumnIndex(SocialMonitoring.imagePath)));
                socialMonitoringModel.setMom(cursor.getString(cursor.getColumnIndex(SocialMonitoring.mom)));
                socialMonitoringModel.setEmail(cursor.getString(cursor.getColumnIndex(SocialMonitoring.smemail)));
                socialMonitoringModel.setMomFilename(cursor.getString(cursor.getColumnIndex(SocialMonitoring.momFileName)));
                socialMonitoringModel.setImageFilename(cursor.getString(cursor.getColumnIndex(SocialMonitoring.imageName)));
                socialMonitoringModel.setDatetime(cursor.getString(cursor.getColumnIndex(SocialMonitoring.datetime)));
                socialMonitoringModel.setOtherEvent(cursor.getString(cursor.getColumnIndex(SocialMonitoring.otherevent)));



                list.add(socialMonitoringModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        dbase.close();

        return list;
    }

    public boolean deleteSocialMonitoringList(String id){


        //SQLiteDatabase db = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        return  dbase.delete(Table_SocialMonitoring, SocialMonitoring.smId+"=?",new String[]{id})>0;
    }

    public static String newWell() {
        return "CREATE TABLE " + Table_NewWell +
                "(" +
                NewWellHelper.wellId + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                NewWellHelper.state + " TEXT, " +
                NewWellHelper.district + " TEXT, " +
                NewWellHelper.block + " TEXT, " +
                NewWellHelper.gramPanchayat + " TEXT, " +
                NewWellHelper.latitude + " TEXT, " +
                NewWellHelper.longitude + " TEXT, " +
                NewWellHelper.wellNo + " TEXT, " +
                NewWellHelper.villageName + " TEXT, " +
                NewWellHelper.source + " TEXT, " +
                NewWellHelper.wellType + " TEXT, " +
                NewWellHelper.waterLevel + " TEXT, " +
                NewWellHelper.year + " TEXT, " +
                NewWellHelper.month + " TEXT, " +
                NewWellHelper.nwdate + " TEXT, " +
                NewWellHelper.nwemail + " TEXT, " +
                NewWellHelper.nwimage + " TEXT, " +
                NewWellHelper.nwimageName + " TEXT " +
                ")";

    }

    public boolean insertNewWell(NewWellModel newWellModel){

        //SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        ContentValues cv = new ContentValues();

        cv.put(NewWellHelper.wellId,newWellModel.getWellId());
        cv.put(NewWellHelper.state,newWellModel.getState());
        cv.put(NewWellHelper.district,newWellModel.getDistrict());
        cv.put(NewWellHelper.block,newWellModel.getBlock());
        cv.put(NewWellHelper.gramPanchayat,newWellModel.getGramPanchayat());
        cv.put(NewWellHelper.latitude,newWellModel.getLatitude());
        cv.put(NewWellHelper.longitude,newWellModel.getLongitude());
        cv.put(NewWellHelper.wellNo,newWellModel.getWellNo());
        cv.put(NewWellHelper.villageName,newWellModel.getVillageName());
        cv.put(NewWellHelper.source,newWellModel.getSource());
        cv.put(NewWellHelper.wellType,newWellModel.getWellType());
        cv.put(NewWellHelper.waterLevel,newWellModel.getWaterLevel());
        cv.put(NewWellHelper.year,newWellModel.getYear());
        cv.put(NewWellHelper.month,newWellModel.getMonth());
        cv.put(NewWellHelper.nwdate,newWellModel.getDatetime());
        cv.put(NewWellHelper.nwemail,newWellModel.getEmail());
        cv.put(NewWellHelper.nwimage,newWellModel.getImage());
        cv.put(NewWellHelper.nwimageName,newWellModel.getImageName());


        //long rid =   sqLiteDatabase.insert(Table_NewWell, null, cv);
        long rid =   dbase.insert(Table_NewWell, null, cv);

        //sqLiteDatabase.close();
        dbase.close();
        if (rid!=-1){
            return true;
        }else {
            return false;
        }



    }

    public ArrayList<NewWellModel> getNewWellList() {

        //SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<NewWellModel> list = new ArrayList<>();

        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        Cursor cursor = dbase.rawQuery("SELECT * FROM " + Table_NewWell, null);

        NewWellModel newWellModel;

        if (cursor.moveToFirst()) {
            do {
                newWellModel = new NewWellModel();

                newWellModel.setWellId(cursor.getString(cursor.getColumnIndex(NewWellHelper.wellId)));
                newWellModel.setState(cursor.getString(cursor.getColumnIndex(NewWellHelper.state)));
                newWellModel.setDistrict(cursor.getString(cursor.getColumnIndex(NewWellHelper.district)));
                newWellModel.setBlock(cursor.getString(cursor.getColumnIndex(NewWellHelper.block)));
                newWellModel.setGramPanchayat(cursor.getString(cursor.getColumnIndex(NewWellHelper.gramPanchayat)));
                newWellModel.setLatitude(cursor.getString(cursor.getColumnIndex(NewWellHelper.latitude)));
                newWellModel.setLongitude(cursor.getString(cursor.getColumnIndex(NewWellHelper.longitude)));
                newWellModel.setWellNo(cursor.getString(cursor.getColumnIndex(NewWellHelper.wellNo)));
                newWellModel.setVillageName(cursor.getString(cursor.getColumnIndex(NewWellHelper.villageName)));
                newWellModel.setSource(cursor.getString(cursor.getColumnIndex(NewWellHelper.source)));
                newWellModel.setWellType(cursor.getString(cursor.getColumnIndex(NewWellHelper.wellType)));
                newWellModel.setWaterLevel(cursor.getString(cursor.getColumnIndex(NewWellHelper.waterLevel)));
                newWellModel.setYear(cursor.getString(cursor.getColumnIndex(NewWellHelper.year)));
                newWellModel.setMonth(cursor.getString(cursor.getColumnIndex(NewWellHelper.month)));
                newWellModel.setDatetime(cursor.getString(cursor.getColumnIndex(NewWellHelper.nwdate)));
                newWellModel.setEmail(cursor.getString(cursor.getColumnIndex(NewWellHelper.nwemail)));
                newWellModel.setImage(cursor.getString(cursor.getColumnIndex(NewWellHelper.nwimage)));
                newWellModel.setImageName(cursor.getString(cursor.getColumnIndex(NewWellHelper.nwimageName)));



                list.add(newWellModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        dbase.close();

        return list;
    }

    public boolean deleteNewWellList(String id){


        //SQLiteDatabase db = this.getWritableDatabase();
        dbase = getReadableDatabase(BuildConfig.SQLITE_PASSWORD);
        if (!dbase.isOpen())
        {
            dbase = SQLiteDatabase.openDatabase(DB_Path,BuildConfig.SQLITE_PASSWORD,null,SQLiteDatabase.OPEN_READWRITE);

        }

        return  dbase.delete(Table_NewWell, NewWellHelper.wellId+"=?",new String[]{id})>0;
    }


}
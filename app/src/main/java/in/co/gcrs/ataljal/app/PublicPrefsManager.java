package in.co.gcrs.ataljal.app;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * MyAppPrefsManager handles some Prefs of AndroidShopApp Application
 **/


public class PublicPrefsManager {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor prefsEditor;

    private int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "ataljal_public";


    private static final String IS_FIRST_TIME_PUBLIC_LAUNCH = "IsFirstTimePublicLaunch";


    private static final String PUBLICSTATE ="publicstate";
    private static final String PUBLICDISTRICT = "publicdistrict";
    private static final String PUBLICBLOCK ="publicblock";
    private static final String PUBLICGRAMPANCHAYAT = "publicgrampanchayat";


    public PublicPrefsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        prefsEditor = sharedPreferences.edit();
    }

    public void setFirstTimePublicLaunch(boolean isFirstTimePublicLaunch) {
        prefsEditor.putBoolean(IS_FIRST_TIME_PUBLIC_LAUNCH, isFirstTimePublicLaunch);
        prefsEditor.commit();
    }

    public boolean isFirstTimePublicLaunch() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME_PUBLIC_LAUNCH, true);
    }

    //STATE
    public void setPublicstate(String publicstate){
        prefsEditor.putString(PUBLICSTATE,publicstate);
        prefsEditor.commit();
        prefsEditor.apply();

    }

    public String getPublicstate(){
        return sharedPreferences.getString(PUBLICSTATE,"");
    }



    //DISTRICT
    public void setPublicdistrict(String publicdistrict){
        prefsEditor.putString(PUBLICDISTRICT,publicdistrict);
        prefsEditor.commit();
        prefsEditor.apply();

    }

    public String getPublicdistrict(){
        return sharedPreferences.getString(PUBLICDISTRICT,"");
    }

    //BLOCK
    public void setPublicblock(String publicblock){
        prefsEditor.putString(PUBLICBLOCK,publicblock);
        prefsEditor.commit();
        prefsEditor.apply();

    }

    public String getPublicblock(){
        return sharedPreferences.getString(PUBLICBLOCK,"");
    }



    //GRAM PANCHAYAT
    public void setPublicgrampanchayat(String publicgrampanchayat){
        prefsEditor.putString(PUBLICGRAMPANCHAYAT,publicgrampanchayat);
        prefsEditor.commit();

    }

    public String getPublicgrampanchayat(){
        return sharedPreferences.getString(PUBLICGRAMPANCHAYAT

                ,"");
    }

}

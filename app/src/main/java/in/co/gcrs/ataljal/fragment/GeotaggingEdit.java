package in.co.gcrs.ataljal.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.adapters.GeotaggingAdapter;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.model.GeotaggingModel;


public class GeotaggingEdit extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private RequestQueue requestQueue;
    private TextView text;
    private MyAppPrefsManager myAppPrefsManager;
    private ArrayList<GeotaggingModel> arrayList = new ArrayList<>();
    private GeotaggingModel geotaggingModel;
    private GeotaggingAdapter geotaggingAdapter;


    public GeotaggingEdit() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_geotagging_edit, container, false);

        myAppPrefsManager = new MyAppPrefsManager(getContext());
        requestQueue = Volley.newRequestQueue(getContext());
        progressBar =view.findViewById(R.id.progressbar);
        text = view.findViewById(R.id.text);

        recyclerView = view.findViewById(R.id.recyclerViewGeotagg);
        geotaggingAdapter =new GeotaggingAdapter(getContext(),arrayList);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        if (isNetworkAvailable()){
            if (!progressBar.isShown()) {
                progressBar.setVisibility(View.VISIBLE);
            }
            getgeotaggingData();
        }else {
            Toast.makeText(getContext(), "Internet not available..!", Toast.LENGTH_SHORT).show();
            if (progressBar.isShown()) {
                progressBar.setVisibility(View.GONE);
            }

            text.setVisibility(View.VISIBLE);
        }


        return view;
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public void getgeotaggingData() {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getgeotagging.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("kcr",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    arrayList.clear();
                    geotaggingAdapter.notifyDataSetChanged();

                    if (jsonArray.length()>0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);

                            geotaggingModel = new GeotaggingModel();

                            geotaggingModel.setgId(heroObject.getString("id"));
                            geotaggingModel.setState(heroObject.getString("state"));
                            geotaggingModel.setDistrict(heroObject.getString("district"));
                            geotaggingModel.setBlock(heroObject.getString("block"));
                            geotaggingModel.setGramPanchayat(heroObject.getString("gp"));
                            geotaggingModel.setVillagename(heroObject.getString("village"));
                            geotaggingModel.setSitename(heroObject.getString("sitename"));
                            geotaggingModel.setDate(heroObject.getString("date"));
                            geotaggingModel.setTypeofStructure(heroObject.getString("typeofstructure"));
                            geotaggingModel.setStorageCapacity(heroObject.getString("storagecapacity"));
                            geotaggingModel.setNoOfFillings(heroObject.getString("nooffillings"));
                            geotaggingModel.setLatitude(heroObject.getString("latitude"));
                            geotaggingModel.setLongitude(heroObject.getString("longitude"));
                            geotaggingModel.setImage1(heroObject.getString("image1"));
                            geotaggingModel.setImage2(heroObject.getString("image2"));
                            if (!heroObject.getString("others").equals("")) {
                                geotaggingModel.setOthers(heroObject.getString("others"));
                            }else {
                                geotaggingModel.setOthers("");
                            }

                            arrayList.add(geotaggingModel);
                            if (progressBar.isShown()) {
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                        geotaggingAdapter = new GeotaggingAdapter(getContext(),arrayList);
                        recyclerView.setAdapter(geotaggingAdapter);
                    }
                    else {
                        Toast.makeText(getContext(), "The data not available for Geotagging", Toast.LENGTH_SHORT).show();
                        text.setVisibility(View.VISIBLE);
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("state",myAppPrefsManager.getState());
                params.put("district",myAppPrefsManager.getDistrict());
                params.put("block",myAppPrefsManager.getBlock());
                params.put("gpname",myAppPrefsManager.getGrampanchayat());
                Log.e("params edit",params.toString());

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
package in.co.gcrs.ataljal.model;

public class SocialMonitoringModel {
    private String id;
    private String state;
    private String district;
    private String block;
    private String grampanchayat;
    private String latitude;
    private String longitude;
    private String date;
    private String location;
    private String noOfAttendees;
    private String maleParticipants;
    private String femaleParticipants;
    private String otherParticipants;
    private String event;
    private String imagePath;
    private String mom;
    private String email;
    private String momFilename;
    private String imageFilename;
    private String datetime;
    private String otherEvent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getGrampanchayat() {
        return grampanchayat;
    }

    public void setGrampanchayat(String grampanchayat) {
        this.grampanchayat = grampanchayat;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNoOfAttendees() {
        return noOfAttendees;
    }

    public void setNoOfAttendees(String noOfAttendees) {
        this.noOfAttendees = noOfAttendees;
    }

    public String getMaleParticipants() {
        return maleParticipants;
    }

    public void setMaleParticipants(String maleParticipants) {
        this.maleParticipants = maleParticipants;
    }

    public String getFemaleParticipants() {
        return femaleParticipants;
    }

    public void setFemaleParticipants(String femaleParticipants) {
        this.femaleParticipants = femaleParticipants;
    }

    public String getOtherParticipants() {
        return otherParticipants;
    }

    public void setOtherParticipants(String otherParticipants) {
        this.otherParticipants = otherParticipants;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getMom() {
        return mom;
    }

    public void setMom(String mom) {
        this.mom = mom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMomFilename() {
        return momFilename;
    }

    public void setMomFilename(String momFilename) {
        this.momFilename = momFilename;
    }

    public String getImageFilename() {
        return imageFilename;
    }

    public void setImageFilename(String imageFilename) {
        this.imageFilename = imageFilename;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getOtherEvent() {
        return otherEvent;
    }

    public void setOtherEvent(String otherEvent) {
        this.otherEvent = otherEvent;
    }
}

package in.co.gcrs.ataljal.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.activities.CameraGeoActivity;
import in.co.gcrs.ataljal.activities.Selection;
import in.co.gcrs.ataljal.api.Status;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.database.DbHelper;
import in.co.gcrs.ataljal.model.RainfallModel;
import in.co.gcrs.ataljal.myviewmodel.UploadImgViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class RainfallDataCollection extends Fragment {

    private TextView textViewState,textViewDistrict,textViewBlock,textViewGp;
    private EditText editTextVillage,editTextLocationDetials,editTextSiteName,editTextRainfall,editTextRainGauge;
    private Button save;

    private String siteName="",rainfall="",latitude="",longitude="",raingauge="",strDate="";
    private RequestQueue requestQueue;
    private MyAppPrefsManager myAppPrefsManager;
    private ProgressDialog progressDialog;
    private DbHelper dbHelper;
    private RainfallModel rainfallModel;
    private TextView date,tvlatitude,tvlongitude;
    private ImageView image1,image2;
    private String imagePath1="",imagePath2="",serverPath1="",serverPath2="",imageName1="",imageName2="";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private Bitmap bitmap;
    SimpleDateFormat df,df1,df2;
    String formattedDate="",logDate="",formattedDate1 = "";
    Date c,c1 ;
    private Fragment fragment;
    private FragmentManager fragmentManager;

    String id="",stname="",dtName="",bkname="",gpname="",village = "",imageCode="";
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1002;
    private static final int PERMISSION_REQUEST_CODE = 1003;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    double loclatitude=0,locLongitude=0;
    Geocoder geocoder;
    private String addressLocation="";
    public RainfallDataCollection() {
        // Required empty public constructor
    }

    UploadImgViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_rainfall_data_collection, container, false);
        viewModel = new ViewModelProvider(this).get(UploadImgViewModel.class);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionRainfall));

        dbHelper = new DbHelper(getContext());
        rainfallModel = new RainfallModel();
        fragmentManager = getActivity().getSupportFragmentManager();

        requestQueue = Volley.newRequestQueue(getContext());
        myAppPrefsManager = new MyAppPrefsManager(getContext());

        progressDialog= new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        textViewState = view.findViewById(R.id.textViewState);
        textViewDistrict = view.findViewById(R.id.textViewDistrict);
        textViewBlock = view.findViewById(R.id.textViewBlock);
        textViewGp = view.findViewById(R.id.textViewGp);
        editTextVillage=view.findViewById(R.id.editTextVillage);
        editTextLocationDetials=view.findViewById(R.id.editTextLocationDetials);
        editTextSiteName = view.findViewById(R.id.editTextSiteName);
        editTextRainfall = view.findViewById(R.id.editTextRainfall);
        editTextRainGauge = view.findViewById(R.id.rainGauge);
        date = view.findViewById(R.id.date);
        tvlatitude = view.findViewById(R.id.latitude);
        tvlongitude = view.findViewById(R.id.longitude);
        image1 = view.findViewById(R.id.image1);
        image2 = view.findViewById(R.id.image2);

        c= Calendar.getInstance().getTime();
        df= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH);
        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        formattedDate = df.format(c).toUpperCase();
        logDate = df1.format(c).toUpperCase();

        c1= Calendar.getInstance().getTime();
        df2= new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        formattedDate1 = df2.format(c1).toUpperCase();
//        logDate = df1.format(c).toUpperCase();

        save = view.findViewById(R.id.buttonSave);

        textViewState.setText(myAppPrefsManager.getState());
        textViewDistrict.setText(myAppPrefsManager.getDistrict());
        textViewBlock.setText(myAppPrefsManager.getBlock());
        textViewGp.setText(myAppPrefsManager.getGrampanchayat());
        tvlatitude.setText(myAppPrefsManager.getLatitude());
        tvlongitude.setText(myAppPrefsManager.getLongitude());
        date.setText(formattedDate1);
        strDate = formattedDate;
        getAddress();

        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions(getContext())) {
                    Intent intent = new Intent(getContext(), CameraGeoActivity.class);
                    intent.putExtra("value", "1");
                    startActivityForResult(intent, 1);
                }
                /*imageCode="1";
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                }*/
            }
        });

        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions(getContext())) {
                    Intent intent = new Intent(getContext(), CameraGeoActivity.class);
                    intent.putExtra("value", "1");
                    startActivityForResult(intent, 2);
                }
                /*imageCode="2";
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 2);
                }*/
               /* if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 100);


                }else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 2);

                }*/

            }
        });

        if(getArguments()!=null){
            id = getArguments().getString("id");
            stname=getArguments().getString("state");
            dtName=getArguments().getString("district");
            bkname=getArguments().getString("block");
            gpname=getArguments().getString("gp");
            village=getArguments().getString("village");
            siteName=getArguments().getString("sitename");
            raingauge=getArguments().getString("raingauge");
            rainfall=getArguments().getString("rainfall");
            String img1=getArguments().getString("image1");
            String img2=getArguments().getString("image2");

            textViewState.setText(stname);
            textViewDistrict.setText(dtName);
            textViewBlock.setText(bkname);
            textViewGp.setText(gpname);
            editTextVillage.setText(village);
            editTextSiteName.setText(siteName);
            editTextRainGauge.setText(raingauge);
            editTextRainfall.setText(rainfall);
            if (!img1.equals("")){
                serverPath1=img1;
                Glide.with(getActivity()).load(img1).into(image1);
            }else {
                image1.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
            }
            if (!img2.equals("")){
                serverPath2=img2;
                Glide.with(getActivity()).load(img2).into(image2);
            }else {
                image2.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
            }
            latitude = getArguments().getString("latitude");
            longitude = getArguments().getString("longitude");
            tvlatitude.setText(latitude);
            tvlongitude.setText(longitude);

        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!myAppPrefsManager.getCategory().equals("")) {
                    if (!myAppPrefsManager.getState().equals("")) {
                        if (!myAppPrefsManager.getDistrict().equals("")) {
                            if (!myAppPrefsManager.getBlock().equals("")) {
                                if (!myAppPrefsManager.getGrampanchayat().equals("")) {
                                    if (!editTextVillage.getText().toString().equals("")) {
                                        if (!editTextSiteName.getText().toString().trim().equals("")) {
                                            if (!editTextLocationDetials.getText().toString().equals("")) {
                                                if (!tvlatitude.getText().toString().equals("")) {

                                                    if (!tvlongitude.getText().toString().equals("Select Month")) {

                                                        if (!editTextRainGauge.getText().toString().trim().equals("")) {

                                                            if (!strDate.equals("")) {

                                                                if (!editTextRainfall.getText().toString().trim().startsWith(".") && !editTextRainfall.getText().toString().trim().equals("")) {

                                                                    if (Double.parseDouble(editTextRainfall.getText().toString().trim()) > 0.0) {

//                                                                if (!imagePath1.equals("") || !serverPath1.equals("")) {

//                                                                    if (!imagePath2.equals("") || !serverPath2.equals("")) {

                                                                        siteName = editTextSiteName.getText().toString().trim();
                                                                        rainfall = editTextRainfall.getText().toString().trim();
                                                                        raingauge = editTextRainGauge.getText().toString().trim();

                                                                        rainfallModel.setState(myAppPrefsManager.getState());
                                                                        rainfallModel.setDistrict(myAppPrefsManager.getDistrict());
                                                                        rainfallModel.setBlock(myAppPrefsManager.getBlock());
                                                                        rainfallModel.setGramPanchayat(myAppPrefsManager.getGrampanchayat());
                                                                        rainfallModel.setSitename(siteName);
                                                                        rainfallModel.setRainGauge(raingauge);
                                                                        rainfallModel.setLatitude(myAppPrefsManager.getLatitude());
                                                                        rainfallModel.setLongitude(myAppPrefsManager.getLongitude());
                                                                        rainfallModel.setDate(formattedDate);
                                                                        rainfallModel.setRainfall(rainfall);
                                                                        rainfallModel.setImage1(imagePath1);
                                                                        rainfallModel.setImage2(imagePath2);
                                                                        rainfallModel.setEmail(myAppPrefsManager.getUserEmail());
                                                                        rainfallModel.setImagename1(imageName1);
                                                                        rainfallModel.setImagename2(imageName2);

                                                                        if (isNetworkAvailable()) {


                                                                            if (progressDialog != null && !progressDialog.isShowing()) {
                                                                                progressDialog.show();
                                                                            }
                                                                            if (!id.equals("")) {
                                                                                if (!imagePath1.equals("")) {
                                                                                    insertImage1();
                                                                                } else if (!imagePath2.equals("")) {
                                                                                    insertImage2();
                                                                                } else {
                                                                                    updateRainfall(id, siteName, raingauge, rainfall, serverPath1, serverPath2);
                                                                                }
                                                                            } else {
                                                                                if (!imagePath1.equals("")) {
                                                                                    insertImage1();
                                                                                } else if (!imagePath2.equals("")) {
                                                                                    insertImage2();
                                                                                } else {
                                                                                    insertRainfall(siteName, raingauge, rainfall);
                                                                                }
//                                                                                insertImage1();

                                                                            }
                                                                        } else {

                                                                            if (progressDialog != null && !progressDialog.isShowing()) {
                                                                                progressDialog.show();
                                                                            }

                                                                            boolean status = dbHelper.insertRainfall(rainfallModel);
                                                                            if (status == true) {
                                                                                if (progressDialog != null && progressDialog.isShowing()) {
                                                                                    progressDialog.dismiss();
                                                                                }

                                                            /*editTextSiteName.setText("");
                                                            editTextRainfall.setText("");
                                                            editTextRainGauge.setText("");
                                                            image1.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
                                                            image2.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));*/
                                                                                fragment = new DashBoardFragment();
                                                                                fragmentManager.beginTransaction()
                                                                                        .remove(fragment)
                                                                                        .commit();
                                                                                fragmentManager.popBackStack();

                                                                                Toast.makeText(getContext(), "Data saved in offline mode and will sync with server once online", Toast.LENGTH_SHORT).show();
                                                                            } else {
                                                                                if (progressDialog != null && progressDialog.isShowing()) {
                                                                                    progressDialog.dismiss();
                                                                                }

                                                                                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        }
//                                                                    }
//                                                                    else {
//                                                                        Toast.makeText(getContext(), "Please Select image2", Toast.LENGTH_SHORT).show();
//                                                                    }
//                                                                }
//                                                                else {
//                                                                    Toast.makeText(getContext(), "Please Select image1", Toast.LENGTH_SHORT).show();
//                                                                }
                                                                    } else {
                                                                        editTextRainfall.setError("Enter valid Rainfall");
                                                                        editTextRainfall.requestFocus();
                                                                    }

                                                                } else {
                                                                    editTextRainfall.setError("Enter Rainfall");
                                                                    editTextRainfall.requestFocus();
                                                                }
                                                            } else {
                                                                Toast.makeText(getContext(), "Please enter Date", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } else {
                                                            Toast.makeText(getContext(), "Enter Rain Gauge", Toast.LENGTH_SHORT).show();
                                                        }

                                                    } else {
                                                        Toast.makeText(getContext(), "Select Longitude", Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    Toast.makeText(getContext(), "Select Latitude", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                            else {
                                                editTextLocationDetials.setError("Enter Location Details");
                                                editTextLocationDetials.requestFocus();
                                            }
                                        } else {
                                            editTextSiteName.setError("Enter SiteName");
                                            editTextSiteName.requestFocus();
                                        }
                                    } else {
                                        editTextVillage.setError("Enter Village Name");
                                        editTextVillage.requestFocus();
                                    }
                                } else {
                                        Toast.makeText(getContext(), "Select Gram Panchayat", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getContext(), "Select Block", Toast.LENGTH_SHORT).show();
                                }
                        } else {
                            Toast.makeText(getContext(), "Select District", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getContext(), "Select State", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "Connect Internet and select nodal officer", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }
    public void getAddress()
    {
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(myAppPrefsManager.getLatitude()), Double.parseDouble(myAppPrefsManager.getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            addressLocation=address;
            editTextLocationDetials.setText(address);
            Log.d("kcr",address);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    private static boolean checkAndRequestPermissions(final Context context) {

        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    101);
            return false;
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        checkLocationPermission();

    }

    private void checkLocationPermission(){

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    //showAppSettings();
                    displayLocationSettingsRequest(getActivity());
                }

            }
        } else {
            //displayLocationSettings();
            displayLocationSettingsRequest(getActivity());

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);


        Task<LocationSettingsResponse> result=LocationServices.getSettingsClient(getActivity().getApplicationContext())
                .checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task)
            {

                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    //Toast.makeText(Selection.this, "GPS is on", Toast.LENGTH_SHORT).show();
                }
                catch  (ApiException apiException)
                {
                    switch (apiException.getStatusCode())
                    {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            ResolvableApiException resolvableApiException=(ResolvableApiException)apiException;
                            try {
                                resolvableApiException.startResolutionForResult(getActivity(),1001);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }

                }
            }
        });
    }


        @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
                Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // Permission Denied
                Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT)
                        .show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        if (requestCode == 101) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Camera permission", Toast.LENGTH_SHORT)
                        .show();
                getActivity().finish();
            } else if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Storage permission",
                        Toast.LENGTH_SHORT).show();
                getActivity().finish();
            } else {
                switch (imageCode) {
                    case "1": {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 1);
                        break;
                    }
                    case "2": {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 2);
                        break;
                    }
                }
            }
        }

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    Log.d("asd", "zxc1");
                    //Do the stuff that requires permission...
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        //Show permission explanation dialog...


                        // we can ask again

                        Log.d("asd", "zxc11");

                        // 2

                        askPermission();
                    } else {

                        // showAppSettings();

                        Log.d("asd", "zxc111");
                        //Never ask again selected, or device policy prohibits the app from having that permission.
                        //So, disable that feature, or fall back to another situation...
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        if (requestCode==1) {
            if (resultCode == Activity.RESULT_OK) {
                    String path = data.getStringExtra("path");

                    imagePath1 = path;

                    bitmap = BitmapFactory.decodeFile(path);


                    ByteArrayOutputStream out = new ByteArrayOutputStream();

                    if (bitmap != null) {

                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

                        image1.setImageBitmap(bitmap);
                    }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }
            /*if (resultCode==Activity.RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                image1.setImageBitmap(thumbnail);

                saveImage(thumbnail);
                //  Toast.makeText(getActivity(), "Image Saved!", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }*/
        }else if (requestCode==2) {
            if (resultCode == Activity.RESULT_OK) {

                String path = data.getStringExtra("path");

                imagePath2 = path;

                bitmap = BitmapFactory.decodeFile(path);


                ByteArrayOutputStream out = new ByteArrayOutputStream();

                if (bitmap != null) {

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

                    image2.setImageBitmap(bitmap);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }
            /*if (resultCode==Activity.RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                image2.setImageBitmap(thumbnail);

                saveImage(thumbnail);
                //  Toast.makeText(getActivity(), "Image Saved!", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }*/
        }

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    getLocation();


                    break;

                case Activity.RESULT_CANCELED:
                    Toast.makeText(getContext(), "Please Enable Location", Toast.LENGTH_SHORT).show();
                    // displayLocationSettings();
                    displayLocationSettingsRequest(getContext());

            }
        }

    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        }
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {



                    // current location

                    loclatitude = location.getLatitude();
                    locLongitude = location.getLongitude();

                    myAppPrefsManager.setLatitude(String.valueOf(loclatitude));
                    myAppPrefsManager.setLongitude(String.valueOf(locLongitude));


                }

            }
        });
    }


    private void  insertImage1(){
//imagePath
        File pic = new File(imagePath1);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverPath1 = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Image Upload "+imageName1+" Successful");
                        Log.d("kcr",serverPath1);
                        if (!id.equals("")) {
                                        if (!imagePath2.equals("")) {

                                            insertImage2();

                                        }else {
                                            updateRainfall(id,siteName,raingauge,rainfall,serverPath1,serverPath2);
                                        }
                                    }else {

                                        if (!imagePath2.equals("")) {

                                            insertImage2();

                                        }else {
                                            insertRainfall(siteName, raingauge, rainfall);
                                        }
                                    }

                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == Status.LOADING) {
                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Image Upload "+imageName1+" Successful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL+ "imageupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//
//                .setMultipartFile("img", "multipart/form-data", new File(imagePath1))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success")) {
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Image Upload "+imageName1+" Successful");
//
//                                    serverPath1 = jsonObject.getString("path");
//                                    Log.d("kcr",serverPath1);
//
//                                    if (!id.equals("")) {
//                                        if (!imagePath2.equals("")) {
//
//                                            insertImage2();
//
//                                        }else {
//                                            updateRainfall(id,siteName,raingauge,rainfall,serverPath1,serverPath2);
//                                        }
//                                    }else {
//
//                                        if (!imagePath2.equals("")) {
//
//                                            insertImage2();
//
//                                        }else {
//                                            insertRainfall(siteName, raingauge, rainfall);
//                                        }
//                                    }
//
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Image Upload "+imageName1+" Successful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse", e.toString());
//                            }else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//
//
//                    }
//                });
    }
    private void  insertImage2(){
        //imagePath
        File pic = new File(imagePath2);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverPath2 = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Image Upload "+imageName2+" Successful");
                        Log.d("kcr",serverPath2);
                        if (!id.equals("")){

                                        updateRainfall(id,siteName,raingauge,rainfall,serverPath1,serverPath2);
                                    }else {
                                        insertRainfall(siteName, raingauge, rainfall);
                                    }


                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == Status.LOADING) {
                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Image Upload "+imageName2+" Unsuccessful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//
//                .setMultipartFile("img", "multipart/form-data", new File(imagePath2))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success")) {
//
//                                    serverPath2 = jsonObject.getString("path");
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Image Upload "+imageName2+" Successful");
//
//                                    Log.d("kcr",serverPath2);
//                                    if (!id.equals("")){
//
//                                        updateRainfall(id,siteName,raingauge,rainfall,serverPath1,serverPath2);
//                                    }else {
//                                        insertRainfall(siteName, raingauge, rainfall);
//                                    }
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Image Upload "+imageName2+" Unsuccessful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse", e.toString());
//                            }else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//
//
//                    }
//                });
    }

    protected void insertRainfall(final String siteName,final String rainGauge,final String rainfall) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                //ConstantValues.URL1+"web/users.jsp",
                BuildConfig.SERVER_URL+"insertrainfall.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);



                            if (obj.getString("status").equals("success")){

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Data insertion Successful");

                                /*editTextSiteName.setText("");
                                editTextRainfall.setText("");
                                editTextRainGauge.setSelection(0);
                                image1.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
                                image2.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));*/
                                fragment = new DashBoardFragment();
                                fragmentManager.beginTransaction()
                                        .remove(fragment)
                                        .commit();
                                fragmentManager.popBackStack();


                                Toast.makeText(getContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();
                            }else {
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Data Insertion Unsuccessful");

                                Toast.makeText(getContext(), ""+obj.getString("status"), Toast.LENGTH_SHORT).show();
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("statename", myAppPrefsManager.getState());
                params.put("districtname", myAppPrefsManager.getDistrict());
                params.put("blockname", myAppPrefsManager.getBlock());
                params.put("gpname", myAppPrefsManager.getGrampanchayat());
                params.put("sitename", siteName);
                params.put("date", formattedDate);
                params.put("raingauge", rainGauge);
                params.put("rainfall", rainfall);
                params.put("latitude", myAppPrefsManager.getLatitude());
                params.put("longitude", myAppPrefsManager.getLongitude());
                params.put("image1", serverPath1);
                params.put("image2", serverPath2);
                params.put("email", myAppPrefsManager.getUserEmail());




                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    protected void updateRainfall(final String id,final String siteName,final String rainGauge,
                                  final String rainfall,final String image1,final String image2) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                //ConstantValues.URL1+"web/users.jsp",
                BuildConfig.SERVER_URL+"updaterainfall.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);



                            if (obj.getString("status").equals("success")){

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Data update Successful");

                                fragment = new DashBoardFragment();
                                fragmentManager.beginTransaction()
                                        .remove(fragment)
                                        .commit();
                                fragmentManager.popBackStack();


                                Toast.makeText(getContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();
                            }else {
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Rainfall","Data update Unsuccessful");

                                Toast.makeText(getContext(), ""+obj.getString("status"), Toast.LENGTH_SHORT).show();
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();



                params.put("id", id);
                params.put("sitename", siteName);
                params.put("date", formattedDate);
                params.put("raingauge", rainGauge);
                params.put("rainfall", rainfall);
                params.put("image1", image1);
                params.put("image2", image2);
                params.put("email", myAppPrefsManager.getUserEmail());




                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"AtalJal");
        // have the object build the directory structure, if needed.
        if (!directory.exists()) {
            directory.mkdirs();
        }

        try {
            File f = new File(directory, "atalJal_"+Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("kcr", "File Saved::--->" + f.getAbsolutePath());
            if (imageCode.equals("1")) {
                imagePath1 = f.getAbsolutePath();

                Path path = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = Paths.get(imagePath1);
                    fileName = path.getFileName();
                    imageName1 = fileName.toString();
                }else {
                    imageName1 = imagePath1.substring(imagePath1.indexOf("/")+1);
                }

            }
            if (imageCode.equals("2")){
                imagePath2 = f.getAbsolutePath();

                Path path = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = Paths.get(imagePath2);
                    fileName = path.getFileName();
                    imageName2 = fileName.toString();
                }else {
                    imageName2 = imagePath2.substring(imagePath2.indexOf("/")+1);
                }
            }

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();

    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);

                return params;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }
}
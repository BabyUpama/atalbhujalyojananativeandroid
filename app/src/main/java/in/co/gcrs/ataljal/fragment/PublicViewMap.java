package in.co.gcrs.ataljal.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.app.PublicPrefsManager;
import in.co.gcrs.ataljal.model.ExistingWellModel;
import in.co.gcrs.ataljal.model.GWMStations;
import in.co.gcrs.ataljal.model.GeotaggingModel;
import in.co.gcrs.ataljal.model.NewWellModel;
import in.co.gcrs.ataljal.model.WellInventoryModel;


public class PublicViewMap extends Fragment implements LocationListener {


    GoogleMap gMap;
    MapView mapView;
    private double currentLatitude,currentLongitude,latitude1,longitude1;
    FusedLocationProviderClient mFusedLocationProviderClient;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int REQUEST_CHECK_LOCATION_SETTINGS = 100;
    LatLng latLng,latLng1;
    PublicPrefsManager publicPrefsManager;
    MyAppPrefsManager myAppPrefsManager;
    private TextView textViewWellId,textViewLatitudeExistingWell,textViewLongitudeExistingWell,textViewStateExistingWell,textViewDistrictExistingWell
            ,textViewBlockExistingWell,textViewGPExistingWell,textViewSiteNameExistingWell,textViewSourceExistingWell,textViewPre2019ExistingWell,textViewPost2019ExistingWell;
    private TextView textViewWateLevel,textViewTypeOfWellExistingWell;
    private String wateLevelExistingData="";
    private EditText editTextWaterLevelExistingWell;
    private Button buttonUpdateExistingWell;
    private ProgressDialog progressDialog;
    GWMStations gwmStations;
    WellInventoryModel wellInventoryModel;
    private FloatingActionButton floatingActionButtonFilter;
    private Dialog dialog;
    private TextView close;

    CheckBox checkboxWellInventory,checkboxNewWell,checkboxExistingWell,checkboxgeoTagging;


    RequestQueue requestQueue;

    private LinearLayout linearLayoutWaterLevelExistingWell;

    private TextView select;

    private NewWellModel newWellModel;


    private GeotaggingModel geotaggingModel;
    private ExistingWellModel existingWellModel;


    private ArrayList<String> stateArrayList,districtArrayList, blockArrayList, gramPanchayatArrayList;
    private Spinner nodalOfficer, spinnerState , spinnerDistrict, spinnerBlock, spinnerGramPanchayat;
    private String mapState="", mapDistrict="", mapBlock="", mapGp="";

    public PublicViewMap() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_public_view_map, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Public view Map");

        publicPrefsManager=new PublicPrefsManager(getContext());
        myAppPrefsManager=new MyAppPrefsManager(getContext());
        requestQueue=Volley.newRequestQueue(getContext());
        newWellModel = new NewWellModel();

        mapView=view.findViewById(R.id.mapViewUser);

        checkboxWellInventory=view.findViewById(R.id.checkboxWellInventory);
        checkboxNewWell=view.findViewById(R.id.checkboxNewWell);
        checkboxExistingWell=view.findViewById(R.id.checkboxExistingWell);
        checkboxgeoTagging=view.findViewById(R.id.checkboxgeoTagging);


        progressDialog=new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait..!!");
        progressDialog.setCanceledOnTouchOutside(false);

        mapView.onCreate(savedInstanceState);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isNetworkAvailable()) {

            loadMap();
        }else {
            Toast.makeText(getContext(), "Inernet not available", Toast.LENGTH_SHORT).show();
        }

        floatingActionButtonFilter = view.findViewById(R.id.floatingActionButtonFilter);
        floatingActionButtonFilter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (isNetworkAvailable()) {

                    //showSelection();

                }else {
                    Toast.makeText(getContext(), "Internet Not available!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        checkboxExistingWell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                gMap.clear();
                Toast.makeText(getContext(), "Loading Please wait... ", Toast.LENGTH_LONG).show();
                    if(b) {
                        getExistingWellData();

                        if(checkboxNewWell.isChecked())
                        {
                            getNewWellData();
                        }
                        if (checkboxWellInventory.isChecked())
                        {
                            getWellInventoryData();
                        }
                        if (checkboxgeoTagging.isChecked())
                        {
                            getgeotaggingData();
                        }

                    }else{
                        if(checkboxNewWell.isChecked())
                        {
                            getNewWellData();
                        }
                        if (checkboxWellInventory.isChecked())
                        {
                            getWellInventoryData();
                        }
                        if (checkboxgeoTagging.isChecked())
                        {
                            getgeotaggingData();
                        }
                    }
            }
        });


        checkboxNewWell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                gMap.clear();
                Toast.makeText(getContext(), "Loading Please wait... ", Toast.LENGTH_LONG).show();
                if(b)
                {
                    getNewWellData();
                    if(checkboxExistingWell.isChecked())
                    {
                        getExistingWellData();
                    }
                    if (checkboxWellInventory.isChecked())
                    {
                        getWellInventoryData();
                    }
                    if (checkboxgeoTagging.isChecked())
                    {
                        getgeotaggingData();
                    }

                }else{
                    if(checkboxExistingWell.isChecked())
                    {
                        getExistingWellData();
                    }
                    if (checkboxWellInventory.isChecked())
                    {
                        getWellInventoryData();
                    }
                    if (checkboxgeoTagging.isChecked())
                    {
                        getgeotaggingData();
                    }
                }
            }
        });


        checkboxWellInventory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                gMap.clear();
                Toast.makeText(getContext(), "Loading Please wait... ", Toast.LENGTH_LONG).show();
                if(b)
                {
                    if(progressDialog!=null){
                        progressDialog.show();
                    }
                    getWellInventoryData();

                    if(checkboxExistingWell.isChecked())
                    {
                       getExistingWellData();
                    }
                    if (checkboxNewWell.isChecked())
                    {
                        getNewWellData();
                    }
                    if (checkboxgeoTagging.isChecked())
                    {
                        getgeotaggingData();
                    }

                }else {
                    if(checkboxExistingWell.isChecked())
                    {
                        getExistingWellData();
                    }
                    if (checkboxNewWell.isChecked())
                    {
                        getNewWellData();
                    }
                    if (checkboxgeoTagging.isChecked())
                    {
                        getgeotaggingData();
                    }
                }
            }
        });

        checkboxgeoTagging.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean b) {
                gMap.clear();
                Toast.makeText(getContext(), "Loading Please wait... ", Toast.LENGTH_LONG).show();
                if (b){
                    getgeotaggingData();
                    if (checkboxWellInventory.isChecked()){
                        getWellInventoryData();
                    }
                    if(checkboxExistingWell.isChecked())
                    {
                       getExistingWellData();
                    }
                    if (checkboxNewWell.isChecked())
                    {
                        getNewWellData();
                    }

                }else{
                    if (checkboxWellInventory.isChecked()){
                        getWellInventoryData();
                    }
                    if(checkboxExistingWell.isChecked())
                    {
                        getExistingWellData();
                    }
                    if (checkboxNewWell.isChecked())
                    {
                        getNewWellData();
                    }
                }
            }
        });




        return view;
    }

    private void loadMap() {

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                gMap = googleMap;
                //  gMap.getUiSettings().setZoomControlsEnabled(true);


                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                gMap.setMyLocationEnabled(true);

                gMap.getUiSettings().setZoomControlsEnabled(true);


                getLocation(googleMap);



                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {

                    }
                });

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        if(marker.getTag() instanceof WellInventoryModel) {
                            if (wellInventoryModel!=null) {
                                wellInventoryModel = (WellInventoryModel) marker.getTag();
                                showWellInventoryInfoDialog(getContext(), wellInventoryModel);
                            }

                        }else if (marker.getTag() instanceof GeotaggingModel) {
                            if (geotaggingModel!=null) {
                                geotaggingModel = (GeotaggingModel) marker.getTag();
                                showGeoTaggingInfoDialog(getContext(), geotaggingModel);
                            }

                        }else if (marker.getTag() instanceof GWMStations) {
                            if (gwmStations!=null) {
                                gwmStations = (GWMStations) marker.getTag();
                                showExistingWellData(getContext(), gwmStations);
                            }

                        }else if (marker.getTag() instanceof NewWellModel) {
                            if (newWellModel!=null) {
                                newWellModel = (NewWellModel) marker.getTag();
                                showNewInfoDialog(getContext(), newWellModel);
                            }
                        }

                        return false;
                    }
                });
            }
        });
    }

    public void showWellInventoryInfoDialog(Context context,WellInventoryModel wellInventory) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.well_inventory_popup, null);
        dialog.setView(dialogView);
        dialog.setCancelable(false);

        ImageView close=dialogView.findViewById(R.id.close);
        ImageView imageView=dialogView.findViewById(R.id.imageView);
        TextView textViewSiteName=dialogView.findViewById(R.id.textViewSiteName);
        TextView textViewState=dialogView.findViewById(R.id.textViewState);
        TextView textViewDistrict=dialogView.findViewById(R.id.textViewDistrict);
        TextView textViewBlock=dialogView.findViewById(R.id.textViewBlock);
        TextView textViewGP=dialogView.findViewById(R.id.textViewGP);
        TextView textViewLatitude=dialogView.findViewById(R.id.textViewLatitude);
        TextView textViewLongitude=dialogView.findViewById(R.id.textViewLongitude);
        TextView textViewOwnerName=dialogView.findViewById(R.id.textViewOwnerName);
        TextView textViewUse=dialogView.findViewById(R.id.textViewUse);
        TextView textViewLocationDetails=dialogView.findViewById(R.id.textViewLocationDetails);
        TextView textViewPumpInstalled=dialogView.findViewById(R.id.textViewPumpInstalled);
        TextView textViewElectricMeter=dialogView.findViewById(R.id.textViewElectricMeter);
        TextView textViewMeasuringPoint=dialogView.findViewById(R.id.textViewMeasuringPoint);
        TextView textViewWellWater=dialogView.findViewById(R.id.textViewWellWater);
        TextView textViewWellDepth=dialogView.findViewById(R.id.textViewWellDepth);
        TextView textViewWellDiameter=dialogView.findViewById(R.id.textViewWellDiameter);
        TextView textViewWaterLevel=dialogView.findViewById(R.id.textViewWaterLevel);
        TextView textViewVillage=dialogView.findViewById(R.id.textViewVillage);
        TextView textViewWellType=dialogView.findViewById(R.id.textViewWellType);


        textViewSiteName.setText(wellInventory.getSiteName());
        textViewState.setText(wellInventory.getState());
        textViewDistrict.setText(wellInventory.getDistrict());
        textViewBlock.setText(wellInventory.getBlock());
        textViewGP.setText(wellInventory.getGramPanchayat());
        textViewLatitude.setText(wellInventory.getLatitude());
        textViewLongitude.setText(wellInventory.getLongitude());
        textViewOwnerName.setText(wellInventory.getOwnerName());
        textViewUse.setText(wellInventory.getUse());
        textViewLocationDetails.setText(wellInventory.getLocationDetails());
        textViewPumpInstalled.setText(wellInventory.getPumpInstalled());
        textViewElectricMeter.setText(wellInventory.getElectricMeterSelecteditem());
        textViewMeasuringPoint.setText(wellInventory.getMeasuringPoint());
        textViewWellWater.setText(wellInventory.getWellWaterPotable());
        textViewWellDepth.setText(wellInventory.getWellDepth());
        textViewWellDiameter.setText(wellInventory.getWellDiameter());
        textViewWaterLevel.setText(wellInventory.getWaterLevelDepth());
        textViewVillage.setText(wellInventory.getVillage());
        textViewWellType.setText(wellInventory.getWellType());

        final AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        if (!wellInventory.getImage().equals("")){
            Glide.with(context).load(wellInventory.getImage()).into(imageView);
        }else {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
        }


    }

    public void showGeoTaggingInfoDialog(Context context,GeotaggingModel geotaggingModel) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.geo_tagging_popup, null);
        dialog.setView(dialogView);
        dialog.setCancelable(false);

        LinearLayout linearOthers = dialogView.findViewById(R.id.linearOthers);
        ImageView imageView=dialogView.findViewById(R.id.close);
        TextView textViewState=dialogView.findViewById(R.id.textViewState);
        TextView textViewDistrict=dialogView.findViewById(R.id.textViewDistrict);
        TextView textViewBlock=dialogView.findViewById(R.id.textViewBlock);
        TextView textViewGP=dialogView.findViewById(R.id.textViewGP);
        TextView textViewVillage=dialogView.findViewById(R.id.textViewVillage);
        TextView textViewSiteName=dialogView.findViewById(R.id.textViewSiteName);
        TextView textViewLatitude=dialogView.findViewById(R.id.textViewLatitude);
        TextView textViewLongitude=dialogView.findViewById(R.id.textViewLongitude);
        TextView textViewTypeOfStructure=dialogView.findViewById(R.id.textViewTypeOfStructure);
        TextView textViewStorageCapacity=dialogView.findViewById(R.id.textViewStorageCapacity);
        TextView textViewNoOfFillings=dialogView.findViewById(R.id.textViewNoOfFillings);
        TextView textViewOthers=dialogView.findViewById(R.id.textViewOthers);

        textViewState.setText(geotaggingModel.getState());
        textViewDistrict.setText(geotaggingModel.getDistrict());
        textViewBlock.setText(geotaggingModel.getBlock());
        textViewGP.setText(geotaggingModel.getGramPanchayat());
        textViewVillage.setText(geotaggingModel.getVillagename());
        textViewSiteName.setText(geotaggingModel.getSitename());
        textViewLatitude.setText(geotaggingModel.getLatitude());
        textViewLongitude.setText(geotaggingModel.getLongitude());
        textViewTypeOfStructure.setText(geotaggingModel.getTypeofStructure());
        textViewStorageCapacity.setText(geotaggingModel.getStorageCapacity());
        textViewNoOfFillings.setText(geotaggingModel.getNoOfFillings());
        ImageView image1 = dialogView.findViewById(R.id.image1);
        ImageView image2 = dialogView.findViewById(R.id.image2);

        if (geotaggingModel.getOthers()!=null){
            linearOthers.setVisibility(View.VISIBLE);
            textViewOthers.setText(geotaggingModel.getOthers());
        }

        if (!geotaggingModel.getImage1().equals("")) {
            Glide.with(context).load(geotaggingModel.getImage1()).into(image1);
        }else {
            Glide.with(context).load(R.drawable.ic_image).into(image1);
        }
        if (!geotaggingModel.getImage2().equals("")) {
            Glide.with(context).load(geotaggingModel.getImage2()).into(image2);
        }else {
            Glide.with(context).load(R.drawable.ic_image).into(image2);
        }


        final AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void showExistingWellData(final Context context, final GWMStations gwmStations) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.existing_well_popup, null);
        dialog.setView(dialogView);
        dialog.setCancelable(false);

        TextView title = dialogView.findViewById(R.id.dialog_title);
        TextView textViewTitle=dialogView.findViewById(R.id.textViewTitle);
        // ImageView share = dialogView.findViewById(R.id.share);
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        textViewWellId=dialogView.findViewById(R.id.textViewWellId);
        textViewLatitudeExistingWell=dialogView.findViewById(R.id.textViewLatitudeExistingWell);
        textViewLongitudeExistingWell=dialogView.findViewById(R.id.textViewLongitudeExistingWell);
        textViewStateExistingWell=dialogView.findViewById(R.id.textViewStateExistingWell);
        textViewDistrictExistingWell=dialogView.findViewById(R.id.textViewDistrictExistingWell);

        textViewBlockExistingWell=dialogView.findViewById(R.id.textViewBlockExistingWell);
        textViewSiteNameExistingWell=dialogView.findViewById(R.id.textViewSiteNameExistingWell);
        textViewSourceExistingWell=dialogView.findViewById(R.id.textViewSourceExistingWell);
        textViewPre2019ExistingWell=dialogView.findViewById(R.id.textViewPremonsoonExistingWell);

        textViewPost2019ExistingWell=dialogView.findViewById(R.id.textViewPostmonsoonExistingWell);
        editTextWaterLevelExistingWell=dialogView.findViewById(R.id.editTextWaterLevelExistingWell);
        textViewTypeOfWellExistingWell=dialogView.findViewById(R.id.textViewTypeOfWellExistingWell);
        ImageView imageView = dialogView.findViewById(R.id.existingimage);
        buttonUpdateExistingWell=dialogView.findViewById(R.id.buttonUpdateExistingWell);

        linearLayoutWaterLevelExistingWell=dialogView.findViewById(R.id.linearLayoutWaterLevelExistingWell);
        textViewWateLevel=dialogView.findViewById(R.id.textViewWateLevel);
        textViewWateLevel.setVisibility(View.GONE);
        textViewTitle.setText("Existing Observation Well");


        textViewStateExistingWell.setText(gwmStations.getState());
        textViewDistrictExistingWell.setText(gwmStations.getDistrict());
        textViewTypeOfWellExistingWell.setText(gwmStations.getWelltype());
        textViewBlockExistingWell.setText(gwmStations.getBlock());
        textViewSiteNameExistingWell.setText(gwmStations.getSitename());
        textViewSourceExistingWell.setText(gwmStations.getSource());
        textViewPre2019ExistingWell.setText(gwmStations.getPre2019());
        textViewPost2019ExistingWell.setText(gwmStations.getPost2019());
        textViewWellId.setText(gwmStations.getWellId());
        textViewLatitudeExistingWell.setText(gwmStations.getLatitude());
        textViewLongitudeExistingWell.setText(gwmStations.getLongitude());
        if (!gwmStations.getImage().equals("")){
            Glide.with(context).load(gwmStations.getImage()).into(imageView);
        }else {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
        }
        ImageView close = dialogView.findViewById(R.id.close);


        buttonUpdateExistingWell.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {





                if(linearLayoutWaterLevelExistingWell.getVisibility()==View.VISIBLE)
                {
                    if(!editTextWaterLevelExistingWell.getText().toString().equals(""))
                    {
                        wateLevelExistingData=editTextWaterLevelExistingWell.getText().toString();
                        Log.d("llayout","llyes");
                        // updateExistingWellData(wateLevelExistingData);
                    }
                    else
                    {
                        editTextWaterLevelExistingWell.setError("Enter Water Level Data");
                        editTextWaterLevelExistingWell.requestFocus();
                    }
                }
                else
                {
                    Log.d("llayout","llno");
                    //  updateExistingWellData( wateLevelExistingData);
                }

            }
        });

        textViewWateLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(linearLayoutWaterLevelExistingWell.getVisibility()==View.GONE)
                {
                    linearLayoutWaterLevelExistingWell.setVisibility(View.VISIBLE);
                }
                else
                {
                    linearLayoutWaterLevelExistingWell.setVisibility(View.GONE);
                }
            }
        });

        final AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    public void showNewInfoDialog(Context context,NewWellModel newWellModel) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.existing_well_popup, null);
        dialog.setView(dialogView);
        dialog.setCancelable(false);

        ImageView textViewClose=dialogView.findViewById(R.id.close);
        TextView textViewTitle=dialogView.findViewById(R.id.textViewTitle);

        TextView textViewWellId=dialogView.findViewById(R.id.textViewWellId);
        TextView textViewLatitudeExistingWell=dialogView.findViewById(R.id.textViewLatitudeExistingWell);
        TextView textViewLongitudeExistingWell=dialogView.findViewById(R.id.textViewLongitudeExistingWell);
        TextView textViewStateExistingWell=dialogView.findViewById(R.id.textViewStateExistingWell);
        TextView textViewDistrictExistingWell=dialogView.findViewById(R.id.textViewDistrictExistingWell);
        TextView textViewBlockExistingWell=dialogView.findViewById(R.id.textViewBlockExistingWell);
        TextView textViewGPExistingWell=dialogView.findViewById(R.id.textViewGPExistingWell);
        TextView textViewSiteNameExistingWell=dialogView.findViewById(R.id.textViewSiteNameExistingWell);
        TextView textViewTypeOfWellExistingWell=dialogView.findViewById(R.id.textViewTypeOfWellExistingWell);
        TextView textViewSource = dialogView.findViewById(R.id.textViewSourceExistingWell);
        TextView textViewPremonsoonExistingWell = dialogView.findViewById(R.id.textViewPremonsoonExistingWell);
        TextView textViewPostmonsoonExistingWell = dialogView.findViewById(R.id.textViewPostmonsoonExistingWell);
        ImageView existingImageview = dialogView.findViewById(R.id.existingimage);
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        textViewLatitudeExistingWell.setText(newWellModel.getLatitude());
        textViewLongitudeExistingWell.setText(newWellModel.getLongitude());
        textViewWellId.setText(newWellModel.getWellId());
        textViewStateExistingWell.setText(newWellModel.getState());
        textViewDistrictExistingWell.setText(newWellModel.getDistrict());
        textViewBlockExistingWell.setText(newWellModel.getBlock());
        textViewGPExistingWell.setText(newWellModel.getGramPanchayat());
        textViewSiteNameExistingWell.setText(newWellModel.getVillageName());
        textViewTypeOfWellExistingWell.setText(newWellModel.getWellType());
        textViewSource.setText(newWellModel.getSource());
        textViewPremonsoonExistingWell.setText(newWellModel.getPre2019());
        textViewPostmonsoonExistingWell.setText(newWellModel.getPost2019());

        if (!newWellModel.getImage().equals("")){
          Glide.with(context).load(newWellModel.getImage()).into(existingImageview);
        }else {
            existingImageview.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
        }


        textViewWateLevel=dialogView.findViewById(R.id.textViewWateLevel);
        textViewWateLevel.setVisibility(View.GONE);

        textViewTitle.setText("New Observation Well");
        final AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        textViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void getLocation(final GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            // return;
        }


        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    //Getting longitude and latitude
                    currentLatitude = location.getLatitude();
                    currentLongitude = location.getLongitude();

                    myAppPrefsManager.setLatitude(String.valueOf(currentLatitude));
                    myAppPrefsManager.setLongitude(String.valueOf(currentLongitude));


                    LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    //googleMap.addMarker(new MarkerOptions().position(sydney).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_gps)));
                    // googleMap.addMarker(new MarkerOptions().position(myLocation).title("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(5).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                }

            }
        });
    }
    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {

                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();

            }
        }


    };


    public void getExistingWellData()
    {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getexistingwells.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                Log.d("kcrexisitngwell",response);
                try
                {
                    JSONArray jsonArray = new JSONArray(response);
                    if (progressDialog != null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }
                    if (jsonArray.length()>0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);

                            gwmStations = new GWMStations();
                            gwmStations.setState(heroObject.getString("state"));
                            gwmStations.setDistrict(heroObject.getString("district"));
                            gwmStations.setBlock(heroObject.getString("block"));
                            gwmStations.setSitename(heroObject.getString("sitename"));
                            gwmStations.setSource(heroObject.getString("source"));
                            gwmStations.setWellId(heroObject.getString("wellid"));
                            gwmStations.setWelltype(heroObject.getString("typeofwell"));
                            latitude1 = Double.parseDouble(heroObject.getString("latitude"));
                            longitude1 = Double.parseDouble(heroObject.getString("longitude"));
                            gwmStations.setPre2019(heroObject.getString("pre2019"));
                            gwmStations.setPost2019(heroObject.getString("post2019"));
                            gwmStations.setUid(heroObject.getString("uid"));
                            gwmStations.setLatitude(String.valueOf(latitude1));
                            gwmStations.setLongitude(String.valueOf(longitude1));
                            gwmStations.setImage(heroObject.getString("image"));
                            latLng1 = new LatLng(latitude1, longitude1);
                            gMap.addMarker(new MarkerOptions().position(latLng1).title("").icon(getMarkerIcon("#43a047"))).setTag(gwmStations);
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng1).zoom(10).build();
                            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }else {
                        Toast.makeText(getContext(), "The data not available for Existing wells!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressDialog!=null&&progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                if (progressDialog!=null&&progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("state",publicPrefsManager.getPublicstate());
                params.put("district",publicPrefsManager.getPublicdistrict());
                params.put("block",publicPrefsManager.getPublicblock());
                //params.put("grampanchayat", gp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void getNewWellData() {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getnewwells.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("kcrnewwell",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (jsonArray.length()>0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);

                            newWellModel = new NewWellModel();
                            newWellModel.setState(heroObject.getString("state"));


                            newWellModel.setWellId(heroObject.getString("wellid"));//
                            newWellModel.setState(heroObject.getString("state"));//
                            newWellModel.setVillageName(heroObject.getString("sitename"));//
                            newWellModel.setDistrict(heroObject.getString("district"));//
                            newWellModel.setBlock(heroObject.getString("block"));//
                            newWellModel.setSource(heroObject.getString("source"));//
                            newWellModel.setGramPanchayat(heroObject.getString("grampanchayat"));//
                            newWellModel.setWellType(heroObject.getString("typeofwell"));//
                            newWellModel.setWellId(heroObject.getString("wellid"));
                            newWellModel.setPre2019(heroObject.getString("pre2019"));
                            newWellModel.setPost2019(heroObject.getString("post2019"));
                            newWellModel.setImage(heroObject.getString("image"));
                            try {
                                latitude1 = Double.parseDouble(heroObject.getString("latitude"));
                            }catch (Exception e){

                            }
                            try {
                                longitude1 = Double.parseDouble(heroObject.getString("longitude"));
                            }catch (Exception e){

                            }
                            //
                            //



                            newWellModel.setLatitude(String.valueOf(latitude1));
                            newWellModel.setLongitude(String.valueOf(longitude1));
                            latLng1 = new LatLng(latitude1, longitude1);
                            gMap.addMarker(new MarkerOptions().position(latLng1).title("").icon(getMarkerIcon("#fdd835"))).setTag(newWellModel);
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng1).zoom(10).build();
                            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }else {
                        Toast.makeText(getContext(), "The data not available for New wells!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressDialog!=null&&progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressDialog!=null&&progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("state",publicPrefsManager.getPublicstate());
                params.put("district",publicPrefsManager.getPublicdistrict());
                params.put("block",publicPrefsManager.getPublicblock());
                params.put("grampanchayat",publicPrefsManager.getPublicgrampanchayat());

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    public void getWellInventoryData()
    {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getwellinventory.jsp", new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("kcrwellinven",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (progressDialog != null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }
                    if (jsonArray.length()>0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);
                            wellInventoryModel = new WellInventoryModel();
                            wellInventoryModel.setState(heroObject.getString("state"));
                            wellInventoryModel.setDistrict(heroObject.getString("district"));
                            wellInventoryModel.setBlock(heroObject.getString("block"));
                            latitude1 = Double.parseDouble(heroObject.getString("latitude"));
                            longitude1 = Double.parseDouble(heroObject.getString("longitude"));
                            wellInventoryModel.setSiteName(heroObject.getString("sitename"));
                            wellInventoryModel.setOwnerName(heroObject.getString("ownername"));
                            wellInventoryModel.setUse(heroObject.getString("use"));
                            wellInventoryModel.setGramPanchayat(heroObject.getString("gp"));
                            wellInventoryModel.setLocationDetails(heroObject.getString("locationdetails"));
                            wellInventoryModel.setPumpInstalled(heroObject.getString("pumpinstalled"));
                            wellInventoryModel.setElectricMeterSelecteditem(heroObject.getString("electricmeterselecteditem"));
                            wellInventoryModel.setMeasuringPoint(heroObject.getString("measuringpoint"));
                            wellInventoryModel.setWellWaterPotable(heroObject.getString("wellwaterpotable"));
                            wellInventoryModel.setWellDepth(heroObject.getString("welldepth"));
                            wellInventoryModel.setWellDiameter(heroObject.getString("welldiameter"));
                            wellInventoryModel.setWaterLevelDepth(heroObject.getString("waterleveldepth"));
                            wellInventoryModel.setVillage(heroObject.getString("village"));
                            wellInventoryModel.setWellType(heroObject.getString("welltype"));
                            wellInventoryModel.setLatitude(String.valueOf(latitude1));
                            wellInventoryModel.setLongitude(String.valueOf(longitude1));
                            wellInventoryModel.setImage(heroObject.getString("image"));

                            latLng1 = new LatLng(latitude1, longitude1);


                            gMap.addMarker(new MarkerOptions().position(latLng1).title("").icon(getMarkerIcon("#5e35b1"))).setTag(wellInventoryModel);
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng1).zoom(10).build();
                            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }else {
                        Toast.makeText(getContext(), "The data not available for Well Inventory!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressDialog!=null&&progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressDialog!=null&&progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("state",publicPrefsManager.getPublicstate());
                params.put("district",publicPrefsManager.getPublicdistrict());
                params.put("block",publicPrefsManager.getPublicblock());
                params.put("gp",publicPrefsManager.getPublicgrampanchayat());


                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void getgeotaggingData() {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getgeotagging.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("kcr",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (jsonArray.length()>0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);

                            geotaggingModel = new GeotaggingModel();

                            latitude1 = Double.parseDouble(heroObject.getString("latitude"));
                            longitude1 = Double.parseDouble(heroObject.getString("longitude"));
                            geotaggingModel.setState(heroObject.getString("state"));
                            geotaggingModel.setDistrict(heroObject.getString("district"));
                            geotaggingModel.setBlock(heroObject.getString("block"));
                            geotaggingModel.setGramPanchayat(heroObject.getString("gp"));
                            geotaggingModel.setVillagename(heroObject.getString("village"));
                            geotaggingModel.setSitename(heroObject.getString("sitename"));
                            geotaggingModel.setTypeofStructure(heroObject.getString("typeofstructure"));
                            geotaggingModel.setStorageCapacity(heroObject.getString("storagecapacity"));
                            geotaggingModel.setNoOfFillings(heroObject.getString("nooffillings"));
                            geotaggingModel.setLatitude(String.valueOf(latitude1));
                            geotaggingModel.setLongitude(String.valueOf(longitude1));
                            geotaggingModel.setImage1(heroObject.getString("image1"));
                            geotaggingModel.setImage2(heroObject.getString("image2"));
                            geotaggingModel.setOthers(heroObject.getString("others"));

                            latLng1 = new LatLng(latitude1, longitude1);


                            gMap.addMarker(new MarkerOptions().position(latLng1).title("").icon(getMarkerIcon("#d81b60"))).setTag(geotaggingModel);
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng1).zoom(10).build();
                            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }else {
                        Toast.makeText(getContext(), "The data not available for Geo Tagging Structures!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressDialog!=null&&progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressDialog!=null&&progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("state",publicPrefsManager.getPublicstate());
                params.put("district",publicPrefsManager.getPublicdistrict());
                params.put("block",publicPrefsManager.getPublicblock());
                params.put("gpname",publicPrefsManager.getPublicgrampanchayat());


                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    displayLocationSettingsRequest(getActivity());

                    loadMap();


                    LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location location = lm
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if(location != null){
                        currentLatitude = location.getLatitude();
                        currentLongitude = location.getLongitude();

                        myAppPrefsManager.setLatitude(String.valueOf(currentLatitude));
                        myAppPrefsManager.setLongitude(String.valueOf(currentLongitude));



                    }



                }
                else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                    alertDialogBuilder.setTitle("Change Permissions in Settings");
                    alertDialogBuilder
                            .setMessage("" +
                                    "\nClick SETTINGS to Manually Set\n" + "Permissions to Access Location")
                            .setCancelable(false)
                            .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, 1000);
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }

            }
            break;


            default:
                // Toast.makeText(this, "Please Allow", Toast.LENGTH_SHORT).show();
                // getDeviceLocation();
                break;


        }

    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:

                            getLocation(gMap);

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the result
                                // in onActivityResult().
                                status.startResolutionForResult(getActivity(), REQUEST_CHECK_LOCATION_SETTINGS);
                                //   getDeviceLocation();
                            } catch (IntentSender.SendIntentException e) {
                                //   Log.i(TAG, "PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            ///  Log.i(TAG, "Location profile are inadequate, and cannot be fixed here. Dialog not created.");
                            break;
                    }
                }
            });
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case REQUEST_CHECK_LOCATION_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation(gMap);


                        break;

                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getContext(), "Please Enable Location", Toast.LENGTH_SHORT).show();
                }
                break;


        }

    }



    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

        if (mFusedLocationProviderClient != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
    

}
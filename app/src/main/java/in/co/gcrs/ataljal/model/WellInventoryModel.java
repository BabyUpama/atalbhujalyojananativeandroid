package in.co.gcrs.ataljal.model;

public class WellInventoryModel {

     String wellInId ;
     String wellNo ;
     String date ;
     String state ;
     String district ;
     String block ;
     String gramPanchayat ;
     String village ;
     String siteName ;
     String locationDetails ;
     String ownerName ;
     String latitude ;
     String longitude ;
     String wellDiameter ;
     String wellDepth ;
     String measuringPoint ;
     String wellType ;
     String wellWaterPotable ;
     String waterLevelDepth ;
     String use ;
     String pumpInstalled ;
     String typeofPump ;
     String pumpCapacity ;
     String discharge ;
     String pumpingDuration ;
     String daysPumped ;
     String electricMeterSelecteditem ;
     String majorCrops ;
     String datetime ;
     String email ;
     String image ;
     String imageName;

    public String getWellInId() {
        return wellInId;
    }

    public void setWellInId(String wellInId) {
        this.wellInId = wellInId;
    }

    public String getWellNo() {
        return wellNo;
    }

    public void setWellNo(String wellNo) {
        this.wellNo = wellNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getGramPanchayat() {
        return gramPanchayat;
    }

    public void setGramPanchayat(String gramPanchayat) {
        this.gramPanchayat = gramPanchayat;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getLocationDetails() {
        return locationDetails;
    }

    public void setLocationDetails(String locationDetails) {
        this.locationDetails = locationDetails;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getWellDiameter() {
        return wellDiameter;
    }

    public void setWellDiameter(String wellDiameter) {
        this.wellDiameter = wellDiameter;
    }

    public String getWellDepth() {
        return wellDepth;
    }

    public void setWellDepth(String wellDepth) {
        this.wellDepth = wellDepth;
    }

    public String getMeasuringPoint() {
        return measuringPoint;
    }

    public void setMeasuringPoint(String measuringPoint) {
        this.measuringPoint = measuringPoint;
    }

    public String getWellType() {
        return wellType;
    }

    public void setWellType(String wellType) {
        this.wellType = wellType;
    }

    public String getWellWaterPotable() {
        return wellWaterPotable;
    }

    public void setWellWaterPotable(String wellWaterPotable) {
        this.wellWaterPotable = wellWaterPotable;
    }

    public String getWaterLevelDepth() {
        return waterLevelDepth;
    }

    public void setWaterLevelDepth(String waterLevelDepth) {
        this.waterLevelDepth = waterLevelDepth;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getPumpInstalled() {
        return pumpInstalled;
    }

    public void setPumpInstalled(String pumpInstalled) {
        this.pumpInstalled = pumpInstalled;
    }

    public String getTypeofPump() {
        return typeofPump;
    }

    public void setTypeofPump(String typeofPump) {
        this.typeofPump = typeofPump;
    }

    public String getPumpCapacity() {
        return pumpCapacity;
    }

    public void setPumpCapacity(String pumpCapacity) {
        this.pumpCapacity = pumpCapacity;
    }

    public String getDischarge() {
        return discharge;
    }

    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }

    public String getPumpingDuration() {
        return pumpingDuration;
    }

    public void setPumpingDuration(String pumpingDuration) {
        this.pumpingDuration = pumpingDuration;
    }

    public String getDaysPumped() {
        return daysPumped;
    }

    public void setDaysPumped(String daysPumped) {
        this.daysPumped = daysPumped;
    }

    public String getElectricMeterSelecteditem() {
        return electricMeterSelecteditem;
    }

    public void setElectricMeterSelecteditem(String electricMeterSelecteditem) {
        this.electricMeterSelecteditem = electricMeterSelecteditem;
    }

    public String getMajorCrops() {
        return majorCrops;
    }

    public void setMajorCrops(String majorCrops) {
        this.majorCrops = majorCrops;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}

package in.co.gcrs.ataljal.fragment;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;


import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.activities.Login;
import in.co.gcrs.ataljal.activities.MainActivity;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.customs.CircularImageView;


public class ProfileFragment extends Fragment {

    View rootView;


    MyAppPrefsManager appPrefs;

    
    CircularImageView profile_image;
    Button /*btn_edit_profile,*/ btn_logout;
    TextView profile_name, profile_email;

    TextView  official_web, rate_app, /*privacy_policy,*/ service_terms,forgotPassword;

    MyAppPrefsManager myAppPrefsManager;

    GoogleApiClient googleApiClient;
    private Fragment fragment;
    private FragmentManager fragmentManager;






    @Override
    public void onStart() {
        super.onStart();



        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        googleApiClient.connect();


        appPrefs = new MyAppPrefsManager(getContext());

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.profile, container, false);


        MainActivity.actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
       // ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Settings");
        fragmentManager  = getActivity().getSupportFragmentManager();


        // Binding Layout Views
        rate_app = (TextView) rootView.findViewById(R.id.rate_app);

        official_web = (TextView) rootView.findViewById(R.id.official_web);

        service_terms = (TextView) rootView.findViewById(R.id.service_terms);
        forgotPassword = (TextView) rootView.findViewById(R.id.forgotPassword);
       // privacy_policy = (TextView) rootView.findViewById(R.id.privacy_policy);



    
        btn_logout = (Button) rootView.findViewById(R.id.btn_logout);
      //  btn_edit_profile = (Button) rootView.findViewById(R.id.btn_edit_account);
        profile_name = (TextView) rootView.findViewById(R.id.profile_name);
        profile_email = (TextView) rootView.findViewById(R.id.profile_email);
        profile_image = (CircularImageView) rootView.findViewById(R.id.profile_image);


        myAppPrefsManager = new MyAppPrefsManager(getContext());
        profile_name.setText(myAppPrefsManager.getUserName());
        profile_email.setText(myAppPrefsManager.getUserEmail());


        if(myAppPrefsManager.getUserImage().equals("")){
            Glide.with(getContext()).asBitmap().load(R.drawable.profile).into(profile_image);
        }
        else{
            Glide.with(getContext()).asBitmap().load(myAppPrefsManager.getUserImage()).into(profile_image);
        }

        setupAppBarHeader();
        
        


        official_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String urlString = "https://ataljal.in/";
                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(urlString));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setPackage("com.android.chrome");
                try {
                    getContext().startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    // Chrome browser presumably not installed so allow user to choose instead
                    intent.setPackage(null);
                    getContext().startActivity(intent);
                }

            }
        });
    

    
    
        rate_app.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(getContext());
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                View dialogView = inflater.inflate(R.layout.dialog_info, null);
                dialog.setView(dialogView);
                dialog.setCancelable(true);

                final TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                final TextView dialog_message = (TextView) dialogView.findViewById(R.id.dialog_message);
                final Button dialog_button_positive = (Button) dialogView.findViewById(R.id.dialog_button_positive);
                final Button dialog_button_negative = (Button) dialogView.findViewById(R.id.dialog_button_negative);

                dialog_title.setText("Rate App");
                dialog_message.setText(getContext().getString(R.string.rate_app_msg));
                dialog_button_positive.setText(getContext().getString(R.string.rate_now));
                dialog_button_negative.setText(getContext().getString(R.string.not_now));


                final android.app.AlertDialog alertDialog = dialog.create();
                alertDialog.show();

                dialog_button_negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                dialog_button_positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();

                        //Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=in.co.gcrs.ataljal" + getContext().getPackageName());
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=in.co.gcrs.ataljal");
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(goToMarket);

                       /* try {
                            getContext().startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getContext(), "Couldn't launch the market", Toast.LENGTH_SHORT).show();
                        }*/
                    }
                });

            }
        });
    
    
       /* privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            
            }
        });*/
    

    
        service_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String urlString = "https://ataljal.in/";
                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(urlString));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setPackage("com.android.chrome");
                try {
                    getContext().startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    // Chrome browser presumably not installed so allow user to choose instead
                    intent.setPackage(null);
                    getContext().startActivity(intent);
                }
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new ChangePassword();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();

            }
        });
    
    
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Edit UserID in SharedPreferences


                myAppPrefsManager.setUserLoggedIn(false);
                myAppPrefsManager.setUserImage("");
                myAppPrefsManager.clearSession();


                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {

                        myAppPrefsManager.setUserLoggedIn(false);


                        Intent logout = new Intent(getContext(), Login.class);
                        logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(logout);
                        ((MainActivity)getContext()).finish();

                        getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);




                    }

                });

                myAppPrefsManager.setUserLoggedIn(false);
                myAppPrefsManager.setFirstTimeLaunch(true);
                myAppPrefsManager.setCategory("");
                myAppPrefsManager.clearSession();
                Intent logout = new Intent(getContext(),Login.class);
                logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                startActivity(logout);
                ((MainActivity)getContext()).finish();

                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
            }
        });


        return rootView;
    }
    
    
    //*********** Setup Header of Navigation Drawer ********//
    
    public void setupAppBarHeader() {
        
        // Check if the User is Authenticated
       /* if (ConstantValues.IS_USER_LOGGED_IN) {
            // Check User's Info from SharedPreferences
            if(myAppPrefsManager.isUserLoggedIn()){

                if(myAppPrefsManager.getUserImage().equals("")){
                    Glide.with(this).asBitmap().load(R.drawable.profile).into(profile_image);
                }
                else {

                    Glide.with(this).asBitmap().load(myAppPrefsManager.getUserImage()).into(profile_image);
                }




            }
            else {
                // Set Default Name, Email and Photo
                profile_image.setImageResource(R.drawable.profile);
                profile_name.setText(getString(R.string.login_or_signup));
                profile_email.setText(getString(R.string.login_or_create_account));
                btn_edit_profile.setText(getString(R.string.login));
                btn_edit_profile.setBackground(getResources().getDrawable(R.drawable.rounded_corners_button_red));
            }
            
        }
        else {
            // Set Default Name, Email and Photo
            profile_image.setImageResource(R.drawable.profile);
            profile_name.setText(getString(R.string.login_or_signup));
            profile_email.setText(getString(R.string.login_or_create_account));
            btn_edit_profile.setText(getString(R.string.login));
            btn_edit_profile.setBackground(getResources().getDrawable(R.drawable.rounded_corners_button_red));
        }
*/







        if(myAppPrefsManager.isUserLoggedIn()){



            profile_image.setImageResource(R.drawable.profile);
            profile_name.setText(myAppPrefsManager.getUserName());
            profile_email.setText(myAppPrefsManager.getUserEmail());





            if(myAppPrefsManager.getUserImage().equals("")){
                Glide.with(this).asBitmap().load(R.drawable.profile).into(profile_image);
            }
            else {

                Glide.with(this).asBitmap().load(myAppPrefsManager.getUserImage()).into(profile_image);
            }




        }
        else {
            // Set Default Name, Email and Photo
            profile_image.setImageResource(R.drawable.profile);
            profile_name.setText(getString(R.string.login_or_signup));
            profile_email.setText(getString(R.string.login_or_create_account));
           // btn_edit_profile.setText(getString(R.string.login));
           // btn_edit_profile.setBackground(getResources().getDrawable(R.drawable.rounded_corners_button_red));





        }






        
        // Handle DrawerHeader Click Listener
    }




    


}

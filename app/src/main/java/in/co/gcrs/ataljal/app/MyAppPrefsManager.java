package in.co.gcrs.ataljal.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;


/**
 * MyAppPrefsManager handles some Prefs of AndroidShopApp Application
 **/


public class MyAppPrefsManager {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor prefsEditor;
    
    private int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "ataljal";

    private static final String KEY_FCM_USER_KEY ="fcm_key";
    private static final String DEVICE_ID ="device_id";

    private  static final String value= "value";


    private static final String USER_LANGUAGE_CODE  = "language_Code";
    private static final String IS_USER_LOGGED_IN = "isLogged_in";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static final String KEY_ShowCaseView = "isFirstTimeShowCase";


    private static final String USER_ID="user_id";
    private static final String USER_EMAIL="user_email";
    private static final String USER_NAME="user_name";
    private static final String USER_MOBILE="user_mobile";

    private static final String USER_IMAGE="user_image";

    private static final String LATITUDE ="latitude";
    private static final String LONGITUDE = "longitude";


    private static final String CATEGORY="category";
    private static final String STATE ="state";
    private static final String DISTRICT = "district";
    private static final String BLOCK ="block";
    private static final String GRAMPANCHAYAT = "grampanchayat";
    private static final String VILLAGE ="village";
    private static final String TAHSIL = "tahsil";

    private static final String USERTYPE = "usertype";

    private static final String LANGUAGE = "language";

    static final String RESULT= "result";


    private static final String IS_FIRST_TIME_PERMISSION="IsFirstTimePermission";
    static final String LAST_LOGIN = "lastlogin";

    public MyAppPrefsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        prefsEditor = sharedPreferences.edit();
    }




    public void setUserMobile(String userMobile){
        prefsEditor.putString(USER_MOBILE,userMobile);
        prefsEditor.commit();

    }





    public String getUserMobile(){
        return sharedPreferences.getString(USER_MOBILE,"");
    }




    public void setLanguage(String language){
        prefsEditor.putString(LANGUAGE,language);
        prefsEditor.commit();

    }
    public String getLanguage(){
        return sharedPreferences.getString(LANGUAGE,"");
    }


    //username
    public void setUserName(String userName){
        prefsEditor.putString(USER_NAME,userName);
        prefsEditor.commit();

    }





    public String getUserName(){
        return sharedPreferences.getString(USER_NAME,"");
    }



    public void setUserId(String userId){
        prefsEditor.putString(USER_ID,userId);
        prefsEditor.commit();
        prefsEditor.apply();

    }

    public String getUserId(){
        return sharedPreferences.getString(USER_ID,"");
    }



    //user email

    public void setUserEmail(String userEmail){
        prefsEditor.putString(USER_EMAIL,userEmail);
        prefsEditor.commit();

    }

    public String getUserEmail(){
        return sharedPreferences.getString(USER_EMAIL,"");
    }


    public  void setResult(Context ctx, String result)
    {
        prefsEditor.putString(RESULT,result);
        prefsEditor.commit();

    }


    public  String getResult()
    {
        return sharedPreferences.getString(RESULT, "");
    }

    //user image

    public  int getValue() {
        return sharedPreferences.getInt(value,0);
    }
    public void setValue(int vl){
        prefsEditor.putInt(value,vl);
        prefsEditor.commit();

    }

    public void setUserImage(String userImage){
        prefsEditor.putString(USER_IMAGE,userImage);
        prefsEditor.commit();

    }

    public String getUserImage(){
        return sharedPreferences.getString(USER_IMAGE,"");
    }



    public String getKeyFcmUserKey(){
        return sharedPreferences.getString(KEY_FCM_USER_KEY,null);
    }

    public void setKeyFcmUserKey(String keyFcmUserKey){
        prefsEditor.putString(KEY_FCM_USER_KEY,keyFcmUserKey);
        prefsEditor.commit();
    }

    //LATITUDE
    public void setLatitude(String latitude){
        prefsEditor.putString(LATITUDE,latitude);
        prefsEditor.commit();

    }

    public String getLatitude(){
        return sharedPreferences.getString(LATITUDE,"0");
    }



    //LONGITUDE
    public void setLongitude(String longitude){
        prefsEditor.putString(LONGITUDE,longitude);
        prefsEditor.commit();

    }

    public String getLongitude(){
        return sharedPreferences.getString(LONGITUDE,"0");
    }

    

    
    public void setUserLanguageCode(String langCode) {
        prefsEditor.putString(USER_LANGUAGE_CODE, langCode);
        prefsEditor.commit();
    }
    
    public String getUserLanguageCode() {
        return sharedPreferences.getString(USER_LANGUAGE_CODE, "en");
    }
    
    
    public void setUserLoggedIn(boolean isUserLoggedIn) {
        prefsEditor.putBoolean(IS_USER_LOGGED_IN, isUserLoggedIn);
        prefsEditor.commit();
    }

    public boolean isUserLoggedIn() {
        return sharedPreferences.getBoolean(IS_USER_LOGGED_IN, false);
    }


    public void setFirstTimeLaunch(boolean isFirstTimeLaunch) {
        prefsEditor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTimeLaunch);
        prefsEditor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }


    // CATEGORY


    public void setCategory(String category){
        prefsEditor.putString(CATEGORY,category);
        prefsEditor.commit();
        prefsEditor.apply();

    }


    public String getCategory(){

        return sharedPreferences.getString(CATEGORY,"national");
    }


    //STATE
    public void setState(String state){
        prefsEditor.putString(STATE,state);
        prefsEditor.commit();
        prefsEditor.apply();

    }

    public String getState(){
        return sharedPreferences.getString(STATE,"");
    }



    //DISTRICT
    public void setDistrict(String district){
        prefsEditor.putString(DISTRICT,district);
        prefsEditor.commit();
        prefsEditor.apply();

    }

    public String getDistrict(){
        return sharedPreferences.getString(DISTRICT,"");
    }

    //BLOCK
    public void setBlock(String block){
        prefsEditor.putString(BLOCK,block);
        prefsEditor.commit();
        prefsEditor.apply();

    }

    public String getBlock(){
        return sharedPreferences.getString(BLOCK,"");
    }



    //GRAM PANCHAYAT
    public void setGrampanchayat(String grampanchayat){
        prefsEditor.putString(GRAMPANCHAYAT,grampanchayat);
        prefsEditor.commit();

    }

    public String getGrampanchayat(){
        return sharedPreferences.getString(GRAMPANCHAYAT,"");
    }

    //VILLAGE
    public void setVillage(String village){
        prefsEditor.putString(VILLAGE,village);
        prefsEditor.commit();

    }

    public String getVillage(){
        return sharedPreferences.getString(VILLAGE,"");
    }


    public void setTahsil(String tahsil){
        prefsEditor.putString(TAHSIL,tahsil);
        prefsEditor.commit();

    }

    public String getTahsil(){
        return sharedPreferences.getString(TAHSIL,"");
    }

    public void setUsertype(String usertype){
        prefsEditor.putString(USERTYPE,usertype);
        prefsEditor.commit();

    }

    public String getUsertype(){
        return sharedPreferences.getString(USERTYPE,"");
    }


    public void setIsFirstTimePermission(boolean isFirstTimePermission){
        prefsEditor.putBoolean(IS_FIRST_TIME_PERMISSION, isFirstTimePermission);
        prefsEditor.commit();

    }

    public boolean isFirstTimePermission(){
        return sharedPreferences.getBoolean(IS_FIRST_TIME_PERMISSION, true);
    }


    public boolean getShowCaseView(){

        return sharedPreferences.getBoolean(KEY_ShowCaseView,true);
    }

    public void setShowCaseView(boolean showCaseView){
        prefsEditor.putBoolean(KEY_ShowCaseView,showCaseView);
        prefsEditor.commit();
    }

    public void setDeviceId(String deviceId){
        prefsEditor.putString(DEVICE_ID,deviceId);
        prefsEditor.commit();

    }

    public String getDeviceId(){
        return sharedPreferences.getString(DEVICE_ID,"");
    }

    public void setLastLogin(String lastLogin){
        prefsEditor.putString(LAST_LOGIN,lastLogin);
        prefsEditor.commit();

    }

    public String getLastLogin(){
        return sharedPreferences.getString(LAST_LOGIN,"");
    }

    public void clearSession(){
        prefsEditor.clear();
        prefsEditor.commit();
    }

}

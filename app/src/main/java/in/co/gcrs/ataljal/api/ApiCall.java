package in.co.gcrs.ataljal.api;

import in.co.gcrs.ataljal.entity.UploadImgEntity;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiCall {

    @Multipart
    @POST("imageupload")
    Call<String> saveImageFileApi(@Part MultipartBody.Part videoPart);

    @Multipart
    @POST("pdfupload")
    Call<String> savePdfFileApi(@Part MultipartBody.Part videoPart);

    @Multipart
    @POST("wordupload")
    Call<String> saveWordFileApi(@Part MultipartBody.Part videoPart);
}

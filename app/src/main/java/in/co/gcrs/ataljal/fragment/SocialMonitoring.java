package in.co.gcrs.ataljal.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.content.CursorLoader;

import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.hbisoft.pickit.PickiT;
import com.hbisoft.pickit.PickiTCallbacks;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.activities.CameraGeoActivity;
import in.co.gcrs.ataljal.api.Status;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.database.DbHelper;
import in.co.gcrs.ataljal.model.SocialMonitoringModel;
import in.co.gcrs.ataljal.myviewmodel.UploadImgViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.icu.util.ULocale.getCountry;
import static android.icu.util.ULocale.getName;

/**
 * A simple {@link Fragment} subclass.
 */
public class SocialMonitoring extends Fragment implements PickiTCallbacks
{

    private TextView textViewState,textViewDistrict,textViewBlock,textViewGramPanchayat,textViewDate,textViewPhotographsFileName,textViewMOMFileName,editTextNoOfAttendees,tvlatitude,tvlongitude;
    private EditText editTextVillage,editTextMale,editTextFeMale,editTextOtherGender,editTextLocation,editTextOthers;
    SimpleDateFormat df,df1,df2;

    private Button buttonMOM,buttonSave;
    private Spinner spinnerEvent;
    private ArrayAdapter eventAdapter;
    PickiT pickiT;

    private String imagePath="",serverImagePath="",filepathMOM="",photo_filename="",mom_filename="",serverFilePathMom="",eventSelectedItem="";
    Bitmap bitmap;
    private int total=0,male=0,female=0,otherGender=0;

    Geocoder geocoder;

    String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    private ProgressDialog progressDialog;
    String formattedDate="",logDate="",savedate="";
    private RequestQueue requestQueue;
    Date c ;
    private String addressLocation="",imageCode="";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 1234;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private MyAppPrefsManager myAppPrefsManager;

    private Fragment fragment;
    private FragmentManager fragmentManager;
    private DbHelper dbHelper;
    private SocialMonitoringModel socialMonitoringModel;
    private final int GALLERY = 123;
    private final int STORAGE=1234;

     public static boolean   isKitKat;

     private String date="",image="",attendees="",mom="",gp="",district="",village="",block="",location="",state="",id="",femalebundle="",event="",
                        malebundle="",othersbundle="",email="",others="";

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1002;
    private static final int PERMISSION_REQUEST_CODE = 1003;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    double loclatitude=0,locLongitude=0;
    private int mYear, mMonth, mDay;
    private LinearLayout linearOthers;
    private ImageView imageview;


    public SocialMonitoring()
    {
        // Required empty public constructor
    }
    UploadImgViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_community_participation, container, false);
        viewModel = new ViewModelProvider(this).get(UploadImgViewModel.class);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionSocialMonitoring));
        isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        progressDialog=new ProgressDialog(getContext());
        pickiT = new PickiT(getContext(), this, getActivity());
        progressDialog.setTitle("Please Wait...");
        progressDialog.setMessage("While inserting Data...");
        progressDialog.setCancelable(false);

        requestQueue= Volley.newRequestQueue(getContext());
        myAppPrefsManager=new MyAppPrefsManager(getContext());
        dbHelper = new DbHelper(getContext());
        fragmentManager = getActivity().getSupportFragmentManager();
        socialMonitoringModel = new SocialMonitoringModel();
        textViewState=view.findViewById(R.id.editTextState);
        textViewDistrict=view.findViewById(R.id.editTextDistrict);
        textViewBlock=view.findViewById(R.id.editTextBlock);
        textViewGramPanchayat=view.findViewById(R.id.editTextGramPanchayat);
        editTextVillage=view.findViewById(R.id.editTextVillage);
        textViewDate=view.findViewById(R.id.textViewDate);
        editTextLocation=view.findViewById(R.id.editTextLocation);

        textViewPhotographsFileName=view.findViewById(R.id.textViewPhotographsFileName);
        textViewMOMFileName=view.findViewById(R.id.textViewMOMFileName);
        editTextNoOfAttendees=view.findViewById(R.id.editTextNoOfAttendees);
        spinnerEvent = view.findViewById(R.id.spinnerEvent);

        tvlatitude = view.findViewById(R.id.latitude);
        tvlongitude = view.findViewById(R.id.longitude);

        editTextMale=view.findViewById(R.id.editTextMale);
        editTextFeMale=view.findViewById(R.id.editTextFemale);
        editTextOtherGender=view.findViewById(R.id.editTextOthergender);
        editTextOthers=view.findViewById(R.id.editTextOthers);
        buttonMOM=view.findViewById(R.id.buttonMOM);
        linearOthers = view.findViewById(R.id.linearOthers);
        buttonSave=view.findViewById(R.id.buttonSave);
        imageview=view.findViewById(R.id.image1);


        textViewState.setText(myAppPrefsManager.getState());
        textViewDistrict.setText(myAppPrefsManager.getDistrict());
        textViewBlock.setText(myAppPrefsManager.getBlock());
        textViewGramPanchayat.setText(myAppPrefsManager.getGrampanchayat());

        tvlatitude.setText(myAppPrefsManager.getLatitude());
        tvlongitude.setText(myAppPrefsManager.getLongitude());

        eventAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_dd_item,R.id.simple_spinner_dropdown, getResources().getStringArray(R.array.event)) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return getView(position, convertView, parent);
            }
        };
        spinnerEvent.setAdapter(eventAdapter);

        c= Calendar.getInstance().getTime();
        df= new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        df2= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH);
        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
         formattedDate = df.format(c).toUpperCase();
         savedate = df2.format(c).toUpperCase();
         logDate = df1.format(c).toUpperCase();

        //textViewDate.setText(formattedDate);
        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        date=dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        textViewDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        if(getArguments()!=null) {

            date=getArguments().getString("date");
            image=getArguments().getString("image");
            attendees=getArguments().getString("attendees");
            mom=getArguments().getString("mom");
            gp=getArguments().getString("gp");
            district=getArguments().getString("district");
            block=getArguments().getString("block");
            village=getArguments().getString("village");
            location=getArguments().getString("location");
            state=getArguments().getString("state");
            id=getArguments().getString("id");
            femalebundle=getArguments().getString("female");
            event=getArguments().getString("event");
            malebundle=getArguments().getString("male");
            othersbundle=getArguments().getString("others");
            email=getArguments().getString("email");

            textViewDate.setText(date);
            editTextVillage.setText(village);
            editTextLocation.setText(location);
            int eventPos = eventAdapter.getPosition(event);
            if (event.equals("Others")){
                linearOthers.setVisibility(View.VISIBLE);
                editTextOthers.setText(getArguments().getString("otherevent"));
            }else {
                linearOthers.setVisibility(View.GONE);
            }
            spinnerEvent.setSelection(eventPos);
            editTextMale.setText(malebundle);
            editTextFeMale.setText(femalebundle);
            editTextOtherGender.setText(othersbundle);
            editTextNoOfAttendees.setText(attendees);

            if (mom!=null){
                serverFilePathMom = mom;
                textViewMOMFileName.setText(mom);
            }
            if (!image.equals("")){
                //textViewPhotographsFileName.setText(image);
                Glide.with(getActivity()).load(image).into(imageview);
                serverImagePath=image;
            }else{
                imageview.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
            }
        }

        getAddress();

        //checkPermisson();

        editTextMale.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(editTextMale.getText().toString().trim())
                        || !TextUtils.isEmpty(editTextFeMale.getText().toString().trim())
                        || !TextUtils.isEmpty(editTextOtherGender.getText().toString().trim())
                ) {


                    int firtValue = TextUtils.isEmpty(editTextMale.getText().toString().trim()) ? 0 : Integer.parseInt(editTextMale.getText().toString().trim());
                    int secondValue = TextUtils.isEmpty(editTextFeMale.getText().toString().trim()) ? 0 : Integer.parseInt(editTextFeMale.getText().toString().trim());
                    int thirdValue = TextUtils.isEmpty(editTextOtherGender.getText().toString().trim()) ? 0 : Integer.parseInt(editTextOtherGender.getText().toString().trim());

                    int answer = firtValue + secondValue + thirdValue ;

                    editTextNoOfAttendees.setText(String.valueOf(answer));
                }else {
                    editTextNoOfAttendees.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextFeMale.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(editTextMale.getText().toString().trim())
                        || !TextUtils.isEmpty(editTextFeMale.getText().toString().trim())
                        || !TextUtils.isEmpty(editTextOtherGender.getText().toString().trim())
                ) {


                    int firtValue = TextUtils.isEmpty(editTextMale.getText().toString().trim()) ? 0 : Integer.parseInt(editTextMale.getText().toString().trim());
                    int secondValue = TextUtils.isEmpty(editTextFeMale.getText().toString().trim()) ? 0 : Integer.parseInt(editTextFeMale.getText().toString().trim());
                    int thirdValue = TextUtils.isEmpty(editTextOtherGender.getText().toString().trim()) ? 0 : Integer.parseInt(editTextOtherGender.getText().toString().trim());

                    int answer = firtValue + secondValue + thirdValue ;

                    editTextNoOfAttendees.setText(String.valueOf(answer));
                }else {
                    editTextNoOfAttendees.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextOtherGender.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(editTextMale.getText().toString().trim())
                        || !TextUtils.isEmpty(editTextFeMale.getText().toString().trim())
                        || !TextUtils.isEmpty(editTextOtherGender.getText().toString().trim())
                ) {


                    int firtValue = TextUtils.isEmpty(editTextMale.getText().toString().trim()) ? 0 : Integer.parseInt(editTextMale.getText().toString().trim());
                    int secondValue = TextUtils.isEmpty(editTextFeMale.getText().toString().trim()) ? 0 : Integer.parseInt(editTextFeMale.getText().toString().trim());
                    int thirdValue = TextUtils.isEmpty(editTextOtherGender.getText().toString().trim()) ? 0 : Integer.parseInt(editTextOtherGender.getText().toString().trim());

                    int answer = firtValue + secondValue + thirdValue ;

                    editTextNoOfAttendees.setText(String.valueOf(answer));
                }else {
                    editTextNoOfAttendees.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        spinnerEvent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                eventSelectedItem = spinnerEvent.getSelectedItem().toString();
                if (eventSelectedItem.equals("Others")){
                    linearOthers.setVisibility(View.VISIBLE);
                }else {
                    linearOthers.setVisibility(View.GONE);
                    others="";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonMOM.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showPictureDialog();
            }
        });

        imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissionsforGallery(getActivity())) {
                    imageCode="1";
                    Intent intent = new Intent(getContext(), CameraGeoActivity.class);
                    intent.putExtra("value","5");
                    startActivityForResult(intent, 4);
                }
            }
        });


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!editTextMale.getText().toString().trim().equals("")){
                    socialMonitoringModel.setMaleParticipants(editTextMale.getText().toString().trim());
                    male = Integer.parseInt(editTextMale.getText().toString().trim());
                }else {
                    socialMonitoringModel.setMaleParticipants("");
                }
                if (!editTextFeMale.getText().toString().trim().equals("")){
                    socialMonitoringModel.setFemaleParticipants(editTextFeMale.getText().toString().trim());
                    female = Integer.parseInt(editTextFeMale.getText().toString().trim());
                }else {
                    socialMonitoringModel.setFemaleParticipants("");
                }
                if (!editTextOtherGender.getText().toString().trim().equals("")){
                    otherGender = Integer.parseInt(editTextOtherGender.getText().toString().trim());
                    socialMonitoringModel.setOtherParticipants(editTextOtherGender.getText().toString().trim());

                }else {

                    socialMonitoringModel.setOtherParticipants("");
                }
                total = male+female+otherGender;

                if (!myAppPrefsManager.getCategory().equals("")) {
                    if (!textViewState.getText().toString().equals("")) {
                        if (!textViewDistrict.getText().toString().equals("")) {
                            if (!textViewBlock.getText().toString().equals("")) {
                                if (!textViewGramPanchayat.getText().toString().equals("")) {
                                    if (!editTextVillage.getText().toString().equals("")) {
                                        if (!textViewDate.getText().toString().equals("")) {
                                            if (!editTextLocation.getText().toString().trim().equals("")) {
                                                if (!editTextNoOfAttendees.getText().toString().trim().equals("")) {
                                                    if (!editTextMale.getText().toString().trim().equals("") || !editTextFeMale.getText().toString().trim().equals("") || !editTextOtherGender.getText().toString().trim().equals("")) {
                                                        if (total == Integer.parseInt(editTextNoOfAttendees.getText().toString())) {
                                                            if (!eventSelectedItem.equals("Select Event")) {
                                                                if (eventSelectedItem.equals("Others")) {
                                                                    if (!editTextOthers.getText().toString().trim().equals("")) {
                                                                        others = editTextOthers.getText().toString().trim();
                                                                        submit();
                                                                    } else {
                                                                        Toast.makeText(getContext(), "Enter Other Event Name", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                } else {
                                                                    submit();
                                                                }
                                                            } else {
                                                                Toast.makeText(getContext(), "Please Select Event", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {
                                                            Toast.makeText(getContext(), "No. of attendees should match with male/female/others participants", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        Toast.makeText(getContext(), "Enter Male /Female /Other Participants", Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    editTextNoOfAttendees.setError("Enter No of Attendees");
                                                    editTextNoOfAttendees.requestFocus();
                                                }
                                            } else {
                                                editTextLocation.setError("Enter Location Details");
                                                editTextLocation.requestFocus();
                                            }
                                        } else {
                                            Toast.makeText(getContext(), "Select Date", Toast.LENGTH_SHORT).show();
                                        }
                                    }else {
                                        editTextVillage.setError("Enter Village Name");
                                        editTextVillage.requestFocus();
                                    }
                                } else {
                                    Toast.makeText(getContext(), "Select Gram Panchayat", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "Select Block", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Select District", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Select State", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "Connect Internet and select nodal officer", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    private void submit(){
        if (filepathMOM.contains(".pdf") || filepathMOM.contains(".docx") || filepathMOM.contains(".jpeg") || filepathMOM.contains(".jpg") || !serverFilePathMom.equals("")) {
//            if (!imagePath.equals("") || !serverImagePath.equals("")) {

                socialMonitoringModel.setState(myAppPrefsManager.getState());
                socialMonitoringModel.setDistrict(myAppPrefsManager.getDistrict());
                socialMonitoringModel.setBlock(myAppPrefsManager.getBlock());
                socialMonitoringModel.setGrampanchayat(myAppPrefsManager.getGrampanchayat());
                socialMonitoringModel.setDate(textViewDate.getText().toString());
                socialMonitoringModel.setLocation(editTextLocation.getText().toString().trim());
                socialMonitoringModel.setLatitude(myAppPrefsManager.getLatitude());
                socialMonitoringModel.setLongitude(myAppPrefsManager.getLongitude());
                socialMonitoringModel.setNoOfAttendees(String.valueOf(total));
                socialMonitoringModel.setEvent(eventSelectedItem);
                socialMonitoringModel.setMom(filepathMOM);
                socialMonitoringModel.setImagePath(imagePath);
                socialMonitoringModel.setEmail(myAppPrefsManager.getUserEmail());
                socialMonitoringModel.setMomFilename(mom_filename);
                socialMonitoringModel.setImageFilename(photo_filename);
                socialMonitoringModel.setDatetime(savedate);
                socialMonitoringModel.setOtherEvent(others);


                if (isNetworkAvailable()) {

                    if (!id.equals("")) {

                        if (progressDialog != null && !progressDialog.isShowing()) {
                            progressDialog.show();
                        }

                        if (!imagePath.equals("")) {
                            insertImage();

                        } else {
                            updateData(id, socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                    socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(), serverFilePathMom,
                                    socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(), serverImagePath, savedate,others);
                        }

                    } else {
                        if (progressDialog != null && !progressDialog.isShowing()) {
                            progressDialog.show();
                        }
                        if (!imagePath.equals("")) {
                            insertImage();

                        } else {
                            if (!filepathMOM.equals(""))
                            {

                                new File(filepathMOM);
                                Uri fileMOM=Uri.fromFile(new File(filepathMOM));
                                String fileExtMOM=MimeTypeMap.getFileExtensionFromUrl(fileMOM.toString());


                                if (fileExtMOM.equals("pdf"))
                                {
                                    insertPdfFileMOM();
                                }else if ( fileExtMOM.equals("docx"))
                                {
                                    insertWordFileMOM();
                                }else if (fileExtMOM.equals("jpeg")||fileExtMOM.equals("jpg")){
                                    insertImageMOM();
                                } else {
                                    Toast.makeText(getContext(), "Please upload valid file For MOM", Toast.LENGTH_SHORT).show();
                                }


                            }
//                            insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                    socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
//                                    socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);

                        }

//                        insertImage();

                    }

                } else {
                    if (progressDialog != null && !progressDialog.isShowing()) {
                        progressDialog.show();
                    }

                    Log.d("kcr", String.valueOf(socialMonitoringModel));
                    boolean status = dbHelper.insertSocialMonitoring(socialMonitoringModel);
                    if (status == true) {

                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Data saved in offline mode and will sync with server once online", Toast.LENGTH_SHORT).show();

                        fragment = new DashBoardFragment();
                        fragmentManager.beginTransaction()
                                .remove(fragment)
                                .commit();
                        fragmentManager.popBackStack();

                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                    }
                }
//            }
//            else {
//                Toast.makeText(getContext(), "Please Capture Image", Toast.LENGTH_SHORT).show();
//            }
        } else {
            Toast.makeText(getContext(), "Please upload valid file For MOM", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        checkLocationPermission();

    }

    private void checkLocationPermission(){

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    //showAppSettings();
                    displayLocationSettingsRequest(getActivity());
                }

            }
        } else {
            //displayLocationSettings();
            displayLocationSettingsRequest(getActivity());

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);


        Task<LocationSettingsResponse> result=LocationServices.getSettingsClient(getActivity().getApplicationContext())
                .checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task)
            {

                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    //Toast.makeText(Selection.this, "GPS is on", Toast.LENGTH_SHORT).show();
                }
                catch  (ApiException apiException)
                {
                    switch (apiException.getStatusCode())
                    {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            ResolvableApiException resolvableApiException=(ResolvableApiException)apiException;
                            try {
                                resolvableApiException.startResolutionForResult(getActivity(),1001);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }

                }
            }
        });
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT)
                            .show();

                }
                break;

            case  REQUEST_CAMERA_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // close the app
                    Toast.makeText(getContext(), "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                    getActivity().finish();

                }else{

                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 4);*/
                    if (imageCode.equals("1")){

                        if (checkAndRequestPermissionsforGallery(getActivity())) {
                            Intent intent = new Intent(getContext(), CameraGeoActivity.class);
                            intent.putExtra("value","5");
                            startActivityForResult(intent, 4);
                        }

                    }else if (imageCode.equals("2")){

                        choosePhotoFromGallary();

                    }else if(imageCode.equals("3")) {
                        Intent intent = new Intent();
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("application/pdf");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION);
                        startActivityForResult(intent, 556);
                    }
                }
            break;



            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        if (requestCode == 101) {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Camera permission", Toast.LENGTH_SHORT)
                        .show();
                getActivity().finish();
            } else if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Storage permission",
                        Toast.LENGTH_SHORT).show();
                getActivity().finish();
            } else {
                if (imageCode.equals("1")){

                    if (checkAndRequestPermissionsforGallery(getActivity())) {
                        Intent intent = new Intent(getContext(), CameraGeoActivity.class);
                        intent.putExtra("value","5");
                        startActivityForResult(intent, 4);
                    }

                }else if (imageCode.equals("2")){

                        choosePhotoFromGallary();

                }else if(imageCode.equals("3")) {
                    Intent intent = new Intent();
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("application/pdf");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(intent, 556);
                }
            }
        }

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    Log.d("asd", "zxc1");
                    //Do the stuff that requires permission...
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        //Show permission explanation dialog...


                        // we can ask again

                        Log.d("asd", "zxc11");

                        // 2

                        askPermission();
                    } else {

                        // showAppSettings();

                        Log.d("asd", "zxc111");
                        //Never ask again selected, or device policy prohibits the app from having that permission.
                        //So, disable that feature, or fall back to another situation...
                    }
                }
            }
        }

    }

    public void getAddress()
    {
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(myAppPrefsManager.getLatitude()), Double.parseDouble(myAppPrefsManager.getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            addressLocation=address;
            editTextLocation.setText(address);
            Log.d("kcr","lat "+myAppPrefsManager.getLatitude()+":"+myAppPrefsManager.getLongitude());
            Log.d("kcr",addressLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (resultCode == getActivity().RESULT_CANCELED)
        {
            return;
        }*/
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {

                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    filepathMOM = getImagePath(getActivity().getApplicationContext(),imageUri);
                    textViewMOMFileName.setText(filepathMOM);
                    Log.d("kcr",""+filepathMOM);

                    Path path = null;
                    Path fileName = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        path = Paths.get(filepathMOM);
                        fileName = path.getFileName();
                        mom_filename = fileName.toString();
                    }else {
                        mom_filename = filepathMOM.substring(filepathMOM.indexOf("/")+1);
                    }
                    Log.d("kcr",""+mom_filename);


                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("kcr",e.getLocalizedMessage());
                    Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }if (requestCode == 4) {
        if (resultCode == Activity.RESULT_OK) {

            String path = data.getStringExtra("path");
            imagePath = path;
            bitmap = BitmapFactory.decodeFile(path);
            //textViewPhotographsFileName.setText(imagePath);
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            if (bitmap != null) {

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

                imageview.setImageBitmap(bitmap);
            }

            Path path1 = null;
            Path fileName = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                path1 = Paths.get(imagePath);
                fileName = path1.getFileName();
                photo_filename = fileName.toString();
            }else {
                photo_filename = imagePath.substring(imagePath.indexOf("/")+1);
            }


        }
        if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
            Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
        }
    }
        else if (requestCode == 556) {
            if (resultCode == RESULT_OK)
            {
                Uri selectedFileUri = data.getData();
                Log.d("kcrMOMURI", "" + selectedFileUri);
                pickiT.getPath(data.getData(), Build.VERSION.SDK_INT);//idi call chesaka adhi listnere ki call avutadi




                Log.d("KcrMomFilepath",""+filepathMOM);
                if (filepathMOM!=null&&!filepathMOM.equals(""))
                {

                    Log.d("kcrMOM", "" + filepathMOM);

                    textViewMOMFileName.setText(filepathMOM);
                }
                else
                {
                    Log.d("kcrMOM", "null");
                }
            }
            if(resultCode==RESULT_CANCELED){
                Toast.makeText(getActivity(), "File Upload Cancelled", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    getLocation();


                    break;

                case Activity.RESULT_CANCELED:
                    Toast.makeText(getContext(), "Please Enable Location", Toast.LENGTH_SHORT).show();
                    // displayLocationSettings();
                    displayLocationSettingsRequest(getContext());

            }
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        }
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {



                    // current location

                    loclatitude = location.getLatitude();
                    locLongitude = location.getLongitude();

                    myAppPrefsManager.setLatitude(String.valueOf(loclatitude));
                    myAppPrefsManager.setLongitude(String.valueOf(locLongitude));


                }

            }
        });
    }


    public String getImagePath(Context context, Uri uri){
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Log.d("kcrMOMProj",""+proj.length);
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        Log.d("kcrMOMCursor",""+cursor.toString());
        if(cursor != null)
        {
            if ( cursor.moveToFirst( ) )
            {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }


    private void  insertImage()
    {
        File pic = new File(imagePath);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverImagePath = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Image Upload "+photo_filename+" Successful");
                        Log.d("kcr",serverImagePath);
                        if (!filepathMOM.equals(""))
                                    {

                                        new File(filepathMOM);
                                        Uri fileMOM=Uri.fromFile(new File(filepathMOM));
                                        String fileExtMOM=MimeTypeMap.getFileExtensionFromUrl(fileMOM.toString());


                                        if (fileExtMOM.equals("pdf"))
                                        {
                                            insertPdfFileMOM();
                                        }else if ( fileExtMOM.equals("docx"))
                                        {
                                            insertWordFileMOM();
                                        }else if (fileExtMOM.equals("jpeg")||fileExtMOM.equals("jpg")){
                                            insertImageMOM();
                                        } else {
                                            Toast.makeText(getContext(), "Please upload valid file For MOM", Toast.LENGTH_SHORT).show();
                                        }


                                    }else{

                                        if (!id.equals("")){

                                            updateData(id,socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                                    socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
                                                    socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);

                                        }else {

                                            insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                                    socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
                                                    socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
                                        }



                                    }
                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == Status.LOADING) {
//                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Image Upload "+photo_filename+" Unsuccessful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//                .setMultipartFile("img", "multipart/form-data", new File(imagePath))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success")) {
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Image Upload "+photo_filename+" Successful");
//
//                                    serverImagePath = jsonObject.getString("path");
//                                    Log.d("kcr",serverImagePath);
//                                    if (!filepathMOM.equals(""))
//                                    {
//
//                                        new File(filepathMOM);
//                                        Uri fileMOM=Uri.fromFile(new File(filepathMOM));
//                                        String fileExtMOM=MimeTypeMap.getFileExtensionFromUrl(fileMOM.toString());
//
//
//                                        if (fileExtMOM.equals("pdf"))
//                                        {
//                                            insertPdfFileMOM();
//                                        }else if ( fileExtMOM.equals("docx"))
//                                        {
//                                            insertWordFileMOM();
//                                        }else if (fileExtMOM.equals("jpeg")||fileExtMOM.equals("jpg")){
//                                            insertImageMOM();
//                                        } else {
//                                            Toast.makeText(getContext(), "Please upload valid file For MOM", Toast.LENGTH_SHORT).show();
//                                        }
//
//
//                                    }else{
//
//                                        if (!id.equals("")){
//
//                                            updateData(id,socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                                    socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
//                                                    socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
//
//                                        }else {
//
//                                            insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                                    socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
//                                                    socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
//                                        }
//
//
//
//                                    }
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Image Upload "+photo_filename+" Unsuccessful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse", e.toString());
//                            }else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//
//
//                    }
//                });
    }


    private void insertPdfFileMOM() {

        File pic = new File(filepathMOM);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.savePdfFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverFilePathMom = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Pdf file Upload "+mom_filename+" Successful");
                        Log.d("kcr",serverFilePathMom);
                        if (!id.equals("")){
                                        updateData(id,socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
                                                socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
                                    }else {

                                        insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(), serverFilePathMom, socialMonitoringModel.getNoOfAttendees(),
                                                socialMonitoringModel.getEvent(), serverImagePath,savedate,others);
                                    }
//
                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == Status.LOADING) {
//                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Pdf file Upload "+mom_filename+" Unsuccessful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });

//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL+"pdfupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//                .setMultipartFile("application/pdf", "multipart/form-data", new File(filepathMOM))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<Response<String>>()
//                {
//                    @Override
//                    public void onCompleted(Exception e, Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success"))
//                                {
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Pdf file Upload "+mom_filename+" Successful");
//
//                                    serverFilePathMom = jsonObject.getString("path");
//                                    Log.d("kcr",serverFilePathMom);
//
//                                    if (!id.equals("")){
//                                        updateData(id,socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
//                                                socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
//                                    }else {
//
//                                        insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(), serverFilePathMom, socialMonitoringModel.getNoOfAttendees(),
//                                                socialMonitoringModel.getEvent(), serverImagePath,savedate,others);
//                                    }
//
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Pdf file Upload "+mom_filename+" Unsuccessful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelsepdf", e.toString());
//                            }else {
//                                Log.d("kcrelsepdf", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//
//
//                    }
//                });
    }


    private void  insertWordFileMOM(){
        File pic = new File(filepathMOM);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveWordFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverFilePathMom = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Word file Upload "+mom_filename+" Successful");
                        Log.d("kcr",serverFilePathMom);
                        if (!id.equals("")){
                                        updateData(id,socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
                                                socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
                                    }else {
                                        insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(), serverFilePathMom, socialMonitoringModel.getNoOfAttendees(),
                                                socialMonitoringModel.getEvent(), serverImagePath,savedate,others);
                                    }
//
                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == Status.LOADING) {
//                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Word file Upload "+mom_filename+" Unsuccessful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });

//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL+"wordupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//                .setMultipartFile("application/pdf", "multipart/form-data", new File(filepathMOM))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success")) {
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Word file Upload "+mom_filename+" Successful");
//
//                                    serverFilePathMom = jsonObject.getString("path");
//                                    Log.d("kcr",serverFilePathMom);
//
//                                    if (!id.equals("")){
//                                        updateData(id,socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
//                                                socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
//                                    }else {
//                                        insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(), serverFilePathMom, socialMonitoringModel.getNoOfAttendees(),
//                                                socialMonitoringModel.getEvent(), serverImagePath,savedate,others);
//                                    }
//
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Word file Upload "+mom_filename+" Unsuccessful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse", e.toString());
//                            }else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//                    }
//                });
    }

    private void insertImageMOM()
    {
        File pic = new File(filepathMOM);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverFilePathMom = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Image Upload "+mom_filename+" Successful");
                        Log.d("kcr",serverFilePathMom);
                        if (!id.equals("")){
                                        updateData(id,socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
                                                socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
                                    }else{
                                        insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom, socialMonitoringModel.getNoOfAttendees(),
                                                socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
                                    }
                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == Status.LOADING) {
//                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Image Upload "+mom_filename+" Successful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL+"imageupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//                .setMultipartFile("application/pdf", "multipart/form-data", new File(filepathMOM))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<Response<String>>()
//                {
//                    @Override
//                    public void onCompleted(Exception e, Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success"))
//                                {
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Image Upload "+mom_filename+" Successful");
//
//                                    serverFilePathMom = jsonObject.getString("path");
//                                    Log.d("kcr",serverFilePathMom);
//
//                                    if (!id.equals("")){
//                                        updateData(id,socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom,
//                                                socialMonitoringModel.getNoOfAttendees(), socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
//                                    }else{
//                                        insertCommunityParticipation(socialMonitoringModel.getLocation(), socialMonitoringModel.getMaleParticipants(),
//                                                socialMonitoringModel.getFemaleParticipants(), socialMonitoringModel.getOtherParticipants(),serverFilePathMom, socialMonitoringModel.getNoOfAttendees(),
//                                                socialMonitoringModel.getEvent(),serverImagePath,savedate,others);
//                                    }
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Image Upload "+mom_filename+" Successful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse img", e.toString());
//                            }else {
//                                Log.d("kcrelse img", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//                    }
//                });
    }

    public void insertCommunityParticipation(final String location,final String male,final String female,final String others,
                                             final String mom,final String attendees,final String event,
                                             final String imagePath,final String datetime,final String otherEvent){
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"insertsocialmonitoring.jsp",
                new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                Log.d("kcr",response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("status").equals("success"))
                    {
                        progressDialog.dismiss();

                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Data Inserted Successfully");

                        Toast.makeText(getContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();

                        fragment = new DashBoardFragment();
                        fragmentManager.beginTransaction()
                                .remove(fragment)
                                .commit();
                        fragmentManager.popBackStack();



                    }else {
                        Log.d("kcr",jsonObject.getString("status"));
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    if(progressDialog!=null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }
                    Log.d("kcr",e.getLocalizedMessage());
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(progressDialog!=null && progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                Log.d("kcr",error.getLocalizedMessage());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();
                params.put("state",myAppPrefsManager.getState());
                params.put("district",myAppPrefsManager.getDistrict());
                params.put("block",myAppPrefsManager.getBlock());
                params.put("grampanchayat",myAppPrefsManager.getGrampanchayat());
                params.put("location",location);
                params.put("date",formattedDate);
                params.put("numberofattendees",attendees);
                params.put("male",male);
                params.put("female",female);
                params.put("others",others);
                params.put("event",event);
                params.put("minutesofmeeting",mom);
                params.put("image",imagePath);
                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("email",myAppPrefsManager.getUserEmail());
                params.put("datetime",datetime);
                params.put("otherevent",otherEvent);

                return params;
            }

        };
        requestQueue.add(stringRequest);
    }

    public void updateData(final String id,final String location,final String male,final String female,final String others,
                                             final String mom,final String attendees,final String event,
                                             final String imagePath,final String datetime,final String otherEvent){
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"updatesocialmonitoring.jsp",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("kcr",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("status").equals("success"))
                            {
                                progressDialog.dismiss();

                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Data updated Successfully");

                                Toast.makeText(getContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();

                                fragment = new DashBoardFragment();
                                fragmentManager.beginTransaction()
                                        .remove(fragment)
                                        .commit();
                                fragmentManager.popBackStack();



                            }else {
                                Log.d("kcr",jsonObject.getString("status"));
                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Social Monitoring","Data update failed");
                            }
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                            if(progressDialog!=null && progressDialog.isShowing())
                            {
                                progressDialog.dismiss();
                            }
                            Log.d("kcr",e.getLocalizedMessage());
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(progressDialog!=null && progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                Log.d("kcr",error.getLocalizedMessage());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();
                params.put("id",id);
                params.put("state",myAppPrefsManager.getState());
                params.put("district",myAppPrefsManager.getDistrict());
                params.put("block",myAppPrefsManager.getBlock());
                params.put("grampanchayat",myAppPrefsManager.getGrampanchayat());
                params.put("location",location);
                params.put("date",formattedDate);
                params.put("numberofattendees",attendees);
                params.put("male",male);
                params.put("female",female);
                params.put("others",others);
                params.put("event",event);
                params.put("minutesofmeeting",mom);
                params.put("image",imagePath);
                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("email",myAppPrefsManager.getUserEmail());
                params.put("datetime",datetime);
                params.put("otherevent",otherEvent);

                return params;
            }

        };
        requestQueue.add(stringRequest);
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private void showPictureDialog()
    {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Select document from storage"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        switch (which) {
                            case 0:
                                if (checkAndRequestPermissionsforGallery(getContext())) {
                                    imageCode="2";
                                    choosePhotoFromGallary();
                                }
                                break;
                            case 1:
                                if (checkAndRequestPermissionsforGallery(getContext())) {
                                    imageCode="3";
                                    Intent intent = new Intent();
                                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                                    intent.setType("application/pdf");
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION);
                                    //  intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                                    //startActivityForResult(Intent.createChooser(intent,"Choose File to upload..."), 6);
                                    startActivityForResult(intent, 556);
                                    break;
                                }
                        }
                    }
                });
        pictureDialog.show();
    }

    private void choosePhotoFromGallary(){

        if (checkAndRequestPermissionsforGallery(getActivity())){
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(galleryIntent, GALLERY);
        }

    }

   /* private void galleryPermission(){


        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY);
        } else {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(galleryIntent, GALLERY);
        }
    }*/

    private static boolean checkAndRequestPermissionsforGallery(final Context context) {

        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    101);
            return false;
        }
        return true;
    }


    public String saveImage(Bitmap myBitmap)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"AtalJal");
        // have the object build the directory structure, if needed.
        if (!directory.exists()) {
            directory.mkdirs();
        }


        try {
            File f = new File(directory, "ataljal_"+Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("kcr", "File Saved::--->" + f.getAbsolutePath());
            if (imageCode.equals("1")) {
                imagePath = f.getAbsolutePath();
                textViewPhotographsFileName.setText(imagePath);
                Path path = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = Paths.get(imagePath);
                    fileName = path.getFileName();
                    photo_filename = fileName.toString();
                }else {
                    photo_filename = imagePath.substring(imagePath.indexOf("/")+1);
                }
                Log.d("kcr",""+photo_filename);

            }
            return f.getAbsolutePath();
        } catch (IOException e1)
        {
            e1.printStackTrace();
        }
        return "";
    }

    @Override
    public void PickiTonUriReturned()
    {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Waiting to receive file...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    @Override
    public void PickiTonStartListener()
    {
        if (progressDialog.isShowing()){
            progressDialog.cancel();
        }

    }

    @Override
    public void PickiTonProgressUpdate(int progress)
    {
        String progressPlusPercent = progress + "%";
       // percentText.setText(progressPlusPercent);
        progressDialog.setProgress(progress);

    }

    @Override
    public void PickiTonCompleteListener(String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String Reason)
    {
        if (wasDriveFile){
            Log.d("kcr","wasDriveFile "+path);
            //Toast.makeText(getContext(), "wasDriveFile"+path, Toast.LENGTH_SHORT).show();
        }else if (wasUnknownProvider){
            Log.d("kcr","wasUnknownProvider "+path);
            //Toast.makeText(getContext(), "wasUnknownProvider"+path, Toast.LENGTH_SHORT).show();
        }else {
            Log.d("kcr","else "+path);
            //Toast.makeText(getContext(), "else"+path, Toast.LENGTH_SHORT).show();
        }
        if(wasSuccessful)
        {
            filepathMOM=path;
            textViewMOMFileName.setText(path);

            Path pathPDF = null;
            Path fileName = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                pathPDF = Paths.get(filepathMOM);
                fileName = pathPDF.getFileName();
                mom_filename = fileName.toString();
            }else {
                mom_filename = filepathMOM.substring(filepathMOM.indexOf("/")+1);

            }
            Log.d("kcr",mom_filename);
        }
        else {
            Log.d("kcr",""+path);
            //Toast.makeText(getContext(), ""+path, Toast.LENGTH_SHORT).show();
        }

    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);




                return params;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

}

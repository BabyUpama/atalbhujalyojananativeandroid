package in.co.gcrs.ataljal.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import androidx.lifecycle.Observer;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.activities.CameraGeoActivity;
import in.co.gcrs.ataljal.activities.Selection;
import in.co.gcrs.ataljal.api.Resource;
import in.co.gcrs.ataljal.api.Status;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.constant.ConstantValues;
import in.co.gcrs.ataljal.database.DbHelper;
import in.co.gcrs.ataljal.entity.UploadImgEntity;
import in.co.gcrs.ataljal.model.WellInventoryModel;
import in.co.gcrs.ataljal.myviewmodel.UploadImgViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class WellInventory extends Fragment
{

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1002;

    private EditText editTextWellNo,editTextVillage,editTextLocationDetials,
            editTextSiteName,editTextOwnerName,editTextDiameterOfWell,
            editTextDepthOfWell,editTextHeightOfMeasuringPoint,editTextDepthToWaterLevel;

    private TextView textViewDate,textViewLatitude,textViewLongitude,textViewState,textViewDistrict,textViewBlock,textViewGramPanchayat;
    private String date="";
    private String dateDb="";
    private int mYear, mMonth, mDay, mHour, mMinute;
    private LinearLayout linearLayoutPumpUse;

    private Spinner spinnerWellType,spinnerUse;
    private ArrayAdapter<String> spinnerWellTypeAdapter,spinnerUseAdapter;
    private String spinnerWellTypeSelectedItem="",spinnerUseSelectedItem="";

    private RadioGroup radioGroupWellWaterPotable,radioGroupPumpInstalled,radioGroupElectricMeter;
    private RadioButton radioButtonWellWaterPotableYes,radioButtonWellWaterPotableNo,radioButtonPumpInstalledYes,radioButtonPumpInstalledNo,
            radioButtonElectricMeterYes,radioButtonElectricMeterNo;

    private String wellWaterPotableSelectedItem="",pumpInstalledSelectedItem="",electricMeterSelectedItem="",imagePath="",serverPath="",fileName1="";
    private String id="",wellno="",village="",sitename="",locationdetails="",ownername="",
            welldiameter="",welldepth="",measuringpoint="",welltype="",wellwaterpotable="",waterleveldepth="",
            use="", pumpinstalled="",electricmeterselecteditem="";

    private RequestQueue requestQueue;

    ProgressDialog progressDialog;
    private MyAppPrefsManager myAppPrefsManager;
    private DbHelper dbHelper;
    private WellInventoryModel wellInventoryModel;

    private Button buttonSave;
    Geocoder geocoder;
    private String addressLocation="";
    Date c ;
    String formattedDate="",logDate="";
    SimpleDateFormat df,df1;
    Fragment fragment;
    FragmentManager fragmentManager;
    private ImageView imageView;
    private Bitmap bitmap;
    private String code = "";
    private RelativeLayout rvimage;
    public WellInventory()
    {
        // Required empty public constructor
    }

    UploadImgViewModel viewModel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_well_inventory, container, false);
        viewModel = new ViewModelProvider(this).get(UploadImgViewModel.class);
        fragmentManager = getActivity().getSupportFragmentManager();

        progressDialog=new ProgressDialog(getContext());
        progressDialog.setMessage("please wait...");
        progressDialog.setCancelable(false);

        myAppPrefsManager = new MyAppPrefsManager(getContext());
        requestQueue= Volley.newRequestQueue(getContext());
        dbHelper = new DbHelper(getContext());
        wellInventoryModel = new WellInventoryModel();

        c= Calendar.getInstance().getTime();
        df= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH);
        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        formattedDate = df.format(c).toUpperCase();
        logDate = df1.format(c).toUpperCase();

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionWellInventory));



        editTextWellNo=view.findViewById(R.id.editTextWellNo);
        textViewState=view.findViewById(R.id.editTextState);
        textViewDistrict=view.findViewById(R.id.editTextDistrict);
        textViewBlock=view.findViewById(R.id.editTextBlock);
        textViewGramPanchayat=view.findViewById(R.id.editTextGramPanchayat);
        editTextVillage=view.findViewById(R.id.editTextVillage);
        editTextSiteName=view.findViewById(R.id.editTextSiteName);
        editTextLocationDetials=view.findViewById(R.id.editTextLocationDetials);
        editTextOwnerName=view.findViewById(R.id.editTextOwnerName);
        textViewLatitude=view.findViewById(R.id.textViewLatitude);
        textViewLongitude=view.findViewById(R.id.textViewLongitude);
        editTextDiameterOfWell=view.findViewById(R.id.editTextDiameterOfWell);
        editTextDepthOfWell=view.findViewById(R.id.editTextDepthOfWell);
        editTextHeightOfMeasuringPoint=view.findViewById(R.id.editTextHeightOfMeasuringPoint);
        editTextDepthToWaterLevel=view.findViewById(R.id.editTextDepthToWaterLevel);
        textViewDate=view.findViewById(R.id.textViewDate);
        imageView=view.findViewById(R.id.image);
        linearLayoutPumpUse=view.findViewById(R.id.linearLayoutPumpUse);

        spinnerWellType=view.findViewById(R.id.spinnerWellType);
        spinnerUse=view.findViewById(R.id.spinnerUse);
        buttonSave=view.findViewById(R.id.buttonSave);



        radioGroupWellWaterPotable=view.findViewById(R.id.radioGroupWellWaterPotable);
        radioGroupPumpInstalled=view.findViewById(R.id.radioGroupPumpInstalled);
        radioGroupElectricMeter=view.findViewById(R.id.radioGroupElectricMeter);
        radioButtonWellWaterPotableYes=view.findViewById(R.id.radioButtonWellWaterPotableYes);
        radioButtonWellWaterPotableNo=view.findViewById(R.id.radioButtonWellWaterPotableNo);
        radioButtonPumpInstalledYes=view.findViewById(R.id.radioButtonPumpInstalledYes);
        radioButtonPumpInstalledNo=view.findViewById(R.id.radioButtonPumpInstalledNo);
        radioButtonElectricMeterYes=view.findViewById(R.id.radioButtonElectricMeterYes);
        radioButtonElectricMeterNo=view.findViewById(R.id.radioButtonElectricMeterNo);



        textViewLatitude.setText(myAppPrefsManager.getLatitude());
        textViewLongitude.setText(myAppPrefsManager.getLongitude());

        spinnerWellTypeAdapter=new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.well_type_well_inventory));
        spinnerWellTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWellType.setAdapter(spinnerWellTypeAdapter);

        spinnerUseAdapter=new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.use_type));
        spinnerUseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUse.setAdapter(spinnerUseAdapter);




        spinnerUse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                spinnerUseSelectedItem=parent.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerWellType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                spinnerWellTypeSelectedItem=parent.getSelectedItem().toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        date=dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        textViewDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }

        });




        radioGroupWellWaterPotable.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (radioGroupWellWaterPotable.getCheckedRadioButtonId()!=-1){

                    if (radioGroupWellWaterPotable.getCheckedRadioButtonId()==R.id.radioButtonWellWaterPotableYes){

                        wellWaterPotableSelectedItem = "Yes";
                    }else {
                        wellWaterPotableSelectedItem="No";
                    }
                }

            }
        });


        radioGroupPumpInstalled.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                if (radioGroupPumpInstalled.getCheckedRadioButtonId()!=-1){

                    if (radioGroupPumpInstalled.getCheckedRadioButtonId()==R.id.radioButtonPumpInstalledYes){
                        pumpInstalledSelectedItem = "Yes";
                    }else {
                        pumpInstalledSelectedItem="No";
                    }

                }
            }
        });


        radioGroupElectricMeter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                if (radioGroupElectricMeter.getCheckedRadioButtonId()!=-1){
                    if (radioGroupElectricMeter.getCheckedRadioButtonId()==R.id.radioButtonElectricMeterYes){
                        electricMeterSelectedItem = "Yes";
                    }else {
                        electricMeterSelectedItem="No";
                    }
                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(getContext(),CameraGeoActivity.class);
                    intent.putExtra("value","1");
                    startActivityForResult(intent, 1);
                }
                /*code = "1";
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                }*/
            }
        });

        getAddress();
        textViewState.setText(myAppPrefsManager.getState());
        textViewDistrict.setText(myAppPrefsManager.getDistrict());
        textViewBlock.setText(myAppPrefsManager.getBlock());
        textViewGramPanchayat.setText(myAppPrefsManager.getGrampanchayat());

        if (getArguments()!=null) {
            id=getArguments().getString("id");
            date=getArguments().getString("date");
            wellno=getArguments().getString("wellno");
            village=getArguments().getString("village");
            locationdetails=getArguments().getString("locationdetails");
            sitename=getArguments().getString("sitename");
            //welltype=getArguments().getString("welltype");
            ownername=getArguments().getString("ownername");
            welldiameter=getArguments().getString("diameter");
            welldepth=getArguments().getString("welldepth");
            measuringpoint=getArguments().getString("measuringpoint");
            wellwaterpotable=getArguments().getString("wellwaterpotable");
            waterleveldepth=getArguments().getString("waterlevel");
            //use=getArguments().getString("use");
            pumpinstalled=getArguments().getString("pumpinstalled");
            electricmeterselecteditem=getArguments().getString("electricmeterselecteditem");
            String image1 =getArguments().getString("image");


            if (!image1.equals("")){
                serverPath=image1;
                Glide.with(getContext()).load(image1).into(imageView);
            }else {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
            }
            textViewDate.setText(date);
            editTextVillage.setText(village);
            editTextWellNo.setText(wellno);
            editTextLocationDetials.setText(locationdetails);
            editTextSiteName.setText(sitename);
            editTextOwnerName.setText(ownername);
            editTextDiameterOfWell.setText(welldiameter);
            editTextDepthOfWell.setText(welldepth);
            editTextHeightOfMeasuringPoint.setText(measuringpoint);
            editTextDepthToWaterLevel.setText(waterleveldepth);

            int wellTypepos = spinnerWellTypeAdapter.getPosition(getArguments().getString("welltype"));
            spinnerWellType.setSelection(wellTypepos);
            int usepos = spinnerUseAdapter.getPosition(getArguments().getString("use"));
            spinnerUse.setSelection(usepos);
            if(wellwaterpotable.equals("Yes")) {
                radioButtonWellWaterPotableYes.setChecked(true);
            } else {
                radioButtonWellWaterPotableNo.setChecked(true);
            }

            if(pumpinstalled.equals("Yes")) {
                radioButtonPumpInstalledYes.setChecked(true);
            } else {
                radioButtonPumpInstalledNo.setChecked(true);
            }

            if(electricmeterselecteditem.equals("Yes")) {
                radioButtonElectricMeterYes.setChecked(true);
            } else {
                radioButtonElectricMeterNo.setChecked(true);
            }
        }

        buttonSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!myAppPrefsManager.getCategory().equals("")) {
                    if (!editTextWellNo.getText().toString().equals("")) {
                            if (!date.equals("")) {
                                if (!textViewState.getText().toString().equals("")) {
                                    if (!textViewDistrict.getText().toString().equals("")) {
                                        if (!textViewBlock.getText().toString().equals("")) {
                                            if (!textViewGramPanchayat.getText().toString().equals("")) {
                                                if (!editTextVillage.getText().toString().equals("")) {
//                                                    if (!editTextSiteName.getText().toString().equals("")) {
                                                        if (!editTextLocationDetials.getText().toString().equals("")) {
                                                            if (!editTextOwnerName.getText().toString().equals("")) {
                                                                if (!textViewLatitude.getText().toString().equals("")) {
                                                                    if (!textViewLongitude.getText().toString().equals("")) {
                                                                        if (!editTextDiameterOfWell.getText().toString().trim().equals("")&&!editTextDiameterOfWell.getText().toString().trim().startsWith(".")) {
                                                                            if (!editTextDepthOfWell.getText().toString().trim().equals("")&&!editTextDepthOfWell.getText().toString().trim().startsWith(".")) {
                                                                                if (!editTextHeightOfMeasuringPoint.getText().toString().trim().equals("")&&!editTextHeightOfMeasuringPoint.getText().toString().trim().startsWith(".")) {
                                                                                    if (!spinnerWellTypeSelectedItem.equals("Select Well Type")) {
                                                                                        if (!wellWaterPotableSelectedItem.equals("")) {
                                                                                            if (!editTextDepthToWaterLevel.getText().toString().trim().equals("")&&!editTextDepthToWaterLevel.getText().toString().trim().startsWith(".")) {
                                                                                                if (!spinnerUseSelectedItem.equals("Select Use")) {
                                                                                                    if (!spinnerUseSelectedItem.equals("")) {
                                                                                                        if(!pumpInstalledSelectedItem.equals("")) {
                                                                                                            if(!electricMeterSelectedItem.equals("")) {
//                                                                                                                if(!imagePath.equals("")||!serverPath.equals("")) {

                                                                                                                wellInventoryModel.setWellNo(editTextWellNo.getText().toString().trim());
                                                                                                                wellInventoryModel.setDate(date);
                                                                                                                wellInventoryModel.setState(textViewState.getText().toString().trim());
                                                                                                                wellInventoryModel.setDistrict(textViewDistrict.getText().toString().trim());
                                                                                                                wellInventoryModel.setBlock(textViewBlock.getText().toString().trim());
                                                                                                                wellInventoryModel.setGramPanchayat(textViewGramPanchayat.getText().toString().trim());
                                                                                                                wellInventoryModel.setVillage(editTextVillage.getText().toString().trim());
                                                                                                                wellInventoryModel.setSiteName(editTextSiteName.getText().toString().trim());
                                                                                                                wellInventoryModel.setLocationDetails(editTextLocationDetials.getText().toString().trim());
                                                                                                                wellInventoryModel.setOwnerName(editTextOwnerName.getText().toString().trim());
                                                                                                                wellInventoryModel.setLatitude(textViewLatitude.getText().toString());
                                                                                                                wellInventoryModel.setLongitude(textViewLongitude.getText().toString());
                                                                                                                wellInventoryModel.setWellDiameter(editTextDiameterOfWell.getText().toString());
                                                                                                                wellInventoryModel.setWellDepth(editTextDepthOfWell.getText().toString());
                                                                                                                wellInventoryModel.setMeasuringPoint(editTextHeightOfMeasuringPoint.getText().toString());
                                                                                                                wellInventoryModel.setWellType(spinnerWellTypeSelectedItem);
                                                                                                                wellInventoryModel.setWellWaterPotable(wellWaterPotableSelectedItem);
                                                                                                                wellInventoryModel.setWaterLevelDepth(editTextDepthToWaterLevel.getText().toString().trim());
                                                                                                                wellInventoryModel.setUse(spinnerUseSelectedItem);
                                                                                                                wellInventoryModel.setPumpInstalled(pumpInstalledSelectedItem);
                                                                                                                wellInventoryModel.setElectricMeterSelecteditem(electricMeterSelectedItem);
                                                                                                                wellInventoryModel.setDatetime(formattedDate);
                                                                                                                wellInventoryModel.setEmail(myAppPrefsManager.getUserEmail());
                                                                                                                wellInventoryModel.setImage(imagePath);
                                                                                                                wellInventoryModel.setImageName(fileName1);

                                                                                                                if (isNetworkAvailable()) {

                                                                                                                    if (!id.equals("")) {

                                                                                                                        if (progressDialog != null) {
                                                                                                                            progressDialog.show();
                                                                                                                        }

                                                                                                                        if (!imagePath.equals("")) {
                                                                                                                            insertImage1();
                                                                                                                        } else {

                                                                                                                            updateDataToDb(editTextWellNo.getText().toString(), textViewDate.getText().toString(), textViewState.getText().toString(), textViewDistrict.getText().toString(),
                                                                                                                                    textViewBlock.getText().toString(), textViewGramPanchayat.getText().toString(), editTextVillage.getText().toString(),
                                                                                                                                    editTextSiteName.getText().toString(), editTextLocationDetials.getText().toString(), editTextOwnerName.getText().toString(),
                                                                                                                                    editTextDiameterOfWell.getText().toString(),
                                                                                                                                    editTextDepthOfWell.getText().toString(), editTextHeightOfMeasuringPoint.getText().toString(), spinnerWellTypeSelectedItem,
                                                                                                                                    wellWaterPotableSelectedItem, editTextDepthToWaterLevel.getText().toString(), spinnerUseSelectedItem, pumpInstalledSelectedItem,
                                                                                                                                    electricMeterSelectedItem, serverPath);
                                                                                                                        }
                                                                                                                    } else {

                                                                                                                        if (progressDialog != null) {
                                                                                                                            progressDialog.show();
                                                                                                                        }
                                                                                                                        if (!imagePath.equals("")) {
                                                                                                                            insertImage1();
                                                                                                                        } else {

                                                                                                                            insertIntoDB(editTextWellNo.getText().toString(), date, textViewState.getText().toString(), textViewDistrict.getText().toString(),
                                                                                                                                    textViewBlock.getText().toString(), textViewGramPanchayat.getText().toString(), editTextVillage.getText().toString(),
                                                                                                                                    editTextSiteName.getText().toString(), editTextLocationDetials.getText().toString(), editTextOwnerName.getText().toString(),
                                                                                                                                    editTextDiameterOfWell.getText().toString(), editTextDepthOfWell.getText().toString(), editTextHeightOfMeasuringPoint.getText().toString(),
                                                                                                                                    spinnerWellTypeSelectedItem, wellWaterPotableSelectedItem, editTextDepthToWaterLevel.getText().toString(), spinnerUseSelectedItem,
                                                                                                                                    pumpInstalledSelectedItem, electricMeterSelectedItem,serverPath);
                                                                                                                        }
//                                                                                                                        insertImage1();

                                                                                                                    }
                                                                                                                } else {

                                                                                                                    if (id.equals("")) {
                                                                                                                        boolean status = dbHelper.insertWellInventory(wellInventoryModel);

                                                                                                                        if (progressDialog != null && !progressDialog.isShowing()) {
                                                                                                                            progressDialog.show();
                                                                                                                        }

                                                                                                                        if (status == true) {

                                                                                                                            Toast.makeText(getContext(), "Data saved in offline mode and will sync with server once online", Toast.LENGTH_SHORT).show();

                                                                                                                            if (progressDialog != null && progressDialog.isShowing()) {
                                                                                                                                progressDialog.dismiss();
                                                                                                                            }

                                                                                                                            fragment = new DashBoardFragment();
                                                                                                                            fragmentManager.beginTransaction()
                                                                                                                                    .remove(fragment)
                                                                                                                                    .commit();
                                                                                                                            fragmentManager.popBackStack();

                                                                                                                        } else {
                                                                                                                            if (progressDialog != null && progressDialog.isShowing()) {
                                                                                                                                progressDialog.dismiss();
                                                                                                                            }
                                                                                                                            Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                                                                                                                        }
                                                                                                                    } else {

                                                                                                                        Toast.makeText(getContext(), "Internet not available!", Toast.LENGTH_SHORT).show();
                                                                                                                    }
                                                                                                                }

//                                                                                                                else {
//                                                                                                                Toast.makeText(getContext(), "Please Upload image", Toast.LENGTH_SHORT).show();
//                                                                                                            }

                                                                                                        }else {
                                                                                                            Toast.makeText(getContext(), "Select Volumetric Electric meter Yes/No", Toast.LENGTH_SHORT).show();
                                                                                                        }
                                                                                                    }else
                                                                                                    {
                                                                                                        Toast.makeText(getContext(), "Pump Installed", Toast.LENGTH_SHORT).show();
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                Toast.makeText(getContext(), "Select Use ", Toast.LENGTH_SHORT).show();
                                                                                            }
                                                                                        } else {
                                                                                            editTextDepthToWaterLevel.setError("Enter Depth To Water Level");
                                                                                            editTextDepthToWaterLevel.requestFocus();
                                                                                        }
                                                                                    } else {
                                                                                        Toast.makeText(getContext(), "Select Well Water Yes/No", Toast.LENGTH_SHORT).show();
                                                                                    }
                                                                                } else {
                                                                                    Toast.makeText(getContext(), "Select Tyep of Well", Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            } else {
                                                                                editTextHeightOfMeasuringPoint.setError("Enter height of Measuring Pont");
                                                                                editTextHeightOfMeasuringPoint.requestFocus();
                                                                            }
                                                                        } else {
                                                                            editTextDepthOfWell.setError("Enter Depth of Well");
                                                                            editTextDepthOfWell.requestFocus();
                                                                        }
                                                                    } else {
                                                                        editTextDiameterOfWell.setError("Enter Diameter of Well");
                                                                        editTextDiameterOfWell.requestFocus();
                                                                    }
                                                                } else {
                                                                    textViewLongitude.setError("Enter Longitude");
                                                                    //textViewLongitude.requestFocus();
                                                                }
                                                            } else {
                                                                textViewLatitude.setError("Enter latitude");
                                                                //textViewLatitude.requestFocus();
                                                            }
                                                        } else {
                                                            editTextOwnerName.setError("Enter Owner Name");
                                                            editTextOwnerName.requestFocus();
                                                        }
                                                    } else {
                                                        editTextLocationDetials.setError("Enter Location Details");
                                                        editTextLocationDetials.requestFocus();
                                                    }
//                                                } else {
//                                                    editTextSiteName.setError("Enter Site Name");
//                                                    editTextSiteName.requestFocus();
//                                                }
                                            } else {
                                                editTextVillage.setError("Enter Village Name");
                                                editTextVillage.requestFocus();
                                            }
                                        } else {
                                            textViewGramPanchayat.setError("Enter GP Name");
                                            //textViewGramPanchayat.requestFocus();
                                        }
                                    } else {
                                        textViewBlock.setError("Enter Block Name");
                                        //textViewBlock.requestFocus();
                                    }
                                } else {
                                    textViewDistrict.setError("Enter District Name");
                                    //textViewDistrict.requestFocus();
                                }
                            } else {
                                textViewState.setError("Enter State Name");
                                //textViewState.requestFocus();
                            }
                        } else {
                            Toast.makeText(getContext(), "Select Date", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        editTextWellNo.setError("Enter Well No");
                        editTextWellNo.requestFocus();
                    }
                }else {
                    Toast.makeText(getContext(), "Connect Internet and select nodal officer", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 101) {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Camera permission", Toast.LENGTH_SHORT)
                        .show();
                getActivity().finish();
            } else if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Storage permission",
                        Toast.LENGTH_SHORT).show();
                getActivity().finish();
            } else {
                if ("1".equals(code)) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                }
            }
        }
    }


    public void insertIntoDB(final String wellno,final String date,final String state,final String district,
                             final String block,final String gp,final String village,
                             final String sitename,final String locationdetails,final String ownername,
                             final String welldiameter,
                             final String welldepth,final String measuringpoint,final String spinnerWellTypeSelectedItem,
                             final String wellWaterPotableSelectedItem,final String waterleveldepth,final String spinnerUseSelectedItem,final String pumpInstalledSelectedItem,
                             final String electricMeterSelectedItem,final String image) {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"insertwellinventory_test.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);


                    if (obj.getString("status").equals("success")) {

                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Well Inventory","Data Inserted Successfully");
                        Toast.makeText(getContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();

                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        /*editTextWellNo.setText("");
                        textViewDate.setText("");
                        editTextVillage.setText("");
                        editTextSiteName.setText("");
                        editTextOwnerName.setText("");
                        editTextDiameterOfWell.setText("");
                        editTextDepthOfWell.setText("");
                        editTextHeightOfMeasuringPoint.setText("");
                        spinnerWellType.setSelection(0);
                        radioGroupWellWaterPotable.clearCheck();
                        editTextDepthToWaterLevel.setText("");
                        spinnerUse.setSelection(0);
                        radioGroupPumpInstalled.clearCheck();
                        radioGroupElectricMeter.clearCheck();*/
                        fragment = new DashBoardFragment();
                        fragmentManager.beginTransaction()
                                .remove(fragment)
                                .commit();
                        fragmentManager.popBackStack();

                    }else {
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Well Inventory",obj.getString("status"));

                        Toast.makeText(getContext(), ""+obj.getString("status"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (progressDialog!=null&&progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }

                    Log.d("kcr", ""+e.getLocalizedMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();
                params.put("wellno",wellno);
                params.put("date",date);
                params.put("state",state);
                params.put("district",district);
                params.put("block",block);
                params.put("gp",gp);
                params.put("village",village);
                params.put("sitename",sitename);
                params.put("locationdetails",locationdetails);
                params.put("ownername",ownername);
                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("welldiameter",welldiameter);
                params.put("welldepth",welldepth);
                params.put("measuringpoint",measuringpoint);
                params.put("welltype",spinnerWellTypeSelectedItem);
                params.put("wellwaterpotable",wellWaterPotableSelectedItem);
                params.put("waterleveldepth",waterleveldepth);
                params.put("use",spinnerUseSelectedItem);
                params.put("pumpinstalled",pumpInstalledSelectedItem);
                params.put("electricmeterselecteditem",electricMeterSelectedItem);
                params.put("datetime",formattedDate);
                params.put("email",myAppPrefsManager.getUserEmail());
                params.put("image",image);
                Log.e("wellinventory",params.toString());
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    public void updateDataToDb(final String wellno,final String date,final String state,final String district,
                               final String block,final String gp,final String village,
                               final String sitename,final String locationdetails,final String ownername,
                               final String welldiameter,
                               final String welldepth,final String measuringpoint,final String spinnerWellTypeSelectedItem,
                               final String wellWaterPotableSelectedItem,final String waterleveldepth,final String spinnerUseSelectedItem,final String pumpInstalledSelectedItem,
                               final String electricMeterSelectedItem,final String image)
    {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"updatewellinventory.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                try {
                    Log.d("kcrwellinvenupd",response);
                    JSONObject obj = new JSONObject(response);


                    if (obj.getString("status").equals("success")) {

                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Well Inventory","Data Updated Successfully");

                        Toast.makeText(getContext(), "Data Updated Successfully", Toast.LENGTH_SHORT).show();

                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        /*editTextWellNo.setText("");
                        textViewDate.setText("");
                        editTextVillage.setText("");
                        editTextSiteName.setText("");
                        editTextOwnerName.setText("");
                        editTextDiameterOfWell.setText("");
                        editTextDepthOfWell.setText("");
                        editTextHeightOfMeasuringPoint.setText("");
                        spinnerWellType.setSelection(0);
                        radioGroupWellWaterPotable.clearCheck();
                        editTextDepthToWaterLevel.setText("");
                        spinnerUse.setSelection(0);
                        radioGroupPumpInstalled.clearCheck();
                        radioGroupElectricMeter.clearCheck();*/
                        fragment = new DashBoardFragment();
                        fragmentManager.beginTransaction()
                                .remove(fragment)
                                .commit();
                        fragmentManager.popBackStack();

                    }else {
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Well Inventory","Data Update Failed");

                        Toast.makeText(getContext(), ""+obj.getString("status"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (progressDialog!=null&&progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }

                    Log.d("kcr", ""+e.getLocalizedMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();
                params.put("id",id);
                params.put("wellno",wellno);
                params.put("date",date);
                params.put("state",state);
                params.put("district",district);
                params.put("block",block);
                params.put("gp",gp);
                params.put("village",village);
                params.put("sitename",sitename);
                params.put("locationdetails",locationdetails);
                params.put("ownername",ownername);
                params.put("welldiameter",welldiameter);
                params.put("welldepth",welldepth);
                params.put("measuringpoint",measuringpoint);
                params.put("welltype",spinnerWellTypeSelectedItem);
                params.put("wellwaterpotable",wellWaterPotableSelectedItem);
                params.put("waterleveldepth",waterleveldepth);
                params.put("use",spinnerUseSelectedItem);
                params.put("pumpinstalled",pumpInstalledSelectedItem);
                params.put("electricmeterselecteditem",electricMeterSelectedItem);
                params.put("datetime",formattedDate);
                params.put("email",myAppPrefsManager.getUserEmail());
                params.put("image",image);
                Log.e("wellinventoryadd",params.toString());
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public void getAddress()
    {
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(myAppPrefsManager.getLatitude()), Double.parseDouble(myAppPrefsManager.getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            addressLocation=address;
            editTextLocationDetials.setText(address);
            Log.d("kcr",address);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        /*if (requestCode==1) {
            if (resultCode== Activity.RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                imageView.setImageBitmap(thumbnail);

                saveImage(thumbnail);
                //  Toast.makeText(getActivity(), "Image Saved!", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }
        }*/

        if (requestCode == 1) {

            if (resultCode == Activity.RESULT_OK) {
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        String path = data.getStringExtra("path");
                        Log.e("TAG",path);
                        imagePath=path;
                        Log.d("kcr",imagePath);

                        Path path1 = null;
                        Path fileName = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            path1 = Paths.get(imagePath);
                            fileName = path1.getFileName();
                            fileName1 = fileName.toString();
                        }else {
                            fileName1 = imagePath.substring(imagePath.indexOf("/")+1);
                        }
//                        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//                        bmOptions.inJustDecodeBounds = false;
                        bitmap = BitmapFactory.decodeFile(path);
                        imageView.setImageBitmap(bitmap);


                    }
                });



            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }
        }
    }

//    public String saveImage(Bitmap myBitmap) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        File directory = null;
//        directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"AtalJal");
//        // have the object build the directory structure, if needed.
//        if (!directory.exists()) {
//            directory.mkdirs();
//        }
//
//        try {
//            File f = new File(directory, "atalJal_"+Calendar.getInstance().getTimeInMillis() + ".jpg");
//            f.createNewFile();
//            FileOutputStream fo = new FileOutputStream(f);
//            fo.write(bytes.toByteArray());
//            MediaScannerConnection.scanFile(getActivity(),
//                    new String[]{f.getPath()},
//                    new String[]{"image/jpeg"}, null);
//            fo.close();
//            Log.d("kcr", "File Saved::--->" + f.getAbsolutePath());
//
//            imagePath = f.getAbsolutePath();
//
//            Path path = null;
//            Path fileName = null;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                path = Paths.get(imagePath);
//                fileName = path.getFileName();
//                fileName1 = fileName.toString();
//            }else {
//                fileName1 = imagePath.substring(imagePath.indexOf("/")+1);
//            }
//
//            return f.getAbsolutePath();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//        return "";
//    }

    private void  insertImage1(){
//imagePath
        File pic = new File(imagePath);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();

                        serverPath = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Well Inventory","Image Upload "+fileName1+" Successful");
                        Log.d("kcr",serverPath);
                        if (!id.equals("")){
                            updateDataToDb(editTextWellNo.getText().toString(),textViewDate.getText().toString(),textViewState.getText().toString(),textViewDistrict.getText().toString(),
                                    textViewBlock.getText().toString(),textViewGramPanchayat.getText().toString(),editTextVillage.getText().toString(),
                                    editTextSiteName.getText().toString(),editTextLocationDetials.getText().toString(),editTextOwnerName.getText().toString(),
                                    editTextDiameterOfWell.getText().toString(),
                                    editTextDepthOfWell.getText().toString(),editTextHeightOfMeasuringPoint.getText().toString(),spinnerWellTypeSelectedItem,
                                    wellWaterPotableSelectedItem,editTextDepthToWaterLevel.getText().toString(),spinnerUseSelectedItem,pumpInstalledSelectedItem,
                                    electricMeterSelectedItem,serverPath);
                        }else {
                            insertIntoDB(editTextWellNo.getText().toString(), date, textViewState.getText().toString(), textViewDistrict.getText().toString(),
                                    textViewBlock.getText().toString(), textViewGramPanchayat.getText().toString(), editTextVillage.getText().toString(),
                                    editTextSiteName.getText().toString(), editTextLocationDetials.getText().toString(), editTextOwnerName.getText().toString(),
                                    editTextDiameterOfWell.getText().toString(), editTextDepthOfWell.getText().toString(), editTextHeightOfMeasuringPoint.getText().toString(),
                                    spinnerWellTypeSelectedItem, wellWaterPotableSelectedItem, editTextDepthToWaterLevel.getText().toString(), spinnerUseSelectedItem,
                                    pumpInstalledSelectedItem, electricMeterSelectedItem,serverPath);
                        }
                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == Status.LOADING) {
//                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Well Inventory","Image Upload "+fileName1+" Unsuccessful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });


//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URLUPLOAD + "imageupload")
//                .setTimeout(60 * 60 * 1000)
//                .setMultipartFile("inputFile", "multipart/form-data", new File(imagePath))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {
//                        if(result!=null) {
//                            Log.d("kcr1234", result.getResult());
//                            try {
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//                                if (jsonObject.getString("status").equals("success")) {
//                                    serverPath = jsonObject.getString("path");
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Well Inventory","Image Upload "+fileName1+" Successful");
//                                    Log.d("kcr",serverPath);
//                                    if (!id.equals("")){
//                                        updateDataToDb(editTextWellNo.getText().toString(),textViewDate.getText().toString(),textViewState.getText().toString(),textViewDistrict.getText().toString(),
//                                                textViewBlock.getText().toString(),textViewGramPanchayat.getText().toString(),editTextVillage.getText().toString(),
//                                                editTextSiteName.getText().toString(),editTextLocationDetials.getText().toString(),editTextOwnerName.getText().toString(),
//                                                editTextDiameterOfWell.getText().toString(),
//                                                editTextDepthOfWell.getText().toString(),editTextHeightOfMeasuringPoint.getText().toString(),spinnerWellTypeSelectedItem,
//                                                wellWaterPotableSelectedItem,editTextDepthToWaterLevel.getText().toString(),spinnerUseSelectedItem,pumpInstalledSelectedItem,
//                                                electricMeterSelectedItem,serverPath);
//
//                                    }else {
//
//                                        insertIntoDB(editTextWellNo.getText().toString(), date, textViewState.getText().toString(), textViewDistrict.getText().toString(),
//                                                textViewBlock.getText().toString(), textViewGramPanchayat.getText().toString(), editTextVillage.getText().toString(),
//                                                editTextSiteName.getText().toString(), editTextLocationDetials.getText().toString(), editTextOwnerName.getText().toString(),
//                                                editTextDiameterOfWell.getText().toString(), editTextDepthOfWell.getText().toString(), editTextHeightOfMeasuringPoint.getText().toString(),
//                                                spinnerWellTypeSelectedItem, wellWaterPotableSelectedItem, editTextDepthToWaterLevel.getText().toString(), spinnerUseSelectedItem,
//                                                pumpInstalledSelectedItem, electricMeterSelectedItem,serverPath);
//                                    }
//
//
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Well Inventory","Image Upload "+fileName1+" Unsuccessful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse", e.toString());
//                            }else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//                    }
//                });
    }

    private static boolean checkAndRequestPermissions(final Context context) {

        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    101);
            return false;
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkLocationPermission();
    }

    private void checkLocationPermission(){

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    //showAppSettings();
                    displayLocationSettingsRequest(getContext());
                }

            }
        } else {
            //displayLocationSettings();
            displayLocationSettingsRequest(getContext());

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }
    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result=LocationServices.getSettingsClient(getContext())
                .checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task)
            {

                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    //Toast.makeText(Selection.this, "GPS is on", Toast.LENGTH_SHORT).show();
                }
                catch  (ApiException apiException)
                {
                    switch (apiException.getStatusCode())
                    {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            ResolvableApiException resolvableApiException=(ResolvableApiException)apiException;
                            try {
                                resolvableApiException.startResolutionForResult(getActivity(),1001);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }

                }
            }
        });
    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);




                return params;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }
}
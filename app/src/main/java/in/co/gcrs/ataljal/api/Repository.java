package in.co.gcrs.ataljal.api;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import in.co.gcrs.ataljal.entity.UploadImgEntity;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {

    public static LiveData<Resource<UploadImgEntity>> saveImageFileApi(MultipartBody.Part imagePart) {
        final MutableLiveData<Resource<UploadImgEntity>> mutableLiveData = new MutableLiveData<>();
        mutableLiveData.setValue(Resource.loading(new UploadImgEntity()));
        ApiCall apiCall = RetrofitService.createLinePatrollingService();
        Call<String> call = apiCall.saveImageFileApi(imagePart);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.code() == 201 || response.code() == 200) {
//                    UtilHelper.LogError("Save Defects with file Success", response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("success")) {
                            UploadImgEntity entity = new Gson().fromJson(jsonObject.toString(), UploadImgEntity.class);
                            Log.e("TAG",""+jsonObject.getString("status")+"\n"+jsonObject.toString());
                            mutableLiveData.setValue(Resource.success(entity, jsonObject.getString("path")));
                        } else {
                            mutableLiveData.setValue(Resource.error(jsonObject.optString("status"), new UploadImgEntity()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
//                        UtilHelper.LogError("save Defects with file exception", e.toString());
                        mutableLiveData.setValue(Resource.error(e.getMessage(), new UploadImgEntity()));
                    }
                } else {
                    mutableLiveData.setValue(Resource.error(response.toString(), new UploadImgEntity()));
//                    UtilHelper.LogError("save Defects with file error", "" + response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
//                UtilHelper.LogError("save Defects with file failure", t.toString());
                mutableLiveData.setValue(Resource.error(t.toString(), new UploadImgEntity()));

            }
        });
        return mutableLiveData;
    }


    public static LiveData<Resource<UploadImgEntity>> savePdfFileApi(MultipartBody.Part imagePart) {
        final MutableLiveData<Resource<UploadImgEntity>> mutableLiveData = new MutableLiveData<>();
        mutableLiveData.setValue(Resource.loading(new UploadImgEntity()));
        ApiCall apiCall = RetrofitService.createLinePatrollingService();
        Call<String> call = apiCall.savePdfFileApi(imagePart);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.code() == 201 || response.code() == 200) {
//                    UtilHelper.LogError("Save Defects with file Success", response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("success")) {
                            UploadImgEntity entity = new Gson().fromJson(jsonObject.toString(), UploadImgEntity.class);
                            Log.e("TAG",""+jsonObject.getString("status")+"\n"+jsonObject.toString());
                            mutableLiveData.setValue(Resource.success(entity, jsonObject.getString("path")));
                        } else {
                            mutableLiveData.setValue(Resource.error(jsonObject.optString("status"), new UploadImgEntity()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
//                        UtilHelper.LogError("save Defects with file exception", e.toString());
                        mutableLiveData.setValue(Resource.error(e.getMessage(), new UploadImgEntity()));
                    }
                } else {
                    mutableLiveData.setValue(Resource.error(response.toString(), new UploadImgEntity()));
//                    UtilHelper.LogError("save Defects with file error", "" + response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
//                UtilHelper.LogError("save Defects with file failure", t.toString());
                mutableLiveData.setValue(Resource.error(t.toString(), new UploadImgEntity()));

            }
        });
        return mutableLiveData;
    }

    public static LiveData<Resource<UploadImgEntity>> saveWordFileApi(MultipartBody.Part imagePart) {
        final MutableLiveData<Resource<UploadImgEntity>> mutableLiveData = new MutableLiveData<>();
        mutableLiveData.setValue(Resource.loading(new UploadImgEntity()));
        ApiCall apiCall = RetrofitService.createLinePatrollingService();
        Call<String> call = apiCall.saveWordFileApi(imagePart);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.code() == 201 || response.code() == 200) {
//                    UtilHelper.LogError("Save Defects with file Success", response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("success")) {
                            UploadImgEntity entity = new Gson().fromJson(jsonObject.toString(), UploadImgEntity.class);
                            Log.e("TAG",""+jsonObject.getString("status")+"\n"+jsonObject.toString());
                            mutableLiveData.setValue(Resource.success(entity, jsonObject.getString("path")));
                        } else {
                            mutableLiveData.setValue(Resource.error(jsonObject.optString("status"), new UploadImgEntity()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
//                        UtilHelper.LogError("save Defects with file exception", e.toString());
                        mutableLiveData.setValue(Resource.error(e.getMessage(), new UploadImgEntity()));
                    }
                } else {
                    mutableLiveData.setValue(Resource.error(response.toString(), new UploadImgEntity()));
//                    UtilHelper.LogError("save Defects with file error", "" + response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
//                UtilHelper.LogError("save Defects with file failure", t.toString());
                mutableLiveData.setValue(Resource.error(t.toString(), new UploadImgEntity()));

            }
        });
        return mutableLiveData;
    }
}

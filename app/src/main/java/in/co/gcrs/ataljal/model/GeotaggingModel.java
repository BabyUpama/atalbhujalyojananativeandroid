package in.co.gcrs.ataljal.model;

public class GeotaggingModel {

    String gId;
    String state;
    String district;
    String block;
    String gramPanchayat;
    String villagename;
    String sitename;
    String locationDetails1 ;
    String latitude;
    String longitude;
    String calenderdate;
    String typeofStructure;
    String storageCapacity;
    String noOfFillings;
    String image1;
    String image2;
    String others;
    String date;
    String deviceid;
    String email;
    String imageName1;
    String imageName2;

    public String getgId() {
        return gId;
    }

    public void setgId(String gId) {
        this.gId = gId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getGramPanchayat() {
        return gramPanchayat;
    }

    public void setGramPanchayat(String gramPanchayat) {
        this.gramPanchayat = gramPanchayat;
    }

    public String getVillagename() {
        return villagename;
    }

    public void setVillagename(String villagename) {
        this.villagename = villagename;
    }

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }
    public String getLocationDetails1() {
        return locationDetails1;
    }

    public void setLocationDetails1(String locationDetails1) {
        this.locationDetails1 = locationDetails1;
    }
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCalenderdate() {
        return calenderdate;
    }

    public void setCalenderdate(String calenderdate) {
        this.calenderdate = calenderdate;
    }
    public String getTypeofStructure() {
        return typeofStructure;
    }

    public void setTypeofStructure(String typeofStructure) {
        this.typeofStructure = typeofStructure;
    }

    public String getStorageCapacity() {
        return storageCapacity;
    }

    public void setStorageCapacity(String storageCapacity) {
        this.storageCapacity = storageCapacity;
    }

    public String getNoOfFillings() {
        return noOfFillings;
    }

    public void setNoOfFillings(String noOfFillings) {
        this.noOfFillings = noOfFillings;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageName1() {
        return imageName1;
    }

    public void setImageName1(String imageName1) {
        this.imageName1 = imageName1;
    }

    public String getImageName2() {
        return imageName2;
    }

    public void setImageName2(String imageName2) {
        this.imageName2 = imageName2;
    }
}

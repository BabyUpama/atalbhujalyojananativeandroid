package in.co.gcrs.ataljal.model;

public class GpProfileModel
{
    private String gpId;
    private String state;
    private String district;
    private String block;
    private String gramPanchayat;
    private String latitude;
    private String longitude;

    private String totalPopulation;
    private String malePopulation;
    private String femalePopulation;
    private String childPopulation;

    /*Other Categories*/

    private String apl;
    private String bpl;
    private String sc;
    private String st;
    private String obc;

    /*HouseHolds*/

    private String households;

    /*Live Stock Population*/

    private String livePopulation;
    private String cows;
    private String bullocks;
    private String buffalos;
    private String sheepsGoats;
    private String liveStockOthers;

    /*Gp Area*/
    private String gpArea;
    private String forestArea;
    private String fallowLand;
    private String cultivableLand;
    private String netSownArea;

    /*area under cultivation*/
    private String irrigatedAreaKharif;
    private String irrigatedAreaRabi;
    private String irrigatedAreaSummer;
    private String nonIrrigatedArea;

    /*Irrigation Type*/
    private String canal;
    private String tanks;
    private String dugwells;
    private String dugwellsBorewells;
    private String borewellTubewell;

    /*Existing Well*/

    private String dugwell;
    private String borewell;
    private String tubewell;
    private String dugcumBorewell;
    private String existingWellOthers;

    /*water lifting sources*/

    private String nonEnergised;
    private String dieselPump;
    private String electricPump;
    private String centrifugalPump;
    private String submersiblePump;

    /*water saving devices*/

    private String sprinklerIrrigation;
    private String dripIrrigation;
    private String waterSavingDevicesOthers;

    private String wellsWithElectricMeter;
    private String wellWithWaterMeter;

    public String getGpId() {
        return gpId;
    }

    public void setGpId(String gpId) {
        this.gpId = gpId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getGramPanchayat() {
        return gramPanchayat;
    }

    public void setGramPanchayat(String gramPanchayat) {
        this.gramPanchayat = gramPanchayat;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTotalPopulation() {
        return totalPopulation;
    }

    public void setTotalPopulation(String totalPopulation) {
        this.totalPopulation = totalPopulation;
    }

    public String getMalePopulation() {
        return malePopulation;
    }

    public void setMalePopulation(String malePopulation) {
        this.malePopulation = malePopulation;
    }

    public String getFemalePopulation() {
        return femalePopulation;
    }

    public void setFemalePopulation(String femalePopulation) {
        this.femalePopulation = femalePopulation;
    }

    public String getChildPopulation() {
        return childPopulation;
    }

    public void setChildPopulation(String childPopulation) {
        this.childPopulation = childPopulation;
    }

    public String getApl() {
        return apl;
    }

    public void setApl(String apl) {
        this.apl = apl;
    }

    public String getBpl() {
        return bpl;
    }

    public void setBpl(String bpl) {
        this.bpl = bpl;
    }

    public String getSc() {
        return sc;
    }

    public void setSc(String sc) {
        this.sc = sc;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getObc() {
        return obc;
    }

    public void setObc(String obc) {
        this.obc = obc;
    }

    public String getHouseholds() {
        return households;
    }

    public void setHouseholds(String households) {
        this.households = households;
    }

    public String getLivePopulation() {
        return livePopulation;
    }

    public void setLivePopulation(String livePopulation) {
        this.livePopulation = livePopulation;
    }

    public String getCows() {
        return cows;
    }

    public void setCows(String cows) {
        this.cows = cows;
    }

    public String getBullocks() {
        return bullocks;
    }

    public void setBullocks(String bullocks) {
        this.bullocks = bullocks;
    }

    public String getBuffalos() {
        return buffalos;
    }

    public void setBuffalos(String buffalos) {
        this.buffalos = buffalos;
    }

    public String getSheepsGoats() {
        return sheepsGoats;
    }

    public void setSheepsGoats(String sheepsGoats) {
        this.sheepsGoats = sheepsGoats;
    }

    public String getLiveStockOthers() {
        return liveStockOthers;
    }

    public void setLiveStockOthers(String liveStockOthers) {
        this.liveStockOthers = liveStockOthers;
    }

    public String getGpArea() {
        return gpArea;
    }

    public void setGpArea(String gpArea) {
        this.gpArea = gpArea;
    }

    public String getForestArea() {
        return forestArea;
    }

    public void setForestArea(String forestArea) {
        this.forestArea = forestArea;
    }

    public String getFallowLand() {
        return fallowLand;
    }

    public void setFallowLand(String fallowLand) {
        this.fallowLand = fallowLand;
    }

    public String getCultivableLand() {
        return cultivableLand;
    }

    public void setCultivableLand(String cultivableLand) {
        this.cultivableLand = cultivableLand;
    }

    public String getNetSownArea() {
        return netSownArea;
    }

    public void setNetSownArea(String netSownArea) {
        this.netSownArea = netSownArea;
    }

    public String getIrrigatedAreaKharif() {
        return irrigatedAreaKharif;
    }

    public void setIrrigatedAreaKharif(String irrigatedAreaKharif) {
        this.irrigatedAreaKharif = irrigatedAreaKharif;
    }

    public String getIrrigatedAreaRabi() {
        return irrigatedAreaRabi;
    }

    public void setIrrigatedAreaRabi(String irrigatedAreaRabi) {
        this.irrigatedAreaRabi = irrigatedAreaRabi;
    }

    public String getIrrigatedAreaSummer() {
        return irrigatedAreaSummer;
    }

    public void setIrrigatedAreaSummer(String irrigatedAreaSummer) {
        this.irrigatedAreaSummer = irrigatedAreaSummer;
    }

    public String getNonIrrigatedArea() {
        return nonIrrigatedArea;
    }

    public void setNonIrrigatedArea(String nonIrrigatedArea) {
        this.nonIrrigatedArea = nonIrrigatedArea;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getTanks() {
        return tanks;
    }

    public void setTanks(String tanks) {
        this.tanks = tanks;
    }

    public String getDugwells() {
        return dugwells;
    }

    public void setDugwells(String dugwells) {
        this.dugwells = dugwells;
    }

    public String getDugwellsBorewells() {
        return dugwellsBorewells;
    }

    public void setDugwellsBorewells(String dugwellsBorewells) {
        this.dugwellsBorewells = dugwellsBorewells;
    }

    public String getBorewellTubewell() {
        return borewellTubewell;
    }

    public void setBorewellTubewell(String borewellTubewell) {
        this.borewellTubewell = borewellTubewell;
    }

    public String getDugwell() {
        return dugwell;
    }

    public void setDugwell(String dugwell) {
        this.dugwell = dugwell;
    }

    public String getBorewell() {
        return borewell;
    }

    public void setBorewell(String borewell) {
        this.borewell = borewell;
    }

    public String getTubewell() {
        return tubewell;
    }

    public void setTubewell(String tubewell) {
        this.tubewell = tubewell;
    }

    public String getDugcumBorewell() {
        return dugcumBorewell;
    }

    public void setDugcumBorewell(String dugcumBorewell) {
        this.dugcumBorewell = dugcumBorewell;
    }

    public String getExistingWellOthers() {
        return existingWellOthers;
    }

    public void setExistingWellOthers(String existingWellOthers) {
        this.existingWellOthers = existingWellOthers;
    }

    public String getNonEnergised() {
        return nonEnergised;
    }

    public void setNonEnergised(String nonEnergised) {
        this.nonEnergised = nonEnergised;
    }

    public String getDieselPump() {
        return dieselPump;
    }

    public void setDieselPump(String dieselPump) {
        this.dieselPump = dieselPump;
    }

    public String getElectricPump() {
        return electricPump;
    }

    public void setElectricPump(String electricPump) {
        this.electricPump = electricPump;
    }

    public String getCentrifugalPump() {
        return centrifugalPump;
    }

    public void setCentrifugalPump(String centrifugalPump) {
        this.centrifugalPump = centrifugalPump;
    }

    public String getSubmersiblePump() {
        return submersiblePump;
    }

    public void setSubmersiblePump(String submersiblePump) {
        this.submersiblePump = submersiblePump;
    }

    public String getSprinklerIrrigation() {
        return sprinklerIrrigation;
    }

    public void setSprinklerIrrigation(String sprinklerIrrigation) {
        this.sprinklerIrrigation = sprinklerIrrigation;
    }

    public String getDripIrrigation() {
        return dripIrrigation;
    }

    public void setDripIrrigation(String dripIrrigation) {
        this.dripIrrigation = dripIrrigation;
    }

    public String getWaterSavingDevicesOthers() {
        return waterSavingDevicesOthers;
    }

    public void setWaterSavingDevicesOthers(String waterSavingDevicesOthers) {
        this.waterSavingDevicesOthers = waterSavingDevicesOthers;
    }

    public String getWellsWithElectricMeter() {
        return wellsWithElectricMeter;
    }

    public void setWellsWithElectricMeter(String wellsWithElectricMeter) {
        this.wellsWithElectricMeter = wellsWithElectricMeter;
    }

    public String getWellWithWaterMeter() {
        return wellWithWaterMeter;
    }

    public void setWellWithWaterMeter(String wellWithWaterMeter) {
        this.wellWithWaterMeter = wellWithWaterMeter;
    }
}

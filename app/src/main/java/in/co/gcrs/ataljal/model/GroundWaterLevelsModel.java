package in.co.gcrs.ataljal.model;

public class GroundWaterLevelsModel {
    String average; 
    String site_type;
    String nov_14;
    String nov_15;
    String latitude;
    String nov_16;
    String aug_14;
    String may_18;
    String may_17;
    String district_name;
    String aug_18;
    String state_name;
    String aug_17;
    String aug_16;
    String aug_15;
    String nov_17;
    String nov_18;
    String block;
    String apr_14;
    String longitude;
    String jan_15;
    String jan_16;
    String jan_17;
    String jan_18;
    String apr_16;
    String apr_15;
    String apr_18;
    String jan_14;
    String apr_17;
    String utilization;
    String site_name ;
    String may_16;
    String may_15;
    String may_14;
    String depth;
    String site_id;
    String jan_19;
    String agwlid;


    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getSite_type() {
        return site_type;
    }

    public void setSite_type(String site_type) {
        this.site_type = site_type;
    }

    public String getNov_14() {
        return nov_14;
    }

    public void setNov_14(String nov_14) {
        this.nov_14 = nov_14;
    }

    public String getNov_15() {
        return nov_15;
    }

    public void setNov_15(String nov_15) {
        this.nov_15 = nov_15;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getNov_16() {
        return nov_16;
    }

    public void setNov_16(String nov_16) {
        this.nov_16 = nov_16;
    }

    public String getAug_14() {
        return aug_14;
    }

    public void setAug_14(String aug_14) {
        this.aug_14 = aug_14;
    }

    public String getMay_18() {
        return may_18;
    }

    public void setMay_18(String may_18) {
        this.may_18 = may_18;
    }

    public String getMay_17() {
        return may_17;
    }

    public void setMay_17(String may_17) {
        this.may_17 = may_17;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getAug_18() {
        return aug_18;
    }

    public void setAug_18(String aug_18) {
        this.aug_18 = aug_18;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getAug_17() {
        return aug_17;
    }

    public void setAug_17(String aug_17) {
        this.aug_17 = aug_17;
    }

    public String getAug_16() {
        return aug_16;
    }

    public void setAug_16(String aug_16) {
        this.aug_16 = aug_16;
    }

    public String getAug_15() {
        return aug_15;
    }

    public void setAug_15(String aug_15) {
        this.aug_15 = aug_15;
    }

    public String getNov_17() {
        return nov_17;
    }

    public void setNov_17(String nov_17) {
        this.nov_17 = nov_17;
    }

    public String getNov_18() {
        return nov_18;
    }

    public void setNov_18(String nov_18) {
        this.nov_18 = nov_18;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getApr_14() {
        return apr_14;
    }

    public void setApr_14(String apr_14) {
        this.apr_14 = apr_14;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getJan_15() {
        return jan_15;
    }

    public void setJan_15(String jan_15) {
        this.jan_15 = jan_15;
    }

    public String getJan_16() {
        return jan_16;
    }

    public void setJan_16(String jan_16) {
        this.jan_16 = jan_16;
    }

    public String getJan_17() {
        return jan_17;
    }

    public void setJan_17(String jan_17) {
        this.jan_17 = jan_17;
    }

    public String getJan_18() {
        return jan_18;
    }

    public void setJan_18(String jan_18) {
        this.jan_18 = jan_18;
    }

    public String getApr_16() {
        return apr_16;
    }

    public void setApr_16(String apr_16) {
        this.apr_16 = apr_16;
    }

    public String getApr_15() {
        return apr_15;
    }

    public void setApr_15(String apr_15) {
        this.apr_15 = apr_15;
    }

    public String getApr_18() {
        return apr_18;
    }

    public void setApr_18(String apr_18) {
        this.apr_18 = apr_18;
    }

    public String getJan_14() {
        return jan_14;
    }

    public void setJan_14(String jan_14) {
        this.jan_14 = jan_14;
    }

    public String getApr_17() {
        return apr_17;
    }

    public void setApr_17(String apr_17) {
        this.apr_17 = apr_17;
    }

    public String getUtilization() {
        return utilization;
    }

    public void setUtilization(String utilization) {
        this.utilization = utilization;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getMay_16() {
        return may_16;
    }

    public void setMay_16(String may_16) {
        this.may_16 = may_16;
    }

    public String getMay_15() {
        return may_15;
    }

    public void setMay_15(String may_15) {
        this.may_15 = may_15;
    }

    public String getMay_14() {
        return may_14;
    }

    public void setMay_14(String may_14) {
        this.may_14 = may_14;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getJan_19() {
        return jan_19;
    }

    public void setJan_19(String jan_19) {
        this.jan_19 = jan_19;
    }

    public String getAgwlid() {
        return agwlid;
    }

    public void setAgwlid(String agwlid) {
        this.agwlid = agwlid;
    }
}

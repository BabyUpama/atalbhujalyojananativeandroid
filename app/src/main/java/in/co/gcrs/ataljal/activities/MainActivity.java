package in.co.gcrs.ataljal.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.firebase.messaging.FirebaseMessaging;

import net.sqlcipher.database.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.adapters.DrawerExpandableListAdapter;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.constant.ConstantValues;
import in.co.gcrs.ataljal.customs.CircularImageView;
import in.co.gcrs.ataljal.customs.RootUtil;
import in.co.gcrs.ataljal.database.DbHelper;
import in.co.gcrs.ataljal.fragment.ContactUs;
import in.co.gcrs.ataljal.fragment.DashBoardFragment;
import in.co.gcrs.ataljal.fragment.Garswcs;
import in.co.gcrs.ataljal.fragment.GroundWaterMonitoringStations;
import in.co.gcrs.ataljal.fragment.ProfileFragment;
import in.co.gcrs.ataljal.fragment.RainFallMain;
import in.co.gcrs.ataljal.fragment.SocialMonitoring;
import in.co.gcrs.ataljal.fragment.WellInventoryDasboard;
import in.co.gcrs.ataljal.interfaces.IOnBackPress;
import in.co.gcrs.ataljal.model.Drawer_Items;
import in.co.gcrs.ataljal.services.NetworkSchedulerService;


public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    ActionBar actionBar;

    DrawerLayout drawerLayout;
    NavigationView navigationDrawer;

    MyAppPrefsManager myAppPrefsManager;

    RequestQueue requestQueue;


    ExpandableListView main_drawer_list;
    DrawerExpandableListAdapter drawerExpandableAdapter;


    boolean doublePressedBackToExit = false;

    private static String mSelectedItem;
    private static final String SELECTED_ITEM_ID = "selected";
    public static ActionBarDrawerToggle actionBarDrawerToggle;

    List<Drawer_Items> listDataHeader = new ArrayList<>();
    Map<Drawer_Items, List<Drawer_Items>> listDataChild = new HashMap<>();

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1002;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private double latitude, longitude;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    FragmentManager fragmentManager;
    String logDate="";

    public static final int MULTIPLE_PERMISSIONS = 10;

    String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};


    private static final int RC_APP_UPDATE=3563;
    private AppUpdateManager appUpdateManager;
    private Dialog dialog;
    private ArrayList<String> stateArrayList,districtArrayList, blockArrayList, gramPanchayatArrayList;
    Spinner nodalOfficer, spinnerState , spinnerDistrict, spinnerBlock, spinnerGramPanchayat;
    LinearLayout stateLinear,districtLinear,blockLinear,grampanchayatLinear;
    String state="", district="", block="", grampanchayat="";

    private ProgressDialog progressDialog;
    private ArrayAdapter statesArrayAdapter,disctrictArrayAdapter,blockArrayAdapter,gramPanchayatArrayAdapter;


    public native String stringFromJNIClass();
    static {
        System.loadLibrary("native-lib");
    }
    DbHelper db;
    Context context;

    @Override
    protected void onStart() {
        super.onStart();

        Intent startServiceIntent = new Intent(this, NetworkSchedulerService.class);
        startService(startServiceIntent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        context = MainActivity.this;

        if(DbHelper.enableSQLCypher)
        {
            SQLiteDatabase.loadLibs(context);
        }

        db = DbHelper.getInstance(context,stringFromJNIClass());

        toolbar = findViewById(R.id.myToolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationDrawer = findViewById(R.id.main_drawer);

        fragmentManager = getSupportFragmentManager();


        myAppPrefsManager = new MyAppPrefsManager(getApplicationContext());

        requestQueue = Volley.newRequestQueue(this);

        appUpdateManager = AppUpdateManagerFactory.create(MainActivity.this);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("Atal Jal");
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);


        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        logDate = mdformat1.format(calendar.getTime()).toUpperCase();
       /* FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( MainActivity.this,
                new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {

                        myAppPrefsManager.setKeyFcmUserKey(instanceIdResult.getToken());
                        Log.d("Token",instanceIdResult.getToken());


                        //insertFeed(username,instanceIdResult.getToken());
                    }
                });*/

        String android_id = Settings.Secure.getString(MainActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("kcr",android_id);
        myAppPrefsManager.setDeviceId(android_id);



        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("kcr", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        myAppPrefsManager.setKeyFcmUserKey(token);
                        // Log and toast
                        String msg = "fcm key: "+token;
                        Log.d("kcr", msg);
                    }
                });

        if (myAppPrefsManager.getKeyFcmUserKey()!=null) {
            if(isNetworkAvailable()) {
                updateKey(myAppPrefsManager.getUserEmail(), myAppPrefsManager.getKeyFcmUserKey());
            }
        }




        if(checkPermissions()){

        }
        setupExpandableDrawerList();

        setupExpandableDrawerHeader();

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawerOpen, R.string.drawerClose) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // Hide OptionsMenu
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Recreate the OptionsMenu
                invalidateOptionsMenu();
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        // Synchronize the indicator with the state of the linked DrawerLayout
        actionBarDrawerToggle.syncState();

        final View.OnClickListener toggleNavigationClickListener = actionBarDrawerToggle.getToolbarNavigationClickListener();

        // Handle ToolbarNavigationClickListener with OnBackStackChangedListener
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                // Check BackStackEntryCount of FragmentManager
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

                    // Set new ToolbarNavigationClickListener
                    actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Close the Drawer if Opened
                            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                                drawerLayout.closeDrawers();
                            } else {
                                // Pop previous Fragment from BackStack
                                getSupportFragmentManager().popBackStack();
                            }
                        }
                    });


                } else {
                    // Set DrawerToggle Indicator and default ToolbarNavigationClickListener
                    actionBar.setTitle(ConstantValues.APP_HEADER);
                    actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
                    actionBarDrawerToggle.setToolbarNavigationClickListener(toggleNavigationClickListener);
                }
            }
        });
        mSelectedItem = savedInstanceState == null ?  ConstantValues.DEFAULT_HOME_STYLE :  savedInstanceState.getString(SELECTED_ITEM_ID);





        if(mSelectedItem!=null) {

            drawerSelectedItemNavigation(mSelectedItem);

        }else{
            //drawerSelectedItemNavigation("Ground Water Monitoring");

            drawerSelectedItemNavigation(getString(R.string.actionHome));

        }

        if (isNetworkAvailable()) {
           /* if (myAppPrefsManager.isFirstTimeLaunch()) {
                    showSelection();
            } else {
                showSelection1();
            }*/
            if (myAppPrefsManager.isFirstTimeLaunch()){
                if (myAppPrefsManager.getCategory().equals("NPMU")){
                    npmuSelection();
                }
            }else {
                if (myAppPrefsManager.getCategory().equals("NPMU")){
                    showSelection1();
                }
            }
        }else {
            Toast.makeText(this, "Internet not available", Toast.LENGTH_SHORT).show();
        }

        scheduleJob();

    }


    public void setupExpandableDrawerList() {

        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        listDataHeader.add(new Drawer_Items(R.drawable.ic_home, getResources().getString(R.string.actionHome)));
        listDataHeader.add(new Drawer_Items(R.drawable.ic_field_data_collection, getResources().getString(R.string.actionFieldDataCollection)));
        //listDataHeader.add(new Drawer_Items(R.drawable.ic_secondary_data, getResources().getString(R.string.actionSecondaryDataCollection)));
        listDataHeader.add(new Drawer_Items(R.drawable.ic_rainfall, getResources().getString(R.string.actionRainfall)));
        listDataHeader.add(new Drawer_Items(R.drawable.ic_pgwm, getResources().getString(R.string.actionSocialMonitoring)));

        listDataHeader.add(new Drawer_Items(R.drawable.ic_contact,getResources().getString(R.string.actionContactUs)));


        List<Drawer_Items> fieldData = new ArrayList<>();
        fieldData.add(new Drawer_Items(R.drawable.ic_well_inventory, getResources().getString(R.string.actionWellInventory)));
        fieldData.add(new Drawer_Items(R.drawable.ic_groundwater_quantity, getResources().getString(R.string.actiongwm)));
        fieldData.add(new Drawer_Items(R.drawable.ic_geo_tagging, getResources().getString(R.string.actionGeoTagging)));

        List<Drawer_Items> secondaryData = new ArrayList<>();
        secondaryData.add(new Drawer_Items(R.drawable.ic_rainfall, getResources().getString(R.string.actionRainfall)));



        listDataChild.put(listDataHeader.get(1), fieldData);
        //listDataChild.put(listDataHeader.get(2),secondaryData);


        // Initialize DrawerExpandableListAdapter
        drawerExpandableAdapter = new DrawerExpandableListAdapter(this, listDataHeader, listDataChild);

        // Bind ExpandableListView and set DrawerExpandableListAdapter to the ExpandableListView
        main_drawer_list = (ExpandableListView) findViewById(R.id.main_drawer_list);
        main_drawer_list.setAdapter(drawerExpandableAdapter);

        drawerExpandableAdapter.notifyDataSetChanged();



        // Handle Group Item Click Listener
        main_drawer_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (drawerExpandableAdapter.getChildrenCount(groupPosition) < 1) {
                    // Navigate to Selected Main Item
                    if (groupPosition == 0) {
                        drawerSelectedItemNavigation(listDataHeader.get(groupPosition).getTitle());
                    }
                    else if (groupPosition == 1) {
                        drawerSelectedItemNavigation(listDataHeader.get(groupPosition).getTitle());
                    }
                    else {
                        drawerSelectedItemNavigation(listDataHeader.get(groupPosition).getTitle());
                    }
                }
                return false;
            }
        });


        // Handle Child Item Click Listener
        main_drawer_list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // Navigate to Selected Child Item
                drawerSelectedItemNavigation(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).getTitle());
                return false;
            }
        });


        // Handle Group Expand Listener
        main_drawer_list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {}
        });
        // Handle Group Collapse Listener
        main_drawer_list.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {}
        });

    }



    public void setupExpandableDrawerHeader() {

        // Binding Layout Views of DrawerHeader
        ImageView  drawer_header = (ImageView) findViewById(R.id.drawer_profile_image);
        CircularImageView drawer_profile_image = (CircularImageView) findViewById(R.id.drawer_profile_image);
        TextView drawer_profile_name = (TextView) findViewById(R.id.drawer_profile_name);
        TextView drawer_profile_email = (TextView) findViewById(R.id.drawer_profile_email);
        TextView drawer_profile_welcome = (TextView) findViewById(R.id.drawer_profile_welcome);

        drawer_profile_welcome.setText(myAppPrefsManager.getLastLogin());
        // Check if the User is Authenticated



        if(myAppPrefsManager.getUserImage().equals("")){
            Glide.with(MainActivity.this).asBitmap().load(R.drawable.profile).into(drawer_header);
        }
        else{
            Glide.with(MainActivity.this).asBitmap().load(myAppPrefsManager.getUserImage()).into(drawer_header);
        }


        Log.d("kcr",myAppPrefsManager.getUsertype());
        if (myAppPrefsManager.getUsertype().equals("Admin View")) {
            if (myAppPrefsManager.isUserLoggedIn()) {

                if (myAppPrefsManager.getUserImage().equals("")) {
                    Glide.with(this).asBitmap().load(R.drawable.profile).into(drawer_profile_image);
                } else {

                    Glide.with(this).asBitmap().load(myAppPrefsManager.getUserImage()).into(drawer_profile_image);
                }

                drawer_profile_name.setText(myAppPrefsManager.getUserName());
                drawer_profile_email.setText(myAppPrefsManager.getUserEmail());


            }
        }else {

            drawer_profile_name.setText("Public User");
        }



    }
    private void drawerSelectedItemNavigation(String selectedItem) {

        Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();


        if (selectedItem.equalsIgnoreCase(getString(R.string.actionHome))) {
            mSelectedItem = selectedItem;

            Bundle bundle = new Bundle();
            bundle.putBoolean("isHeaderVisible", false);


            fragment = new DashBoardFragment();

            fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();

            drawerLayout.closeDrawers();

        }


        if (selectedItem.equalsIgnoreCase(getString(R.string.actionWellInventory))){
            mSelectedItem = selectedItem;

            Bundle bundle = new Bundle();
            bundle.putBoolean("isHeaderVisible", false);

            fragment = new WellInventoryDasboard();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();

            drawerLayout.closeDrawers();
        }else if (selectedItem.equalsIgnoreCase(getString(R.string.actiongwm))){
            mSelectedItem = selectedItem;

            Bundle bundle = new Bundle();
            bundle.putBoolean("isHeaderVisible", false);

            fragment = new GroundWaterMonitoringStations();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();

            drawerLayout.closeDrawers();
        }else if (selectedItem.equalsIgnoreCase(getString(R.string.actionGeoTagging))){
            mSelectedItem = selectedItem;

            Bundle bundle = new Bundle();
            bundle.putBoolean("isHeaderVisible", false);

            fragment = new Garswcs();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();

            drawerLayout.closeDrawers();
        }else if (selectedItem.equalsIgnoreCase(getString(R.string.actionRainfall))){
            mSelectedItem = selectedItem;

            Bundle bundle = new Bundle();
            bundle.putBoolean("isHeaderVisible", false);

            fragment = new RainFallMain();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();

            drawerLayout.closeDrawers();
        }else if (selectedItem.equalsIgnoreCase(getString(R.string.actionSocialMonitoring))){
            mSelectedItem = selectedItem;

            Bundle bundle = new Bundle();
            bundle.putBoolean("isHeaderVisible", false);

            fragment = new SocialMonitoring();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();

            drawerLayout.closeDrawers();
        }else if (selectedItem.equalsIgnoreCase(getResources().getString(R.string.actionContactUs))) {
            mSelectedItem = selectedItem;


            fragment = new ContactUs();
            fragmentManager.beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(getString(R.string.actionHome)).commit();
            drawerLayout.closeDrawers();

        }
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);


        }




    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Configure ActionBarDrawerToggle with new Configuration
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }





    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the Selected NavigationDrawer Item
        outState.putString(SELECTED_ITEM_ID, mSelectedItem);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate toolbar_menu Menu
        if (myAppPrefsManager.isUserLoggedIn()) {
            getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        }

        return true;
    }


    @SuppressLint("RestrictedApi")
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        final   MenuItem settingsItem = menu.findItem(R.id.user);


        Glide.with(this).load(myAppPrefsManager.getUserImage()).apply(RequestOptions.circleCropTransform())
                .into(new CustomTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {

                        settingsItem.setIcon(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });




        return super.onPrepareOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();


        switch (item.getItemId()) {

            case R.id.user:

                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                }

                // Navigate to Les Fragment
                fragment = new ProfileFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
                break;


            case R.id.logout:


                insertUserLog(myAppPrefsManager.getUserEmail().toLowerCase(),getIpAddress(),logDate,"Logout","Logout Success");

                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                }

                myAppPrefsManager.setUserLoggedIn(false);
                myAppPrefsManager.setFirstTimeLaunch(true);
                myAppPrefsManager.setCategory("");

                Intent logout = new Intent(MainActivity.this,Login.class);
                logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(logout);
                finish();
                //myAppPrefsManager.clearSession();

                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                break;


            default:
                break;
        }

        return true;
    }





    @Override
    public void onBackPressed() {


        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
        if (!(fragment instanceof IOnBackPress) || !((IOnBackPress) fragment).onBackPressed())
        {

        }
        // Get FragmentManager
        FragmentManager fm = getSupportFragmentManager();

        Log.d("asd",String.valueOf(fm.getBackStackEntryCount()));


        // Check if NavigationDrawer is Opened
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();

        }
        // Check if BackStack has some Fragments
        else  if (fm.getBackStackEntryCount() > 1) {

            // Pop previous Fragment
            fm.popBackStack();

        }

        else  if(fm.getBackStackEntryCount()==1){

            if (myAppPrefsManager.getCategory().equals("NPMU")){

                if (myAppPrefsManager.isFirstTimeLaunch()){
                    /*insertUserLog(myAppPrefsManager.getUserEmail().toLowerCase(),getIpAddress(),logDate,"Logout","Logout Success");

                    myAppPrefsManager.setUserLoggedIn(false);
                    myAppPrefsManager.setCategory("");

                    Intent logout = new Intent(MainActivity.this,Login.class);
                    logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(logout);
                    finish();
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);*/

                    finish();
                }else {
                    finish();
                }

            }else {
                finish();
            }
            //dismissAlert();

        }


        else {
            this.doublePressedBackToExit = true;
            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

            // Delay of 2 seconds
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    // Set doublePressedBackToExit false after 2 seconds
                    doublePressedBackToExit = false;
                }
            }, 2000);

            finish();
            //dismissAlert();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case LOCATION_PERMISSION_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation();


                        break;

                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Please Enable Location", Toast.LENGTH_SHORT).show();
                        displayLocationSettings();

                }
                break;

            case RC_APP_UPDATE:
                if (resultCode != RESULT_OK) {
                    Log.e("asd", "onActivityResult: app download failed");
                }
                break;

        }

    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        }
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {



                    // current location

                    latitude = location.getLatitude();
                    longitude = location.getLongitude();



                    myAppPrefsManager.setLatitude(String.valueOf(latitude));
                    myAppPrefsManager.setLongitude(String.valueOf(longitude));


                }

            }
        });
    }



    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissionsList, grantResults);

        switch (requestCode) {


            case  LOCATION_PERMISSION_REQUEST_CODE :{


                if (grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                        Log.d("asd","zxc1");
                        //Do the stuff that requires permission...
                    }else if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                                Manifest.permission.ACCESS_FINE_LOCATION)) {
                            //Show permission explanation dialog...


                            // we can ask again

                            Log.d("asd","zxc11");

                            // 2

                            askPermission();
                        }else{

                            // showAppSettings();

                            Log.d("asd","zxc111");
                            //Never ask again selected, or device policy prohibits the app from having that permission.
                            //So, disable that feature, or fall back to another situation...
                        }
                    }
                }
            }




            default:

                break;

        }
    }

    private void displayLocationSettings() {

        final LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                for (Location location : locationResult.getLocations()) {
                                    //Do what you want with location
                                    //like update camera


                                    myAppPrefsManager.setLatitude(String.valueOf(location.getLatitude()));
                                    myAppPrefsManager.setLongitude(String.valueOf(location.getLongitude()));

                                }

                            }
                        },null);

                    }
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(MainActivity.this, LOCATION_PERMISSION_REQUEST_CODE);
                                break;
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.

                            displayLocationSettings();
                            break;
                    }
                }}
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        if(RootUtil.isDeviceRooted()){
            showAlertDialogAndExitApp("This device is rooted. You can't use this app.");
        }else if (RootUtil.isEmulator(getApplicationContext())){
            showAlertDialogAndExitApp("This is Emulator. You can't use this app.");
        }

        checkLocationPermission();




        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(new com.google.android.play.core.tasks.OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)){

                    try {
                        appUpdateManager.startUpdateFlowForResult(
                                appUpdateInfo, AppUpdateType.IMMEDIATE, MainActivity.this, RC_APP_UPDATE);
                    }

                    catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }

                } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED){
                    popupSnackbarForCompleteUpdate();
                } else {
                    Log.e("", "checkForAppUpdateAvailability: something else");
                }
            }
        });


    }

    public void showAlertDialogAndExitApp(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }


    private void checkLocationPermission(){



        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    showAppSettings();
                }

            }
        } else {
            displayLocationSettings();

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }


    private void showAppSettings(){

        Toast.makeText(MainActivity.this, "Please Allow the permission for Location in settings", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setTitle("Change Permissions in Settings");
        alertDialogBuilder
                .setMessage("" +
                        "\nClick SETTINGS to Manually Set\n" + "Permissions to Access Location")
                .setCancelable(false)
                .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1000);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void popupSnackbarForCompleteUpdate() {

        Snackbar snackbar =
                Snackbar.make(
                        findViewById(R.id.linear),
                        "New app is ready!",
                        Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Install", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appUpdateManager != null){
                    appUpdateManager.completeUpdate();
                }
            }
        });

        snackbar.setActionTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleJob() {
        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class))
                .setRequiresCharging(true)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        assert jobScheduler != null;
        jobScheduler.schedule(myJob);
    }

    @Override
    protected void onStop() {
        // A service can be "started" and/or "bound". In this case, it's "started" by this Activity
        // and "bound" to the JobScheduler (also called "Scheduled" by the JobScheduler). This call
        // to stopService() won't prevent scheduled jobs to be processed. However, failing
        // to call stopService() would keep it alive indefinitely.
        stopService(new Intent(this, NetworkSchedulerService.class));
        super.onStop();
    }

    private void npmuSelection(){

        dialog = new Dialog(MainActivity.this);
        /*if(myAppPrefsManager.isFirstTimeLaunch()) {
            dialog.setCancelable(false);
        }*/
        dialog.setContentView(R.layout.item_npmu);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        stateArrayList = new ArrayList<>();
        districtArrayList = new ArrayList<>();
        blockArrayList = new ArrayList<>();
        gramPanchayatArrayList = new ArrayList<>();

        spinnerState = (Spinner)dialog.findViewById(R.id.spinnerStateNpmu);
        spinnerDistrict = (Spinner)dialog.findViewById(R.id.spinnerDistrictNpmu);
        spinnerBlock = (Spinner)dialog.findViewById(R.id.spinnerBlockNpmu);
        spinnerGramPanchayat = (Spinner)dialog.findViewById(R.id.spinnerGrampanchayatNpmu);

        stateLinear = (LinearLayout) dialog.findViewById(R.id.npmuStateLinear);
        districtLinear = (LinearLayout) dialog.findViewById(R.id.npmuDistrictLinear);
        blockLinear = (LinearLayout) dialog.findViewById(R.id.npmuBlockLinear);
        grampanchayatLinear = (LinearLayout) dialog.findViewById(R.id.npmuPanchayatLinear);

        Button submit = dialog.findViewById(R.id.submit);

        if (progressDialog!=null){
            progressDialog.show();
            getStates();
        }



        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView)view).setTextColor(Color.BLACK);
                if(position!=0)
                {

                    state = spinnerState.getSelectedItem().toString();
                    myAppPrefsManager.setState(state);

                    if (progressDialog!=null&&!progressDialog.isShowing()){
                        progressDialog.show();
                    }

                    getDistricts(spinnerState.getSelectedItem().toString());




                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView)view).setTextColor(Color.BLACK);
                if(position!=0){

                    district = spinnerDistrict.getSelectedItem().toString();
                    if(!spinnerDistrict.getSelectedItem().toString().equals("Select District")){
                        district = spinnerDistrict.getSelectedItem().toString();
                        myAppPrefsManager.setDistrict(district);

                        if (progressDialog!=null&&!progressDialog.isShowing()){
                            progressDialog.show();
                        }

                        getBlocks(spinnerDistrict.getSelectedItem().toString());

                    }



                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerBlock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)view).setTextColor(Color.BLACK);
                if (i!=0){
                    block = spinnerBlock.getSelectedItem().toString();
                    if (!spinnerBlock.getSelectedItem().toString().equals("Select Block")){
                        block = spinnerBlock.getSelectedItem().toString();
                        myAppPrefsManager.setBlock(block);

                        if (progressDialog!=null&&!progressDialog.isShowing()){
                            progressDialog.show();
                        }

                        getGramPanchayat(block);

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerGramPanchayat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)view).setTextColor(Color.BLACK);
                if (i!=0){
                    grampanchayat = spinnerGramPanchayat.getSelectedItem().toString();
                    myAppPrefsManager.setGrampanchayat(grampanchayat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {

                    if (!stateArrayList.equals("")) {

                        if (statesArrayAdapter!=null && !spinnerState.getSelectedItem().toString().equals("Select State")) {

                            if (!districtArrayList.equals("")) {

                                if (disctrictArrayAdapter!=null && !spinnerDistrict.getSelectedItem().toString().equals("Select District")) {

                                    if (!blockArrayList.equals("")) {

                                        if (blockArrayAdapter!=null && !spinnerBlock.getSelectedItem().toString().equals("Select Block")) {

                                            if (!gramPanchayatArrayList.equals("")) {

                                                if (gramPanchayatArrayAdapter!=null && !spinnerGramPanchayat.getSelectedItem().toString().equals("Select Gram Panchayat")) {

                                                    if (dialog != null) {
                                                        dialog.dismiss();
                                                        myAppPrefsManager.setFirstTimeLaunch(false);

                                                    }
                                                } else {
                                                    Toast.makeText(MainActivity.this, "Please Select Gram Panchayat", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        } else {
                                            Toast.makeText(MainActivity.this, "Please Select Block", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } else {
                                    Toast.makeText(MainActivity.this, "Please Select District", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } else {
                            Toast.makeText(MainActivity.this, "Please Select State", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    Toast.makeText(MainActivity.this, "Internet not Available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (dialog != null) {
            dialog.show();

        }
    }

    private void getStates(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL +"getstateslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr",""+response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            progressDialog.dismiss();

                            stateArrayList.clear();
                            stateArrayList.add("Select State");

                            if (jsonArray.length()>0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    stateArrayList.add(jsonObject.getString("state_name"));

                                }


                                statesArrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, stateArrayList);
                                statesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnerState.setAdapter(statesArrayAdapter);
                                progressDialog.dismiss();
                            }else {
                                progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();

                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("encpassword","this is a password");

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);

    }

    private void getDistricts(final String statename) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getdistrictslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            districtArrayList.clear();

                            districtArrayList.add("Select District");


                            if (jsonArray.length()>0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    districtArrayList.add(jsonObject.getString("district_name"));


                                }

                                disctrictArrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, districtArrayList);
                                disctrictArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnerDistrict.setAdapter(disctrictArrayAdapter);
                                progressDialog.dismiss();
                            }else {
                                progressDialog.dismiss();
                            }
                            progressDialog.dismiss();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",statename);

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getBlocks(final String district_name){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getblockslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            blockArrayList.clear();

                            blockArrayList.add("Select Block");



                            if (jsonArray.length()>0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);


                                    blockArrayList.add(jsonObject.getString("block"));


                                }


                                blockArrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, blockArrayList);
                                blockArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnerBlock.setAdapter(blockArrayAdapter);
                                progressDialog.dismiss();
                            }else {
                                progressDialog.dismiss();
                            }
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",state);

                params.put("district_name",district_name);

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getGramPanchayat(final String block){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getgrampanchayatlist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            gramPanchayatArrayList.clear();
                            gramPanchayatArrayList.add("Select Gram Panchayat");


                            if (jsonArray.length()>0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    gramPanchayatArrayList.add(jsonObject.getString("gname"));


                                }

                                gramPanchayatArrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, gramPanchayatArrayList);
                                gramPanchayatArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnerGramPanchayat.setAdapter(gramPanchayatArrayAdapter);
                                Log.d("kcr", String.valueOf(gramPanchayatArrayList));
                                progressDialog.dismiss();
                            }else {
                                progressDialog.dismiss();
                            }
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",state);
                params.put("district_name",district);
                params.put("block",block);

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void showSelection1(){

        dialog = new Dialog(MainActivity.this);
        //dialog.setCancelable(false);
        dialog.setContentView(R.layout.item_selection1);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        Button buttonExisting  = dialog.findViewById(R.id.buttonExisting);
        Button buttonNew  = dialog.findViewById(R.id.buttonNew);

        if (dialog!=null){
            dialog.show();
        }

        buttonExisting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog!=null){
                    dialog.dismiss();
                }

            }
        });
        buttonNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog!=null){
                    dialog.dismiss();
                }
                //myAppPrefsManager.setFirstTimeLaunch(true);
                npmuSelection();
            }
        });

    }

    private void updateKey(final String email,final String tokenKey){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"update_fcm_token.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONObject jsonObject = new JSONObject(response);
                            progressDialog.dismiss();

                            if (jsonObject.getString("status").equals("success")){
                                Log.d("kcr",""+jsonObject.getString("status"));
                            }else {
                                Log.d("kcr",""+jsonObject.getString("status"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("email",email);
                params.put("user_fcm_token",tokenKey);
                Log.e("firebase params",params.toString());
                return params;
            }

        };

        requestQueue.add(stringRequest);

    }


    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);
                Log.e("logout",params.toString());
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}

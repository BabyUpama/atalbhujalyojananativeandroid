package in.co.gcrs.ataljal.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.adapters.GeotaggingAdapter;
import in.co.gcrs.ataljal.adapters.RainfallAdapter;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.model.GeotaggingModel;
import in.co.gcrs.ataljal.model.RainfallModel;


public class RainFallEdit extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private RequestQueue requestQueue;
    private TextView text;
    private MyAppPrefsManager myAppPrefsManager;
    private ArrayList<RainfallModel> arrayList = new ArrayList<>();
    private RainfallModel rainfallModel;
    private RainfallAdapter rainfallAdapter;


    public RainFallEdit() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_rain_fall_edit, container, false);

        myAppPrefsManager = new MyAppPrefsManager(getContext());
        requestQueue = Volley.newRequestQueue(getContext());
        progressBar =view.findViewById(R.id.progressbar);
        text = view.findViewById(R.id.text);

        recyclerView = view.findViewById(R.id.recyclerViewRainfall);
        rainfallAdapter =new RainfallAdapter(getContext(),arrayList);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (isNetworkAvailable()){
            if (!progressBar.isShown()) {
                progressBar.setVisibility(View.VISIBLE);
            }
            getRainfall();
        }else {
            Toast.makeText(getContext(), "Internet not available..!", Toast.LENGTH_SHORT).show();
            if (progressBar.isShown()) {
                progressBar.setVisibility(View.GONE);
            }

            text.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
    public void getRainfall() {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getrainfall.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("kcr",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    arrayList.clear();
                    rainfallAdapter.notifyDataSetChanged();

                    if (jsonArray.length()>0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);

                            rainfallModel = new RainfallModel();

                            rainfallModel.setrId(heroObject.getString("id"));
                            rainfallModel.setState(heroObject.getString("state"));
                            rainfallModel.setDistrict(heroObject.getString("district"));
                            rainfallModel.setBlock(heroObject.getString("block"));
                            rainfallModel.setGramPanchayat(heroObject.getString("gp"));
                            rainfallModel.setSitename(heroObject.getString("sitename"));
                            rainfallModel.setRainGauge(heroObject.getString("raingauge"));
                            rainfallModel.setRainfall(heroObject.getString("rainfall"));
                            rainfallModel.setLatitude(heroObject.getString("latitude"));
                            rainfallModel.setLongitude(heroObject.getString("longitude"));
                            rainfallModel.setImage1(heroObject.getString("image1"));
                            rainfallModel.setImage2(heroObject.getString("image2"));
                            rainfallModel.setDate(heroObject.getString("date"));


                            arrayList.add(rainfallModel);

                            if (progressBar.isShown()) {
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                        rainfallAdapter = new RainfallAdapter(getContext(),arrayList);
                        recyclerView.setAdapter(rainfallAdapter);
                    }
                    else {
                        Toast.makeText(getContext(), "The data not available for Rain gauge stations", Toast.LENGTH_SHORT).show();
                        text.setVisibility(View.VISIBLE);
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();

                params.put("statename", myAppPrefsManager.getState());
                params.put("districtname", myAppPrefsManager.getDistrict());
                params.put("blockname", myAppPrefsManager.getBlock());
                params.put("gpname", myAppPrefsManager.getGrampanchayat());


                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
package in.co.gcrs.ataljal.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.activities.CameraGeoActivity;
import in.co.gcrs.ataljal.activities.MainActivity;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.constant.ConstantValues;
import in.co.gcrs.ataljal.database.DbHelper;
import in.co.gcrs.ataljal.model.GWMStations;
import in.co.gcrs.ataljal.model.NewWellModel;
import in.co.gcrs.ataljal.myviewmodel.UploadImgViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroundWaterMonitoringStations extends Fragment implements LocationListener
{
    GoogleMap gMap;
    MapView mapView;
    private double currentLatitude,currentLongitude,latitude1,longitude1;
    FusedLocationProviderClient mFusedLocationProviderClient;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int REQUEST_CHECK_LOCATION_SETTINGS = 100;
    LatLng latLng,latLng1;
    MyAppPrefsManager myAppPrefsManager;

    private FloatingActionButton floatingActionButtonFilter;
    private Dialog dialog;
    private Dialog dialog1;

    private TextView close;
    RequestQueue requestQueue;

    private ArrayList<String> wellList;

    private LinearLayout linearLayoutWaterLevelExistingWell;

    private String wellNo="",latitude="",longitude="",state="",district="",block="",gp="",villageName="",waterLevelExistingWell="";

    private  GWMStations gwmStations;



    /* Existing Well */
    private TextView textViewWellId,textViewLatitudeExistingWell,textViewLongitudeExistingWell,textViewStateExistingWell,textViewDistrictExistingWell
            ,textViewBlockExistingWell,textViewGPExistingWell,textViewSiteNameExistingWell,textViewSourceExistingWell,textViewPre2019ExistingWell,textViewPost2019ExistingWell;
    private TextView textViewWateLevel,textViewTypeOfWellExistingWell,etexistingdate;
    private String wateLevelExistingData="",imageCode="",imagePath="",serverPath="",imagePathNewWell="",serverPathNewWell="",existWellImageName="",newWellImageName="";
    private EditText editTextWaterLevelExistingWell,editTextexistingLocationDetials;
    private Button buttonUpdateExistingWell;

    /* New Well */
    private LinearLayout linearLayoutWaterLevelNewWell;
    private EditText editTextWellNo
            ,editTextSiteNameNewWell,editTextWaterLevelNewWell,editTextLocationDetials;

    private TextView editTextStateNewWell,editTextDistrictNewWell,editTextBlockNewWell,etnewdate,editTextGPNewWell
            ,editTextLatitudeNewWell,editTextLongitudeNewWell,select;


    private Spinner spinnerTypeOfWellNewWell,spinnerSourceNewWell;
    private ArrayAdapter spinnerTypeOfWellNewWellAdapter,spinnerSourceNewWellAdapter;
    private String spinnerTypeOfWellNewWellSelectedItem="",spinnerSourceNewWellSelectedItem;

    private Button buttonSaveNewWell;
    private DbHelper dbHelper;
    private NewWellModel newWellModel;
    private LinearLayout settings,newWell,selection,layoutClose;

    private ArrayList<String> stateArrayList,districtArrayList, blockArrayList, gramPanchayatArrayList;
    private Spinner nodalOfficer, spinnerState , spinnerDistrict, spinnerBlock, spinnerGramPanchayat;
    private String mapState="", mapDistrict="", mapBlock="", mapGp="";
    ImageView imageView,existingImageview,imageNew;

    Calendar c ;
    String month="";
    int year=0;

    String[]monthName={"january","february","march", "april", "may", "june", "july",
            "august", "september", "october", "november", "december"};

    Geocoder geocoder;
    private String addressLocation="";
    private ProgressDialog progressDialog;
    Date dt ;
    String formattedDate="",logDate="";
    SimpleDateFormat df,df1;
    private Bitmap bitmap,bitmap1;

    public  GroundWaterMonitoringStations()
    {
        // Required empty public constructor
    }
    UploadImgViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_ground_water_monitoring_stations, container, false);
        viewModel = new ViewModelProvider(this).get(UploadImgViewModel.class);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actiongwm));

        newWellModel = new NewWellModel();
        dbHelper = new DbHelper(getContext());

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait..!!");
        progressDialog.setCanceledOnTouchOutside(false);

        wellList=new ArrayList<>();
        myAppPrefsManager=new MyAppPrefsManager(getContext());
        requestQueue= Volley.newRequestQueue(getContext());
        floatingActionButtonFilter = view.findViewById(R.id.floatingActionButtonFilter);

        dt= Calendar.getInstance().getTime();
        df= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH);
        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        formattedDate = df.format(dt).toUpperCase();
        logDate = df1.format(dt).toUpperCase();


        floatingActionButtonFilter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (!myAppPrefsManager.getCategory().equals("")) {
                    if (!myAppPrefsManager.getState().equals("")) {
                        if (!myAppPrefsManager.getDistrict().equals("")) {
                            if (!myAppPrefsManager.getBlock().equals("")) {
                                if (!myAppPrefsManager.getGrampanchayat().equals("")) {
                                    showNewWellDataDialog();
                                } else {
                                    Toast.makeText(getContext(), "Select Gram Panchayat", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "Select Block", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Select District", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Select State", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "Connect Internet and select nodal officer", Toast.LENGTH_SHORT).show();
                }

            }
        });

        mapView = view.findViewById(R.id.map);

        mapView.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isNetworkAvailable()) {
            if (progressDialog!=null&&!progressDialog.isShowing()){
                progressDialog.show();
            }
            loadMap();
        }else {
            Toast.makeText(getContext(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        return view;
    }


    private void loadMap() {

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                gMap = googleMap;
                //  gMap.getUiSettings().setZoomControlsEnabled(true);


                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                gMap.setMyLocationEnabled(true);

                gMap.getUiSettings().setZoomControlsEnabled(true);


                getLocation(googleMap);

                if (!myAppPrefsManager.getState().equals("")&&!myAppPrefsManager.getDistrict().equals("")&&
                        !myAppPrefsManager.getBlock().equals("")&&!myAppPrefsManager.getGrampanchayat().equals("")) {

                    getExistingWellData(myAppPrefsManager.getState(), myAppPrefsManager.getDistrict(), myAppPrefsManager.getBlock());
                    //newWellData(myAppPrefsManager.getState(), myAppPrefsManager.getDistrict(), myAppPrefsManager.getBlock());
                }else {
                    Toast.makeText(getContext(), "Select State/ District/ Block/ Gram Panchayat", Toast.LENGTH_LONG).show();
                }

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
                {
                    @Override
                    public boolean onMarkerClick(Marker marker)
                    {
                        GWMStations gwmStations = (GWMStations) marker.getTag();

                        if (gwmStations!=null)
                        {
                            showExistingWellData(getContext(), gwmStations);
                            /*if (gwmStations.getWell().equals("existing")) {
                                showExistingWellData(getContext(), gwmStations);
                            }else{
                                showNewWellData(getContext(), gwmStations);
                                //showNewWellData(getContext(),gwmStations);
                            }*/
                        }
                        return false;
                    }
                });
            }
        });

    }



    private void getLocation(final GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);
            // return;
        }


        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    //Getting longitude and latitude
                    currentLatitude = location.getLatitude();
                    currentLongitude = location.getLongitude();

                    myAppPrefsManager.setLatitude(String.valueOf(currentLatitude));
                    myAppPrefsManager.setLongitude(String.valueOf(currentLongitude));


                    LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    //googleMap.addMarker(new MarkerOptions().position(sydney).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_gps)));
                     /*googleMap.addMarker(new MarkerOptions().position(myLocation).title("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                     //Circle
                    CircleOptions circleOptions = new CircleOptions()
                            .center(myLocation)
                            .radius(20000)
                            .fillColor(Color.GREEN);
                    Circle circle = googleMap.addCircle(circleOptions);
                    circle.setStrokeColor(Color.GREEN);    */

                    googleMap.addMarker(new MarkerOptions().position(myLocation).title("current location").icon(BitmapDescriptorFactory.defaultMarker())).showInfoWindow();
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(5).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                }
            }
        });
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {

                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();

            }
        }


    };


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

        if (mFusedLocationProviderClient != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Toast.makeText(this, "Please Allow", Toast.LENGTH_SHORT).show();
        // getDeviceLocation();
        if (requestCode == PERMISSION_REQUEST_CODE) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                displayLocationSettingsRequest(getActivity());

                loadMap();


                LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                Location location = lm
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (location != null) {
                    currentLatitude = location.getLatitude();
                    currentLongitude = location.getLongitude();

                    myAppPrefsManager.setLatitude(String.valueOf(currentLatitude));
                    myAppPrefsManager.setLongitude(String.valueOf(currentLongitude));


                }


            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                alertDialogBuilder.setTitle("Change Permissions in Settings");
                alertDialogBuilder
                        .setMessage("" +
                                "\nClick SETTINGS to Manually Set\n" + "Permissions to Access Location")
                        .setCancelable(false)
                        .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                                intent.setData(uri);
                                startActivityForResult(intent, 1000);
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        }
        if (requestCode == 101) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Camera permission", Toast.LENGTH_SHORT)
                        .show();
                getActivity().finish();
            } else if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Storage permission",
                        Toast.LENGTH_SHORT).show();
                getActivity().finish();
            } else {
                switch (imageCode) {
                    case "1": {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 1);
                        break;
                    }
                    case "2": {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 2);
                        break;
                    }
                }
            }
        }

    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:

                            getLocation(gMap);

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the result
                                // in onActivityResult().
                                status.startResolutionForResult(getActivity(), REQUEST_CHECK_LOCATION_SETTINGS);
                                //   getDeviceLocation();
                            } catch (IntentSender.SendIntentException e) {
                                //   Log.i(TAG, "PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            ///  Log.i(TAG, "Location profile are inadequate, and cannot be fixed here. Dialog not created.");
                            break;
                    }
                }
            });
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_LOCATION_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    getLocation(gMap);


                    break;

                case Activity.RESULT_CANCELED:
                    Toast.makeText(getContext(), "Please Enable Location", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {

                String path = data.getStringExtra("path");

                imagePath=path;
                bitmap = BitmapFactory.decodeFile(path);
                imageView.setImageBitmap(bitmap);

                Path path1 = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path1 = Paths.get(imagePath);
                    fileName = path1.getFileName();
                    existWellImageName = fileName.toString();

                }else {
                    existWellImageName = imagePath.substring(imagePath.indexOf("/")+1);
                }
                Log.d("kcr",""+existWellImageName);
                Log.d("kcr",""+imagePath);




            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }
        }else if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {

                String path = data.getStringExtra("path");

                imagePathNewWell = path;

                Path path1 = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path1 = Paths.get(imagePathNewWell);
                    fileName = path1.getFileName();

                    newWellImageName = fileName.toString();
                }else {
                    newWellImageName = imagePathNewWell.substring(imagePathNewWell.indexOf("/")+1);
                }


                bitmap1 = BitmapFactory.decodeFile(path);
                imageNew.setImageBitmap(bitmap1);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }
        }

    }


    public void getExistingWellData(final String state,final String district,final String block) {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getgroundwaterlevels.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("kcr",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if(jsonArray.length()>0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject heroObject = jsonArray.getJSONObject(i);

                            gwmStations = new GWMStations();
                            gwmStations.setState(heroObject.getString("state"));
                            gwmStations.setDistrict(heroObject.getString("district"));
                            gwmStations.setBlock(heroObject.getString("block"));
                            gwmStations.setGp(heroObject.getString("gp"));
                            gwmStations.setSitename(heroObject.getString("sitename"));
                            gwmStations.setSource(heroObject.getString("source"));
                            gwmStations.setWellId(heroObject.getString("wellid"));
                            gwmStations.setWelltype(heroObject.getString("typeofwell"));
                            try {
                                latitude1 = Double.parseDouble(heroObject.getString("latitude"));
                            } catch (Exception e){

                            }try{
                                longitude1 = Double.parseDouble(heroObject.getString("longitude"));
                            } catch (Exception e){

                            }


                            gwmStations.setPre2019(heroObject.getString("pre2019"));
                            gwmStations.setPost2019(heroObject.getString("post2019"));
                            gwmStations.setUid(heroObject.getString("uid"));
                            gwmStations.setLatitude(String.valueOf(latitude1));
                            gwmStations.setLongitude(String.valueOf(longitude1));
                            gwmStations.setImage(heroObject.getString("image"));
                            gwmStations.setWell(heroObject.getString("well"));
                            gwmStations.setId(heroObject.getString("id"));

                            latLng1 = new LatLng(latitude1, longitude1);


                            if (gwmStations.getWell().equals("existing")) {
                                gMap.addMarker(new MarkerOptions().position(latLng1).title("").icon(getMarkerIcon("#43a047"))).setTag(gwmStations);
                            }else {
                                gMap.addMarker(new MarkerOptions().position(latLng1).title("").icon(getMarkerIcon("#c0ca33"))).setTag(gwmStations);
                            }
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng1).zoom(11).build();
                            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }else {
                        Toast.makeText(getActivity(), "No Data Available!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("kcr",""+e.getLocalizedMessage());
                    if (progressDialog!=null&&progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressDialog!=null&&progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("state",state);
                params.put("district",district);
                params.put("block",block);
                Log.e("google well existing data",params.toString());
                //params.put("grampanchayat",gp);
                //params.put("category",myAppPrefsManager.getCategory());
                /*params.put("state","Haryana");
                params.put("district","Faridabad");
                params.put("block","Faridabad");
                params.put("grampanchayat","NACHAULI");
                params.put("category",myAppPrefsManager.getCategory());*/


                return params;
            }
        };
        requestQueue.add(stringRequest);
    }


    private void showExistingWellData(final Context context, final GWMStations gwmStations) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.existing_well_popup, null);
        dialog.setView(dialogView);
        dialog.setCancelable(false);

        final AlertDialog alertDialog = dialog.create();
        alertDialog.show();
        Date dt= Calendar.getInstance().getTime();
        SimpleDateFormat df= new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        String formattedDate = df.format(dt).toUpperCase();
        TextView title = dialogView.findViewById(R.id.dialog_title);
        // ImageView share = dialogView.findViewById(R.id.share);

        textViewWellId=dialogView.findViewById(R.id.textViewWellId);
        textViewLatitudeExistingWell=dialogView.findViewById(R.id.textViewLatitudeExistingWell);
        textViewLongitudeExistingWell=dialogView.findViewById(R.id.textViewLongitudeExistingWell);
        textViewStateExistingWell=dialogView.findViewById(R.id.textViewStateExistingWell);
        textViewDistrictExistingWell=dialogView.findViewById(R.id.textViewDistrictExistingWell);
        textViewBlockExistingWell=dialogView.findViewById(R.id.textViewBlockExistingWell);
        textViewGPExistingWell=dialogView.findViewById(R.id.textViewGPExistingWell);
        textViewSiteNameExistingWell=dialogView.findViewById(R.id.textViewSiteNameExistingWell);
        editTextexistingLocationDetials=dialogView.findViewById(R.id.editTextexistingLocationDetials);
        textViewSourceExistingWell=dialogView.findViewById(R.id.textViewSourceExistingWell);
        textViewPre2019ExistingWell=dialogView.findViewById(R.id.textViewPremonsoonExistingWell);
        textViewPost2019ExistingWell=dialogView.findViewById(R.id.textViewPostmonsoonExistingWell);
        editTextWaterLevelExistingWell=dialogView.findViewById(R.id.editTextWaterLevelExistingWell);
        textViewTypeOfWellExistingWell=dialogView.findViewById(R.id.textViewTypeOfWellExistingWell);
        etexistingdate = dialogView.findViewById(R.id.etexistingdate);
        buttonUpdateExistingWell=dialogView.findViewById(R.id.buttonUpdateExistingWell);

        linearLayoutWaterLevelExistingWell=dialogView.findViewById(R.id.linearLayoutWaterLevelExistingWell);
        textViewWateLevel=dialogView.findViewById(R.id.textViewWateLevel);

        getAddressexisting();
        textViewStateExistingWell.setText(gwmStations.getState());
        textViewDistrictExistingWell.setText(gwmStations.getDistrict());
        textViewTypeOfWellExistingWell.setText(gwmStations.getWelltype());
        textViewBlockExistingWell.setText(gwmStations.getBlock());
        textViewGPExistingWell.setText(gwmStations.getGp());
        textViewSiteNameExistingWell.setText(gwmStations.getSitename());
        textViewSourceExistingWell.setText(gwmStations.getSource());
        textViewPre2019ExistingWell.setText(gwmStations.getPre2019());
        textViewPost2019ExistingWell.setText(gwmStations.getPost2019());
        textViewWellId.setText(gwmStations.getWellId());
        textViewLatitudeExistingWell.setText(gwmStations.getLatitude());
        textViewLongitudeExistingWell.setText(gwmStations.getLongitude());
        etexistingdate.setText(formattedDate);
        imageView = dialogView.findViewById(R.id.image);
        existingImageview = dialogView.findViewById(R.id.existingimage);

        if (!gwmStations.getImage().equals("")){
            Glide.with(context).load(gwmStations.getImage()).into(existingImageview);
        }else {
            existingImageview.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
        }
        ImageView close = dialogView.findViewById(R.id.close);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(getContext(), CameraGeoActivity.class);
                    intent.putExtra("value","2");
                    startActivityForResult(intent, 1);
                }
                /*imageCode="1";
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                }*/
            }
        });

        buttonUpdateExistingWell.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {


                if(linearLayoutWaterLevelExistingWell.getVisibility()==View.VISIBLE)
                {
                    if(!editTextWaterLevelExistingWell.getText().toString().trim().equals("")&&!editTextWaterLevelExistingWell.getText().toString().trim().startsWith(".")) {
                        wateLevelExistingData = editTextWaterLevelExistingWell.getText().toString();
                        if (isNetworkAvailable()) {
                            if (!imagePath.equals("")) {
                                progressDialog.show();
                                insertImage1(imagePath,wateLevelExistingData,alertDialog,gwmStations);
                            }else {
                                progressDialog.show();

                                updateExistingWellData(wateLevelExistingData, alertDialog,gwmStations);
                            }

                        } else {
                            Toast.makeText(getContext(), "Internet not available", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        editTextWaterLevelExistingWell.setError("Enter Water Level Data");
                        editTextWaterLevelExistingWell.requestFocus();
                    }
                }
                else
                {
                    Log.d("llayout","llno");

                }

            }
        });

        textViewWateLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(linearLayoutWaterLevelExistingWell.getVisibility()==View.GONE)
                {
                    linearLayoutWaterLevelExistingWell.setVisibility(View.VISIBLE);
                }
                else
                {
                    linearLayoutWaterLevelExistingWell.setVisibility(View.GONE);
                }
            }
        });



        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void getAddressexisting() {
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(myAppPrefsManager.getLatitude()), Double.parseDouble(myAppPrefsManager.getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            addressLocation=address;
            editTextexistingLocationDetials.setText(address);
            Log.d("kcr",address);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void  updateWellDataImage(final String waterlevel,final AlertDialog alertDialog,final String uid,final String image,final GWMStations gwmStations) {
        progressDialog.setTitle("Please Wait..!!");
        progressDialog.setMessage("while updating Data");
        progressDialog.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"updategroundwaterlevelswithimage.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                Log.d("kcr",response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.length()>0 && jsonObject.getString("status").equals("success"))
                    {
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Data Inserted Successfully");
                        getExistingWellData(myAppPrefsManager.getState(), myAppPrefsManager.getDistrict(), myAppPrefsManager.getBlock());
                        Toast.makeText(getContext(), "Data Updated Successfully", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        alertDialog.dismiss();

                    }else{

                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Data Insert Failed");

                        Toast.makeText(getContext(), ""+jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    if(progressDialog!=null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                        alertDialog.dismiss();
                    }
                    Toast.makeText(getContext(), ""+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if(progressDialog!=null && progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                    alertDialog.dismiss();
                }
                Toast.makeText(getContext(), ""+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();
                params.put("year",String.valueOf(year));
                params.put("month",String.valueOf(month));
                params.put("groundwaterlevel",waterlevel);
                params.put("state",gwmStations.getState());
                params.put("district",gwmStations.getDistrict());
                params.put("block",gwmStations.getBlock());
                params.put("wellid",gwmStations.getWellId());
                params.put("uid",uid);
                params.put("datetime",formattedDate);
                params.put("email",myAppPrefsManager.getUserEmail());
                params.put("image",image);
                params.put("id",gwmStations.getId());


                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public void  updateExistingWellData(final String waterlevel,final AlertDialog alertDialog,final GWMStations gwmStations) {
        progressDialog.setTitle("Please Wait..!!");
        progressDialog.setMessage("while updating Data");
        progressDialog.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"updategroundwaterlevels.jsp", new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                Log.d("kcr",response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.length()>0 && jsonObject.getString("status").equals("success"))
                    {
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Data Updated Successfully");
                        getExistingWellData(myAppPrefsManager.getState(), myAppPrefsManager.getDistrict(), myAppPrefsManager.getBlock());
                        Toast.makeText(getContext(), "Data Updated Successfully", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        alertDialog.dismiss();

                    }else {
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Data Update Failed");

                        Toast.makeText(getContext(), ""+jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    if(progressDialog!=null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                        alertDialog.dismiss();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if(progressDialog!=null && progressDialog.isShowing())
                {
                    progressDialog.cancel();
                    alertDialog.dismiss();
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String,String> params=new HashMap<>();
                params.put("year",String.valueOf(year));
                params.put("month",String.valueOf(month));
                params.put("groundwaterlevel",waterlevel);
                params.put("state",gwmStations.getState());
                params.put("district",gwmStations.getDistrict());
                params.put("block",gwmStations.getBlock());
                params.put("wellid",gwmStations.getWellId());
                params.put("uid",gwmStations.getUid());
                params.put("datetime",formattedDate);
                params.put("email",myAppPrefsManager.getUserEmail());
                params.put("id",gwmStations.getId());
                Log.e("params to string well update",params.toString());

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }


    public  void showNewWellDataDialog() {

        dialog = new Dialog(getContext());
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.new_well_popup);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        Date dt= Calendar.getInstance().getTime();
        SimpleDateFormat df= new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        String formattedDate = df.format(dt).toUpperCase();

        ImageView info = dialog.findViewById(R.id.info);
        close=dialog.findViewById(R.id.close);
        editTextLatitudeNewWell=dialog.findViewById(R.id.editTextLatitudeNewWell);
        editTextLongitudeNewWell = dialog.findViewById(R.id.editTextLongitudeNewWell);
        editTextStateNewWell=dialog.findViewById(R.id.editTextStateNewWell);
        editTextDistrictNewWell = dialog.findViewById(R.id.editTextDistrictNewWell);
        editTextLocationDetials=dialog.findViewById(R.id.editTextLocationDetials);
        editTextWellNo=dialog.findViewById(R.id.editTextWellNo);
        editTextBlockNewWell=dialog.findViewById(R.id.editTextBlockNewWell);
        editTextGPNewWell = dialog.findViewById(R.id.editTextGPNewWell);
        editTextSiteNameNewWell=dialog.findViewById(R.id.editTextSiteNameNewWell);
        editTextWaterLevelNewWell = dialog.findViewById(R.id.editTextWaterLevelNewWell);
        spinnerSourceNewWell=dialog.findViewById(R.id.spinnerSourceNewWell);
        spinnerTypeOfWellNewWell=dialog.findViewById(R.id.spinnerTypeOfWellNewWell);
        etnewdate = dialog.findViewById(R.id.etnewdate);
        imageNew=dialog.findViewById(R.id.imageNewWell);

        editTextLatitudeNewWell.setText(myAppPrefsManager.getLatitude());
        editTextLongitudeNewWell.setText(myAppPrefsManager.getLongitude());

        getAddress();

        editTextStateNewWell.setText(myAppPrefsManager.getState());
        editTextDistrictNewWell.setText(myAppPrefsManager.getDistrict());
        editTextBlockNewWell.setText(myAppPrefsManager.getBlock());
        editTextGPNewWell.setText(myAppPrefsManager.getGrampanchayat());
        etnewdate.setText(formattedDate);
        spinnerSourceNewWellAdapter=new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.sources));
        spinnerSourceNewWellAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSourceNewWell.setAdapter(spinnerSourceNewWellAdapter);

        imageNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(getContext(), CameraGeoActivity.class);
                    intent.putExtra("value","2");
                    startActivityForResult(intent, 2);
                }
                /*imageCode="2";
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 2);
                }*/
            }
        });


        spinnerSourceNewWell.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spinnerSourceNewWellSelectedItem = spinnerSourceNewWell.getSelectedItem().toString();

                Log.d("kcr",spinnerSourceNewWellSelectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerTypeOfWellNewWellAdapter=new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.well_type));
        spinnerTypeOfWellNewWellAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTypeOfWellNewWell.setAdapter(spinnerTypeOfWellNewWellAdapter);
        spinnerTypeOfWellNewWell.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                spinnerTypeOfWellNewWellSelectedItem = spinnerTypeOfWellNewWell.getSelectedItem().toString();

                Log.d("kcr",spinnerTypeOfWellNewWellSelectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1 = new Dialog(getContext());
                dialog1.setCancelable(false);
                dialog1.setContentView(R.layout.info);


                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog1.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;

                dialog1.getWindow().setAttributes(lp);

                ImageView dialogClose = dialog1.findViewById(R.id.dialogClose);

                dialogClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                if(dialog1!=null){
                    dialog1.show();
                }



            }
        });
        //textViewWateLevelNewWell=dialog.findViewById(R.id.textViewWateLevelNewWell);
        linearLayoutWaterLevelNewWell=dialog.findViewById(R.id.linearLayoutWaterLevelNewWell);
        buttonSaveNewWell=dialog.findViewById(R.id.buttonSaveNewWell);
        buttonSaveNewWell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!editTextWellNo.getText().toString().trim().equals(""))
                {
                    if(!editTextLatitudeNewWell.getText().toString().equals(""))
                    {
                        if(!editTextLongitudeNewWell.getText().toString().equals(""))
                        {
                            if(!editTextSiteNameNewWell.getText().toString().trim().equals(""))
                            {
                                if(!spinnerSourceNewWellSelectedItem.equals("Select Ownership Type"))
                                {
                                    if(!editTextWaterLevelNewWell.getText().toString().trim().equals("")&&!editTextWaterLevelNewWell.getText().toString().startsWith("."))
                                    {
                                        if(!spinnerTypeOfWellNewWellSelectedItem.equals("Select Well Type")) {
                                            if (!imagePathNewWell.equals("")) {
                                                Log.d("welltypespinner", spinnerTypeOfWellNewWellSelectedItem);

                                                newWellModel.setState(myAppPrefsManager.getState());
                                                newWellModel.setDistrict(myAppPrefsManager.getDistrict());
                                                newWellModel.setBlock(myAppPrefsManager.getBlock());
                                                newWellModel.setGramPanchayat(myAppPrefsManager.getGrampanchayat());
                                                newWellModel.setLatitude(myAppPrefsManager.getLatitude());
                                                newWellModel.setLongitude(myAppPrefsManager.getLongitude());
                                                newWellModel.setWellNo(editTextWellNo.getText().toString().trim());
                                                newWellModel.setVillageName(editTextSiteNameNewWell.getText().toString().trim());
                                                newWellModel.setSource(spinnerSourceNewWellSelectedItem);
                                                newWellModel.setWellType(spinnerTypeOfWellNewWellSelectedItem);
                                                newWellModel.setWaterLevel(editTextWaterLevelNewWell.getText().toString().trim());
                                                newWellModel.setYear(String.valueOf(year));
                                                newWellModel.setMonth(month);
                                                newWellModel.setDatetime(formattedDate);
                                                newWellModel.setEmail(myAppPrefsManager.getUserEmail());
                                                newWellModel.setImage(imagePathNewWell);
                                                newWellModel.setImageName(newWellImageName);

                                                if (isNetworkAvailable()) {

                                                    Log.d("kcr", spinnerSourceNewWellSelectedItem);

                                                    if (progressDialog != null && !progressDialog.isShowing()) {
                                                        progressDialog.show();
                                                    }

                                                    insertImageNewWell(editTextWellNo.getText().toString().trim(), editTextSiteNameNewWell.getText().toString().trim(),
                                                            spinnerTypeOfWellNewWellSelectedItem, spinnerSourceNewWellSelectedItem,
                                                            editTextWaterLevelNewWell.getText().toString().trim(),imagePathNewWell, dialog);


                                                } else {
                                                    boolean status = dbHelper.insertNewWell(newWellModel);

                                                    if (progressDialog != null && !progressDialog.isShowing()) {
                                                        progressDialog.show();
                                                    }

                                                    if (status == true) {
                                                        if (progressDialog != null && progressDialog.isShowing()) {
                                                            progressDialog.dismiss();
                                                        }

                                                        dialog.dismiss();

                                                        Toast.makeText(getContext(), "Data saved in offline mode and will sync with server once online", Toast.LENGTH_SHORT).show();
                                                    } else {

                                                        if (progressDialog != null && progressDialog.isShowing()) {
                                                            progressDialog.dismiss();
                                                        }

                                                        Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }else {
                                                Toast.makeText(getContext(), "Please Upload Image", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        else
                                        {
                                            Toast.makeText(getContext(), "Select Type of Well", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else
                                    {
                                        editTextWaterLevelNewWell.setError("Enter Water Level..");
                                        editTextWaterLevelNewWell.requestFocus();
                                    }

                                }
                                else {
                                    Toast.makeText(getContext(), "Select Ownership Type", Toast.LENGTH_SHORT).show();
                                }

                            }
                            else
                            {
                                editTextSiteNameNewWell.setError("Enter Site/Village Name..");
                                editTextSiteNameNewWell.requestFocus();
                            }
                        }
                        else
                        {
                            editTextLongitudeNewWell.setError("Enter Longitude..");
                            editTextLongitudeNewWell.requestFocus();
                        }
                    }
                    else
                    {
                        editTextLatitudeNewWell.setError("Enter Latitude..");
                        editTextLatitudeNewWell.requestFocus();
                    }
                }
                else
                {
                    editTextWellNo.setError("Enter Well No..");
                    editTextWellNo.requestFocus();
                }

            }
        });

        close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });


        if(dialog!=null){
            dialog.show();

        }
    }
    public void getAddress()
    {
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(myAppPrefsManager.getLatitude()), Double.parseDouble(myAppPrefsManager.getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            addressLocation=address;
            editTextLocationDetials.setText(address);
            Log.d("kcr",address);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void  saveToDb(final String wellno,final String sitename,final String  welltype,final String source,
                          final String waterlevel,final String image,final Dialog dialog) {

        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"insertgroundwaterlevels.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("kcr",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getString("status").equals("success")) {

                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM Stations","New Well Inserted Successfully");
                                getExistingWellData(myAppPrefsManager.getState(), myAppPrefsManager.getDistrict(), myAppPrefsManager.getBlock());
                                Toast.makeText(getContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                                dialog.dismiss();

                            }else {
                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM Stations","New Well Insert Failed");

                                progressDialog.dismiss();
                                Log.d("kcr else",""+jsonObject.getString("status"));
                            }
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Log.d("kcr catch", ""+e.getLocalizedMessage());
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                Log.d("kcr error", ""+error.getLocalizedMessage());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                HashMap<String,String> params=new HashMap<>();
                params.put("year",String.valueOf(year));
                params.put("month",String.valueOf(month));
                params.put("wellno",wellno);
                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("state",myAppPrefsManager.getState());
                params.put("district",myAppPrefsManager.getDistrict());
                params.put("block",myAppPrefsManager.getBlock());
                params.put("grampanchayat",myAppPrefsManager.getGrampanchayat());
                params.put("sitename",sitename);
                params.put("typeofwell",welltype);
                params.put("source",source);
                params.put("groundwaterlevel",waterlevel);
                params.put("datetime",formattedDate);
                params.put("email",myAppPrefsManager.getUserEmail());
                params.put("image",image);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"AtalJal");
        // have the object build the directory structure, if needed.
        if (!directory.exists()) {
            directory.mkdirs();
        }

        try {
            File f = new File(directory, "atalJal_"+Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("kcr", "File Saved::--->" + f.getAbsolutePath());
            if (imageCode.equals("1")) {
                imagePath = f.getAbsolutePath();

                Path path = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = Paths.get(imagePath);
                    fileName = path.getFileName();
                    existWellImageName = fileName.toString();
                }else {
                    existWellImageName = imagePath.substring(imagePath.indexOf("/")+1);
                }

            }else if (imageCode.equals("2")){
                imagePathNewWell = f.getAbsolutePath();

                Path path = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = Paths.get(imagePathNewWell);
                    fileName = path.getFileName();

                    newWellImageName = fileName.toString();
                }else {
                    newWellImageName = imagePathNewWell.substring(imagePathNewWell.indexOf("/")+1);
                }

            }

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void  insertImage1(final String image,final String wateLevelExistingData,final AlertDialog alertDialog,final GWMStations gwmStations){
        File pic = new File(imagePath);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == in.co.gcrs.ataljal.api.Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverPath = entity.message;
                        Log.d("kcr",serverPath);
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Well Image "+existWellImageName+" updated Successfully");
//
                                    updateWellDataImage(wateLevelExistingData, alertDialog, gwmStations.getUid(),serverPath,gwmStations);
                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == in.co.gcrs.ataljal.api.Status.LOADING) {
//                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Well Image "+existWellImageName+" update Failed");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL+ "imageupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//                .setMultipartFile("img", "multipart/form-data", new File(image))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success")) {
//
//                                    serverPath = jsonObject.getString("path");
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Well Image "+existWellImageName+" updated Successfully");
//
//                                    updateWellDataImage(wateLevelExistingData, alertDialog, gwmStations.getUid(),serverPath,gwmStations);
//
//
//
//                                } else {
//
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Well Image "+existWellImageName+" update Failed");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//
//                        } else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse", e.toString());
//                            }else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//                    }
//                });
    }

    private void  insertImageNewWell(final String wellno,final String siteName,final String wellType,
                                     final String source,final String waterLevel,final String image,final Dialog alertDialog) {
        File pic = new File(imagePathNewWell);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == in.co.gcrs.ataljal.api.Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverPathNewWell = entity.message;
                        Log.d("kcr",serverPathNewWell);
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","New Well Image "+newWellImageName+" updated Successfully");
                                    saveToDb(wellno, siteName, wellType, source, waterLevel, serverPathNewWell, dialog);
                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == in.co.gcrs.ataljal.api.Status.LOADING) {
//                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Well Image "+newWellImageName+" update Failed");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL + "imageupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//
//                .setMultipartFile("img", "multipart/form-data", new File(image))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {
//
//                        if (result != null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success")) {
//
//                                    serverPathNewWell = jsonObject.getString("path");
//
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","New Well Image "+newWellImageName+" updated Successfully");
//
//                                    saveToDb(wellno, siteName, wellType, source, waterLevel, serverPathNewWell, dialog);
//
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"GWM stations","Well Image "+newWellImageName+" update Failed");
//
//                                    if (progressDialog != null) {
//                                        if (progressDialog.isShowing()) {
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if (progressDialog != null) {
//                                    if (progressDialog.isShowing()) {
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//
//                        } else {
//
//                            if (e != null) {
//                                Log.d("kcrelse", e.toString());
//                            } else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if (progressDialog != null) {
//                                if (progressDialog.isShowing()) {
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//
//
//                    }
//                });
    }

    private static boolean checkAndRequestPermissions(final Context context) {

        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    101);
            return false;
        }
        return true;
    }


    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);




                return params;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}


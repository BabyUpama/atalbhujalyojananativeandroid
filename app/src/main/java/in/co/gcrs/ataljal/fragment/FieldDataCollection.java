package in.co.gcrs.ataljal.fragment;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;


public class FieldDataCollection extends Fragment {

    private CardView wellInventory,groundWaterMonitoring,groundwaterquality,geoTagging;

    private Fragment fragment;
    private FragmentManager fragmentManager;
    private MyAppPrefsManager myAppPrefsManager;


    public FieldDataCollection() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_field_data_collection, container, false);

        myAppPrefsManager = new MyAppPrefsManager(getContext());
        fragmentManager = getActivity().getSupportFragmentManager();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionFieldDataCollection));

        wellInventory = view.findViewById(R.id.wellInventory);
        groundWaterMonitoring = view.findViewById(R.id.groundWaterMonitoring);
        groundwaterquality = view.findViewById(R.id.groundwaterquality);
        geoTagging = view.findViewById(R.id.geoTagging);


        wellInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new WellInventoryDasboard();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(null)
                        .commit();
            }
        });

        groundWaterMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("kcr",""+myAppPrefsManager.getUsertype().equals("Admin View"));
                if (myAppPrefsManager.getUsertype().equals("Admin View")) {

                    fragment = new GroundWaterMonitoringStations();
                    fragmentManager.beginTransaction()
                            .replace(R.id.main_fragment, fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .addToBackStack(null)
                            .commit();
                }else {
                    fragment = new PublicViewMap();
                    fragmentManager.beginTransaction()
                            .replace(R.id.main_fragment, fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });
        groundwaterquality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new GroundWaterQualityFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(null)
                        .commit();
            }
        });
        geoTagging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //fragment = new GeoTaggingArtificialRecharge();
                fragment = new Garswcs();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(null)
                        .commit();
            }
        });


        return view;
    }
}
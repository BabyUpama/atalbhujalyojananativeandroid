package in.co.gcrs.ataljal.fragment;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.co.gcrs.ataljal.R;


public class ContactUs extends Fragment {



    public ContactUs() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_contact_us1, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.actionContactUs);

        return view;
    }
}
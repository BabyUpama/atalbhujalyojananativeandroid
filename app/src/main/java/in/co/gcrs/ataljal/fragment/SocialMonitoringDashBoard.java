package in.co.gcrs.ataljal.fragment;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.co.gcrs.ataljal.R;


public class SocialMonitoringDashBoard extends Fragment
{
    CardView cardViewCollectSocialMonitoring,cardViewSentEditSocialMonitoring;
    Fragment fragment;
    FragmentManager fragmentManager;



    public SocialMonitoringDashBoard() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_social_monitoring_dash_board, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionSocialMonitoring));

        cardViewCollectSocialMonitoring=view.findViewById(R.id.cardViewCollectSocialMonitoring);
        cardViewSentEditSocialMonitoring=view.findViewById(R.id.cardViewSentEditSocialMonitoring);
        fragmentManager=getActivity().getSupportFragmentManager();
        cardViewCollectSocialMonitoring.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fragment=new SocialMonitoring();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("Home")
                        .commit();

            }
        });

        cardViewSentEditSocialMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new SocialEdit();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("Home")
                        .commit();
            }
        });

        return view;
    }
}
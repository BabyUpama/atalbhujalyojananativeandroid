package in.co.gcrs.ataljal.model;

public class RainfallModel {

    String rId;
    String state;
    String district;
    String block;
    String gramPanchayat;
    String sitename;
    String rainfall;
    String latitude;
    String longitude;
    String date;
    String rainGauge;
    String image1;
    String image2;
    String email;
    String imagename1;
    String imagename2;

    public String getrId() {
        return rId;
    }

    public void setrId(String rId) {
        this.rId = rId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getGramPanchayat() {
        return gramPanchayat;
    }

    public void setGramPanchayat(String gramPanchayat) {
        this.gramPanchayat = gramPanchayat;
    }

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getRainfall() {
        return rainfall;
    }

    public void setRainfall(String rainfall) {
        this.rainfall = rainfall;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRainGauge() {
        return rainGauge;
    }

    public void setRainGauge(String rainGauge) {
        this.rainGauge = rainGauge;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImagename1() {
        return imagename1;
    }

    public void setImagename1(String imagename1) {
        this.imagename1 = imagename1;
    }

    public String getImagename2() {
        return imagename2;
    }

    public void setImagename2(String imagename2) {
        this.imagename2 = imagename2;
    }
}

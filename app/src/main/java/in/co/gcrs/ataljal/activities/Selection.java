package in.co.gcrs.ataljal.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.customs.RootUtil;

public class Selection extends AppCompatActivity {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1002;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private double latitude, longitude;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private MyAppPrefsManager myAppPrefsManager;
    private LinearLayout admin,publicview;
    boolean doublePressedBackToExit = false;
    public static final int MULTIPLE_PERMISSIONS = 10;

    String[] permissions= new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    private Dialog dialog;

    private static final int RC_APP_UPDATE=3563;
    private AppUpdateManager appUpdateManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        myAppPrefsManager = new MyAppPrefsManager(Selection.this);
        appUpdateManager = AppUpdateManagerFactory.create(Selection.this);

        admin = findViewById(R.id.admin);
        publicview = findViewById(R.id.publicview);

        if(checkPermissions()){

        }

        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAppPrefsManager.setUsertype("Admin View");
                if (myAppPrefsManager.isUserLoggedIn()){
                    Intent intent  = new Intent(Selection.this,MainActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent  = new Intent(Selection.this,Login.class);
                    startActivity(intent);
                }
            }
        });

        publicview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAppPrefsManager.setUsertype("Public View");
                Intent intent  = new Intent(Selection.this,PublicView.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {

            this.doublePressedBackToExit = true;
            //Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

            // Delay of 2 seconds
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    // Set doublePressedBackToExit false after 2 seconds
                    doublePressedBackToExit = false;
                }
            }, 2000);

            //finish();
        dismissAlert();
    }

    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case LOCATION_PERMISSION_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation();


                        break;

                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Please Enable Location", Toast.LENGTH_SHORT).show();
                       // displayLocationSettings();
                        displayLocationSettingsRequest(Selection.this);

                }
                break;

        }

    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        }
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(Selection.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {



                    // current location

                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.d("SelectionLAt",String.valueOf(latitude));

                    myAppPrefsManager.setLatitude(String.valueOf(latitude));
                    myAppPrefsManager.setLongitude(String.valueOf(longitude));


                }

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissionsList, grantResults);

        switch (requestCode) {


            case  LOCATION_PERMISSION_REQUEST_CODE :{


                if (grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                        Log.d("asd","zxc1");
                        //Do the stuff that requires permission...
                    }else if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Selection.this,
                                Manifest.permission.ACCESS_FINE_LOCATION)) {
                            //Show permission explanation dialog...


                            // we can ask again

                            Log.d("asd","zxc11");

                            // 2

                            askPermission();
                        }else{

                            // showAppSettings();

                            Log.d("asd","zxc111");
                            //Never ask again selected, or device policy prohibits the app from having that permission.
                            //So, disable that feature, or fall back to another situation...
                        }
                    }
                }
            }




            default:

                break;

        }
    }

    private void displayLocationSettings() {

        final LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (ContextCompat.checkSelfPermission(Selection.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                for (Location location : locationResult.getLocations()) {
                                    //Do what you want with location
                                    //like update camera


                                    myAppPrefsManager.setLatitude(String.valueOf(location.getLatitude()));
                                    myAppPrefsManager.setLongitude(String.valueOf(location.getLongitude()));

                                }

                            }
                        },null);

                    }
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(Selection.this, LOCATION_PERMISSION_REQUEST_CODE);
                                break;
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.

                            //displayLocationSettings();
                            displayLocationSettingsRequest(Selection.this);
                            break;
                    }
                }}
        });

    }

    private void checkLocationPermission(){

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    //showAppSettings();
                    displayLocationSettingsRequest(Selection.this);
                }

            }
        } else {
            //displayLocationSettings();
            displayLocationSettingsRequest(Selection.this);

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(Selection.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }


    private void showAppSettings(){

        Toast.makeText(Selection.this, "Please Allow the permission for Location in settings", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Selection.this);
        alertDialogBuilder.setTitle("Change Permissions in Settings");
        alertDialogBuilder
                .setMessage("" +
                        "\nClick SETTINGS to Manually Set\n" + "Permissions to Access Location")
                .setCancelable(false)
                .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1000);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        /*PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("kcr", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("kcr", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(Login.this, 111);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("kcr", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("kcr", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });*/
        Task<LocationSettingsResponse> result=LocationServices.getSettingsClient(getApplicationContext())
                .checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task)
            {

                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    //Toast.makeText(Selection.this, "GPS is on", Toast.LENGTH_SHORT).show();
                }
                catch  (ApiException apiException)
                {
                    switch (apiException.getStatusCode())
                    {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            ResolvableApiException resolvableApiException=(ResolvableApiException)apiException;
                            try {
                                resolvableApiException.startResolutionForResult(Selection.this,1001);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if(RootUtil.isDeviceRooted()){
            showAlertDialogAndExitApp("This device is rooted/ Emulator . You can't use this app.");
        }else if (RootUtil.isEmulator(getApplicationContext())){
            showAlertDialogAndExitApp("This is Emulator. You can't use this app.");
        }

        checkLocationPermission();

        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(new com.google.android.play.core.tasks.OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)){

                    try {
                        appUpdateManager.startUpdateFlowForResult(
                                appUpdateInfo, AppUpdateType.IMMEDIATE, Selection.this, RC_APP_UPDATE);
                    }

                    catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }

                } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED){
                    popupSnackbarForCompleteUpdate();
                } else {
                    Log.e("", "checkForAppUpdateAvailability: something else");
                }
            }
        });


    }

    private void popupSnackbarForCompleteUpdate() {

        Snackbar snackbar =
                Snackbar.make(
                        findViewById(R.id.linear),
                        "New app is ready!",
                        Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Install", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appUpdateManager != null){
                    appUpdateManager.completeUpdate();
                }
            }
        });

        snackbar.setActionTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }

    private void dismissAlert(){

        dialog = new Dialog(Selection.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.close_alert);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        if (dialog != null) {
            dialog.show();

        }

        Button yes = dialog.findViewById(R.id.yes);
        Button no = dialog.findViewById(R.id.no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }

    public void showAlertDialogAndExitApp(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(Selection.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }

}
package in.co.gcrs.ataljal.model;

public class SarpanchDetailsModel {
    String sId;
    String state;
    String district;
    String block;
    String gramPanchayat;
    String sarpanchName;
    String sarpanchContact;
    String panchayatSecretary;
    String secretaryContact;
    String latitude;
    String longitude;

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getGramPanchayat() {
        return gramPanchayat;
    }

    public void setGramPanchayat(String gramPanchayat) {
        this.gramPanchayat = gramPanchayat;
    }

    public String getSarpanchName() {
        return sarpanchName;
    }

    public void setSarpanchName(String sarpanchName) {
        this.sarpanchName = sarpanchName;
    }

    public String getSarpanchContact() {
        return sarpanchContact;
    }

    public void setSarpanchContact(String sarpanchContact) {
        this.sarpanchContact = sarpanchContact;
    }

    public String getPanchayatSecretary() {
        return panchayatSecretary;
    }

    public void setPanchayatSecretary(String panchayatSecretary) {
        this.panchayatSecretary = panchayatSecretary;
    }

    public String getSecretaryContact() {
        return secretaryContact;
    }

    public void setSecretaryContact(String secretaryContact) {
        this.secretaryContact = secretaryContact;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}

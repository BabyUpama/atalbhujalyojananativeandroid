package in.co.gcrs.ataljal.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.android.volley.DefaultRetryPolicy;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.customs.RootUtil;


public class Signup extends AppCompatActivity {

    Toolbar toolbar;
    ActionBar actionBar;

    Button signupBtn;

    TextView signup_loginText;
    EditText user_firstname, user_email, user_password,user_cnfpassword, user_mobile,department_name,editCaptcha;

    LinearLayout stateLinear,districtLinear,blockLinear,grampanchayatLinear;

    Spinner spinnerState , spinnerDistrict, spinnerBlock, spinnerGramPanchayat;

    String state="",district="",block="",grampanchayat="",password="",cnfPassword="";


    private ArrayList<String> stateArrayList,districtArrayList, blockArrayList, gramPanchayatArrayList;
    private ArrayList<String> stateIdList,districtIdList,blockIdList, panchayatIdList;

    private MyAppPrefsManager myAppPrefsManager;
    Spinner nodalOfficer;



    private static final int REQUEST_CHECK_LOCATION_SETTINGS = 1001;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 100;

    RequestQueue requestQueue;


    private FusedLocationProviderClient mFusedLocationProviderClient;
    private ProgressDialog progressDialog;
    String strDate,logDate="",mailData="",captcha_id="",hash="";
    private ImageView show_pass_btn_password,show_pass_btn_confirmpassword,captcha,refresh;
    private ArrayAdapter statesArrayAdapter,disctrictArrayAdapter,blockArrayAdapter,gramPanchayatArrayAdapter;
    StringBuffer passText,passNum,passSpecial;
    StringBuffer emailText,emailNum;
    private String hardcodedkey = "eLr3H11BSnaF_r-OhFOjKv:APA91bEFFNatnS3GsStg9D7ISpXEa2lUCHzg85rbgwJp7qYbr8RL-QhfcSrxng4IV2C3x6U5CQtNfRANCSKO1q5ukldxWa0-BVbq4IKG1yUtKHy_trE8OKwnnCygijZYLy0BG57PQbDW";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        myAppPrefsManager = new MyAppPrefsManager(getApplicationContext());

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        progressDialog = new ProgressDialog(Signup.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH);
        SimpleDateFormat mdformat1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        strDate = mdformat.format(calendar.getTime()).toUpperCase();
        logDate = mdformat1.format(calendar.getTime()).toUpperCase();

        // setting Toolbar
        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.app_name));
        actionBar.setDisplayHomeAsUpEnabled(false);




        stateArrayList = new ArrayList<>();
        districtArrayList = new ArrayList<>();
        blockArrayList = new ArrayList<>();
        gramPanchayatArrayList = new ArrayList<>();

        stateIdList = new ArrayList<>();
        districtIdList = new ArrayList<>();
        blockIdList = new ArrayList<>();
        panchayatIdList = new ArrayList<>();




    // Binding Layout Views
        stateLinear = (LinearLayout) findViewById(R.id.stateLinear);
        districtLinear = (LinearLayout) findViewById(R.id.districtLinear);
        blockLinear = (LinearLayout) findViewById(R.id.blockLinear);
        grampanchayatLinear = (LinearLayout) findViewById(R.id.panchayatLinear);

        spinnerState = (Spinner)findViewById(R.id.spinneerState);
        spinnerDistrict = (Spinner)findViewById(R.id.spinnerDistrict);
        spinnerBlock = (Spinner)findViewById(R.id.spinnerBlock);
        spinnerGramPanchayat = (Spinner)findViewById(R.id.spinnerGrampanchayat);

        nodalOfficer = (Spinner) findViewById(R.id.nodalOfficer);
        user_firstname = (EditText) findViewById(R.id.user_firstname);
        user_email = (EditText) findViewById(R.id.user_email);
        user_password = (EditText) findViewById(R.id.user_password);
        user_cnfpassword = (EditText) findViewById(R.id.user_cnfpassword);
        user_mobile = (EditText) findViewById(R.id.user_mobile);
        department_name = (EditText) findViewById(R.id.department);
        show_pass_btn_password=findViewById(R.id.show_pass_btn_password);
        show_pass_btn_confirmpassword=findViewById(R.id.show_pass_btn_confirmpassword);

        signupBtn = (Button) findViewById(R.id.signupBtn);

        captcha=findViewById(R.id.captcha);
        refresh = findViewById(R.id.refresh);
        editCaptcha=findViewById(R.id.editCaptcha);


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()){

                    if (progressDialog!=null&&!progressDialog.isShowing()){
                        progressDialog.show();
                    }
                    getCaptcha();
                }else {
                    Toast.makeText(Signup.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                }
            }
        });




        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("kcr", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        myAppPrefsManager.setKeyFcmUserKey(token);
                        // Log and toast
                        String msg = "fcm key: " + token;
                        Log.d("kcr", msg);
                    }

                });

        show_pass_btn_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId()==R.id.show_pass_btn_password){

                    if(user_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance()))
                    {
                        ((ImageView)(view)).setImageResource(R.drawable.ic_hide_password);

                        //Show Password
                        user_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        user_password.setSelection(user_password.length());
                    }
                    else{
                        ((ImageView)(view)).setImageResource(R.drawable.ic_password_eye);

                        //Hide Password
                        user_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        user_password.setSelection(user_password.length());

                    }
                }
            }
        });

        show_pass_btn_confirmpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId()==R.id.show_pass_btn_confirmpassword){

                    if(user_cnfpassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance()))
                    {
                        ((ImageView)(view)).setImageResource(R.drawable.ic_hide_password);

                        //Show Password
                        user_cnfpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        user_cnfpassword.setSelection(user_cnfpassword.length());
                    }
                    else{
                        ((ImageView)(view)).setImageResource(R.drawable.ic_password_eye);

                        //Hide Password
                        user_cnfpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                    }
                }
            }
        });



        final ArrayAdapter nodalAdapter = new ArrayAdapter(Signup.this,android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.nodal));
        nodalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nodalOfficer.setAdapter(nodalAdapter);


        nodalOfficer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i==0){
                    stateLinear.setVisibility(View.GONE);
                    districtLinear.setVisibility(View.GONE);
                    blockLinear.setVisibility(View.GONE);
                    grampanchayatLinear.setVisibility(View.GONE);

                } else if (i==1){
                    stateLinear.setVisibility(View.GONE);
                    districtLinear.setVisibility(View.GONE);
                    blockLinear.setVisibility(View.GONE);
                    grampanchayatLinear.setVisibility(View.GONE);

                } else if (i==2){
                    stateLinear.setVisibility(View.VISIBLE);
                    districtLinear.setVisibility(View.VISIBLE);
                    blockLinear.setVisibility(View.VISIBLE);
                    grampanchayatLinear.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (isNetworkAvailable()){
            if (progressDialog!=null){
                progressDialog.show();
            }
            getStates();
        }
        if (isNetworkAvailable()){
            if (progressDialog!=null&&!progressDialog.isShowing()){
                progressDialog.show();
            }
            getCaptcha();
        }


        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView)view).setTextColor(Color.BLACK);
                    state = spinnerState.getSelectedItem().toString();

                    if (progressDialog!=null){
                        progressDialog.show();
                    }
                    getDistricts(spinnerState.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView)view).setTextColor(Color.BLACK);

                    district = spinnerDistrict.getSelectedItem().toString();
                    if(!spinnerDistrict.getSelectedItem().toString().equals("Select District")){
                        district = spinnerDistrict.getSelectedItem().toString();
                        getBlocks(spinnerDistrict.getSelectedItem().toString());

                        if (progressDialog!=null){
                            progressDialog.show();
                        }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerBlock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)view).setTextColor(Color.BLACK);
                    block = spinnerBlock.getSelectedItem().toString();
                    if (!spinnerBlock.getSelectedItem().toString().equals("Select Block")){
                        block = spinnerBlock.getSelectedItem().toString();
                        getGramPanchayat(block);

                        if (progressDialog!=null){
                            progressDialog.show();
                        }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerGramPanchayat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)view).setTextColor(Color.BLACK);
                grampanchayat = spinnerGramPanchayat.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    signup_loginText = (TextView) findViewById(R.id.signup_loginText);
        signup_loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish SignUpActivity to goto the LoginActivity
                finish();
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
            }
        });

        editCaptcha.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //do what you want on the press of 'done'
                    if (isNetworkAvailable()) {
                        submit();
                    }else {
                        Toast.makeText(Signup.this, "Internet not available!", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    //if (textCaptcha.checkAnswer(editCaptcha.getText().toString().trim())) {
                        submit();
                } else {
                    Toast.makeText(Signup.this, "Internet not available!", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    private void submit(){
        if (!nodalOfficer.getSelectedItem().toString().equals("Select Nodal Officer")) {

            //if (nodalOfficer.getSelectedItem().toString().equals("Admin")||nodalOfficer.getSelectedItem().toString().equals("National Level Officer")) {
            if (nodalOfficer.getSelectedItem().toString().equals("NPMU")) {

                check();
            } else {
                if (nodalOfficer.getSelectedItem().toString().equals("State Level Officer")) {
                    if (!spinnerState.getSelectedItem().toString().equals("Select State")) {
                        check();
                    } else {
                        Toast.makeText(Signup.this, "Please Select State", Toast.LENGTH_SHORT).show();
                    }
                } else if (nodalOfficer.getSelectedItem().toString().equals("District Level Officer")) {
                    if (!spinnerState.getSelectedItem().toString().equals("Select State")) {
                        if (!spinnerDistrict.getSelectedItem().toString().equals("Select District")) {
                            check();
                        } else {
                            Toast.makeText(Signup.this, "Please Select District", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(Signup.this, "Please Select State", Toast.LENGTH_SHORT).show();
                    }
                } else if (nodalOfficer.getSelectedItem().toString().equals("Block Level Officer")) {
                    if (!spinnerState.getSelectedItem().toString().equals("Select State")) {

                        if (!spinnerDistrict.getSelectedItem().toString().equals("Select District")) {

                            if (!spinnerBlock.getSelectedItem().toString().equals("Select Block")) {
                                check();
                            } else {
                                Toast.makeText(Signup.this, "Please Select Block", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Signup.this, "Please Select District", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(Signup.this, "Please Select State", Toast.LENGTH_SHORT).show();
                    }
                } else if (nodalOfficer.getSelectedItem().toString().equals("Gram Panchayat Level Officer") || nodalOfficer.getSelectedItem().toString().equalsIgnoreCase("District Implementation Partner")) {
                    if (statesArrayAdapter != null && !spinnerState.getSelectedItem().toString().equals("Select State")) {

                        if (disctrictArrayAdapter != null && !spinnerDistrict.getSelectedItem().toString().equals("Select District")) {

                            if (blockArrayAdapter != null && !spinnerBlock.getSelectedItem().toString().equals("Select Block")) {

                                if (gramPanchayatArrayAdapter != null && !spinnerGramPanchayat.getSelectedItem().toString().equals("Select Gram Panchayat")) {

                                    check();
                                } else {
                                    Toast.makeText(Signup.this, "Please Select Gram Panchayat", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(Signup.this, "Please Select Block", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Signup.this, "Please Select District", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(Signup.this, "Please Select State", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        } else {
            Toast.makeText(Signup.this, "Please select Nodal Officer", Toast.LENGTH_SHORT).show();
        }
    }


    private void check() {
            String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+[a-z]";
            String emailpattern1 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+[a-z]+\\.+[a-z]+[a-z]";
            if (!user_firstname.getText().toString().trim().equals("")) {
                if (user_email.getText().toString().trim().matches(emailpattern)||user_email.getText().toString().trim().matches(emailpattern1)) {
                    String email = user_email.getText().toString().trim();
                    String[] email1 = email.split("@");
                    String userId1 = email1[0];
                    String userId2 = email1[1];
                    if (!user_password.getText().toString().trim().equals("")) {
                        if (user_password.getText().toString().trim().length() >= 8) {
                            password = user_password.getText().toString().trim();
                            if (isValidPassword(password)) {
                                if (!user_cnfpassword.getText().toString().trim().equals("")) {
                                    if (user_cnfpassword.getText().toString().trim().length() >= 8) {
                                        cnfPassword = user_cnfpassword.getText().toString().trim();
                                        if (isValidPassword(cnfPassword)) {
                                            if (password.equals(cnfPassword)) {
                                                splitPassword(password);
                                                splitEmail(userId1);
                                                //Log.d("kcr",""+emailText.toString().toLowerCase().contains(passText.toString().toLowerCase()));
                                                if (!passText.toString().toLowerCase().matches(emailText.toString().toLowerCase())) {
                                                    if (user_mobile.getText().toString().trim().length() == 10 && (user_mobile.getText().toString().trim().startsWith("9") ||
                                                            user_mobile.getText().toString().trim().startsWith("8") || user_mobile.getText().toString().trim().startsWith("7") ||
                                                            user_mobile.getText().toString().trim().startsWith("6"))) {
                                                        if (!department_name.getText().toString().trim().equals("")) {

                                                            if (nodalOfficer.getSelectedItem().toString().equals("NPMU")) {
                                                                myAppPrefsManager.setCategory(nodalOfficer.getSelectedItem().toString());

                                                            } else if (nodalOfficer.getSelectedItem().toString().equals("Gram Panchayat Level Officer")) {
                                                                myAppPrefsManager.setCategory(nodalOfficer.getSelectedItem().toString());

                                                            }
                                                            if (!editCaptcha.getText().toString().trim().equals("")) {
                                                                if (editCaptcha.getText().toString().trim().length() == 6) {

                                                                    if (progressDialog != null && !progressDialog.isShowing()) {
                                                                        progressDialog.show();
                                                                    }

                                                                    hash = get_SHA_256_SecurePassword(user_password.getText().toString().trim());

                                                                    setUserSignUp(user_firstname.getText().toString().trim(), user_email.getText().toString().trim().toLowerCase(), nodalOfficer.getSelectedItem().toString(), user_mobile.getText().toString().trim(),
                                                                            department_name.getText().toString().trim(),user_password.getText().toString().trim(),state, district,block, grampanchayat, hash, captcha_id, editCaptcha.getText().toString().trim());
                                                                } else {
                                                                    Toast.makeText(this, "Enter valid captcha", Toast.LENGTH_SHORT).show();
                                                                }
                                                            } else {
                                                                editCaptcha.setError("Please enter Captcha");
                                                                editCaptcha.requestFocus();
                                                            }
                                                        } else {
                                                            department_name.setError("Please enter Department Name");
                                                            department_name.requestFocus();
                                                        }
                                                    } else {
                                                        user_mobile.setError("Please enter valid mobile number");
                                                        user_mobile.requestFocus();
                                                    }
                                                }else {
                                                    Toast.makeText(this, "password should not be same as email address", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Toast.makeText(this, "Password should match with Confirm Password", Toast.LENGTH_SHORT).show();
                                            }
                                        }else {
                                            Toast.makeText(this, "Confirm Password should Contain Capital Letter, Numbers and Special Characters", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        user_cnfpassword.setError("Please enter 8 characters");
                                        user_cnfpassword.requestFocus();
                                    }
                                } else {
                                    user_cnfpassword.setError("Please enter Valid  Confirm Password");
                                    user_cnfpassword.requestFocus();
                                }
                            }else {
                                Toast.makeText(this, "Password should Contain Capital Letter, Numbers and Special Characters", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            user_password.setError("Please enter 8 characters");
                            user_password.requestFocus();
                        }
                    }else {
                        user_password.setError("Please enter Valid Password");
                        user_password.requestFocus();
                    }
                } else {
                    user_email.setError("Please enter Valid Email");
                    user_email.requestFocus();
                }
            } else {
                user_firstname.setError("Please enter Valid UserName");
                user_firstname.requestFocus();
            }
    }


    //*********** Receives the result from a previous call of startActivityForResult(Intent, int) ********//

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {

            return;
        }
    }


    //*********** This method is invoked for every call on requestPermissions(Activity, String[], int) ********//

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissionsList, grantResults);

        switch (requestCode) {


            case  LOCATION_PERMISSION_REQUEST_CODE :{


                if (grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                        Log.d("asd","zxc1");
                        //Do the stuff that requires permission...
                    }else if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Signup.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            //Show permission explanation dialog...


                            // we can ask again

                            Log.d("asd","zxc11");

                            // 2

                            askPermission();
                        }else{

                            // showAppSettings();

                            Log.d("asd","zxc111");
                            //Never ask again selected, or device policy prohibits the app from having that permission.
                            //So, disable that feature, or fall back to another situation...
                        }
                    }
                }
            }




            default:

                break;

        }
    }




    @Override
    public void onBackPressed() {
        // Finish SignUpActivity to goto the LoginActivity
        finish();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
    }



    @Override
    public void onResume() {
        super.onResume();

        if(RootUtil.isDeviceRooted()){
            showAlertDialogAndExitApp("This device is rooted. You can't use this app.");
        }else if (RootUtil.isEmulator(getApplicationContext())){
            showAlertDialogAndExitApp("This is Emulator. You can't use this app.");
        }

        checkLocationPermission();


    }

    public void showAlertDialogAndExitApp(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(Signup.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }


    private void checkLocationPermission(){



        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    showAppSettings();
                }

            }
        } else {
            displayLocationSettings();

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(Signup.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    private void showAppSettings(){

        Toast.makeText(Signup.this, "Please Allow the permission for Location in settings", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Signup.this);
        alertDialogBuilder.setTitle("Change Permissions in Settings");
        alertDialogBuilder
                .setMessage("" +
                        "\nClick SETTINGS to Manually Set\n" + "Permissions to Access Location")
                .setCancelable(false)
                .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1000);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void displayLocationSettings() {

        final LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (ContextCompat.checkSelfPermission(Signup.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                for (Location location : locationResult.getLocations()) {
                                    //Do what you want with location
                                    //like update camera


                                    myAppPrefsManager.setLatitude(String.valueOf(location.getLatitude()));
                                    myAppPrefsManager.setLongitude(String.valueOf(location.getLongitude()));

                                }

                            }
                        },null);

                    }
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(Signup.this, REQUEST_CHECK_LOCATION_SETTINGS);
                                break;
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.

                            displayLocationSettings();
                            break;
                    }
                }}
        });

    }

    protected void setUserSignUp(final String username, final String email,final String nodalOfficer, final String contactNumber,
                                 final String departmentName, final String password,final String stateName,final String DistrictName,
                                 final String blockName,final String gramPanchayatName,final String hashed,final String capid,final String captcha) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"users_encrypt.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                /*if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }*/

                                mailData = "1";
                                mailData = mailData+","+email;
                                mailData = mailData+","+nodalOfficer;
                                mailData = mailData+","+contactNumber;
                                mailData = mailData+","+username;

                                switch (nodalOfficer) {
                                    case "NPMU":

                                        mailData = mailData+","+"0";
                                        mailData = mailData+","+"0";
                                        mailData = mailData+","+"0";
                                        mailData = mailData+","+"0";
                                        mailData = mailData+","+departmentName;
                                        mailData = mailData+","+"2";
                                        sendEmail(mailData,email);
                                        insertPasswordLog(email,hash,logDate);
                                        break;
                                    case "Gram Panchayat Level Officer":

                                        mailData = mailData+","+stateName;
                                        mailData = mailData+","+DistrictName;
                                        mailData = mailData+","+blockName;
                                        mailData = mailData+","+gramPanchayatName;
                                        mailData = mailData+","+departmentName;
                                        mailData = mailData+","+"2";
                                        sendEmail(mailData,email);
                                        insertPasswordLog(email,hash,logDate);
                                        break;
                                }

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Sign Up","user created");


                            } else if (obj.getString("status").equals("user already exist")){

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Sign Up","user already exist");

                                Toast.makeText(Signup.this, "User already Exist Please Login", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(Signup.this,Login.class);
                                startActivity(intent);

                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                            }else if (obj.getString("status").equals("grampanchayat already exist")){

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
//                                mailData = "1";
//                                mailData = mailData+","+email;
//                                mailData = mailData+","+nodalOfficer;
//                                mailData = mailData+","+contactNumber;
//                                mailData = mailData+","+username;
//
//                                switch (nodalOfficer) {
//                                    case "NPMU":
//
//                                        mailData = mailData+","+"0";
//                                        mailData = mailData+","+"0";
//                                        mailData = mailData+","+"0";
//                                        mailData = mailData+","+"0";
//                                        mailData = mailData+","+departmentName;
//                                        mailData = mailData+","+"2";
//                                        sendEmail(mailData,email);
//                                        insertPasswordLog(email,hash,logDate);
//                                        break;
//                                    case "Gram Panchayat Level Officer":
//
//                                        mailData = mailData+","+stateName;
//                                        mailData = mailData+","+DistrictName;
//                                        mailData = mailData+","+blockName;
//                                        mailData = mailData+","+gramPanchayatName;
//                                        mailData = mailData+","+departmentName;
//                                        mailData = mailData+","+"2";
//                                        sendEmail(mailData,email);
//                                        insertPasswordLog(email,hash,logDate);
//                                        break;
//                                }
                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Sign Up","gram panchayat already exist");

//                                Toast.makeText(Signup.this, "gram panchayat already Exist", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(Signup.this,Login.class);
                                startActivity(intent);

                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                            }else if (obj.getString("status").equals("Invalid captcha")){

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Sign Up","Invalid Captcha");

                                if (isNetworkAvailable()) {

                                    if (progressDialog != null && !progressDialog.isShowing()) {
                                        progressDialog.show();
                                    }
                                    getCaptcha();
                                }

                                Toast.makeText(Signup.this, ""+obj.getString("status"), Toast.LENGTH_LONG).show();

                            } else {
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(user_email.getText().toString().trim().toLowerCase(),getIpAddress(),logDate,"Sign Up",obj.getString("status"));

                                Toast.makeText(Signup.this, ""+obj.getString("status"), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(Signup.this,Login.class);
                                startActivity(intent);

                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("kcr", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            if (isNetworkAvailable()){
                                if (progressDialog!=null&&!progressDialog.isShowing()){
                                    progressDialog.show();
                                }
                                getCaptcha();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror "+error.getLocalizedMessage());
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        if (isNetworkAvailable()){
                            if (progressDialog!=null&&!progressDialog.isShowing()){
                                progressDialog.show();
                            }
                            getCaptcha();
                        }


                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("UserName", username);
                params.put("Email", email);
                params.put("NodalOfficer", nodalOfficer);
                params.put("ContactNumber", contactNumber);
                params.put("DepartmentName", departmentName);
                params.put("Password", password);
                params.put("StateName", stateName);
                params.put("DistrictName", DistrictName);
                params.put("BlockName", blockName);
                params.put("GramPanchayatName", gramPanchayatName);
                params.put("latitude", myAppPrefsManager.getLatitude());
                params.put("longitude", myAppPrefsManager.getLongitude());
                params.put("datetime", strDate);
//                params.put("fcm_token_key",myAppPrefsManager.getKeyFcmUserKey());
                params.put("fcm_token_key",hardcodedkey);
                params.put("hashpassword",hashed);
                params.put("capid",capid);
                params.put("captcha",captcha);
                Log.e("params",params.toString());
                return params;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            } else if (obj.getString("status").equals("user already exist")){

                                Log.d("kcr",""+obj.getString("status"));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);
                Log.e("params",params.toString());



                return params;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    protected void insertPasswordLog(final String email,final String password, final String datetime) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"passwordlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){

                                Log.d("kcr",""+obj.getString("status"));

                            } else {

                                Log.d("kcr",""+obj.getString("status"));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("password", password);
                params.put("datetime", datetime);
                Log.e("params",params.toString());
                return params;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    protected void getCaptcha() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                BuildConfig.SERVER_URL+"captcha.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        //getting the whole json object from the response
                        try {
                            JSONObject obj = new JSONObject(response);

                           captcha_id = obj.getString("captcha_id");
                           String byteCapctha = obj.getString("captcha");
                            byte[] decodedString = Base64.decode(byteCapctha, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            captcha.setImageBitmap(decodedByte);
                            Log.e("params",obj.toString());
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Log.d("kcr",e.getLocalizedMessage());

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getStates(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getstateslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            Log.d("kcr",response);
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            stateArrayList.clear();
                            stateArrayList.add("Select State");

                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                stateArrayList.add(jsonObject.getString("state_name"));

                            }


                            statesArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,stateArrayList);
                            statesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerState.setAdapter(statesArrayAdapter);

                            progressDialog.dismiss();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",state);
                Log.e("params",params.toString());
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getDistricts(final String statename)
    {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getdistrictslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            districtArrayList.clear();

                            districtArrayList.add("Select District");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                districtArrayList.add(jsonObject.getString("district_name"));

                            }


                            disctrictArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,districtArrayList);
                            disctrictArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerDistrict.setAdapter(disctrictArrayAdapter);

                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("zxcv", e.getLocalizedMessage());
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",statename);
                Log.e("params",params.toString());
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getBlocks(final String district_name){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL +"getblockslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            blockArrayList.clear();

                            blockArrayList.add("Select Block");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                blockArrayList.add(jsonObject.getString("block"));

                            }


                            blockArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,blockArrayList);
                            blockArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerBlock.setAdapter(blockArrayAdapter);
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",state);

                params.put("district_name",district_name);
                Log.e("params",params.toString());

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);

    }

    private void getGramPanchayat(final String block){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getgrampanchayatlist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            gramPanchayatArrayList.clear();
                            gramPanchayatArrayList.add("Select Gram Panchayat");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                gramPanchayatArrayList.add(jsonObject.getString("gname"));

                            }


                            gramPanchayatArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,gramPanchayatArrayList);
                            gramPanchayatArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerGramPanchayat.setAdapter(gramPanchayatArrayAdapter);
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",state);
                params.put("district_name",district);
                params.put("block",block);
                Log.e("params",params.toString());
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void sendEmail(final String data,final String email){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        //getting the whole json object from the response

                        // JSONObject jsonObject = new JSONObject(response);

                        try {

                            if (response.equals("Please Check Your Registered Mail for Registration")) {

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                Intent intent = new Intent(Signup.this, Login.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                                Toast.makeText(Signup.this, "" + response, Toast.LENGTH_SHORT).show();

                            }else if(response.equals("Invalid Email")) {

                                Toast.makeText(Signup.this, "Invalid email address", Toast.LENGTH_SHORT).show();

                                deleteAccount(email);
                            }else if(response.equals("")){

                                Toast.makeText(Signup.this, "Invalid email address", Toast.LENGTH_SHORT).show();

                                deleteAccount(email);
                            }

                        }catch (Exception e) {
                            // JSON error
                            e.printStackTrace();
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            Log.d("kcr catch",""+e.getLocalizedMessage());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("kcr error",""+error.getLocalizedMessage());
                        /*if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }*/
                        if (error.getLocalizedMessage()==null){
                            deleteAccount(email);
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();

                params.put("Data",data);
                Log.e("params1",params.toString());
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void deleteAccount(final String email){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"deleteaccount.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {


                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){


                                //    showGroundWaterLevelsDialog();
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                //Toast.makeText(Signup.this, "Please try After sometime...!", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(Signup.this, Login.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                                insertUserLog(email,getIpAddress(),logDate,"account delete","Account deleted Successfully");


                            } else {
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                Intent intent = new Intent(Signup.this, Login.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                                insertUserLog(email,getIpAddress(),logDate,"account delete","Account delete Failed");

                                Toast.makeText(Signup.this, ""+obj.getString("status"), Toast.LENGTH_SHORT).show();
                            }





                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            insertUserLog(email,getIpAddress(),logDate,"account delete","Account delete Failed");

                            Log.d("kcr delete catch", ""+e.getLocalizedMessage());
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Log.d("kcr delete vollyerror",""+error.getLocalizedMessage());

                        insertUserLog(email,getIpAddress(),logDate,"account delete","Account delete Failed");
                    }
                }){
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();

                params.put("email",email);
                Log.e("params1",params.toString());
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private static String get_SHA_256_SecurePassword(String passwordToHash)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            //md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    private void splitPassword(String str)
    {
        passText = new StringBuffer();
        passNum = new StringBuffer();
        passSpecial = new StringBuffer();

        for (int i=0; i<str.length(); i++)
        {
            if (Character.isDigit(str.charAt(i)))
                passNum.append(str.charAt(i));
            else if(Character.isAlphabetic(str.charAt(i)))
                passText.append(str.charAt(i));
            else
                passSpecial.append(str.charAt(i));
        }
        
    }

    private void splitEmail(String str)
    {
        emailText = new StringBuffer();
        emailNum = new StringBuffer();

        for (int i=0; i<str.length(); i++)
        {
            if (Character.isDigit(str.charAt(i)))
                emailNum.append(str.charAt(i));
            else if(Character.isAlphabetic(str.charAt(i)))
                emailText.append(str.charAt(i));

        }

    }

}
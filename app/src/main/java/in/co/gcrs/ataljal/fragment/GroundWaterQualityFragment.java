package in.co.gcrs.ataljal.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.Utils.AppLogger;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.databinding.FragmentGroundWaterQualityBinding;

public class GroundWaterQualityFragment extends Fragment implements AdapterView.OnItemSelectedListener,
        View.OnClickListener {

    private ArrayAdapter<String> spinnerWellTypeAdapter, spinnerBacterialAdapter;
    private String spinnerWellTypeSelectedItem = "", spinnerBacterialSelectedItem = "", bacterialtest, others = "";
    ;
    private ProgressDialog progressDialog;
    private MyAppPrefsManager myAppPrefsManager;
    private Geocoder geocoder;
    private String addressLocation = "";
    private Date c;
    private String formattedDate = "";
    private SimpleDateFormat df;
    private FragmentGroundWaterQualityBinding fragmentGroundWaterQualityBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate the layout for this fragment
//        View view=inflater.inflate(R.layout.fragment_ground_water_quality, container, false);
        fragmentGroundWaterQualityBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ground_water_quality, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionWaterQuality));

        myAppPrefsManager = new MyAppPrefsManager(getContext());

        c = Calendar.getInstance().getTime();
        df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        formattedDate = df.format(c).toUpperCase();
//        logDate = df1.format(c).toUpperCase();

        spinnerWellTypeAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.well_type_well_inventory));
        spinnerWellTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentGroundWaterQualityBinding.spinnerWellType.setAdapter(spinnerWellTypeAdapter);
        fragmentGroundWaterQualityBinding.spinnerWellType.setOnItemSelectedListener(this);

        spinnerBacterialAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.bacterial_test));
        spinnerBacterialAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentGroundWaterQualityBinding.spinnerbacterialtest.setAdapter(spinnerBacterialAdapter);
        fragmentGroundWaterQualityBinding.spinnerbacterialtest.setOnItemSelectedListener(this);

        getAddress();
        fragmentGroundWaterQualityBinding.editTextState.setText(myAppPrefsManager.getState());
        fragmentGroundWaterQualityBinding.editTextDistrict.setText(myAppPrefsManager.getDistrict());
        fragmentGroundWaterQualityBinding.editTextBlock.setText(myAppPrefsManager.getBlock());
        fragmentGroundWaterQualityBinding.editTextGramPanchayat.setText(myAppPrefsManager.getGrampanchayat());
        fragmentGroundWaterQualityBinding.textViewLatitude.setText(myAppPrefsManager.getLatitude());
        fragmentGroundWaterQualityBinding.textViewLongitude.setText(myAppPrefsManager.getLongitude());
        fragmentGroundWaterQualityBinding.textViewDate.setText(formattedDate);
        fragmentGroundWaterQualityBinding.buttonSave.setOnClickListener(this);

        return fragmentGroundWaterQualityBinding.getRoot();
    }

    private void getAddress() {
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(myAppPrefsManager.getLatitude()), Double.parseDouble(myAppPrefsManager.getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            addressLocation = address;
            fragmentGroundWaterQualityBinding.editTextLocationDetials.setText(address);
            Log.d("kcr", address);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
        Spinner spinWellType = (Spinner) parent;
        Spinner spinBacterial = (Spinner) parent;
        if (spinWellType.getId() == R.id.spinnerWellType) {
            spinnerWellTypeSelectedItem = parent.getSelectedItem().toString();
        }
        if (spinBacterial.getId() == R.id.spinnerbacterialtest) {
            bacterialtest = fragmentGroundWaterQualityBinding.spinnerbacterialtest.getSelectedItem().toString();
            if (bacterialtest.equals("Others")) {
                fragmentGroundWaterQualityBinding.linearOthers.setVisibility(View.VISIBLE);
            } else {
                fragmentGroundWaterQualityBinding.linearOthers.setVisibility(View.GONE);
                others = "";
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onClick(View view) {
        if (view == fragmentGroundWaterQualityBinding.buttonSave) {
            if (!myAppPrefsManager.getCategory().equals("")) {
                if (!fragmentGroundWaterQualityBinding.editTextState.getText().toString().equals("")) {
                    if (!fragmentGroundWaterQualityBinding.editTextDistrict.getText().toString().equals("")) {
                        if (!fragmentGroundWaterQualityBinding.editTextBlock.getText().toString().equals("")) {
                            if (!fragmentGroundWaterQualityBinding.editTextGramPanchayat.getText().toString().equals("")) {
                                if (!fragmentGroundWaterQualityBinding.editTextVillage.getText().toString().equals("")) {
                                    if (!fragmentGroundWaterQualityBinding.editTextLocationDetials.getText().toString().equals("")) {
                                        if (!fragmentGroundWaterQualityBinding.textViewLatitude.getText().toString().equals("")) {
                                            if (!fragmentGroundWaterQualityBinding.textViewLongitude.getText().toString().equals("")) {
                                                if (!fragmentGroundWaterQualityBinding.textViewDate.equals("")) {
                                                    if (!spinnerWellTypeSelectedItem.equals("Select Well Type")) {
                                                        if (!fragmentGroundWaterQualityBinding.editTextWellNo.getText().toString().equals("")) {
                                                            if (!fragmentGroundWaterQualityBinding.editTextwelldepth.getText().toString().matches("")) {
                                                                if (!fragmentGroundWaterQualityBinding.editTextph.getText().toString().matches("")) {
                                                                    if (!fragmentGroundWaterQualityBinding.editTexttotalhardness.getText().toString().trim().equals("")) {
                                                                        if (!fragmentGroundWaterQualityBinding.editTextalkalinity.getText().toString().trim().equals("")) {
                                                                            if (!fragmentGroundWaterQualityBinding.editTextnitrate.getText().toString().trim().equals("")) {
                                                                                if (!fragmentGroundWaterQualityBinding.editTextflouride.getText().toString().trim().equals("")) {
                                                                                    if (!fragmentGroundWaterQualityBinding.editTextiron.getText().toString().trim().equals("")) {
                                                                                        if (!fragmentGroundWaterQualityBinding.editTextarsenic.getText().toString().trim().equals("")) {
                                                                                            if (!fragmentGroundWaterQualityBinding.editTextec.getText().toString().trim().equals("")) {
                                                                                                if (!spinnerBacterialSelectedItem.equals("Select Bacterial Test")) {
                                                                                                    saveFormData();

                                                                                                } else {
                                                                                                    AppLogger.showToastSmall(getContext(), "Select Bacterial test");
                                                                                                }
                                                                                            } else {
                                                                                                fragmentGroundWaterQualityBinding.editTextec.setError("Enter value of ec");
                                                                                                fragmentGroundWaterQualityBinding.editTextec.requestFocus();
                                                                                            }
                                                                                        } else {
                                                                                            fragmentGroundWaterQualityBinding.editTextarsenic.setError("Enter value of arsenic");
                                                                                            fragmentGroundWaterQualityBinding.editTextarsenic.requestFocus();
                                                                                        }
                                                                                    } else {
                                                                                        fragmentGroundWaterQualityBinding.editTextiron.setError("Enter value of iron");
                                                                                        fragmentGroundWaterQualityBinding.editTextiron.requestFocus();
                                                                                    }
                                                                                } else {
                                                                                    fragmentGroundWaterQualityBinding.editTextflouride.setError("Enter value of flouride");
                                                                                    fragmentGroundWaterQualityBinding.editTextflouride.requestFocus();
                                                                                }
                                                                            } else {
                                                                                fragmentGroundWaterQualityBinding.editTextnitrate.setError("Enter value of nitrate");
                                                                                fragmentGroundWaterQualityBinding.editTextnitrate.requestFocus();
                                                                            }
                                                                        } else {
                                                                            fragmentGroundWaterQualityBinding.editTextalkalinity.setError("Enter value of alkalinity");
                                                                            fragmentGroundWaterQualityBinding.editTextalkalinity.requestFocus();
                                                                        }
                                                                    } else {
                                                                        fragmentGroundWaterQualityBinding.editTexttotalhardness.setError("Enter total hardness");
                                                                        fragmentGroundWaterQualityBinding.editTexttotalhardness.requestFocus();
                                                                    }

                                                                } else {
                                                                    fragmentGroundWaterQualityBinding.editTextph.setError("Enter value of ph");
                                                                    fragmentGroundWaterQualityBinding.editTextph.requestFocus();
                                                                }
                                                            } else {
                                                                fragmentGroundWaterQualityBinding.editTextwelldepth.setError("Enter Depth of well");
                                                                fragmentGroundWaterQualityBinding.editTextwelldepth.requestFocus();
//                                                                AppLogger.showToastSmall(getContext(),"Enter Depth of well");
                                                            }

                                                        } else {
                                                            fragmentGroundWaterQualityBinding.editTextWellNo.setError("Enter well id");
                                                            fragmentGroundWaterQualityBinding.editTextWellNo.requestFocus();
                                                        }
                                                    } else {
                                                        AppLogger.showToastSmall(getContext(), "Select Well Type");
                                                    }
                                                } else {
                                                    fragmentGroundWaterQualityBinding.textViewDate.setError("Enter Date");

                                                }
                                            } else {
                                                fragmentGroundWaterQualityBinding.textViewLongitude.setError("Enter Longitude");

                                            }
                                        } else {
                                            fragmentGroundWaterQualityBinding.textViewLatitude.setError("Enter Latitude");
                                        }
                                    } else {
                                        fragmentGroundWaterQualityBinding.editTextLocationDetials.setError("Enter Location Details");
                                        fragmentGroundWaterQualityBinding.editTextLocationDetials.requestFocus();
                                    }
                                } else {
                                    fragmentGroundWaterQualityBinding.editTextVillage.setError("Enter Village");
                                    fragmentGroundWaterQualityBinding.editTextVillage.requestFocus();
                                }
                            } else {
                                fragmentGroundWaterQualityBinding.editTextGramPanchayat.setError("Enter Grampanchayat");

                            }
                        } else {
                            fragmentGroundWaterQualityBinding.editTextBlock.setError("Enter Block");
                        }
                    } else {
                        fragmentGroundWaterQualityBinding.editTextDistrict.setError("Enter district");

                    }
                } else {
                    fragmentGroundWaterQualityBinding.editTextState.setError("Enter State");
                }

            } else {
                AppLogger.showToastSmall(getContext(), "Connect Internet and select nodal officer");
            }

        }
    }

    private void saveFormData() {
        if(isNetworkAvailable()){
            AppLogger.showToastSmall(getContext(),"Hello Save");
        }
    }
    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}

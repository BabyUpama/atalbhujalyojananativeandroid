package in.co.gcrs.ataljal.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.app.PublicPrefsManager;
import in.co.gcrs.ataljal.constant.ConstantValues;
import in.co.gcrs.ataljal.customs.LogOutTimerUtil;
import in.co.gcrs.ataljal.customs.RootUtil;
import in.co.gcrs.ataljal.fragment.PublicDashBoard;
import in.co.gcrs.ataljal.interfaces.IOnBackPress;

public class PublicView extends AppCompatActivity {

    ActionBar actionBar;
    Toolbar toolbar;
    Fragment fragment;
    FragmentManager fragmentManager;
    boolean doublePressedBackToExit = false;
    private PublicPrefsManager publicPrefsManager;
    private MyAppPrefsManager myAppPrefsManager;
    private RequestQueue requestQueue;
    private Dialog dialog;
    private ArrayList<String> stateArrayList,districtArrayList, blockArrayList, gramPanchayatArrayList;
    Spinner nodalOfficer, spinnerState , spinnerDistrict, spinnerBlock, spinnerGramPanchayat;
    LinearLayout stateLinear,districtLinear,blockLinear,grampanchayatLinear;
    String state="", district="", block="", grampanchayat="";

    private ProgressDialog progressDialog;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1002;

    private static final int PERMISSION_REQUEST_CODE = 1;

    private double latitude, longitude;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private static final int RC_APP_UPDATE=3563;
    private AppUpdateManager appUpdateManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_view);

        progressDialog = new ProgressDialog(PublicView.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBar.setTitle("Atal Jal");
        /*actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        myAppPrefsManager = new MyAppPrefsManager(getApplicationContext());
        publicPrefsManager = new PublicPrefsManager(getApplicationContext());
        requestQueue  = Volley.newRequestQueue(getApplicationContext());
        appUpdateManager = AppUpdateManagerFactory.create(PublicView.this);

        fragmentManager = getSupportFragmentManager();

            fragment = new PublicDashBoard();
            fragmentManager.beginTransaction()
                    .replace(R.id.public_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null).commit();

        if (isNetworkAvailable()) {
            if (publicPrefsManager.isFirstTimePublicLaunch()) {
               showSelection();
            } else {
                showSelection1();
            }
        }else {
            Toast.makeText(this, "Internet not available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {


        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.public_fragment);
        if (!(fragment instanceof IOnBackPress) || !((IOnBackPress) fragment).onBackPressed())
        {

        }


        // Get FragmentManager
        FragmentManager fm = getSupportFragmentManager();

        Log.d("asd",String.valueOf(fm.getBackStackEntryCount()));

        if (fm.getBackStackEntryCount() > 1) {

            // Pop previous Fragment
            fm.popBackStack();

        }

        else  if(fm.getBackStackEntryCount()==1){

            finish();

        }


        else {
            this.doublePressedBackToExit = true;
            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

            // Delay of 2 seconds
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    // Set doublePressedBackToExit false after 2 seconds
                    doublePressedBackToExit = false;
                }
            }, 2000);

            finish();
        }
    }
    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private void showSelection(){

        dialog = new Dialog(PublicView.this);
        /*if (publicPrefsManager.isFirstTimePublicLaunch()) {
            dialog.setCancelable(false);
        }*/
        dialog.setContentView(R.layout.item_selection);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        stateArrayList = new ArrayList<>();
        districtArrayList = new ArrayList<>();
        blockArrayList = new ArrayList<>();
        gramPanchayatArrayList = new ArrayList<>();

        TextView select = dialog.findViewById(R.id.select);
        select.setVisibility(View.GONE);
        nodalOfficer = (Spinner) dialog.findViewById(R.id.nodalOfficer);
        nodalOfficer.setVisibility(View.GONE);
        spinnerState = (Spinner)dialog.findViewById(R.id.spinneerState);
        spinnerDistrict = (Spinner)dialog.findViewById(R.id.spinnerDistrict);
        spinnerBlock = (Spinner)dialog.findViewById(R.id.spinnerBlock);
        spinnerGramPanchayat = (Spinner)dialog.findViewById(R.id.spinnerGrampanchayat);

        stateLinear = (LinearLayout) dialog.findViewById(R.id.stateLinear);
        districtLinear = (LinearLayout) dialog.findViewById(R.id.districtLinear);
        blockLinear = (LinearLayout) dialog.findViewById(R.id.blockLinear);
        grampanchayatLinear = (LinearLayout) dialog.findViewById(R.id.panchayatLinear);

        Button submit = dialog.findViewById(R.id.submit);


        final ArrayAdapter nodalAdapter = new ArrayAdapter(PublicView.this,android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.nodal));
        nodalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nodalOfficer.setAdapter(nodalAdapter);

        nodalOfficer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!=0) {
                   // myAppPrefsManager.setCategory(nodalOfficer.getSelectedItem().toString());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getStates();

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView)view).setTextColor(Color.BLACK);
                if(position!=0)
                {

                    state = spinnerState.getSelectedItem().toString();
                    publicPrefsManager.setPublicstate(state);

                    if (progressDialog!=null&&!progressDialog.isShowing()){
                        progressDialog.show();
                    }

                    getDistricts(spinnerState.getSelectedItem().toString());




                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView)view).setTextColor(Color.BLACK);
                if(position!=0){

                    district = spinnerDistrict.getSelectedItem().toString();
                    if(!spinnerDistrict.getSelectedItem().toString().equals("Select District")){
                        district = spinnerDistrict.getSelectedItem().toString();
                        publicPrefsManager.setPublicdistrict(district);

                        if (progressDialog!=null&&!progressDialog.isShowing()){
                            progressDialog.show();
                        }

                        getBlocks(spinnerDistrict.getSelectedItem().toString());

                    }



                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerBlock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)view).setTextColor(Color.BLACK);
                if (i!=0){
                    block = spinnerBlock.getSelectedItem().toString();
                    if (!spinnerBlock.getSelectedItem().toString().equals("Select Block")){
                        block = spinnerBlock.getSelectedItem().toString();
                        publicPrefsManager.setPublicblock(block);

                        if (progressDialog!=null&&!progressDialog.isShowing()){
                            progressDialog.show();
                        }

                        getGramPanchayat(block);

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerGramPanchayat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)view).setTextColor(Color.BLACK);
                if (i!=0){
                    grampanchayat = spinnerGramPanchayat.getSelectedItem().toString();
                    publicPrefsManager.setPublicgrampanchayat(grampanchayat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {

                            if (!stateArrayList.equals("")) {

                                if (!spinnerState.getSelectedItem().toString().equals("Select State")) {

                                    if (!districtArrayList.equals("")) {

                                        if (!spinnerDistrict.getSelectedItem().toString().equals("Select District")) {

                                            if (!blockArrayList.equals("")) {

                                                if (!spinnerBlock.getSelectedItem().toString().equals("Select Block")) {

                                                    if (!gramPanchayatArrayList.equals("")) {

                                                        if (!spinnerGramPanchayat.getSelectedItem().toString().equals("Select Gram Panchayat")) {

                                                            if (dialog != null) {
                                                                dialog.dismiss();
                                                                publicPrefsManager.setFirstTimePublicLaunch(false);

                                                                fragment = new PublicDashBoard();
                                                                fragmentManager.beginTransaction()
                                                                        .replace(R.id.public_fragment, fragment)
                                                                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                                                        .addToBackStack(null).commit();

                                                            }
                                                        } else {
                                                            Toast.makeText(PublicView.this, "Please Select Gram Panchayat", Toast.LENGTH_SHORT).show();
                                                        }

                                                    }
                                                } else {
                                                    Toast.makeText(PublicView.this, "Please Select Block", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        } else {
                                            Toast.makeText(PublicView.this, "Please Select District", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                } else {
                                    Toast.makeText(PublicView.this, "Please Select State", Toast.LENGTH_SHORT).show();
                                }
                            }
                }else {
                    Toast.makeText(PublicView.this, "Internet not Available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (dialog != null) {
            dialog.show();

        }

    }
    private void getStates(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL +"getstateslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            stateArrayList.clear();
                            stateArrayList.add("Select State");

                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                stateArrayList.add(jsonObject.getString("state_name"));

                            }


                            ArrayAdapter statesArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,stateArrayList);
                            statesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerState.setAdapter(statesArrayAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();

                params.put("latitude",myAppPrefsManager.getLatitude());
                params.put("longitude",myAppPrefsManager.getLongitude());
                params.put("encpassword","this is a password");

                return params;
            }

        };

        requestQueue.add(stringRequest);

    }

    private void getDistricts(final String statename) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getdistrictslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            districtArrayList.clear();

                            districtArrayList.add("Select District");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                districtArrayList.add(jsonObject.getString("district_name"));
                                progressDialog.dismiss();

                            }

                            ArrayAdapter disctrictArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,districtArrayList);
                            disctrictArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerDistrict.setAdapter(disctrictArrayAdapter);



                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",statename);

                return params;
            }

        };

        requestQueue.add(stringRequest);

    }

    private void getBlocks(final String district_name){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getblockslist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            blockArrayList.clear();

                            blockArrayList.add("Select Block");



                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);


                                progressDialog.dismiss();

                                blockArrayList.add(jsonObject.getString("block"));



                            }


                            ArrayAdapter blockArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,blockArrayList);
                            blockArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerBlock.setAdapter(blockArrayAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",state);
                params.put("district_name",district_name);

                return params;
            }

        };

        requestQueue.add(stringRequest);

    }

    private void getGramPanchayat(final String block){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"getgrampanchayatlist.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);

                        try {
                            //getting the whole json object from the response

                            JSONArray jsonArray = new JSONArray(response);

                            gramPanchayatArrayList.clear();
                            gramPanchayatArrayList.add("Select Gram Panchayat");


                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                gramPanchayatArrayList.add(jsonObject.getString("gname"));
                                progressDialog.dismiss();

                            }


                            ArrayAdapter gramPanchayatArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,gramPanchayatArrayList);
                            gramPanchayatArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerGramPanchayat.setAdapter(gramPanchayatArrayAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Log.d("zxcv", e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("state_name",state);
                params.put("district_name",district);
                params.put("block",block);

                return params;
            }

        };

        requestQueue.add(stringRequest);

    }

    private void showSelection1(){

        dialog = new Dialog(PublicView.this);
        //dialog.setCancelable(false);
        dialog.setContentView(R.layout.item_selection1);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        Button buttonExisting  = dialog.findViewById(R.id.buttonExisting);
        Button buttonNew  = dialog.findViewById(R.id.buttonNew);

        if (dialog!=null){
            dialog.show();
        }

        buttonExisting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog!=null){
                    dialog.dismiss();
                    /*fragment = new PublicDashBoard();
                    fragmentManager.beginTransaction()
                            .replace(R.id.public_fragment, fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack(null).commit();*/
                }
            }
        });
        buttonNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog!=null){
                    dialog.dismiss();
                }
                //myAppPrefsManager.setFirstTimeLaunch(true);
                showSelection();
            }
        });



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case LOCATION_PERMISSION_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation();


                        break;

                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Please Enable Location", Toast.LENGTH_SHORT).show();
                        displayLocationSettings();

                }
                break;

        }

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        }
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(PublicView.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {



                    // current location

                    latitude = location.getLatitude();
                    longitude = location.getLongitude();



                    myAppPrefsManager.setLatitude(String.valueOf(latitude));
                    myAppPrefsManager.setLongitude(String.valueOf(longitude));


                }

            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissionsList, grantResults);

        switch (requestCode) {


            case  LOCATION_PERMISSION_REQUEST_CODE :{


                if (grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                        Log.d("asd","zxc1");
                        //Do the stuff that requires permission...
                    }else if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(PublicView.this,
                                Manifest.permission.ACCESS_FINE_LOCATION)) {
                            //Show permission explanation dialog...


                            // we can ask again

                            Log.d("asd","zxc11");

                            // 2

                            askPermission();
                        }else{

                            // showAppSettings();

                            Log.d("asd","zxc111");
                            //Never ask again selected, or device policy prohibits the app from having that permission.
                            //So, disable that feature, or fall back to another situation...
                        }
                    }
                }
            }




            default:

                break;

        }
    }

    private void displayLocationSettings() {


        final LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (ContextCompat.checkSelfPermission(PublicView.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                for (Location location : locationResult.getLocations()) {
                                    //Do what you want with location
                                    //like update camera


                                    myAppPrefsManager.setLatitude(String.valueOf(location.getLatitude()));
                                    myAppPrefsManager.setLongitude(String.valueOf(location.getLongitude()));

                                }

                            }
                        },null);

                    }
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(PublicView.this, LOCATION_PERMISSION_REQUEST_CODE);
                                break;
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.

                            displayLocationSettings();
                            break;
                    }
                }}
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        checkLocationPermission();

        if(RootUtil.isDeviceRooted()){
            showAlertDialogAndExitApp("This device is rooted. You can't use this app.");
        }else if (RootUtil.isEmulator(getApplicationContext())){
            showAlertDialogAndExitApp("This is Emulator. You can't use this app.");
        }

        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(new com.google.android.play.core.tasks.OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)){

                    try {
                        appUpdateManager.startUpdateFlowForResult(
                                appUpdateInfo, AppUpdateType.FLEXIBLE, PublicView.this, RC_APP_UPDATE);
                    }

                    catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }

                } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED){
                    popupSnackbarForCompleteUpdate();
                } else {
                    Log.e("", "checkForAppUpdateAvailability: something else");
                }
            }
        });


    }
    public void showAlertDialogAndExitApp(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(PublicView.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }


    private void checkLocationPermission(){

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    showAppSettings();
                }

            }
        } else {
            displayLocationSettings();

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(PublicView.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }


    private void showAppSettings(){

        Toast.makeText(PublicView.this, "Please Allow the permission for Location in settings", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PublicView.this);
        alertDialogBuilder.setTitle("Change Permissions in Settings");
        alertDialogBuilder
                .setMessage("" +
                        "\nClick SETTINGS to Manually Set\n" + "Permissions to Access Location")
                .setCancelable(false)
                .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1000);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void popupSnackbarForCompleteUpdate() {

        Snackbar snackbar =
                Snackbar.make(
                        findViewById(R.id.linear),
                        "New app is ready!",
                        Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Install", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appUpdateManager != null){
                    appUpdateManager.completeUpdate();
                }
            }
        });

        snackbar.setActionTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }
}
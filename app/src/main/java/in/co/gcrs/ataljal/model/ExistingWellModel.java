package in.co.gcrs.ataljal.model;

public class ExistingWellModel
{
    String grampanchayat;
    String district ;
    String latitude ;
    String sitename ;
    String typeofwell;
    String block ;
    String state ;
    String source ;
    String wellid ;
    String status ;
    String longitude;


    public String getGP() {
        return grampanchayat;
    }

    public void setGP(String grampanchayat) {
        this.grampanchayat = grampanchayat;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }


    public String getSiteName() {
        return sitename;
    }

    public void setSiteName(String sitename) {
        this.sitename = sitename;
    }

    public String getTypeofwell() {
        return typeofwell;
    }

    public void setTypeofwell(String typeofwell) {
        this.typeofwell = typeofwell;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }


    public String getWellId() {
        return wellid;
    }

    public void setWellId(String wellid) {
        this.wellid = wellid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}

package in.co.gcrs.ataljal.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.co.gcrs.ataljal.BuildConfig;
import in.co.gcrs.ataljal.R;
import in.co.gcrs.ataljal.activities.CameraGeoActivity;
import in.co.gcrs.ataljal.activities.Selection;
import in.co.gcrs.ataljal.api.Status;
import in.co.gcrs.ataljal.app.MyAppPrefsManager;
import in.co.gcrs.ataljal.database.DbHelper;
import in.co.gcrs.ataljal.model.GeotaggingModel;
import in.co.gcrs.ataljal.myviewmodel.UploadImgViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class GeoTaggingArtificialRecharge extends Fragment {


    private EditText editTextStorageCapacity,editTextNoofFillings,editTextOthers,editTextVillage,editTextSiteName;
    private Spinner spinnerStructure;
    private ArrayAdapter structureAdapter;
    private Button save;
    private String storageCapacity,noofFillings,structure,others="";
    private ImageView image1,image2;
    private String locationdetails="",imagePath1="",imagePath2="",serverPath1="",serverPath2="",imagename1="",imagename2="";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private Bitmap bitmap;

    private MyAppPrefsManager myAppPrefsManager;
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog;
    private DbHelper dbHelper;
    private GeotaggingModel geotaggingModel;
    private TextView count,editTextState,editTextDistrict,editTextBlock,editTextGramPanchayat,editTextLocationDetials,
            textViewLatitude,textViewLongitude,textViewDate;
    private LinearLayout linearOthers,layoutCount;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    Date c ;
    String formattedDate="",logDate="",code="";
    SimpleDateFormat df,df1;

    String id="",state="",district="",block="",gp="",date="",other="",capacity="",fillings="";
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1002;
    private static final int PERMISSION_REQUEST_CODE = 1003;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    double loclatitude=0,locLongitude=0;
    UploadImgViewModel viewModel;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Geocoder geocoder;
    private String addressLocation="";
    public GeoTaggingArtificialRecharge() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_geo_tagging_artificial_recharge, container, false);
        viewModel = new ViewModelProvider(this).get(UploadImgViewModel.class);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.actionGeoTagging));

        requestQueue = Volley.newRequestQueue(getContext());
        myAppPrefsManager = new MyAppPrefsManager(getContext());
        fragmentManager = getActivity().getSupportFragmentManager();
        dbHelper = new DbHelper(getContext());
        geotaggingModel = new GeotaggingModel();

        progressDialog= new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        c= Calendar.getInstance().getTime();
        df= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.ENGLISH);
        df1= new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        formattedDate = df.format(c).toUpperCase();
        logDate = df1.format(c).toUpperCase();

        editTextState = view.findViewById(R.id.editTextState);
        editTextDistrict = view.findViewById(R.id.editTextDistrict);
        editTextBlock = view.findViewById(R.id.editTextBlock);
        editTextGramPanchayat = view.findViewById(R.id.editTextGramPanchayat);
        editTextVillage = view.findViewById(R.id.editTextVillage);
        editTextSiteName = view.findViewById(R.id.editTextSiteName);
        textViewLatitude = view.findViewById(R.id.textViewLatitude);
        textViewLongitude = view.findViewById(R.id.textViewLongitude);
        editTextLocationDetials=view.findViewById(R.id.editTextLocationDetials);
        editTextStorageCapacity = view.findViewById(R.id.editTextStorageCapacity);
        editTextNoofFillings = view.findViewById(R.id.editTextNoofFillings);
        editTextOthers = view.findViewById(R.id.editTextOthers);
        spinnerStructure = view.findViewById(R.id.spinnerStructure);
        textViewDate=view.findViewById(R.id.textViewDate);
        image1 = view.findViewById(R.id.image1);
        image2 = view.findViewById(R.id.image2);
        count = view.findViewById(R.id.count);
        linearOthers = view.findViewById(R.id.linearOthers);
        layoutCount = view.findViewById(R.id.layoutCount);
        getAddress();
        editTextState.setText(myAppPrefsManager.getState());
        editTextDistrict.setText(myAppPrefsManager.getDistrict());
        editTextBlock.setText(myAppPrefsManager.getBlock());
        editTextGramPanchayat.setText(myAppPrefsManager.getGrampanchayat());
        textViewLatitude.setText(myAppPrefsManager.getLatitude());
        textViewLongitude.setText(myAppPrefsManager.getLongitude());


        save = view.findViewById(R.id.buttonSave);

        structureAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_dd_item, R.id.simple_spinner_dropdown, getResources().getStringArray(R.array.arstructure)) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent)
            {
                return getView(position, convertView, parent);
            }
        };
        spinnerStructure.setAdapter(structureAdapter);

        spinnerStructure.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    structure = spinnerStructure.getSelectedItem().toString();
                    if (structure.equals("Others")){
                        linearOthers.setVisibility(View.VISIBLE);
                    }else {
                        linearOthers.setVisibility(View.GONE);
                        others="";
                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions(getContext())) {
                Intent intent= new Intent(getContext(), CameraGeoActivity.class);
                intent.putExtra("value","3");
                startActivityForResult(intent,1);
                }
                /*code="1";
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                }*/

            }
        });

        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions(getContext())) {
                Intent intent= new Intent(getContext(), CameraGeoActivity.class);
                intent.putExtra("value","3");
                startActivityForResult(intent,2);
                }
                /*code="2";
                if (checkAndRequestPermissions(getContext())){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 2);
                }*/

            }
        });
        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        date=dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        textViewDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }

        });
        if (getArguments()!=null){
            id = getArguments().getString("id");
            date=getArguments().getString("date");
            state = getArguments().getString("state");
            district = getArguments().getString("district");
            block = getArguments().getString("block");
            gp = getArguments().getString("gp");
            locationdetails=getArguments().getString("locationdetails");
            int structurepos = structureAdapter.getPosition(getArguments().getString("structure"));
            spinnerStructure.setSelection(structurepos);
            capacity = getArguments().getString("capacity");
            fillings = getArguments().getString("fillings");
            String dbimage1 = getArguments().getString("image1");
            String dbimage2 = getArguments().getString("image2");
            other = getArguments().getString("others");
            if (getArguments().getString("structure").equals("Others")){
                linearOthers.setVisibility(View.VISIBLE);
                editTextOthers.setText(other);
            }else {
                linearOthers.setVisibility(View.GONE);
            }
            textViewDate.setText(date);
            editTextStorageCapacity.setText(capacity);
            editTextNoofFillings.setText(fillings);
            editTextVillage.setText(getArguments().getString("village"));
            editTextSiteName.setText(getArguments().getString("sitename"));
            textViewLatitude.setText(getArguments().getString("latitude"));
            textViewLongitude.setText(getArguments().getString("longitude"));

            if (!dbimage1.equals("")){
                serverPath1=dbimage1;
                Glide.with(getActivity()).load(dbimage1).into(image1);
            }else {
                image1.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
            }
            if (!dbimage2.equals("")){
                serverPath2 = dbimage2;
                Glide.with(getActivity()).load(dbimage2).into(image2);
            }else {
                image2.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
            }

        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!date.equals("")) {
                    if (!myAppPrefsManager.getCategory().equals("")) {
                        if (!myAppPrefsManager.getState().equals("")) {
                            if (!myAppPrefsManager.getDistrict().equals("")) {
                                if (!myAppPrefsManager.getBlock().equals("")) {
                                    if (!myAppPrefsManager.getGrampanchayat().equals("")) {
                                        if (!editTextVillage.getText().toString().trim().equals("")) {
//                                            if (!editTextSiteName.getText().toString().trim().equals("")) {
                                                if (!editTextLocationDetials.getText().toString().equals("")) {
                                                    if (!textViewLatitude.getText().toString().trim().equals("")) {
                                                        if (!textViewLongitude.getText().toString().trim().equals("")) {
                                                            if (!spinnerStructure.getSelectedItem().equals("Select Type of Structure")) {
                                                                if (structure.equals("Others")) {
                                                                    if (!editTextOthers.getText().toString().equals("")) {
                                                                        others = editTextOthers.getText().toString();
                                                                        extra();
                                                                    } else {
                                                                        Toast.makeText(getContext(), "Enter Other Structure Name", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                } else {
                                                                    extra();
                                                                }

                                                            } else {
                                                                Toast.makeText(getContext(), "Select Type of Structure", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {
                                                            Toast.makeText(getContext(), "Enter longitude", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        Toast.makeText(getContext(), "Enter latitude", Toast.LENGTH_SHORT).show();
                                                    }
                                                }else {
                                                    editTextLocationDetials.setError("Enter Location Details");
                                                    editTextLocationDetials.requestFocus();
                                                }
//                                                } else {
//                                                    Toast.makeText(getContext(), "Enter site name", Toast.LENGTH_SHORT).show();
//                                                }
                                        } else {
                                            Toast.makeText(getContext(), "Enter village name", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getContext(), "Select Gram Panchayat", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getContext(), "Select Block", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "Select District", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Select State", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Connect Internet and select nodal officer", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "Select Date", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (isNetworkAvailable()) {
            layoutCount.setVisibility(View.VISIBLE);
            getCountPraticular();
        }else {
            layoutCount.setVisibility(View.GONE);
        }


        return view;
    }

    public void extra(){
        if (!editTextStorageCapacity.getText().toString().trim().equals("")&&!editTextStorageCapacity.getText().toString().trim().startsWith(".")) {
            if (!editTextNoofFillings.getText().toString().trim().equals("")&&!editTextNoofFillings.getText().toString().trim().startsWith(".")) {
//                if (!imagePath1.equals("")||!serverPath1.equals("")) {
//                    if (!imagePath2.equals("")||!serverPath2.equals("")) {
                        storageCapacity = editTextStorageCapacity.getText().toString().trim();
                        noofFillings = editTextNoofFillings.getText().toString().trim();

                        geotaggingModel.setState(myAppPrefsManager.getState());
                        geotaggingModel.setDistrict(myAppPrefsManager.getDistrict());
                        geotaggingModel.setBlock(myAppPrefsManager.getBlock());
                        geotaggingModel.setGramPanchayat(myAppPrefsManager.getGrampanchayat());
                        geotaggingModel.setVillagename(editTextVillage.getText().toString().trim());
//                        geotaggingModel.setSitename(editTextSiteName.getText().toString().trim());
                        geotaggingModel.setLocationDetails1(editTextLocationDetials.getText().toString().trim());
                        geotaggingModel.setCalenderdate(date);
                        geotaggingModel.setTypeofStructure(spinnerStructure.getSelectedItem().toString());
                        geotaggingModel.setStorageCapacity(storageCapacity);
                        geotaggingModel.setNoOfFillings(noofFillings);
                        geotaggingModel.setImage1(imagePath1);
                        geotaggingModel.setImage2(imagePath2);
                        geotaggingModel.setOthers(others);
                        geotaggingModel.setLatitude(textViewLatitude.getText().toString());
                        geotaggingModel.setLongitude(textViewLatitude.getText().toString());
                        geotaggingModel.setDate(formattedDate);
                        geotaggingModel.setEmail(myAppPrefsManager.getUserEmail());
                        geotaggingModel.setImageName1(imagename1);
                        geotaggingModel.setImageName2(imagename2);

                        if (isNetworkAvailable()) {

                            if (progressDialog != null && !progressDialog.isShowing()) {
                                progressDialog.show();
                            }
                            if (!id.equals("")) {
                                if (!imagePath1.equals("")) {
                                    insertImage1();
                                } else if (!imagePath2.equals("")) {
                                    insertImage2();
                                } else {
                                    updateGeoTagging(id,editTextVillage.getText().toString().trim(), editTextSiteName.getText().toString().trim(),
                                            editTextLocationDetials.getText().toString().trim(),textViewLatitude.getText().toString().trim(),textViewLongitude.getText().toString().trim(),
                                            textViewDate.getText().toString(),spinnerStructure.getSelectedItem().toString(), storageCapacity, noofFillings, serverPath1, serverPath2);
                                }

                            } else {
                                if (!imagePath1.equals("")) {
                                    insertImage1();

                                } else if (!imagePath2.equals("")) {
                                    insertImage2();
                                }
                                else{
                                    insertGeoTagging(editTextVillage.getText().toString().trim(),
                                            editTextSiteName.getText().toString().trim(),editTextLocationDetials.getText().toString().trim(),textViewLatitude.getText().toString(),
                                            textViewLongitude.getText().toString().trim(), textViewDate.getText().toString(),structure, storageCapacity, noofFillings);
                                }
                            }

                        } else {
                            boolean status = dbHelper.insertGeoTagging(geotaggingModel);
                            if (progressDialog != null && !progressDialog.isShowing()) {
                                progressDialog.show();
                            }

                            if (status == true) {

                                progressDialog.dismiss();

                                fragment = new DashBoardFragment();
                                fragmentManager.beginTransaction()
                                        .remove(fragment)
                                        .commit();
                                fragmentManager.popBackStack();


                                Toast.makeText(getContext(), "Data saved in offline mode and will sync with server once online", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
//                    }
//                    else {
//                        Toast.makeText(getContext(), "please upload image 2", Toast.LENGTH_SHORT).show();
//                    }
//                }
//                else {
//                    Toast.makeText(getContext(), "Please upload image 1", Toast.LENGTH_SHORT).show();
//                }
            } else {
                editTextNoofFillings.setError("Enter no of fillings");
                editTextNoofFillings.requestFocus();
            }
        } else {
            editTextStorageCapacity.setError("Enter Storage Capacity");
            editTextStorageCapacity.requestFocus();
        }
    }

    private static boolean checkAndRequestPermissions(final Context context) {

        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    101);
            return false;
        }
        return true;
    }

    public void getAddress()
    {
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(myAppPrefsManager.getLatitude()), Double.parseDouble(myAppPrefsManager.getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            addressLocation=address;
            editTextLocationDetials.setText(address);
            Log.d("kcr",address);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
                Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // Permission Denied
                Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT)
                        .show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if (requestCode == 101) {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Camera permission", Toast.LENGTH_SHORT)
                        .show();
                getActivity().finish();
            } else if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "you can't use this app without Storage permission",
                        Toast.LENGTH_SHORT).show();
                getActivity().finish();
            } else {
                switch (code) {
                    case "1": {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 1);
                        break;
                    }
                    case "2": {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 2);
                        break;
                    }
                }
            }
        }

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    Log.d("asd", "zxc1");
                    //Do the stuff that requires permission...
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        //Show permission explanation dialog...


                        // we can ask again

                        Log.d("asd", "zxc11");

                        // 2

                        askPermission();
                    } else {

                        // showAppSettings();

                        Log.d("asd", "zxc111");
                        //Never ask again selected, or device policy prohibits the app from having that permission.
                        //So, disable that feature, or fall back to another situation...
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

       if (requestCode==1) {
            if (resultCode == Activity.RESULT_OK) {
                String path = data.getStringExtra("path");
                imagePath1 = path;
                bitmap = BitmapFactory.decodeFile(path);
                image1.setImageBitmap(bitmap);

                Path path1 = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path1 = Paths.get(imagePath1);
                    fileName = path1.getFileName();
                    imagename1 = fileName.toString();
                }else {
                    imagename1 = imagePath1.substring(imagePath1.indexOf("/")+1);
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
            }

        }else if (requestCode==2){
           if (resultCode == Activity.RESULT_OK) {
                   String path = data.getStringExtra("path");

                   imagePath2 = path;

                   bitmap = BitmapFactory.decodeFile(path);
                   image2.setImageBitmap(bitmap);

               Path path1 = null;
               Path fileName = null;
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                   path1 = Paths.get(imagePath2);
                   fileName = path1.getFileName();
                   imagename2 = fileName.toString();
               }else {
                   imagename2 = imagePath2.substring(imagePath2.indexOf("/")+1);
               }

           }
           if (resultCode == Activity.RESULT_CANCELED) {
               //Write your code if there's no result
               Toast.makeText(getActivity(), "Camera Cancelled", Toast.LENGTH_SHORT).show();
           }

       }

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    getLocation();


                    break;

                case Activity.RESULT_CANCELED:
                    Toast.makeText(getContext(), "Please Enable Location", Toast.LENGTH_SHORT).show();
                    // displayLocationSettings();
                    displayLocationSettingsRequest(getContext());

            }
        }

    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        }
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {



                    // current location

                    loclatitude = location.getLatitude();
                    locLongitude = location.getLongitude();

                    myAppPrefsManager.setLatitude(String.valueOf(loclatitude));
                    myAppPrefsManager.setLongitude(String.valueOf(locLongitude));


                }

            }
        });
    }

    private void  insertImage1(){

        //imagePath
        File pic = new File(imagePath1);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverPath1 = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Image Upload "+imagename1+" Successful");
                        Log.d("kcr",serverPath1);
                        if (!imagePath2.equals("")) {
                                        insertImage2();
                                    }else {
                                        if (!id.equals("")){
                                            updateGeoTagging(id,editTextVillage.getText().toString().trim(), editTextSiteName.getText().toString().trim(),
                                                    editTextLocationDetials.getText().toString().trim(),textViewLatitude.getText().toString().trim(),textViewLongitude.getText().toString().trim(),
                                                    textViewDate.getText().toString(),spinnerStructure.getSelectedItem().toString(), storageCapacity, noofFillings, serverPath1, serverPath2);
                                        }else {
                                            insertGeoTagging(editTextVillage.getText().toString().trim(),
                                                    editTextSiteName.getText().toString().trim(),textViewLatitude.getText().toString(),
                                                    textViewLongitude.getText().toString().trim(),editTextLocationDetials.getText().toString().trim(),
                                                    textViewDate.getText().toString(),structure, storageCapacity, noofFillings);
                                        }
                                    }

                    }
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                } else if (entity.status == Status.LOADING) {
                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Image Upload "+imagename1+" Unsuccessful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URLUPLOAD+ "imageupload")
//                .setTimeout(60 * 60 * 1000)
//                .setMultipartFile("img", "multipart/form-data", new File(imagePath1))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success")) {
//
//                                    serverPath1 = jsonObject.getString("path");
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Image Upload "+imagename1+" Successful");
//
//                                    Log.d("kcr",serverPath1);
//                                    if (!imagePath2.equals("")) {
//                                        insertImage2();
//                                    }else {
//                                        if (!id.equals("")){
//                                            updateGeoTagging(id,editTextVillage.getText().toString().trim(), editTextSiteName.getText().toString().trim(),
//                                                    textViewLatitude.getText().toString().trim(),textViewLongitude.getText().toString().trim(),
//                                                    spinnerStructure.getSelectedItem().toString(), storageCapacity, noofFillings, serverPath1, serverPath2);
//                                        }else {
//                                            insertGeoTagging(editTextVillage.getText().toString().trim(),
//                                                    editTextSiteName.getText().toString().trim(),textViewLatitude.getText().toString(),
//                                                    textViewLongitude.getText().toString().trim(),
//                                                    structure, storageCapacity, noofFillings);
//                                        }
//                                    }
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Image Upload "+imagename1+" Unsuccessful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse", e.toString());
//                            }else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//
//
//                    }
//                });
    }

    private void  insertImage2(){
        //imagePath
        File pic = new File(imagePath2);
        Log.e("TAG",pic.getName());
        Log.e("TAG",pic.getPath());
        Log.e("TAG",pic.getAbsolutePath());
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("inputfile", pic.getName(), RequestBody.create(pic, MediaType.parse("image/*")));
        viewModel.saveImageFileApi(imagePart).observe(getViewLifecycleOwner(), entity -> {
            if (entity != null) {
                if (entity.status == Status.SUCCESS) {
                    if (entity.data != null) {
//                        Log.e("TAG",entity.data.getStatus());
                        Log.e("TAG_path",entity.message);
//                        Toast.makeText(requireContext(), "entity.data.getStatus()",Toast.LENGTH_SHORT).show();
                        serverPath2 = entity.message;
                        insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Image Upload "+imagename2+" Successful");

                                    Log.d("kcr",serverPath2);
                                    if (!id.equals("")){

                                        updateGeoTagging(id,editTextVillage.getText().toString().trim(), editTextSiteName.getText().toString().trim(),
                                                editTextLocationDetials.getText().toString().trim(),textViewLatitude.getText().toString().trim(),textViewLongitude.getText().toString().trim(),
                                                textViewDate.getText().toString(),spinnerStructure.getSelectedItem().toString(), storageCapacity, noofFillings, serverPath1, serverPath2);
                                    }else {
                                        insertGeoTagging(editTextVillage.getText().toString().trim(),
                                                editTextSiteName.getText().toString().trim(),textViewLatitude.getText().toString(),
                                                textViewLongitude.getText().toString().trim(),editTextLocationDetials.getText().toString().trim(),
                                                textViewDate.getText().toString(),structure, storageCapacity, noofFillings);
                                    }

                    }


                                    if(progressDialog!=null){
                                        if(progressDialog.isShowing()){
                                            progressDialog.dismiss();
                                        }
                                    }
                } else if (entity.status == Status.LOADING) {
                    Toast.makeText(requireContext(), "loading",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("TAG",entity.message);
                    Toast.makeText(requireContext(), entity.message,Toast.LENGTH_SHORT).show();
                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Image Upload "+imagename1+" Unsuccessful");
                    if(progressDialog!=null){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });

//        Ion.with(getContext())
//                .load(BuildConfig.SERVER_URL+ "imageupload.jsp")
//                .setTimeout(60 * 60 * 1000)
//                .setMultipartFile("img", "multipart/form-data", new File(imagePath2))
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {
//
//                        if(result!=null) {
//
//                            Log.d("kcr1234", result.getResult());
//
//
//                            try {
//
//                                JSONObject jsonObject = new JSONObject(result.getResult());
//
//                                if (jsonObject.getString("status").equals("success")) {
//
//                                    serverPath2 = jsonObject.getString("path");
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Image Upload "+imagename2+" Successful");
//
//                                    Log.d("kcr",serverPath2);
//                                    if (!id.equals("")){
//
//                                        updateGeoTagging(id,editTextVillage.getText().toString().trim(), editTextSiteName.getText().toString().trim(),
//                                                textViewLatitude.getText().toString().trim(),textViewLongitude.getText().toString().trim(),
//                                                spinnerStructure.getSelectedItem().toString(), storageCapacity, noofFillings, serverPath1, serverPath2);
//                                    }else {
//                                        insertGeoTagging(editTextVillage.getText().toString().trim(),
//                                                editTextSiteName.getText().toString().trim(),textViewLatitude.getText().toString(),
//                                                textViewLongitude.getText().toString().trim(),
//                                                structure, storageCapacity, noofFillings);
//                                    }
//
//                                } else {
//                                    Toast.makeText(getContext(), jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
//
//                                    insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Image Upload "+imagename1+" Unsuccessful");
//
//                                    if(progressDialog!=null){
//                                        if(progressDialog.isShowing()){
//                                            progressDialog.dismiss();
//                                        }
//                                    }
//                                }
//
//                            } catch (JSONException ex) {
//                                Log.d("kcr", "error" + ex.toString());
//
//                                if(progressDialog!=null){
//                                    if(progressDialog.isShowing()){
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            }
//
//
//                        }
//                        else {
//
//                            if(e!=null) {
//                                Log.d("kcrelse", e.toString());
//                            }else {
//                                Log.d("kcrelse", "failed");
//                            }
//
//                            if(progressDialog!=null){
//                                if(progressDialog.isShowing()){
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }
//
//
//                    }
//                });
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    protected void insertGeoTagging(final String village,final String sitename,final String locationdetails,
                                    final String latitude,final String longitude,final String cdate,final String structure
                                    ,final String storageCapacity,final String noofFillings) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                //ConstantValues.URL1+"web/users.jsp",
                BuildConfig.SERVER_URL+"insertgeotagging.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
//getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);



                            if (obj.getString("status").equals("success")){

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Data Insertion Successful");
                                /*editTextStorageCapacity.setText("");
                                editTextNoofFillings.setText("");
                                spinnerStructure.setSelection(0);
                                image1.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
                                image2.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));*/

                                fragment = new DashBoardFragment();
                                fragmentManager.beginTransaction()
                                        .remove(fragment)
                                        .commit();
                                fragmentManager.popBackStack();

                                Toast.makeText(getContext(), "Data Saved Sucessfully", Toast.LENGTH_SHORT).show();

                                    getCountPraticular();

                            }else {
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Data Insertion Unsuccessful");

                                Toast.makeText(getContext(), ""+obj.getString("status"), Toast.LENGTH_SHORT).show();
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("statename", myAppPrefsManager.getState());
                params.put("districtname", myAppPrefsManager.getDistrict());
                params.put("blockname", myAppPrefsManager.getBlock());
                params.put("gpname", myAppPrefsManager.getGrampanchayat());
                params.put("village", village);
                params.put("sitename", sitename);
                params.put("locationdetails1",locationdetails); /// new add param
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("calenderdate",cdate); /// new add param
                params.put("typeofstructure", structure);
                params.put("storagecapacity", storageCapacity);
                params.put("nooffillings", noofFillings);
                params.put("image1", serverPath1);
                params.put("image2", serverPath2);
                params.put("deviceid",myAppPrefsManager.getDeviceId());
                params.put("others",others);
                params.put("date",formattedDate);
                params.put("email",myAppPrefsManager.getUserEmail());

                Log.e("insertgeotagging",params.toString());



                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    protected void getCountPraticular() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                //ConstantValues.URL1+"web/users.jsp",
                BuildConfig.SERVER_URL+"getdevicegeotagcount.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
//getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);


                            if (obj.getString("status").equals("success")) {

                                count.setText(obj.getString("count"));


                            } else {
                                count.setText(obj.getString("0"));
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();


                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                //params.put("deviceid",myAppPrefsManager.getDeviceId());
                params.put("statename",myAppPrefsManager.getState());
                params.put("districtname",myAppPrefsManager.getDistrict());
                params.put("blockname",myAppPrefsManager.getBlock());
                params.put("gpname",myAppPrefsManager.getGrampanchayat());
                Log.e("geotagcount",params.toString());

                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    protected void updateGeoTagging(final String id,final String villagename,final String sitename,final String locationdetails,
                                    final String latitude,final String longitude,final String cdate,final String structure,
                                    final String storageCapacity,final String noofFillings,final String image1,
                                    final String image2) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"updategeotagging.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                        //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);



                            if (obj.getString("status").equals("success")){

                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Data Update Successful");

                                fragment = new DashBoardFragment();
                                fragmentManager.beginTransaction()
                                        .remove(fragment)
                                        .commit();
                                fragmentManager.popBackStack();

                                Toast.makeText(getContext(), "Data Saved Sucessfully", Toast.LENGTH_SHORT).show();

                            }else {
                                if (progressDialog!=null&&progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }

                                insertUserLog(myAppPrefsManager.getUserEmail(),getIpAddress(),logDate,"Geo Tagging Artificial Rechare Structure","Data Update Unsuccessful");

                                Toast.makeText(getContext(), ""+obj.getString("status"), Toast.LENGTH_SHORT).show();
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            Log.d("kcr", ""+e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Log.d("kcr", "volleyerror"+error.getLocalizedMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();



                params.put("id", id);
                params.put("village", villagename);
                params.put("sitename", sitename);
                params.put("typeofstructure", structure);
                params.put("storagecapacity", storageCapacity);
                params.put("locationdetails1",locationdetails); /// new add param
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("calenderdate",cdate); /// new add param
                params.put("nooffillings", noofFillings);
                params.put("image1", image1);
                params.put("image2", image2);
                params.put("others",others);
                params.put("date",formattedDate);
                params.put("deviceid",myAppPrefsManager.getDeviceId());
                params.put("email",myAppPrefsManager.getUserEmail());
                Log.e("updategeotagging",params.toString());
                return params;
            }


        };


        requestQueue.add(stringRequest);
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"AtalJal");
        // have the object build the directory structure, if needed.
        if (!directory.exists()) {
            directory.mkdirs();
        }

        try {
            File f = new File(directory, "ataljal_"+Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("kcr", "File Saved::--->" + f.getAbsolutePath());
            if (code.equals("1")) {
                imagePath1 = f.getAbsolutePath();

                Path path = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = Paths.get(imagePath1);
                    fileName = path.getFileName();
                    imagename1 = fileName.toString();
                }else {
                    imagename1 = imagePath1.substring(imagePath1.indexOf("/")+1);
                }
            }
            if (code.equals("2")){
                imagePath2 = f.getAbsolutePath();

                Path path = null;
                Path fileName = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = Paths.get(imagePath2);
                    fileName = path.getFileName();
                    imagename2 = fileName.toString();
                }else {
                    imagename2 = imagePath2.substring(imagePath2.indexOf("/")+1);
                }

            }
            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
    @Override
    public void onResume() {
        super.onResume();

        checkLocationPermission();

    }

    private void checkLocationPermission(){

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                askPermission();

            } else {

                if(myAppPrefsManager.isFirstTimePermission()){
                    // askLocationPermission();

                    myAppPrefsManager.setIsFirstTimePermission(false);

                    askPermission();
                }

                else {

                    //showAppSettings();
                    displayLocationSettingsRequest(getActivity());
                }

            }
        } else {
            //displayLocationSettings();
            displayLocationSettingsRequest(getActivity());

        }
    }

    private void  askPermission(){
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);


        Task<LocationSettingsResponse> result=LocationServices.getSettingsClient(getActivity().getApplicationContext())
                .checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task)
            {

                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    //Toast.makeText(Selection.this, "GPS is on", Toast.LENGTH_SHORT).show();
                }
                catch  (ApiException apiException)
                {
                    switch (apiException.getStatusCode())
                    {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            ResolvableApiException resolvableApiException=(ResolvableApiException)apiException;
                            try {
                                resolvableApiException.startResolutionForResult(getActivity(),1001);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }

                }
            }
        });
    }

    protected void insertUserLog(final String email,final String ipaddress, final String datetime,
                                 final String action, final String status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                BuildConfig.SERVER_URL+"userlog.jsp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("kcr", response);


                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            if (obj.getString("status").equals("success")){


                                Log.d("kcr",""+obj.getString("status"));

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.d("asdf", ""+e.getMessage());
                            if (progressDialog!=null&&progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d("asdf", "volleyerror");
                        if (progressDialog!=null&&progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("email", email);
                params.put("ipaddress", ipaddress);
                params.put("datetime", datetime);
                params.put("action", action);
                params.put("status", status);

                Log.e("userlog",params.toString());


                return params;
            }


        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }



}
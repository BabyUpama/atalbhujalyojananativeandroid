package in.co.gcrs.ataljal.api;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import in.co.gcrs.ataljal.BuildConfig;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class RetrofitService {
    static int time = 60 * 2;
    private final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(time, TimeUnit.SECONDS)
            .writeTimeout(time, TimeUnit.SECONDS)
            .readTimeout(time, TimeUnit.SECONDS)
            .build();

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URLUPLOAD)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(new Gson()))
            .client(okHttpClient)
            .build();

    @SuppressWarnings("unchecked")
    public static <S> S createLinePatrollingService() {
        return retrofit.create((Class<S>) ApiCall.class);
    }
}

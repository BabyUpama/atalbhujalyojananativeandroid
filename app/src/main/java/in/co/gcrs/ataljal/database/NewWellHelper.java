package in.co.gcrs.ataljal.database;

public class NewWellHelper {

    public static final String wellId="wellid";
    public static final String wellNo="wellno";
    public static final String latitude="latitude";
    public static final String longitude="longitude";
    public static final String state="state";
    public static final String district="district";
    public static final String block="block";
    public static final String gramPanchayat="grampanchayat";
    public static final String villageName="villagename";
    public static final String source="source";
    public static final String wellType="welltype";
    public static final String waterLevel="waterlevel";
    public static final String year="year";
    public static final String month="month";
    public static final String nwdate="date";
    public static final String nwemail="email";
    public static final String nwimage="image";
    public static final String nwimageName="imagename";
}

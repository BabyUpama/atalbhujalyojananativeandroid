package in.co.gcrs.ataljal.api;

public enum Status {
    TYPE,
    SUCCESS,
    ERROR,
    LOADING
}
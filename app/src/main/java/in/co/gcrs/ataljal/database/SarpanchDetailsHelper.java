package in.co.gcrs.ataljal.database;

public class SarpanchDetailsHelper {
    public static final String sId="id";
    public static final String state="state";
    public static final String district="district";
    public static final String block="block";
    public static final String grampanchayat="grampanchayat";
    public static final String sarpanchName="sarpanchname";
    public static final String sarpanchContact="sarpanchcontact";
    public static final String panchayatSecretary="panchayatsecretary";
    public static final String secretaryContact="secretarycontact";
    public static final String latitude="latitude";
    public static final String longitude="longitude";
}

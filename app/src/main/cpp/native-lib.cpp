#include <jni.h>
#include <string>


extern "C"
JNIEXPORT jstring JNICALL
Java_in_co_gcrs_ataljal_activities_MainActivity_stringFromJNIClass(
        JNIEnv *env,
        jobject /* this */)
{
    std::string hello = "#Ataljal@1121!";
    return env->NewStringUTF(hello.c_str());
}